package com.beaconwatcher.rightips;

import io.branch.referral.Branch;

import java.io.File;
import java.net.MalformedURLException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

import org.piwik.sdk.Piwik;
import org.piwik.sdk.Tracker;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.UserProfileData;
import com.beaconwatcher.rightips.services.OpenUDID_manager;
import com.bw.libraryproject.app.ApplicationController;
import com.survivingwithandroid.weather.lib.WeatherClient;
import com.survivingwithandroid.weather.lib.WeatherClient.ClientBuilder;
import com.survivingwithandroid.weather.lib.WeatherConfig;
import com.survivingwithandroid.weather.lib.provider.openweathermap.OpenweathermapProviderType;

public class RighTipsApplication extends ApplicationController {
	private String LOGGER_TAG = "Piwik_Tracker";

	// Logged in user profile
	UserProfileData mUserProfile = null;

	// External Global libraries
	Tracker piwikTracker;

	// Branch.io for invite friend feature.
	// This Library is used to send deeplink with some data for recepient of the
	// Share feature to show user info who invited.
	Branch branch;

	// This library "WeatherLib" is used to fetch current weather and weather
	// forecast
	// - See more at:
	// http://survivingwithandroid.github.io/WeatherLib/android_weatherlib_start.html#sthash.L3LPixXN.dpuf
	// This is main instance used to work with weatherLib.
	WeatherClient weatherClient;

	/*
	 * //This library "WeatherLib" is used to fetch current weather and weather
	 * forecast //- See more at:
	 * http://survivingwithandroid.github.io/WeatherLib
	 * /android_weatherlib_start.html#sthash.L3LPixXN.dpuf //This is main
	 * instance used to work with weatherLib. WeatherClient weatherClient;
	 */

	public Boolean isBLEAvailable = true;

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		initPiwik();
		initOpenUIDI();
	}

	public Piwik getGlobalSettings() {
		return Piwik.getInstance(this);
	}

	public Tracker getTracker() {
		if (piwikTracker != null) {
			return piwikTracker;
		}

		try {
			piwikTracker = getGlobalSettings().newTracker(getTrackerUrl(),
					getSiteId(), getAuthToken());
		} catch (MalformedURLException e) {
			Log.i(LOGGER_TAG, getTrackerUrl());
			Log.w(LOGGER_TAG, "url is malformed", e);
			return null;
		}

		return piwikTracker;

	}

	public WeatherClient getWeatherClient() {
		if (weatherClient != null) {
			return weatherClient;
		}
		try {
			WeatherConfig config = new WeatherConfig();
			config.ApiKey = "fe728b7b35e73fb245c16301532d8e8d";

			weatherClient = new ClientBuilder()
					.attach(this)
					.provider(new OpenweathermapProviderType())
					.httpClient(
							com.survivingwithandroid.weather.lib.client.okhttp.WeatherDefaultClient.class)
					.config(config).build();
		} catch (Throwable t) {
			t.printStackTrace();
		}
		return weatherClient;

	}

	public Branch getBranch() {
		if (branch != null) {
			return branch;
		}

		try {
			branch = Branch.getInstance(this);
		} catch (Exception e) {
			Log.w(LOGGER_TAG, "Branch not initialized", e);
			return null;
		}
		return branch;
	}

	public String getTrackerUrl() {
		return "http://kiwip-prod.elasticbeanstalk.com/";
	}

	public String getAuthToken() {
		return "";
	}

	public Integer getSiteId() {
		return 4;
	}

	public String getUserId() {
		if (getProfile() != null) {
			return Long.toString(mUserProfile.user_id);
		}
		return null;
	}

	public String getUserName() {
		if (getProfile() != null) {
			return mUserProfile.user_name;
		}
		return "";
	}

	// For Login
	public boolean isLoggedIn() {
		if (getPreferences("LoggedIn", "0").equals("1")) {
			return true;
		}
		return false;
	}

	public void setIsLoggedIn(boolean b) {
		savePreferences("LoggedIn", b == true ? "1" : "0");

		if (b == true) {
			savePreferences(AppConstants.PREF_LOGGED_IN, "1");
		} else {
			savePreferences(AppConstants.PREF_LOGGED_IN, "0");
			savePreferences(AppConstants.PREF_LOGIN_DATA, "");
		}

	}

	public String getUID() {
		if (getProfile() != null) {
			return Integer.toString(mUserProfile.uid);
		}
		return null;
	}

	public UserProfileData getProfile() {
		if (mUserProfile != null)
			return mUserProfile;
		mUserProfile = (UserProfileData) this.getGSONPreferences(
				AppConstants.PREF_LOGIN_DATA, "", UserProfileData.class);
		if (mUserProfile != null && !mUserProfile.equals("")) {
			return mUserProfile;
		} else {
			mUserProfile = null;
		}
		return null;
	}

	public void setProfile(UserProfileData profile) {
		mUserProfile = profile;
		saveGSONPreferences(AppConstants.PREF_LOGIN_DATA, profile);
	}

	public void initPiwik() {
		// do not send http requests
		getGlobalSettings().setDryRun(false);
		getTracker().setDispatchInterval(5)
		// .trackAppDownload()
				.reportUncaughtExceptions(true);
	}

	private void initOpenUIDI() {
		OpenUDID_manager.sync(this);
	}

	public String getUDID() {
		String udid = null;
		if (OpenUDID_manager.isInitialized()) {
			udid = OpenUDID_manager.getOpenUDID();
		}

		return udid;
	}

	public static String printKeyHash(Activity context) {
		PackageInfo packageInfo;
		String key = null;
		try {
			// getting application package name, as defined in manifest
			String packageName = context.getApplicationContext()
					.getPackageName();

			// Retriving package info
			packageInfo = context.getPackageManager().getPackageInfo(
					packageName, PackageManager.GET_SIGNATURES);

			Log.e("Package Name=", context.getApplicationContext()
					.getPackageName());

			for (Signature signature : packageInfo.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				key = new String(Base64.encode(md.digest(), 0));

				// String key = new String(Base64.encodeBytes(md.digest()));
				Log.e("Key Hash=", key);
			}
		} catch (NameNotFoundException e1) {
			Log.e("Name not found", e1.toString());
		} catch (NoSuchAlgorithmException e) {
			Log.e("No such an algorithm", e.toString());
		} catch (Exception e) {
			Log.e("Exception", e.toString());
		}

		return key;
	}

	public void hideKeyboard(View view) {
		// Hide Soft Keyboard.
		if (view != null) {
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
	}
	

	public void showErrorToast(Context context, String type) {
		String errorMsg = getResources()
				.getString(R.string.generic_error_msg_a)
				+ " "
				+ type
				+ "\n"
				+ getResources().getString(R.string.generic_error_msg_b);
		Toast.makeText(context, errorMsg, Toast.LENGTH_LONG).show();
	}

	/***************************************************************************
	 * CLEAR APP CACHE ON UPDATE VERSION OF ANY OTHER EVEN
	 **************************************************************************/

	public void clearApplicationData() {
		File cache = getCacheDir();
		File appDir = new File(cache.getParent());
		if (appDir.exists()) {
			String[] children = appDir.list();
			for (String s : children) {
				if (!s.equals("lib")) {
					deleteDir(new File(appDir, s));
					Log.i("TAG",
							"**************** File /data/data/APP_PACKAGE/" + s
									+ " DELETED *******************");
				}
			}
		}
	}

	public static boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		return dir.delete();
	}

}
