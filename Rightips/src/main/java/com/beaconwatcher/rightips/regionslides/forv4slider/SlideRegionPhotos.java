package com.beaconwatcher.rightips.regionslides.forv4slider;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.PanoramioPhoto;
import com.bw.libraryproject.entities.LocationData;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * This is a slider with a description TextView.
 */
public class SlideRegionPhotos extends Fragment {
	private String TAG = "SlideRegionPhotos";

	private LocationData mLocation;

	private View mRootView;
	private RelativeLayout mPLoader;
	private GridView mGridView;
	private GridViewAdapter mAdapter;

	private AsyncHttpResponseHandler responseHandler;
	private ArrayList<PanoramioPhoto> mPhotoArray = new ArrayList<PanoramioPhoto>();

	private double panoramioDistance = 0.02;

	private int mGridColumns = 2;
	private int mCellSize = 100;

	public void setNumColumns(int cols) {
		mGridColumns = cols;
	}

	public void setCellSize(int s) {
		mCellSize = s;
	}

	public SlideRegionPhotos(LocationData ld) {
		mLocation = ld;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.slide_region_photos, container,
				false);
		TextView title = (TextView) mRootView.findViewById(R.id.tf_title);
		title.setText(mLocation.getCityName() + " - Photos");
		mGridView = (GridView) mRootView.findViewById(R.id.gridView);
		mPLoader = (RelativeLayout) mRootView.findViewById(R.id.pLoader);

		loadPhotos();
		return mRootView;
	}

	public void loadPhotos() {

		responseHandler = new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response) {
				// TODO Auto-generated method stub
				JSONObject jo;

				mPLoader.setVisibility(View.GONE);

				try {
					jo = new JSONObject(response);
					if (jo.has("photos")) {
						JSONArray arr = jo.getJSONArray("photos");
						for (int i = 0; i < arr.length(); i++) {
							JSONObject jPhoto = arr.getJSONObject(i);

							PanoramioPhoto photo = new PanoramioPhoto();
							photo.photo_id = jPhoto.getInt("photo_id");
							photo.photo_title = jPhoto.getString("photo_title");
							photo.photo_url = jPhoto
									.getString("photo_file_url");
							mPhotoArray.add(photo);
						}

						if (mPhotoArray.size() > 0) {
							mAdapter = new GridViewAdapter(getContext(),
									R.layout.panoramio_photo_item, mPhotoArray,
									mCellSize);
							mGridView.setNumColumns(mGridColumns);
							mGridView.setAdapter(mAdapter);
							// mAdapter.notifyDataSetChanged();
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				Log.d(TAG, response);
			}

			@Override
			public void onFailure(Throwable error, String msg) {
				// TODO Auto-generated method stub
				mPLoader.setVisibility(View.GONE);

			}
		};

		AsyncHttpClient client = new AsyncHttpClient();
		String url = "http://www.panoramio.com/map/get_panoramas.php?set=public&from=0&to=30&minx="
				+ (mLocation.getLongitude() - panoramioDistance)
				+ "&miny="
				+ (mLocation.getLatitude() - panoramioDistance)
				+ "&maxx="
				+ (mLocation.getLongitude() + panoramioDistance)
				+ "&maxy="
				+ (mLocation.getLatitude() + panoramioDistance)
				+ "&size=medium&mapfilter=true";
		Log.d(TAG, url);
		try {
			client.get(url, responseHandler);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public class GridViewAdapter extends ArrayAdapter {
		private Context context;
		private int layoutResourceId;
		private int cellSize;
		private ArrayList<PanoramioPhoto> data = new ArrayList<PanoramioPhoto>();

		public GridViewAdapter(Context context, int layoutResourceId,
				ArrayList<PanoramioPhoto> data, int cellSize) {
			super(context, layoutResourceId, data);
			this.layoutResourceId = layoutResourceId;
			this.context = context;
			this.data = data;
			this.cellSize = cellSize;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (data.size() == 0)
				return null;

			View row = convertView;
			ViewHolder holder = null;

			if (row == null) {
				LayoutInflater inflater = ((Activity) context)
						.getLayoutInflater();
				row = inflater.inflate(layoutResourceId, parent, false);

				LayoutParams params = row.getLayoutParams();
				params.width = cellSize;
				params.height = cellSize;

				row.setLayoutParams(params);

				holder = new ViewHolder();
				holder.image = (ImageView) row.findViewById(R.id.image);
				holder.pBar = (ProgressBar) row.findViewById(R.id.preloader);
				row.setTag(holder);
			} else {
				holder = (ViewHolder) row.getTag();
			}

			PanoramioPhoto photo = data.get(position);
			final ProgressBar pBar = holder.pBar;

			Picasso.with(context).load(photo.photo_url)
					.error(R.drawable.cat_no_image).resize(cellSize, cellSize)
					.centerCrop().into(holder.image, new Callback() {
						@Override
						public void onSuccess() {
							// TODO Auto-generated method stub
							pBar.setVisibility(View.GONE);
						}

						@Override
						public void onError() {
							// TODO Auto-generated method stub

						}
					});
			return row;
		}
	}

	static class ViewHolder {
		ImageView image;
		ProgressBar pBar;
	}

}
