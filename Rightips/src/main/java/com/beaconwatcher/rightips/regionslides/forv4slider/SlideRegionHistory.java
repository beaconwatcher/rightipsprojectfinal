package com.beaconwatcher.rightips.regionslides.forv4slider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.PanoramioPhoto;
import com.bw.libraryproject.entities.LocationData;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

/**
 * This is a slider with a description TextView.
 */
public class SlideRegionHistory extends Fragment {
	private String TAG = "SlideRegionPhotos";

	private LocationData mLocation;
	private View mRootView;
	private AsyncHttpResponseHandler responseHandler;

	public SlideRegionHistory(LocationData ld) {
		mLocation = ld;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.slide_region_history, container,
				false);
		TextView title = (TextView) mRootView.findViewById(R.id.tf_title);
		title.setText(mLocation.getCityName() + " - Photos");
		return mRootView;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	public void loadHistory() {

		responseHandler = new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response) {
				// TODO Auto-generated method stub
				JSONObject jo;

				mRootView.findViewById(R.id.preloader).setVisibility(View.GONE);

				try {
					jo = new JSONObject(response);
					if (jo.has("photos")) {
						JSONArray arr = jo.getJSONArray("photos");
						for (int i = 0; i < arr.length(); i++) {
							JSONObject jPhoto = arr.getJSONObject(i);

							PanoramioPhoto photo = new PanoramioPhoto();
							photo.photo_id = jPhoto.getInt("photo_id");
							photo.photo_title = jPhoto.getString("photo_title");
							photo.photo_url = jPhoto
									.getString("photo_file_url");
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				Log.d(TAG, response);
			}

			@Override
			public void onFailure(Throwable error, String msg) {
				// TODO Auto-generated method stub
				mRootView.findViewById(R.id.preloader).setVisibility(View.GONE);
			}
		};

		AsyncHttpClient client = new AsyncHttpClient();
		String url = "http://www.panoramio.com/map/get_panoramas.php";
		Log.d(TAG, url);
		try {
			client.get(url, responseHandler);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
