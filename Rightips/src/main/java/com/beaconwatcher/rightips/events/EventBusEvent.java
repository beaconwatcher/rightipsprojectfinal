package com.beaconwatcher.rightips.events;

import java.util.ArrayList;

import com.beaconwatcher.rightips.entities.UserLikes;
import com.beaconwatcher.rightips.events.EventBusEventInterface;

public class EventBusEvent implements EventBusEventInterface {
	private Object data;
	private String type;

	public EventBusEvent(String type) {
		this.type = type;
	}

	@Override
	public void setData(Object data) {
		// TODO Auto-generated method stub
		this.data = data;
	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public Object getData() {
		return data;
	}
}