package com.beaconwatcher.rightips.events;

public class ProfilePicChangedEvent {
	private String url;

	public ProfilePicChangedEvent(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}
}