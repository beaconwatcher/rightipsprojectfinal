package com.beaconwatcher.rightips.events;

import java.util.ArrayList;

import com.beaconwatcher.rightips.entities.UserCommentPhoto;

public class UserPhotosLoadedEvent {
	private ArrayList<UserCommentPhoto> mArray;

	public UserPhotosLoadedEvent(ArrayList<UserCommentPhoto> arr) {
		mArray = arr;
	}

	public ArrayList<UserCommentPhoto> getPhotos() {
		return mArray;
	}
}