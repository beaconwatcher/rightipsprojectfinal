package com.beaconwatcher.rightips.events;

import com.beaconwatcher.rightips.entities.UserProfileData;

public class UserProfileLoadedEvent {
	private UserProfileData profile;

	public UserProfileLoadedEvent(UserProfileData profile) {
		this.profile = profile;
	}

	public UserProfileData getProfile() {
		return profile;
	}
}