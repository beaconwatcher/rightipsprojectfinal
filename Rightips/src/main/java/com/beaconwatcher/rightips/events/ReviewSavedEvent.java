package com.beaconwatcher.rightips.events;

public class ReviewSavedEvent {
	private String message;

	public ReviewSavedEvent(String msg) {
		message = msg;
	}

	public String getMessage() {
		return message;
	}
}