package com.beaconwatcher.rightips.entities.piwik;

public class PiwikNotification {
	public String note_id;
	public String note_title;
	public String note_site_id;
	public String note_site_name;
	public String note_zone;
}
