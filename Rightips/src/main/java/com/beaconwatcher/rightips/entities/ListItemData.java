package com.beaconwatcher.rightips.entities;

import java.util.ArrayList;

public class ListItemData {
	public int id = 0;
	public int uid = 0;
	public int rate = 0;
	public int site_id = 0;
	public int created = 0;
	public int likes = 0;
	public String strId = "0";
	public String is_liked = "0";
	public String type = "";
	public String site_title = "";
	public String site_address = "";
	public String name = "";
	public String title = "";
	public String text = "";
	public String profile_pic = "";
	public String img = "";
	public String distance = "";
	public String[] tags = null;
	public boolean deleteable = true;
}
