package com.beaconwatcher.rightips.entities;

public class SearchParamsData {
	public Double latitude = 0.0;
	public Double longitude = 0.0;
	public String cityName = "";
	public String keyword = "";
}
