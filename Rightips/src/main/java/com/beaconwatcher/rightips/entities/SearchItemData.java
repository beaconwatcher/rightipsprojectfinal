package com.beaconwatcher.rightips.entities;

import java.util.ArrayList;

public class SearchItemData {
	public int nt_id = 0;
	public String site_name = "";
	public String site_address = "";
	public String nt_name = "";
	public String nt_title = "";
	public String net_details = "";
	public String distance = "";
	public ArrayList<NotificationTags> tags;
	public ArrayList<NotificationImages> images;

	public class NotificationImages {
		public String url;
	}

	public class NotificationTags {
		public int id = 0;
		public String name = "";
	}

}
