package com.beaconwatcher.rightips.entities;

import android.os.Parcel;
import android.os.Parcelable;

public class SiteReviewData implements Parcelable {
	private int id = 0;
	private int uid = 0;
	private int rev_rate = 0;
	private int site_id = 0;
	private int rev_date = 0;
	private int likes = 0;
	private String site_title = "";
	private String user_name = "";
	private String rev_text = "";
	private String profile_pic = "";

	/**
	 * Standard basic constructor for non-parcel object creation
	 */
	public SiteReviewData() {
		;
	};

	/**
	 * 
	 * Constructor to use when re-constructing object from a parcel
	 * 
	 * @param in
	 *            a parcel from which to read this object
	 */
	public SiteReviewData(Parcel in) {
		readFromParcel(in);
	}

	/**
	 * standard getter functions
	 */

	public int getID() {
		return id;
	}

	public int getUID() {
		return uid;
	}

	public int getRate() {
		return rev_rate;
	}

	public int getSiteID() {
		return site_id;
	}

	public int getDate() {
		return rev_date;
	}

	public int getLikes() {
		return likes;
	}

	public String getSiteTitle() {
		return site_title;
	}

	public String getUserName() {
		return user_name;
	}

	public String getText() {
		return rev_text;
	}

	public String getProfilePic() {
		return profile_pic;
	}

	/**
	 * standard setter functions
	 */

	public void setID(int i) {
		id = i;
	}

	public void setUID(int u) {
		uid = u;
	}

	public void setRate(int r) {
		rev_rate = r;
	}

	public void setSiteID(int sid) {
		site_id = sid;
	}

	public void setDate(int date) {
		rev_date = date;
	}

	public void setLikes(int l) {
		likes = l;
	}

	public void setSiteTitle(String st) {
		site_title = st;
	}

	public void setUserName(String uname) {
		user_name = uname;
	}

	public void setText(String txt) {
		rev_text = txt;
	}

	public void setProfilePic(String pic) {
		profile_pic = pic;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// We just need to write each field into the
		// parcel. When we read from parcel, they
		// will come back in the same order
		dest.writeInt(id);
		dest.writeInt(uid);
		dest.writeInt(rev_rate);
		dest.writeInt(site_id);
		dest.writeInt(rev_date);
		dest.writeInt(likes);
		dest.writeString(site_title);
		dest.writeString(user_name);
		dest.writeString(rev_text);
		dest.writeString(profile_pic);
	}

	/**
	 * 
	 * Called from the constructor to create this object from a parcel.
	 * 
	 * @param in
	 *            parcel from which to re-create object
	 */
	private void readFromParcel(Parcel in) {

		// We just need to read back each
		// field in the order that it was
		// written to the parcel
		id = in.readInt();
		uid = in.readInt();
		rev_rate = in.readInt();
		site_id = in.readInt();
		rev_date = in.readInt();
		likes = in.readInt();
		site_title = in.readString();
		user_name = in.readString();
		rev_text = in.readString();
		profile_pic = in.readString();
	}

	/**
	 * 
	 * This field is needed for Android to be able to create new objects,
	 * individually or as arrays.
	 * 
	 * This also means that you can use use the default constructor to create
	 * the object and use another method to hyrdate it as necessary.
	 * 
	 * I just find it easier to use the constructor. It makes sense for the way
	 * my brain thinks ;-)
	 * 
	 */
	public static final Creator CREATOR = new Creator() {
		public SiteReviewData createFromParcel(Parcel in) {
			return new SiteReviewData(in);
		}

		public SiteReviewData[] newArray(int size) {
			return new SiteReviewData[size];
		}
	};
}
