package com.beaconwatcher.rightips.entities;

public class UserCommentPhoto {
	public int com_id = 0;
	public int uid_fk = 0;
	public int like_count = 0;
	public int com_status_id = 0;
	public String comment = "";
	public String created = "";
	public String com_img = "";
	public String profile_img = "";
	public String username = "";
}
