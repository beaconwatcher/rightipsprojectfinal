package com.beaconwatcher.rightips.fragments;

import java.util.ArrayList;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ListView;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.customadapters.ListItemAdapter;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.ListItemData;
import com.beaconwatcher.rightips.events.ApplicationEvent;
import com.beaconwatcher.rightips.events.EventBusEvent;
import com.beaconwatcher.rightips.services.IntentServiceFactory;

public class FragmentUserPhotos extends AbstractFragment {

	// protected static final String TAG="FragmentReviews";
	private LayoutInflater mLayoutInflater;
	// Views
	private View mRootView;
	private GridView mGridView;
	private ListView mListView;

	// varibales
	private String mUserID;
	private String mUserName;
	private String mProfilePic;
	private String mode = AppConstants.PROFILE_PHOTOS_MODE_GRID;
	private Class mClazz;

	private ArrayList<ListItemData> mPhotosArray;

	private ListItemAdapter mGridAdapter;
	private ListItemAdapter mVerticalAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		TAG = "FragmentUserPhotos";
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mContext = getActivity();
		mApp = (RighTipsApplication) getActivity()
				.getApplicationContext();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mLayoutInflater = inflater;
		mRootView = inflater.inflate(R.layout.fragment_user_photos, container,
				false);
		mLoader = mRootView.findViewById(R.id.loader_photos);
		mGridView = (GridView) mRootView.findViewById(R.id.grid_view);
		mListView = (ListView) mRootView.findViewById(R.id.list_view);
		return mRootView;
	}

	public void changeMode(String uid, String uname, String pic, String m) {
		mUserID = uid;
		mUserName = uname;
		mProfilePic = pic;
		mode = m;

		showPhotos();
	}

	private void showPhotos() {
		if (mPhotosArray == null) {
			loadPhotos();
		} else {
			updatePhotos();
		}
	}

	private void loadPhotos() {
		showLoader(true);
		// Log.d("FragmentReviews", "getReviews id: "+mSiteID);
		mClazz = IntentServiceFactory.getInstance().getIntentServiceClass(
				AppConstants.ACTION_GET_USER_PHOTOS);
		Intent intent = new Intent(mContext, mClazz);
		intent.putExtra(AppConstants.INTENT_SERVICE_TAG, TAG);
		intent.putExtra(AppConstants.USER_ID, mUserID);
		intent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
				AppConstants.ACTION_GET_USER_PHOTOS);
		mContext.startService(intent);
	}

	public void onEvent(EventBusEvent event) {
		showLoader(false);
		if (event.getType().equals(AppConstants.ACTION_GET_USER_PHOTOS)) {
			mPhotosArray = (ArrayList<ListItemData>) event.getData();
			if (mPhotosArray != null && mPhotosArray.size() > 0) {
				updatePhotos();
			}
		}
	}

	private void updatePhotos() {
		if (mode.equals(AppConstants.PROFILE_PHOTOS_MODE_GRID)) {
			mGridView.setVisibility(View.VISIBLE);
			mListView.setVisibility(View.INVISIBLE);
			if (mGridAdapter == null) {
				mGridAdapter = new ListItemAdapter(
						AppConstants.LIST_ITEM_TYPE_PHOTOS_GRID, mPhotosArray);
				mGridView.setAdapter(mGridAdapter);
			}
		} else if (mode.equals(AppConstants.PROFILE_PHOTOS_MODE_VERTICAL)) {
			mGridView.setVisibility(View.INVISIBLE);
			mListView.setVisibility(View.VISIBLE);
			if (mVerticalAdapter == null) {
				mVerticalAdapter = new ListItemAdapter(
						AppConstants.LIST_ITEM_TYPE_PHOTOS_VERTICAL,
						mPhotosArray);
				mListView.setAdapter(mVerticalAdapter);
			}
		}
	}

}
