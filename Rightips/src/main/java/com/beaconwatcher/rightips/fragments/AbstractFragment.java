package com.beaconwatcher.rightips.fragments;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.bw.libraryproject.app.ApplicationController;
import com.bw.libraryproject.entities.LocationData;
import com.bw.libraryproject.entities.NotificationData;
import com.bw.libraryproject.events.intentservice.IntentServiceEvent;
import com.bw.libraryproject.events.intentservice.NoRecordFoundEvent;

import de.greenrobot.event.EventBus;

public class AbstractFragment extends Fragment implements FragmentInterface {

	public FragmentInterface mFragmentInterface;
	public RighTipsApplication mApp;
	public Context mContext;
	public String TAG;

	public View mLoader;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mContext = getActivity();
		mApp = (RighTipsApplication) mContext.getApplicationContext();

	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		try {
			if (activity instanceof FragmentInterface) {
				mFragmentInterface = (FragmentInterface) activity;
			}
		} catch (ClassCastException e) {
			throw new RuntimeException(
					getActivity().getClass().getSimpleName()
							+ " must implement the suicide listener to use this fragment",
					e);
		}
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EventBus.getDefault().register(this);
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		EventBus.getDefault().unregister(this);
		super.onStop();
	}
	
	
	
	
	
	
	
	
	
	
	

	// This method will be called when a MessageEvent is posted
	public void onEvent(Class<?> event) {
	};

	public void onEvent(IntentServiceEvent event) {
		if (event.tag.equals(TAG)) {
			showLoader(false);
			mApp.hideDialog();
			if (event.type.equals(AppConstants.INTENT_SERVICE_MESSAGE)) {
				//mApp.showToast(getActivity(), TAG + ": " + event.message, Toast.LENGTH_SHORT);
				mApp.showDialogMessage(getActivity(), event.message);
				
			} else if (event.type
					.equals(AppConstants.INTENT_SERVICE_ERROR_MESSAGE)) {
				mApp.showErrorToast(mContext, TAG);
			}
		}
	}

	public void onEvent(NoRecordFoundEvent event) {
		if (event.tag.equals(TAG)) {
			showLoader(false);
			mApp.hideDialog();
			mApp.showToast(mContext, event.message, Toast.LENGTH_SHORT);
		}
	}

	public void showLoader(Boolean b) {
		if (mLoader != null)
			mLoader.setVisibility((b == true) ? View.VISIBLE : View.INVISIBLE);
	}
	
	
	
	/*
	 * public<T> T getAttributeById(Class<T> type, int[] a, int b) { TypedArray
	 * arr = mInflaterActivity.obtainStyledAttributes(mAttributes, a);
	 * if(arr!=null){ if(type.isInstance(Integer.class)){ int o = arr.getInt(b,
	 * -1); return type.cast(arr.getInt(b, -1)); } else
	 * if(type.isInstance(String.class)){ return type.cast(arr.getString(b)); }
	 * 
	 * else if(type==Boolean.class){ Boolean bool=arr.getBoolean(b, true);
	 * return type.cast(bool); } } arr.recycle();
	 * 
	 * return null; }
	 */

	@Override
	public void onHistoryItemClicked(NotificationData nd) {
	}

	@Override
	public void onFragmentSuicide(String tag) {
	}

	@Override
	public void onInviteFriendsClicked() {
	}

	@Override
	public void onMapIconClicked(NotificationData nd) {
	}

	@Override
	public void onSliderItemClicked(ArrayList<NotificationData> arr) {
	}

	@Override
	public void onRegionIconClicked(LocationData ld) {
	}

}
