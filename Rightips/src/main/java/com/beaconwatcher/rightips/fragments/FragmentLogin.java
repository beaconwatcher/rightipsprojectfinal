package com.beaconwatcher.rightips.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.activities.LoginActivity;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.UserProfileData;
import com.beaconwatcher.rightips.events.ApplicationEvent;
import com.bw.libraryproject.app.ApplicationController;
import com.loopj.android.http.AsyncHttpResponseHandler;

import de.greenrobot.event.EventBus;

public class FragmentLogin extends Fragment {
	private static final String TAG = "RighTips Login Fragment";
	private ApplicationController mApp;
	private Context mContext;

	// UI Elements
	private LinearLayout mBtnSignIn;
	private EditText tfEmail;
	private EditText tfPassword;

	private String email = "";
	private String password = "";

	AsyncHttpResponseHandler handler;

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		mApp = (ApplicationController) getActivity().getApplicationContext();
		mContext = getActivity();

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragement_login, container,
				false);

		tfEmail = (EditText) rootView.findViewById(R.id.tfEmail);
		tfPassword = (EditText) rootView.findViewById(R.id.tfPassword);

		rootView.findViewById(R.id.btnSignup).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						FragmentTransaction ft = getFragmentManager()
								.beginTransaction();
						ft.replace(android.R.id.content, new FragmentSignup());
						// ft.replace(R.id.fragement_login, new
						// SignupFragment());
						ft.commit();
					}
				});

		rootView.findViewById(R.id.btnLogin).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						email = tfEmail.getText().toString();
						password = tfPassword.getText().toString();

						if (email.equals("") || password.equals("")) {
							Toast.makeText(
									mContext,
									mContext.getResources().getString(
											R.string.lbl_fill_all_fields),
									Toast.LENGTH_SHORT).show();
							return;
						}

						UserProfileData profile = new UserProfileData();
						profile.email = email;
						profile.password = password;
						ApplicationEvent evt = new ApplicationEvent(
								AppConstants.ATTEMPT_LOGIN, profile, null);
						EventBus.getDefault().post(evt);
						/*
						 * LoginUserData d=new LoginUserData("0", "0", email,
						 * password, "0"); ((LoginActivity)
						 * LoginFragment.this.getActivity()).login(d);
						 */
					}
				});

		return rootView;
	}
}
