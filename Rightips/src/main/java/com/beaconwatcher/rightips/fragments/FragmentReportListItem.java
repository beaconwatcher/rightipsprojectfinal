package com.beaconwatcher.rightips.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.ListItemData;
import com.beaconwatcher.rightips.events.ApplicationEvent;
import com.beaconwatcher.rightips.services.IntentServiceFactory;
import com.bw.libraryproject.interfaces.SuicidalFragmentListener;
import com.google.gson.reflect.TypeToken;

import de.greenrobot.event.EventBus;

public class FragmentReportListItem extends AbstractFragment {
	private Context mContext;
	private LayoutInflater mInflater;
	View mRootView;

	ListItemData mData;

	private SuicidalFragmentListener suicideListener;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		TAG = "ReportCommentFragment";
	}

	@Override
	public View onCreateView(LayoutInflater infl, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mContext = getActivity();
		mRootView = infl.inflate(R.layout.fragement_report_comment, container,
				false);
		mInflater = infl;

		mLoader = mRootView.findViewById(R.id.loader_report);
		mRootView.findViewById(R.id.btn_spam)
				.setOnClickListener(mClickListener);
		mRootView.findViewById(R.id.btn_abusive).setOnClickListener(
				mClickListener);
		mRootView.findViewById(R.id.btn_cancel).setOnClickListener(
				mClickListener);

		if (getArguments() != null) {
			setData(getArguments());
		}

		return mRootView;
	}

	public void setData(Bundle bundle) {
		String str = bundle.getString(AppConstants.LIST_ITEM_DATA);
		mData = (ListItemData) mApp.getClassObjectFromJsonString(str,
				ListItemData.class);
		/*
		 * mUserID=bundle.getString(AppConstants.USER_ID);
		 * mItemID=Integer.toString(bundle.getInt(AppConstants.LIST_ITEM_ID));
		 * mType=bundle.getString(AppConstants.LIST_ITEM_TYPE);
		 */
	}

	OnClickListener mClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {

			case R.id.btn_spam:
				reportComment("1");
				break;

			case R.id.btn_abusive:
				reportComment("2");
				break;

			case R.id.btn_cancel:
				// getActivity().getFragmentManager().popBackStack();
				EventBus.getDefault()
						.post(new ApplicationEvent(
								AppConstants.LIST_ACTIONS_CANCELED, null, null));
				break;
			}
		}
	};

	private void reportComment(String statusID) {
		if (mData == null)
			return;
		showLoader(true);
		Class mClazz = IntentServiceFactory.getInstance()
				.getIntentServiceClass(AppConstants.REPORT_LIST_ITEM);
		Intent intent = new Intent(mContext, mClazz);
		intent.putExtra(AppConstants.INTENT_SERVICE_TAG, TAG);
		intent.putExtra(AppConstants.USER_ID, mApp.getUID());
		intent.putExtra(AppConstants.LIST_ITEM_ID, Integer.toString(mData.id));
		intent.putExtra(AppConstants.LIST_ITEM_REPORT_ID, statusID);
		intent.putExtra(AppConstants.LIST_ITEM_TYPE, mData.type);
		intent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
				AppConstants.REPORT_LIST_ITEM);
		mContext.startService(intent);
	}
}
