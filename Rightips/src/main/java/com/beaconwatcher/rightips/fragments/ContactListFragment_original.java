package com.beaconwatcher.rightips.fragments;

import io.branch.referral.Branch.BranchLinkCreateListener;
import io.branch.referral.BranchError;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Fragment;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.ContactListItem;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.makeramen.segmented.PinkRadioGroup;

public class ContactListFragment_original extends Fragment {

	private static String TAG = "ContactListFragment";

	private LayoutInflater inflater;
	private TextView mTxt;
	private View rootView;
	private PinkRadioGroup mRadioGroup;
	private ListView mContactList;
	private RelativeLayout mBtnInvite;

	private RighTipsApplication mApp;

	private ArrayList<ContactListItem> mContactArray = new ArrayList<ContactListItem>();
	MyContactAdapter mAdapter;

	private static String MODE_SMS = "sms";
	private static String MODE_EMAIL = "email";

	private String mMode = MODE_SMS;

	// SMS/Email
	private String message;
	private ArrayList<String> numbersToSendSMS = new ArrayList<String>();
	private ArrayList<String> numbersToSendEmail = new ArrayList<String>();
	private String SENT = "sent";
	private String DELIVERED = "delivered";

	ArrayList<PendingIntent> sendPIArray = new ArrayList<PendingIntent>();
	ArrayList<PendingIntent> deliveredPIArray = new ArrayList<PendingIntent>();

	@Override
	public View onCreateView(LayoutInflater infl, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mAdapter = new MyContactAdapter(mContactArray, getActivity());

		rootView = infl.inflate(R.layout.fragement_contacts, container, false);
		inflater = infl;
		mTxt = (TextView) rootView.findViewById(R.id.txt);
		mRadioGroup = (PinkRadioGroup) rootView.findViewById(R.id.radioGroup);
		mContactList = (ListView) rootView.findViewById(R.id.list_contacts);
		mBtnInvite = (RelativeLayout) rootView.findViewById(R.id.btn_invite);

		mContactList.setAdapter(mAdapter);

		mBtnInvite.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getMsgText();
			}
		});

		mRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				switch (checkedId) {
				case R.id.radio_sms:
					mMode = MODE_SMS;
					break;

				case R.id.radio_email:
					mMode = MODE_EMAIL;
					break;
				}
				fetchContacts();
			}
		});

		fetchContacts();
		return rootView;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		mApp = (RighTipsApplication) getActivity()
				.getApplicationContext();

		super.onCreate(savedInstanceState);
		Intent sentIntent = new Intent(SENT);
		/* Create Pending Intents */
		PendingIntent sentPI = PendingIntent.getBroadcast(getActivity(), 0,
				sentIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		Intent deliveryIntent = new Intent(DELIVERED);
		PendingIntent deliverPI = PendingIntent.getBroadcast(getActivity(), 0,
				deliveryIntent, PendingIntent.FLAG_UPDATE_CURRENT);

		sendPIArray.add(sentPI);
		deliveredPIArray.add(deliverPI);

		/* Register for SMS send action */
		getActivity().registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				String result = "";

				switch (getResultCode()) {
				case Activity.RESULT_OK:
					Log.d("ContactList", "SMS sent successfully");
					result = "SMS sent successfully";
					break;
				case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
					Log.e("ContactList",
							"SMS Transmission failed, check your balance or signals");
					result = "Error sending SMS";
					break;
				case SmsManager.RESULT_ERROR_RADIO_OFF:
					Log.e("ContactList",
							"SMS Transmission failed, check Radio signals");
					result = "SMS Transmission failed, check Radio signals";
					break;
				case SmsManager.RESULT_ERROR_NULL_PDU:
					Log.e("ContactList",
							"SMS Transmission failed, No PDU defined");
					result = "SMS Transmission failed, No PDU defined";
					break;
				case SmsManager.RESULT_ERROR_NO_SERVICE:
					Log.e("ContactList", "SMS Transmission failed, No service");
					result = "SMS Transmission failed, No service";
					break;
				}

				Toast.makeText(getActivity(), result, Toast.LENGTH_LONG).show();
				sendNextSMS();
			}

		}, new IntentFilter(SENT));
	}

	private void fetchContacts() {
		mContactArray.clear();

		ContentResolver cr = getActivity().getContentResolver();
		Cursor cur = getActivity().getContentResolver().query(
				ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

		String phone = null;
		String emailContact = null;
		String emailType = null;
		String image_uri = "";
		Bitmap bitmap = null;

		if (cur.getCount() > 0) {
			while (cur.moveToNext()) {
				String id = cur.getString(cur
						.getColumnIndex(ContactsContract.Contacts._ID));
				String name = cur
						.getString(cur
								.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

				if (mMode.equals(MODE_SMS)) {
					if (Integer
							.parseInt(cur.getString(cur
									.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
						ContactListItem item = new ContactListItem();
						item.setId(id);
						item.setName(name);

						Cursor pCur = cr
								.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
										null,
										ContactsContract.CommonDataKinds.Phone.CONTACT_ID
												+ " = ?", new String[] { id },
										null);
						while (pCur.moveToNext()) {
							phone = pCur
									.getString(pCur
											.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
							item.setContact(phone);
						}
						pCur.close();
						mContactArray.add(item);
					}
				}

				else {
					Cursor emailCur = cr.query(
							ContactsContract.CommonDataKinds.Email.CONTENT_URI,
							null,
							ContactsContract.CommonDataKinds.Email.CONTACT_ID
									+ " = ?", new String[] { id }, null);
					while (emailCur.moveToNext()) {
						ContactListItem item = new ContactListItem();
						item.setId(id);
						item.setName(name);

						emailContact = emailCur
								.getString(emailCur
										.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
						item.setContact(emailContact);
						mContactArray.add(item);

					}
					emailCur.close();
				}

				/*
				 * if (image_uri != null) {
				 * System.out.println(Uri.parse(image_uri)); try { bitmap =
				 * MediaStore
				 * .Images.Media.getBitmap(getActivity().getContentResolver(),
				 * Uri.parse(image_uri)); sb.append("\n Image in Bitmap:" +
				 * bitmap); System.out.println(bitmap); } catch
				 * (FileNotFoundException e) { // TODO Auto-generated catch
				 * block e.printStackTrace(); } catch (IOException e) { // TODO
				 * Auto-generated catch block e.printStackTrace(); }
				 * sb.append("\n........................................"); }
				 */
			}
		}

		mAdapter.notifyDataSetChanged();
	}

	private void inviteFriends(ArrayList<String> arr) {
		if (mMode.equals(MODE_SMS)) {
			Toast.makeText(getActivity(), "Sending SMS to: " + arr.toString(),
					Toast.LENGTH_LONG).show();
			sendSMS(arr);
		} else {
			Toast.makeText(getActivity(),
					"Sending Email to: " + arr.toString(), Toast.LENGTH_LONG)
					.show();
			sendEmail(arr);
		}
	}

	private String getMsgText() {
		/*
		 * //Check if user logged in LoginUserData ld = (LoginUserData)
		 * ((PiwikApplicationController
		 * )getActivity().getApplicationContext()).getGSONPreferences(
		 * AppConstants.PREF_LOGIN_DATA, "", LoginUserData.class);
		 * 
		 * 
		 * 
		 * final String
		 * msg=getActivity().getResources().getString(R.string.invitation_txt);
		 * 
		 * JSONObject obj = new JSONObject(); try{ if (ld!=null) { obj.put("id",
		 * ld.getUID()); obj.put("name", ld.getFullName()); obj.put("image_url",
		 * ld.getProfileImageURL()); }
		 * 
		 * } catch(JSONException e){ }
		 * 
		 * mApp.getBranch().getShortUrl(obj, new BranchLinkCreateListener() {
		 * 
		 * @Override public void onLinkCreate(String url, BranchError error) {
		 * Log.i(TAG, "Ready to share my link = " + url);
		 * message=msg.concat("\n"+url);//=msg+"\n"+url;
		 * inviteFriends(mAdapter.getSelectedItems());
		 * //Toast.makeText(getActivity(), url, Toast.LENGTH_LONG).show(); } });
		 * 
		 * return msg;
		 */

		return "";
	}

	private void sendSMS(ArrayList<String> arr) {
		// IF SMS feature is not selected
		numbersToSendSMS = arr;
		sendNextSMS();
	}

	private void sendNextSMS() {
		if (numbersToSendSMS.size() > 0) {
			String phoneNmbr = numbersToSendSMS
					.get(numbersToSendSMS.size() - 1);
			numbersToSendSMS.remove(numbersToSendSMS.size() - 1);

			sendActualSMS(phoneNmbr, message);
		}
	}

	private void sendActualSMS(String phoneNmbr, String msg) {
		if (phoneNmbr.equals("")) {
			sendNextSMS();
		} else {
			try {
				Log.d("ContactList", "Sending SMS to=" + phoneNmbr
						+ ", content=" + msg);

				/* Send SMS */
				SmsManager smsManager = SmsManager.getDefault();
				ArrayList<String> msgStringArray = smsManager
						.divideMessage(msg);
				smsManager.sendMultipartTextMessage(phoneNmbr, null,
						msgStringArray, sendPIArray, deliveredPIArray);
			} catch (Exception ex) {
				Log.d("ContactList", ex.getMessage());
				Toast.makeText(getActivity(), ex.getMessage().toString(),
						Toast.LENGTH_LONG).show();
				ex.printStackTrace();
				sendNextSMS();
			}
		}
	}

	private void sendEmail(ArrayList<String> arr) {
		// IF SMS feature is not selected
		numbersToSendEmail = arr;
		sendNextMail();
	}

	private void sendNextMail() {
		// String mailFrom = "noreply@rightips.com";
		String mailFrom = "ez4me2contact@gmail.com";
		if (numbersToSendEmail.size() > 0) {
			sendPhpMail(mailFrom,
					numbersToSendEmail.get(numbersToSendEmail.size() - 1));
			numbersToSendEmail.remove(numbersToSendEmail.size() - 1);
		}
	}

	private void sendPhpMail(String from, String to) {
		Log.d("ContactList", "Sending Email>  from=" + from + "to=" + to);

		RequestParams params = new RequestParams();
		params.put("sendFrom", from);
		params.put("sendTo", to);
		params.put("subject", "RighTips Invitation");
		params.put("body", message);

		AsyncHttpClient httpClient = new AsyncHttpClient();
		try {
			// httpClient.post("http://212.129.7.43/emailsender/send_mail.php?send=1",
			// params, handler);
			httpClient.post(
					"http://esol-tech.com/emailsender/send_mail.php?send=1",
					params, handler);
		} catch (Exception e) {
			Log.d("ContactList", "Failed to send Email= " + e.getMessage());
			Toast.makeText(getActivity(), "Error Sending Email",
					Toast.LENGTH_SHORT).show();
		}
	}

	AsyncHttpResponseHandler handler = new AsyncHttpResponseHandler() {
		@Override
		public void onStart() {
			Log.d("ContactList", "Sending PHP Mail=onStart");
		}

		@Override
		public void onSuccess(String response) {
			Log.d("ContactList", "Sending PHP Mail=onSuccess");
			if (numbersToSendEmail.size() > 0) {
				sendNextMail();
			} else {
				Toast.makeText(getActivity(), "Email Send successfuly",
						Toast.LENGTH_SHORT).show();
			}
		}

		@Override
		public void onFailure(Throwable error, String content) {
			Log.e("ContactList",
					"Sending PHP Mail onFailure error=" + error.toString()
							+ "content=" + content);
			Toast.makeText(getActivity(), "Error Sending Email",
					Toast.LENGTH_SHORT).show();
			sendNextMail();
		}

		@Override
		public void onFinish() {
			Log.d("ContactList", "Sending PHP Mail=onFinish");
		}
	};

	public class MyContactAdapter extends BaseAdapter {
		private Context mContext;
		ViewHolder viewHolder;

		private ArrayList<ContactListItem> _contacts;

		private String[] selContacts = new String[] {};

		public MyContactAdapter(ArrayList<ContactListItem> arr, Context c) {
			mContext = c;
			_contacts = arr;
		}

		public ArrayList<String> getSelectedItems() {
			ArrayList<String> arr = new ArrayList<String>();
			for (int i = 0; i < _contacts.size(); i++) {
				if (_contacts.get(i).getSelected() == true) {
					arr.add(_contacts.get(i).getContact());
				}
			}
			return arr;
		}

		@Override
		public int getCount() {
			return _contacts.size();// images.length;
		}

		@Override
		public long getItemId(int i) {
			return i;
		}

		@Override
		public ContactListItem getItem(int position) {
			// TODO Auto-generated method stub
			return _contacts.get(position);
		}

		@Override
		public View getView(int i, View reuseableView, ViewGroup viewGroup) {
			if (reuseableView == null) {
				reuseableView = inflater.inflate(R.layout.contact_list_item,
						null);
				reuseableView.setLayoutParams(new AbsListView.LayoutParams(
						AbsListView.LayoutParams.MATCH_PARENT, getResources()
								.getDimensionPixelSize(
										R.dimen.contact_item_height)));

				viewHolder = new ViewHolder();

				// cache the views
				viewHolder.name = (TextView) reuseableView
						.findViewById(R.id.txt_name);
				/*
				 * viewHolder.contact = (TextView) reuseableView
				 * .findViewById(R.id.txt_contact);
				 */
				viewHolder.checkBox = (CheckBox) reuseableView
						.findViewById(R.id.cb_contact);

				// link the cached views to the convertview
				reuseableView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) reuseableView.getTag();
			}

			viewHolder.checkBox.setId(i);

			ContactListItem contact = getItem(i);

			viewHolder.name.setText(contact.getName());
			// viewHolder.contact.setText(contact.getContact());

			viewHolder.checkBox.setChecked(_contacts.get(i).getSelected());

			viewHolder.checkBox.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					_contacts.get(v.getId()).setSelected(
							((CheckBox) v).isChecked());
				}
			});
			return reuseableView;
		}

		// class for caching the views in a row
		private class ViewHolder {
			TextView name, contact;
			CheckBox checkBox;
		}

	}
}
