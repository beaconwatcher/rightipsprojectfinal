package com.beaconwatcher.rightips.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.ListItemData;
import com.beaconwatcher.rightips.events.ApplicationEvent;
import com.beaconwatcher.rightips.services.IntentServiceFactory;
import com.bw.libraryproject.activities.InternetActivity;
import com.bw.libraryproject.events.intentservice.IntentServiceEvent;
import com.bw.libraryproject.interfaces.SuicidalFragmentListener;

import de.greenrobot.event.EventBus;

public class FragmentListItemActions extends AbstractFragment {
	private Context mContext;
	private LayoutInflater mInflater;

	// Views
	private View mRootView;
	private View mBtnDelete;
	private View mBtnReport;

	private View mFragmentReportContainer;
	private FragmentReportListItem mFragmentReport;
	private SuicidalFragmentListener suicideListener;

	private Bundle mBundle;
	private ListItemData mData;

	public boolean deleteable = true;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		TAG = "FragmentListItemActions";
	}

	@Override
	public View onCreateView(LayoutInflater infl, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mContext = getActivity();
		mRootView = infl.inflate(R.layout.fragment_listitem_actions, container,
				false);
		mInflater = infl;
		mFragmentReportContainer = mRootView
				.findViewById(R.id.report_container);
		mLoader = mRootView.findViewById(R.id.loader_actions);
		// getChildFragmentManager().beginTransaction().hide(mFragmentReport).commit();

		mBtnDelete = mRootView.findViewById(R.id.btn_delete);
		mBtnReport = mRootView.findViewById(R.id.btn_report);

		mBtnDelete.setOnClickListener(mClickListener);
		mBtnReport.setOnClickListener(mClickListener);
		mRootView.findViewById(R.id.btn_cancel).setOnClickListener(
				mClickListener);

		if (getArguments() != null) {
			setData(getArguments());
		}

		return mRootView;
	}

	public void setData(Bundle bundle) {
		mBundle = bundle;
		mFragmentReportContainer.setVisibility(View.INVISIBLE);
		// mFragmentReport.setData(bundle);
		String str = bundle.getString(AppConstants.LIST_ITEM_DATA);
		mData = (ListItemData) mApp.getClassObjectFromJsonString(str,
				ListItemData.class);

		if (Integer.toString(mData.uid).equals(mApp.getUID())
				&& mData.deleteable == true) {
			mBtnReport.setVisibility(View.GONE);
			mBtnDelete.setVisibility(View.VISIBLE);
		}

		else {
			mBtnReport.setVisibility(View.VISIBLE);
			mBtnDelete.setVisibility(View.GONE);
		}

	}

	OnClickListener mClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {

			case R.id.btn_delete:
				showRemoveCommentAlert();
				break;

			case R.id.btn_report:
				addReportFragment();
				mFragmentReportContainer.setVisibility(View.VISIBLE);
				break;

			case R.id.btn_cancel:
				mFragmentReportContainer.setVisibility(View.INVISIBLE);
				EventBus.getDefault()
						.post(new ApplicationEvent(
								AppConstants.LIST_ACTIONS_CANCELED, null, null));
				break;
			}
		}
	};

	private void addReportFragment() {
		mFragmentReport = new FragmentReportListItem();
		mFragmentReport.setArguments(mBundle);

		if (this.getActivity() instanceof InternetActivity) {
			((InternetActivity) getActivity()).changeFragment(mFragmentReport,
					mFragmentReportContainer.getId(), null);
		}
	}

	private void showRemoveCommentAlert() {
		new AlertDialog.Builder(this.getActivity())
				.setTitle(
						mContext.getResources().getString(
								R.string.wall_comment_remove_confirm_title))
				.setMessage(
						mContext.getResources().getString(
								R.string.wall_comment_remove_confirm_msg))
				.setIcon(android.R.drawable.ic_dialog_alert)
				.setPositiveButton(
						mContext.getResources().getString(R.string.lbl_yes),
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int whichButton) {
								removeListItem();
							}
						})
				.setNegativeButton(
						mContext.getResources().getString(R.string.lbl_no),
						null).show();
	}

	private void removeListItem() {
		if (mData == null)
			return;
		showLoader(true);
		Class mClazz = IntentServiceFactory.getInstance()
				.getIntentServiceClass(AppConstants.REMOVE_LIST_ITEM);
		Intent intent = new Intent(mContext, mClazz);
		intent.putExtra(AppConstants.INTENT_SERVICE_TAG, TAG);
		intent.putExtra(AppConstants.USER_ID, mApp.getUID());
		intent.putExtra(AppConstants.LIST_ITEM_ID, Integer.toString(mData.id));
		intent.putExtra(AppConstants.LIST_ITEM_TYPE, mData.type);
		intent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
				AppConstants.REMOVE_LIST_ITEM);
		mContext.startService(intent);
	}

	/*
	 * public void onEvent(IntentServiceEvent event){
	 * mFragmentReportContainer.setVisibility(View.INVISIBLE);
	 * EventBus.getDefault().post(new
	 * ApplicationEvent(AppConstants.LIST_ACTIONS_CANCELED, null, null)); }
	 */
}
