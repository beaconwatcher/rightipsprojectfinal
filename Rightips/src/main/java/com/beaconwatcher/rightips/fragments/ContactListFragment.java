package com.beaconwatcher.rightips.fragments;

import io.branch.referral.Branch.BranchLinkCreateListener;
import io.branch.referral.BranchError;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.ContactListItem;
import com.beaconwatcher.rightips.entities.UserProfileData;
import com.beaconwatcher.rightips.services.GetPhoneContactsService;
import com.bw.libraryproject.customadapers.FilterableSortableAdapter;
import com.makeramen.segmented.PinkRadioGroup;

public class ContactListFragment extends Fragment {

	private static String TAG = "ContactListFragment";

	private LayoutInflater inflater;
	private TextView mTxt;
	private View rootView;
	private PinkRadioGroup mRadioGroup;
	private EditText mTfSearch;
	private ListView mContactList;
	private RelativeLayout mBtnInvite;
	private TextView mOutputText;
	private RelativeLayout mLoader;

	private RighTipsApplication mApp;

	private ArrayList<ContactListItem> mFetchedPhoneContacts;
	private ArrayList<ContactListItem> mFetchedEmailContacts;

	private ArrayList<ContactListItem> mContactArray = new ArrayList<ContactListItem>();
	MyContactAdapter mAdapter;

	private String mMode = GetPhoneContactsService.MODE_SMS;

	// SMS/Email
	private String message;
	private ArrayList<String> numbersToSendSMS = new ArrayList<String>();
	private ArrayList<String> numbersToSendEmail = new ArrayList<String>();
	private String SENT = "sent";
	private String DELIVERED = "delivered";

	ArrayList<PendingIntent> sendPIArray = new ArrayList<PendingIntent>();
	ArrayList<PendingIntent> deliveredPIArray = new ArrayList<PendingIntent>();

	@Override
	public View onCreateView(LayoutInflater infl, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mAdapter = new MyContactAdapter(mContactArray, getActivity()
				.getApplicationContext());

		rootView = infl.inflate(R.layout.fragement_contacts, container, false);
		inflater = infl;
		mTxt = (TextView) rootView.findViewById(R.id.txt);
		mRadioGroup = (PinkRadioGroup) rootView.findViewById(R.id.radioGroup);
		mTfSearch = (EditText) rootView.findViewById(R.id.tf_search);
		mContactList = (ListView) rootView.findViewById(R.id.list_contacts);
		mBtnInvite = (RelativeLayout) rootView.findViewById(R.id.btn_invite);
		mLoader = (RelativeLayout) rootView.findViewById(R.id.loader);

		// mOutputText = (TextView) rootView.findViewById(R.id.output_text);

		mContactList.setAdapter(mAdapter);

		mBtnInvite.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getMsgText();
			}
		});

		mRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				// mTfSearch.setText("");
				switch (checkedId) {
				case R.id.radio_sms:
					mMode = GetPhoneContactsService.MODE_SMS;
					fetchContacts();
					break;

				case R.id.radio_email:
					mMode = GetPhoneContactsService.MODE_EMAIL;
					fetchContacts();
					break;

				case R.id.radio_whatsapp:
					mMode = GetPhoneContactsService.MODE_WHATSAPP;
					getMsgText();
					break;
				}
			}
		});

		mTfSearch.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				mAdapter.getFilter().filter(s.toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
			}
		});
		return rootView;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		mApp = (RighTipsApplication) getActivity()
				.getApplicationContext();

		super.onCreate(savedInstanceState);
		Intent sentIntent = new Intent(SENT);
		/* Create Pending Intents */
		PendingIntent sentPI = PendingIntent.getBroadcast(getActivity(), 0,
				sentIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		Intent deliveryIntent = new Intent(DELIVERED);
		PendingIntent deliverPI = PendingIntent.getBroadcast(getActivity(), 0,
				deliveryIntent, PendingIntent.FLAG_UPDATE_CURRENT);

		sendPIArray.add(sentPI);
		deliveredPIArray.add(deliverPI);

		fetchContacts();
	}

	/*
	 * When request to GetPhoneContactsService sent and after successful
	 * response, we need to tell app that contacts are fetched. That
	 * IntentServices broadcasts this event.
	 */
	IntentFilter contactIntentFilter = new IntentFilter(
			GetPhoneContactsService.GET_PHONE_CONTACTS_ACTION);
	BroadcastReceiver contactBroadCastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			ArrayList<ContactListItem> phoneContacts = intent
					.getParcelableArrayListExtra(GetPhoneContactsService.CONTACTS);
			mContactArray = phoneContacts;
			refreshContactList();
		}
	};

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(
				contactBroadCastReceiver);
		super.onPause();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
				contactBroadCastReceiver, contactIntentFilter);
	}

	private void fetchContacts() {
		showLoader();
		// call GetPhoneContactsService.
		mContactArray.clear();
		Intent intent = new Intent(getActivity(), GetPhoneContactsService.class);
		intent.putExtra("mode", mMode);
		getActivity().startService(intent);
	}

	private void refreshContactList() {
		mAdapter.clear();
		mAdapter.addAll(mContactArray);
		mAdapter.getFilter().filter(mTfSearch.getText());

		/*
		 * mAdapter.sort(new Comparator<ContactListItem>() { public int
		 * compare(ContactListItem p1, ContactListItem p2) { return
		 * p1.getName().compareToIgnoreCase(p2.getName()); } });
		 */

		hideLoader();
	}

	private void getPhoneNumberFromContact(ContactListItem item) {
		Cursor phoneCur = getActivity().getContentResolver().query(
				ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
				ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
				new String[] { item.getId() }, null);
		if (phoneCur.getCount() > 0) {
			phoneCur.moveToFirst();
			String number = phoneCur
					.getString(phoneCur
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
			item.setContact(number);
			phoneCur.close();
		}
	}

	private void inviteFriends(String str) {
		// Toast.makeText(getActivity(),
		// "Sending Invitation To: "+mAdapter.getSelectedItems().toString(),
		// Toast.LENGTH_LONG).show();

		if (mMode.equals(GetPhoneContactsService.MODE_SMS)) {
			sendSMS(str);
		} else if (mMode.equals(GetPhoneContactsService.MODE_EMAIL)) {
			sendEmail(str);
		}

		else if (mMode.equals(GetPhoneContactsService.MODE_WHATSAPP)) {
			openWhatsApp();
		}
	}

	private String getMsgText() {
		showLoader();
		// Check if user logged in
		UserProfileData profile = ((RighTipsApplication) getActivity()
				.getApplicationContext()).getProfile();

		final String msg = getActivity().getResources().getString(
				R.string.invitation_txt);
		JSONObject obj = new JSONObject();
		try {
			if (profile != null) {
				obj.put("id", profile.uid);
				obj.put("name", profile.user_name);
				obj.put("image_url", profile.profile_pic);
			}
		} catch (JSONException e) {
		}

		mApp.getBranch().getShortUrl(obj, new BranchLinkCreateListener() {
			@Override
			public void onLinkCreate(String url, BranchError error) {
				Log.i(TAG, "Ready to share my link = " + url);
				message = msg.concat("\n" + url);// =msg+"\n"+url;
				inviteFriends(mAdapter.getSelectedItems());
				// Toast.makeText(getActivity(), url, Toast.LENGTH_LONG).show();
			}
		});

		return msg;
	}

	private void sendSMS(String str) {
		// IF SMS feature is not selected
		hideLoader();
		Intent smsIntent = new Intent(Intent.ACTION_VIEW);
		smsIntent.setType("vnd.android-dir/mms-sms");
		smsIntent.putExtra("address", str);
		smsIntent.putExtra("sms_body", message);
		startActivity(smsIntent);
	}

	private void sendEmail(String str) {
		hideLoader();
		// IF SMS feature is not selected
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("message/rfc822");
		intent.putExtra(Intent.EXTRA_EMAIL, new String[] { str });
		intent.putExtra(Intent.EXTRA_SUBJECT, "RighTips Invitation");
		intent.putExtra(Intent.EXTRA_TEXT, message);
		Intent mailer = Intent.createChooser(intent, null);
		startActivity(mailer);
	}

	/*************************************************************
	 * SHARE WHATS APP
	 *************************************************************/
	public void openWhatsApp() {
		hideLoader();
		PackageManager pm = getActivity().getPackageManager();
		try {
			Intent waIntent = new Intent(Intent.ACTION_SEND);
			waIntent.setType("text/plain");

			PackageInfo info = pm.getPackageInfo("com.whatsapp",
					PackageManager.GET_META_DATA);
			// Check if package exists or not. If not then code
			// in catch block will be called
			waIntent.setPackage("com.whatsapp");
			waIntent.putExtra(Intent.EXTRA_TEXT, message);
			startActivity(Intent.createChooser(waIntent, "Share with"));

		} catch (NameNotFoundException e) {
			Toast.makeText(getActivity(), "WhatsApp not Installed",
					Toast.LENGTH_LONG).show();
		}
	}

	public class MyContactAdapter extends FilterableSortableAdapter {
		ViewHolder viewHolder;

		public MyContactAdapter(ArrayList<ContactListItem> arr, Context c) {
			super(c, 0);
		}

		public String getSelectedItems() {
			String str = "";
			for (int i = 0; i < getCount(); i++) {
				if (((ContactListItem) getItem(i)).getSelected() == true) {
					String contact = ((ContactListItem) getItem(i))
							.getContact();
					str = str + contact + ",";
				}
			}

			if (str.length() > 0 && str.charAt(str.length() - 1) == ',') {
				str = str.substring(0, str.length() - 1);
			}
			return str;
		}

		@Override
		public View getView(int i, View reuseableView, ViewGroup viewGroup) {
			if (reuseableView == null) {
				reuseableView = inflater.inflate(R.layout.contact_list_item,
						null);
				reuseableView.setLayoutParams(new AbsListView.LayoutParams(
						AbsListView.LayoutParams.MATCH_PARENT, getResources()
								.getDimensionPixelSize(
										R.dimen.contact_item_height)));

				viewHolder = new ViewHolder();

				// cache the views
				viewHolder.name = (TextView) reuseableView
						.findViewById(R.id.txt_name);
				viewHolder.img = (ImageView) reuseableView
						.findViewById(R.id.profile_pic);
				/*
				 * viewHolder.contact = (TextView) reuseableView
				 * .findViewById(R.id.txt_contact);
				 */
				viewHolder.checkBox = (CheckBox) reuseableView
						.findViewById(R.id.cb_contact);

				// link the cached views to the convertview
				reuseableView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) reuseableView.getTag();
			}

			viewHolder.checkBox.setId(i);

			ContactListItem contact = (ContactListItem) getItem(i);

			viewHolder.name.setText(contact.getName());
			// viewHolder.contact.setText(contact.getContact());

			viewHolder.checkBox.setChecked(contact.getSelected());

			viewHolder.checkBox.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ContactListItem item = (ContactListItem) getItem(v.getId());
					((ContactListItem) getItem(v.getId()))
							.setSelected(((CheckBox) v).isChecked());
					item.setSelected(((CheckBox) v).isChecked());

					if (item.getContact().equals("")) {
						getPhoneNumberFromContact(item);
					}
				}
			});

			if (contact.getPhotoUrl() != null
					&& !contact.getPhotoUrl().equals("")) {
				Bitmap bitmap;
				try {
					bitmap = MediaStore.Images.Media.getBitmap(getActivity()
							.getContentResolver(), Uri.parse(contact
							.getPhotoUrl()));
					viewHolder.img.setImageBitmap(bitmap);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
				}
			} else {
				Bitmap bm = BitmapFactory.decodeResource(getResources(),
						R.drawable.cat_no_image);
				viewHolder.img.setImageBitmap(bm);
			}
			return reuseableView;
		}

		// class for caching the views in a row
		private class ViewHolder {
			TextView name, contact;
			ImageView img;
			CheckBox checkBox;
		}
	}

	private void showLoader() {
		if (mLoader != null) {
			mLoader.setVisibility(View.VISIBLE);
		}
	}

	private void hideLoader() {
		if (mLoader != null) {
			mLoader.setVisibility(View.INVISIBLE);
		}
	}
}
