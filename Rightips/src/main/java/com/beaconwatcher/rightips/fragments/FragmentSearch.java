package com.beaconwatcher.rightips.fragments;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.customadapters.ListItemAdapter;
import com.beaconwatcher.rightips.customadapters.ListItemAdapter.ListItemNotifier;
import com.beaconwatcher.rightips.customdialogs.LocationDialog;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.ListItemData;
import com.beaconwatcher.rightips.entities.SearchParamsData;
import com.beaconwatcher.rightips.entities.piwik.PiwikConstants;
import com.beaconwatcher.rightips.events.ApplicationEvent;
import com.beaconwatcher.rightips.events.EventBusEvent;
import com.beaconwatcher.rightips.google.location.GoogleLocationAPIHelper;
import com.beaconwatcher.rightips.google.location.GoogleLocationAPIHelper.AutoCompleteNotifier;
import com.beaconwatcher.rightips.services.IntentServiceFactory;
import com.bw.libraryproject.entities.LocationData;
import com.bw.libraryproject.events.intentservice.NoRecordFoundEvent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationServices;

import de.greenrobot.event.EventBus;

public class FragmentSearch extends AbstractFragment {
	private static final String ERROR_TAG = "Fragment Search";
	private RighTipsApplication mApp;
	public Context mContext;
	private Intent serviceIntent;

	// Views
	private View mRootView;
	private RelativeLayout mSearchCont;
	private RelativeLayout mLocationCont;

	private ToggleButton mBtnLocation;

	private EditText mTfSearch;
	private EditText mTfSearchLocation;

	private TextView mBtnSearch;
	private ListView mLocationsList;

	private View mOverlay;

	// Data
	private ListItemAdapter mAdapter;
	private ArrayList<ListItemData> mLocationArr;
	private ListItemData mFirstItem;
	private GoogleLocationAPIHelper autoCompleteHelper;
	private LayoutParams mParams;
	private int mMode = 0;

	private String mKeyword = "";
	private String mLocationText = "";

	
	//Location Settings
	private LocationData mLocation = null;

	// Location Services
	private boolean locationSettingsChecked = false;
	private GoogleApiClient mGoogleApiClient;
	private LocationManager lm = null;
	private boolean gps_enabled, network_enabled;
	
	
	private boolean calledFromOutside = false;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.TAG = "FragmentSearch";
		mApp = (RighTipsApplication) getActivity()
				.getApplicationContext();
		mContext = getActivity();

		autoCompleteHelper = new GoogleLocationAPIHelper(mNotifier);

		mFirstItem = new ListItemData();
		mFirstItem.id = 0;
		mFirstItem.strId="0";
		mFirstItem.name = mContext.getResources().getString(
				R.string.lbl_current_location);
		mLocationArr = new ArrayList<ListItemData>();
		//mLocationArr.add(mFirstItem);
		
		
		// Location Client
		mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
				.addApi(LocationServices.API)
				.addConnectionCallbacks(mGoogleAPIConnectionCallbacks)
				.addOnConnectionFailedListener(mGoogleAPIConnectionFailedListener).build();
		//mGoogleApiClient.connect();
	}
	
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		// TODO Auto-generated method stub
		super.setUserVisibleHint(isVisibleToUser);
		if(isVisibleToUser){
			Toast.makeText(mContext, "Fragment Search is visible again", Toast.LENGTH_LONG).show();
		}
		
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(mApp.getPreferences("locationSettingsChecked", "").equals("")){
			checkLocationSettings();
		}
	}
	

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.fragement_search, container);
		mLoader = mRootView.findViewById(R.id.loader_search);

		mSearchCont = (RelativeLayout) mRootView.findViewById(R.id.search_cont);
		mLocationCont = (RelativeLayout) mRootView
				.findViewById(R.id.location_container);

		mParams = mSearchCont.getLayoutParams();

		mBtnLocation = (ToggleButton) mRootView.findViewById(R.id.btn_location);
		mBtnLocation.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked == true) {
					switchMode(2);
				} else {
					switchMode(1);
				}
			}
		});

		mBtnSearch = (TextView) mRootView.findViewById(R.id.btn_search);
		mBtnSearch.setOnClickListener(mClickListener);

		mLocationsList = (ListView) mRootView.findViewById(R.id.locations_list);

		mOverlay = mRootView.findViewById(R.id.search_overlay);
		mOverlay.setOnClickListener(mClickListener);

		mRootView.findViewById(R.id.btn_search_expander).setOnClickListener(
				mClickListener);

		mAdapter = new ListItemAdapter(AppConstants.LIST_ITEM_LOCATIONS,
				mLocationArr, mLocationsList, mItemClickNotifier);
		mLocationsList.setAdapter(mAdapter);

		mTfSearch = (EditText) mRootView.findViewById(R.id.tf_search);
		mTfSearchLocation = (EditText) mRootView.findViewById(R.id.tf_location);

		mTfSearchLocation.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				autoCompleteHelper.getAutoComplete(s.toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
		loadLastSearchParams();
		return mRootView;
	};

	OnClickListener mClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {

			case R.id.btn_search_expander:
				mParams.height = LayoutParams.WRAP_CONTENT;
				mSearchCont.setLayoutParams(mParams);
				mOverlay.setVisibility(View.VISIBLE);
				switchMode(1);
				showKeyboard();
				break;

			case R.id.btn_search:
				calledFromOutside  = false;
				callSearch(mTfSearch.getText().toString());
				break;

			case R.id.search_overlay:
				switchMode(0);
				break;
			}
		}
	};

	public void onEvent(EventBusEvent evt) {
		showLoader(false);
	}
	
	
	public void onEvent(NoRecordFoundEvent evt) {
		showLoader(false);
		if(evt.tag.equals(TAG)){
			mApp.showDialogMessage(mContext, mContext.getResources().getString(R.string.no_record_search));
		}
	}
	
	
	//Search API called from outside of this fragment.
	//We must check if location is found.
	public void onEvent(ApplicationEvent evt) {
		if(evt.getType().equals(AppConstants.SEARCH_TAGS_CLICKED)){
			calledFromOutside = true;
			mKeyword=(String) evt.getData();
			if(mLocation==null){
				checkLocationSettings();
			}
			else{
				callSearch(mKeyword);
			}
		}
	}
	
	public void setLocation(LocationData ld){
		mLocation=ld;
		updateSearchButtonText();
	}

	

	public void switchMode(int i) {
		mMode = i;

		switch (mMode) {
		case 0:
			mBtnLocation.setChecked(false);
			mParams.height = 0;
			mSearchCont.setLayoutParams(mParams);
			mOverlay.setVisibility(View.INVISIBLE);
			hideKeyboard();
			break;

		case 1:
			mBtnLocation.setChecked(false);
			mLocationCont.setVisibility(View.GONE);
			if (mLocation != null) {
				mBtnSearch.setVisibility(View.VISIBLE);
			}
			clearLocationList();
			mTfSearch.requestFocus();
			//showKeyboard();
			break;

		case 2:
			mBtnSearch.setVisibility(View.GONE);
			mLocationCont.setVisibility(View.VISIBLE);
			clearLocationList();
			mTfSearchLocation.requestFocus();
			showKeyboard();
			break;
		}
	}

	private void updateSearchButtonText() {
		mBtnSearch.setText(mContext.getResources().getString(
				R.string.lbl_search)
				+ " "
				+ mContext.getResources().getString(R.string.lbl_in)
				+ " " + mLocation.getCityName().toUpperCase());
	}

	private void clearLocationList() {
		mLocationArr.clear();
		mLocationArr.add(mFirstItem);
		mAdapter.notifyDataSetChanged();
	}

	AutoCompleteNotifier mNotifier = new AutoCompleteNotifier() {
		public void onAutoCompleteNotified(ArrayList<ListItemData> arr) {
			mLocationArr.clear();
			mLocationArr.add(mFirstItem);
			mLocationArr.addAll(arr);
			mAdapter.notifyDataSetChanged();
		};

		public void onLocationFound(
				LocationData location) {
			mLocation = location;
			mLocationText = mTfSearch.getText().toString();
			autoCompleteHelper.fetchCityName(mContext, location);
			switchMode(1);
		};

		public void onCityNameFound(LocationData location) {
			mApp.getTracker()
			.trackEvent(
					PiwikConstants.CATEGORY_LOCATION,
					PiwikConstants.ACTION_LOCATION_SEARCHED,
					"{'Keyword': "+mKeyword+", "+ "'Location: '"+mLocation.getCityName()+"}");

			
			
			if(calledFromOutside==false){
				switchMode(1);
			}
			
			showLoader(false);
			mLocation = location;
			updateSearchButtonText();
			saveLocationInLocalStorage();
			EventBus.getDefault().post(new ApplicationEvent(AppConstants.LOCATION_CHANGED, mLocation, null));
		};

		public void onCityNameError(String str) {
			showLoader(false);
		};

	};

	ListItemNotifier mItemClickNotifier = new ListItemNotifier() {
		public void onItemClicked(Object item) {
			showLoader(true);
			hideKeyboard();
			
			if((ListItemData)item==mFirstItem){
				checkLocationSettings();
			}
			else{
				autoCompleteHelper.getLocationFromId(((ListItemData) item).strId);
			}
		};
	};
	
	
	private void callSearch(String keyword) {
		// Hide Soft Keyboard.
		hideKeyboard();
		mKeyword = keyword;
		if (mLocation != null) {
			showLoader(true);
			Class mClazz = IntentServiceFactory.getInstance()
					.getIntentServiceClass(
							AppConstants.START_SEARCH_WITH_KEYWORD);
			Intent intent = new Intent(mContext, mClazz);
			intent.putExtra(AppConstants.INTENT_SERVICE_TAG, TAG);
			intent.putExtra(AppConstants.SEARCH_KEYWORD, mKeyword);
			intent.putExtra(AppConstants.LAST_LATITUDE,
					Double.toString(mLocation.getLatitude()));
			intent.putExtra(AppConstants.LAST_LONGITUDE,
					Double.toString(mLocation.getLongitude()));

			intent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
					AppConstants.START_SEARCH_WITH_KEYWORD);
			mContext.startService(intent);
			saveSearchIntoLocalStorage();
			switchMode(0);
			
			mApp.getTracker()
			.trackEvent(
					PiwikConstants.CATEGORY_SEARCH,
					PiwikConstants.ACTION_SEARCH_CLICKED,
					"{'Keyword': "+mKeyword+", "+ "'Location: '"+mLocation.getCityName()+"}");
		}
	}

	private void hideKeyboard() {
		View view = getActivity().getCurrentFocus();
		mApp.hideKeyboard(view);
	}
	
	private void showKeyboard(){
		InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
	}
	

	private void loadLastSearchParams() {
		SearchParamsData lastSearch = (SearchParamsData) mApp
				.getGSONPreferences(AppConstants.LAST_SEARCH_PARAMS, null,
						SearchParamsData.class);
		if (lastSearch != null) {
			mKeyword = lastSearch.keyword;
			mLocation = new LocationData();
			mLocation.setLatitude(lastSearch.latitude);
			mLocation.setLongitude(lastSearch.longitude);
			mLocation.setCityName(lastSearch.cityName);

			mTfSearch.setText(mKeyword);
			mBtnSearch.setVisibility(View.VISIBLE);
			updateSearchButtonText();
		}
	}
	
	
	private void saveLocationInLocalStorage(){
		//Save data in local storage
		mApp.saveGSONPreferences(AppConstants.LAST_LOCATION_DATA, mLocation);
		String locString = mApp.getJSONStringFromClassObject(mLocation);
		if (mLocation.getSource().equals(AppConstants.LOCATION_SOURCE_GPS)) {
				mApp.getTracker().trackEvent(PiwikConstants.CATEGORY_LOCATION, PiwikConstants.ACTION_CURRENT_LOCATION, locString);
			}
		else {
			mApp.getTracker().trackEvent(PiwikConstants.CATEGORY_LOCATION, PiwikConstants.ACTION_LOCATION_SEARCHED, locString);
		}
	}
	
	

	private void saveSearchIntoLocalStorage() {
		SearchParamsData lastSearch = new SearchParamsData();
		lastSearch.cityName = mLocation.getCityName();
		lastSearch.latitude = mLocation.getLatitude();
		lastSearch.longitude = mLocation.getLongitude();
		lastSearch.keyword = mKeyword;
		mApp.saveGSONPreferences(AppConstants.LAST_SEARCH_PARAMS, lastSearch);
	}
	
	
	private void checkLocationSettings() {
		if (lm == null)
			lm = (LocationManager) mContext
					.getSystemService(Context.LOCATION_SERVICE);

		try {
			gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
		} catch (Exception ex) {
		}

		try {
			network_enabled = lm
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		} catch (Exception ex) {
		}

		if (!gps_enabled && !network_enabled) {
			showLoader(false);
			LocationDialog locDialog=new LocationDialog(getActivity());
			locDialog.show();
			mApp.savePreferences("locationSettingsChecked", "yes");
		}
		
		else {
			checkGoogleClient();
		}
	}
	
	
	/***********************************************************
	 * CHECK IF LOCATION SETTINGS ENABLED
	 **********************************************************/

	/***********************************************************
	 * LOCATION FEATURE
	 **********************************************************/

	/*
	 * Whenever we need to get latest GPS locaiton, we can calls this function.
	 */

	public void checkGoogleClient() {
		if (googleServicesAvailable()) {
			if (mGoogleApiClient.isConnected()) {
				getCurrentLocation();

			} else {
				mGoogleApiClient.connect();
			}
		} else {
			showLoader(false);
			// Toast.makeText(mContext,
			// getResources().getString(R.string.gps_settings_on_error),
			// Toast.LENGTH_LONG).show();
			// mApp.showErrorToast(mContext, "Google Services API");
		}
	}

	/*
	 * Called by Location Services when the request to connect the client
	 * finishes successfully. At this point, you can request the current
	 * location or start periodic updates
	 */
	private ConnectionCallbacks mGoogleAPIConnectionCallbacks = new ConnectionCallbacks() {
		@Override
		public void onConnectionSuspended(int arg0) {
			showLoader(false);
			Log.e(TAG, "Disconnected from Google Services API");
		}

		@Override
		public void onConnected(Bundle arg0) {
			Log.d(TAG, "Connected to Google Services API");
			getCurrentLocation();
			//checkLocationSettings();
		}
	};

	private OnConnectionFailedListener mGoogleAPIConnectionFailedListener = new OnConnectionFailedListener() {
		@Override
		public void onConnectionFailed(ConnectionResult arg0) {
			showLoader(false);
			// TODO Auto-generated method stub
			// Toast.makeText(mContext,
			// "Unable to connect to Google API Client",
			// Toast.LENGTH_SHORT).show();
			// mApp.showErrorToast(mContext, "Google API Connection");

		}
	};

	public void getCurrentLocation() {
		// If Google Play Services is available
		if (googleServicesAvailable()) {
			// Get the current location
			Log.d(TAG,
					"Trying to get location coordinates using Google Services API");

			Location currentLocation = LocationServices.FusedLocationApi
					.getLastLocation(mGoogleApiClient);
			if (currentLocation != null) {
				Log.d(TAG, "Google Map link=https//www.google.com/maps/place/"
						+ currentLocation.getLatitude() + ", "
						+ currentLocation.getLongitude());
				mApp.savePreferences(
						"lastLocation",
						"https://www.google.com/maps/place/"
								+ currentLocation.getLatitude() + ", "
								+ currentLocation.getLongitude());
				
				mLocation = new LocationData();
				mLocation.setLatitude(currentLocation.getLatitude());
				mLocation.setLongitude(currentLocation.getLongitude());
				mLocation.setSource(AppConstants.LOCATION_SOURCE_GPS);
				
				
				
				//If current location found and search is called
				//from outside of this fragments, we must call search api
				if(calledFromOutside == true){
					callSearch(mKeyword);
				}
				
				autoCompleteHelper.fetchCityName(mContext, mLocation);
				
			} else {
				Log.e(TAG,
						"Failed to get coordinates using Google Services API");
				// Toast.makeText(mContext,
				// "Failed to get coordinates using Google Services API",
				// Toast.LENGTH_SHORT).show();
				// mApp.showErrorToast(mContext, "Google Services API");
//				mApp.saveGSONPreferences(AppConstants.LAST_LOCATION_DATA, "");
				mApp.showToast(mContext, "Failed to get current location, Please try again", Toast.LENGTH_LONG);
				showLoader(false);
			}
		}
	}

	
	private boolean googleServicesAvailable() {
		// Check that Google Play services is available
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(mContext);
		// If Google Play services is available
		if (ConnectionResult.SUCCESS == resultCode) {
			return true;
			// Google Play services was not available for some reason
		} else {
			return false;
		}
	}





}
