package com.beaconwatcher.rightips.fragments;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.activities.ActivitySelectImage;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.UserProfileImage;
import com.beaconwatcher.rightips.entities.UserProfileData;
import com.beaconwatcher.rightips.events.EventBusEvent;
import com.beaconwatcher.rightips.services.IntentServiceFactory;
import com.bw.libraryproject.customrenderers.CircularImageView;
import com.bw.libraryproject.utils.BitmapUtilities;
import com.bw.libraryproject.utils.DateUtilities;
import com.squareup.picasso.Picasso;

public class FragmentProfileCover extends AbstractFragment {
	private static final String ERROR_TAG = "Profile Cover";
	private static final int SELECT_IMAGE_REQUEST_CODE = 5000;

	private static final int PROFILE_THUMBNAIL_SIZE = 200;
	private static final int COVER_THUMBNAIL_SIZE = 400;

	private RighTipsApplication mApp;
	private Context mContext;
	private Intent serviceIntent;

	// Views
	public CircularImageView mProfilePicImageView;
	View mBtnCamera;
	View mRootView;
	View mMainContainer;
	View mChooseImageType;

	ImageView mCoverImageView;
	TextView mTxtUsername;
	TextView mTxtDate;

	// Data
	private String mUserID;
	private UserProfileData mProfile;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.TAG = "FragmentProfileCover";
		mApp = (RighTipsApplication) getActivity()
				.getApplicationContext();
		mContext = getActivity();
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.fragement_profile_cover,
				container);

		mLoader = (RelativeLayout) mRootView
				.findViewById(R.id.loader_profile_cover);

		mTxtUsername = (TextView) mRootView.findViewById(R.id.txt_username);
		mTxtDate = (TextView) mRootView.findViewById(R.id.txt_date);
		mProfilePicImageView = (CircularImageView) mRootView
				.findViewById(R.id.profile_pic);
		mCoverImageView = (ImageView) mRootView.findViewById(R.id.img_cover);
		mBtnCamera = mRootView.findViewById(R.id.btn_camera);

		mBtnCamera.setOnClickListener(mClickListener);
		mChooseImageType = mRootView.findViewById(R.id.choose_image_type);
		mRootView.findViewById(R.id.btn_profile).setOnClickListener(
				mClickListener);
		mRootView.findViewById(R.id.btn_cover).setOnClickListener(
				mClickListener);
		mRootView.findViewById(R.id.btn_cancel).setOnClickListener(
				mClickListener);

		return mRootView;
	};

	public void setProfileData(UserProfileData p) {
		mProfile = p;
		mUserID = Integer.toString(p.uid);

		String str = mApp.getUID();
		if (!mUserID.equals(str)) {
			mBtnCamera.setVisibility(View.GONE);
		}

		mTxtUsername.setText(mProfile.user_name);
		if (!mProfile.created.equals("")) {
			String duration = DateUtilities.getDuration(mProfile.created);
			mTxtDate.setText(duration);
		}

		if (mProfile.profile_pic != null && !mProfile.profile_pic.equals("")
				&& !mProfile.profile_pic.equals("null"))
			Picasso.with(mContext).load(mProfile.profile_pic)
					.placeholder(R.drawable.no_photo_round)
					.error(R.drawable.no_photo_round)
					.into(mProfilePicImageView);

		if (mProfile.cover_photo != null && !mProfile.cover_photo.equals("")
				&& !mProfile.cover_photo.equals("null"))
			Picasso.with(mContext).load(mProfile.cover_photo).fit()
					.into(mCoverImageView);
	}

	OnClickListener mClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {

			case R.id.btn_camera:
				askImageSelection();
				break;

			case R.id.btn_profile:
				mChooseImageType.setVisibility(View.GONE);
				addImageSelection(AppConstants.ACTION_SAVE_USER_PROFILE_IMAGE);
				break;

			case R.id.btn_cover:
				mChooseImageType.setVisibility(View.GONE);
				addImageSelection(AppConstants.ACTION_SAVE_USER_COVER_IMAGE);
				break;

			case R.id.btn_cancel:
				mChooseImageType.setVisibility(View.GONE);
				break;
			}
		}
	};

	private void askImageSelection() {
		mChooseImageType.setVisibility(View.VISIBLE);
	}

	private void addImageSelection(String action) {
		Intent intent = new Intent(mContext, ActivitySelectImage.class);
		intent.putExtra(AppConstants.ACTION_TYPE, action);
		startActivityForResult(intent, SELECT_IMAGE_REQUEST_CODE);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		String action = data.getStringExtra(AppConstants.ACTION_TYPE);

		if (requestCode == SELECT_IMAGE_REQUEST_CODE
				&& resultCode == ActivitySelectImage.IMAGE_SELECTION_RESULT_CODE) {
			int thumbnailSize = action
					.equals(AppConstants.ACTION_SAVE_USER_PROFILE_IMAGE) ? PROFILE_THUMBNAIL_SIZE
					: COVER_THUMBNAIL_SIZE;
			Uri uri = (Uri) data.getParcelableExtra(AppConstants.IMAGE_URI);
			if (uri == null)
				return;

			byte[] myByteArray = BitmapUtilities.getByteArray(uri,
					thumbnailSize, mContext);

			showLoader(true);
			Class mClazz = null;
			mClazz = IntentServiceFactory.getInstance().getIntentServiceClass(
					action);
			if (mClazz == null)
				return;

			Intent intent = new Intent(mContext, mClazz);
			intent.putExtra(AppConstants.USER_ID, mUserID);
			intent.putExtra(AppConstants.IMAGE_BYTE_ARRAY, myByteArray);
			intent.putExtra(AppConstants.IMAGE_THUMBNAIL_SIZE, thumbnailSize);
			intent.putExtra(AppConstants.INTENT_SERVICE_TAG, TAG);
			intent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE, action);
			mContext.startService(intent);

		}
	}

	public byte[] getBytes(InputStream inputStream) throws IOException {
		ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
		int bufferSize = 1024;
		byte[] buffer = new byte[bufferSize];

		int len = 0;
		while ((len = inputStream.read(buffer)) != -1) {
			byteBuffer.write(buffer, 0, len);
		}
		return byteBuffer.toByteArray();
	}

	// After image saved on server we get back pic as url of newly saved image
	public void onEvent(EventBusEvent event) {
		showLoader(false);
		if (event.getType().equals(AppConstants.ACTION_SAVE_USER_PROFILE_IMAGE)
				|| event.getType().equals(
						AppConstants.ACTION_SAVE_USER_COVER_IMAGE)) {
			ArrayList<UserProfileImage> arr = (ArrayList<UserProfileImage>) event
					.getData();
			if (arr.size() > 0) {
				String url = arr.get(0).pic;
				if (url != null && !url.equals("") && !url.equals("null")) {
					if (event.getType().equals(
							AppConstants.ACTION_SAVE_USER_PROFILE_IMAGE)) {
						// Notify home page to update url in local storage.
						saveProfilePicIntoLocalPreferences(url);
						// update bitmap.
						mProfile.profile_pic = url;
						Picasso.with(mContext).load(mProfile.profile_pic)
								.placeholder(R.drawable.no_photo_round)
								.error(R.drawable.no_photo_round)
								.into(mProfilePicImageView);
					}

					else {
						mProfile.cover_photo = url;
						saveProfileCoverIntoLocalPreferences(url);
						Picasso.with(mContext).load(mProfile.cover_photo)
								.placeholder(R.drawable.bg_new)
								.error(R.drawable.bg_new).into(mCoverImageView);
					}
				}
			}
		}
	}

	private void saveProfilePicIntoLocalPreferences(String url) {
		UserProfileData profile = mApp.getProfile();
		if (profile != null) {
			profile.profile_pic = url;
			mApp.saveGSONPreferences(AppConstants.PREF_LOGIN_DATA, profile);
		}
	}

	private void saveProfileCoverIntoLocalPreferences(String url) {
		UserProfileData profile = mApp.getProfile();
		if (profile != null) {
			profile.cover_photo = url;
			mApp.saveGSONPreferences(AppConstants.PREF_LOGIN_DATA, profile);
		}
	}
}
