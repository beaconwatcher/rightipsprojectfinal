package com.beaconwatcher.rightips.fragments;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.customslides.NotificationSliderView;
import com.bw.libraryproject.entities.NotificationData;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.BaseSliderView.OnSliderClickListener;


public class FragmentSlider extends NotificationReceiverFragment {
	private Context mContext;
	private LayoutInflater inflater;
	private View rootView;
	private SliderLayout mSlider;

	private String slideIDToShow = "";
	private int slideIndexToShow = 0;

	// private FancyCoverFlow fancyCoverFlow;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mContext = getActivity();
		maxNotifications = 0;
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater infl, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		rootView = infl.inflate(R.layout.fragment_slider, container, false);
		// Set max records to unlimited by assigning 0
		inflater = infl;

		mSlider = (SliderLayout) rootView.findViewById(R.id.slider);
		mSlider.setPresetTransformer(SliderLayout.Transformer.ZoomOutSlide);
		// mSlider.setPresetIndicator(SliderLayout.PresetIndicators.Right_Top);
		// mSlider.setCustomAnimation(new DescriptionAnimation());
		mSlider.setDuration(4000);

		mSlider.setCustomIndicator((PagerIndicator) rootView
				.findViewById(R.id.custom_indicator_big_slider));

		// In case this activity comes from History Item Click
		// We need to show selected slide.
		if (getArguments() != null && !getArguments().isEmpty()) {
			String id = getArguments().getString("id");
			if (id != null) {
				slideIDToShow = id;
			}
		}

		return rootView;
	}

	/*
	 * @Override public void onNotificationBroadcastReceived(NotificationData
	 * nd) { // TODO Auto-generated method stub getAllNotifications(); }
	 */

	@Override
	public void onResume() {
		super.onResume();
		if (getArguments() != null && !getArguments().isEmpty()) {
			ArrayList<NotificationData> arr = getArguments()
					.getParcelableArrayList("notifications");
			if (arr != null) {
				notifications = arr;
				OnAllNotificationFetched();
			}
		}
	}

	@Override
	public void OnAllNotificationFetched() {
		// This is called from onPause application event.
		if (notifications.size() > 0) {
			addSlides();
		}
	}

	private void addSlides() {
		mSlider.removeAllSliders();
		NotificationData nd;
		NotificationSliderView notificationSlide;

		for (int i = 0; i < notifications.size(); i++) {
			nd = notifications.get(i);
			if (slideIDToShow != null && !slideIDToShow.equals("")
					&& slideIDToShow.equals(nd.getNoteID())) {
				slideIndexToShow = i;
				slideIDToShow = "";
			}

			notificationSlide = new NotificationSliderView(mContext);
			// TextSliderView textSliderView = new TextSliderView(mContext);
			notificationSlide.title(nd.getTitle()).siteName(nd.getSiteName())
					.siteAddress(nd.getSiteAddress())
					.description(nd.getDetail())
					.image(nd.getImages().split(",")[0])
					.setScaleType(NotificationSliderView.ScaleType.CenterCrop)
					.setOnSliderClickListener(slideClickListener);

			// initialize a SliderLayout
			// add your extra information
			notificationSlide.getBundle().putParcelable("notification",
					notifications.get(i));
			// .putString("extra", notifications.get(i).getID().toString());

			mSlider.addSlider(notificationSlide);

		}

		if (slideIndexToShow != 0) {
			mSlider.setCurrentPosition(slideIndexToShow);
			slideIndexToShow = 0;
		}

	}

	private OnSliderClickListener slideClickListener = new OnSliderClickListener() {
		@Override
		public void onSliderClick(BaseSliderView slide) {
			// TODO Auto-generated method stub
			NotificationData nd = (NotificationData) slide.getBundle()
					.getParcelable("notification");
			ArrayList<NotificationData> arr = new ArrayList<NotificationData>();
			arr.add(nd);
			mFragmentInterface.onSliderItemClicked(arr);

			/*
			 * Intent intent=new Intent(SliderFragment.this,
			 * NotificationFragment.class); NotificationData
			 * nd=(NotificationData
			 * )slider.getBundle().getParcelable("notification");
			 * intent.putExtra(NotificationFragment.NOTIFICATION_TO_SHOW, nd);
			 * startActivity(intent);
			 */
		}
	};

	@Override
	public void onDestroy() {
		super.onDestroy();
		View rv = rootView.findViewById(R.id.root_view);
		if (rv != null) {
			UnbindDrawables(rv);
		}
		System.gc();
	}

	//
	// public class NotificationAdapter extends FancyCoverFlowAdapter {
	// // private Context mContext;
	// private ArrayList<NotificationData> _notes;
	//
	//
	// public NotificationAdapter(ArrayList<NotificationData> nd){
	// _notes = nd;
	// }
	//
	// @Override
	// public int getCount() {
	// return _notes.size();//images.length;
	// }
	//
	// @Override
	// public long getItemId(int i) {
	// return i;
	// }
	//
	// @Override
	// public NotificationData getItem(int position) {
	// // TODO Auto-generated method stub
	// return _notes.get(position);
	// }
	//
	//
	// @Override
	// public View getCoverFlowItem(final int i, View reuseableView, ViewGroup
	// viewGroup) {
	// final View item;
	//
	// if (reuseableView != null) {
	// item = (View) reuseableView;
	// }
	//
	// else {
	// item=inflater.inflate(R.layout.notification_slide_big, viewGroup, false);
	// item.setLayoutParams(new
	// FancyCoverFlow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
	// ViewGroup.LayoutParams.MATCH_PARENT));
	//
	// double ww= getWindowManager().getDefaultDisplay().getWidth();
	// double margin=ww/8;
	// ww=ww-margin;
	// double hh = getWindowManager().getDefaultDisplay().getHeight();
	// hh=hh-margin;
	//
	// int topHeight=(int) Math.round(hh/2.5);
	//
	// View child1=item.findViewById(R.id.child_1);
	// child1.getLayoutParams().width=(int) ww;
	// child1.getLayoutParams().height=topHeight;
	//
	// View child2=item.findViewById(R.id.child_2);
	// child2.getLayoutParams().width=(int) ww;
	// child2.getLayoutParams().height=(int)
	// (hh)-child1.getLayoutParams().height;
	// }
	//
	// NotificationData nd=getItem(i);
	//
	// TextView tv=(TextView) item.findViewById(R.id.txt_title);
	// tv.setText(nd.getTitle());
	//
	// TextView tv1=(TextView) item.findViewById(R.id.txt_desc);
	// tv1.setText(nd.getDetail());
	//
	// TextView tv2=(TextView) item.findViewById(R.id.txt_site_name);
	// tv2.setText(nd.getSiteName());
	//
	// TextView tv3=(TextView) item.findViewById(R.id.txt_site_addresss);
	// tv3.setText(nd.getSiteAddress());
	//
	//
	//
	// Transformation transformation = new Transformation() {
	// @Override public Bitmap transform(Bitmap source) {
	// int targetWidth = item.findViewById(R.id.notification_image).getWidth();
	//
	// double aspectRatio = (double) source.getHeight() / (double)
	// source.getWidth();
	// int targetHeight = (int) (targetWidth * aspectRatio);
	// Bitmap result = Bitmap.createScaledBitmap(source, targetWidth,
	// targetHeight, false);
	// if (result != source) {
	// // Same bitmap is returned if sizes are the same
	// source.recycle();
	// }
	// return result;
	// }
	//
	// @Override public String key() {
	// return "transformation" + " desiredWidth";
	// }
	// };
	//
	//
	//
	// if(nd.getImages().length() > 0){
	// String url=nd.getImages().split(",")[0];
	//
	// if(url!=null){
	// ImageView img=(ImageView) item.findViewById(R.id.notification_image);
	// //Picasso.with(mContext).load(url).placeholder(R.drawable.cat_no_image).error(R.drawable.cat_no_image).into(img);
	// Picasso.with(mContext)
	// .load(url)
	// .error(android.R.drawable.stat_notify_error)
	// .transform(transformation)
	// .into(img, new Callback() {
	// @Override
	// public void onSuccess() {
	// //holder.progressBar_picture.setVisibility(View.GONE);
	// }
	//
	// @Override
	// public void onError() {
	// //Log.e(LOGTAG, "error");
	// //holder.progressBar_picture.setVisibility(View.GONE);
	// }
	// });
	// }
	// }
	//
	// return item;
	// }
	// }
}
