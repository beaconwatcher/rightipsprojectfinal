package com.beaconwatcher.rightips.fragments;

import java.util.ArrayList;

import com.bw.libraryproject.entities.LocationData;
import com.bw.libraryproject.entities.NotificationData;

public interface ListItemActionsInterface {
	public void onShowActionsClicked();

	public void onFragmentSuicide(String tag);
}
