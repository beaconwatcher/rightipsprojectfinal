package com.beaconwatcher.rightips.fragments;

import java.util.ArrayList;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.beaconwatcher.rightips.db.RTDBManager;
import com.bw.libraryproject.entities.NotificationData;

/*
 * This Activity listens for Broadcast of Notification from RighTipsMonitorService
 * It receive the broadcast and call it's onNotificationReceived function.
 * Other activities extend this as base class and override onNotificationReceived function.
 * Most probably child activities call db to fetch updated records.
 */

public class NotificationReceiverFragment extends AbstractFragment {
	// private NotificationReceiver notificationReceiver;
	private RTDBManager dbManager;

	public int maxNotifications = 10;
	public ArrayList<NotificationData> notifications;

	public void OnAllNotificationFetched() {
	};

	// public void onNotificationBroadcastReceived(NotificationData nd){};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		dbManager = new RTDBManager(getActivity());
		// If user comes to this activity by clicking notification top bar
		// then it needs to add notification into db first.
		/*
		 * NotificationData
		 * nd=getActivity().getIntent().getParcelableExtra(RighTipsMonitorService
		 * .NOTIFICATION_DATA); if(nd!=null){ dbManager.open();
		 * dbManager.insertNotification(nd); }
		 */
		super.onCreate(savedInstanceState);
	}

	public void insertNotificationIntoDB(NotificationData nd) {
		dbManager.open();
		dbManager.insertNotification(nd);
		getAllNotifications();
	}

	public void getAllNotifications() {
		dbManager.open();
		notifications = dbManager.getAllNotifications(Integer
				.toString(maxNotifications));// new
												// ArrayList<NotificationData>();
		OnAllNotificationFetched();
	};

	public RTDBManager getDbManager() {
		dbManager.open();
		return dbManager;
	}

	public void UnbindDrawables(View view) {
		if (view.getBackground() != null) {
			view.getBackground().setCallback(null);
		}
		if (view instanceof ViewGroup) {
			for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
				UnbindDrawables(((ViewGroup) view).getChildAt(i));
			}

			try {
				((ViewGroup) view).removeAllViews();
			} catch (UnsupportedOperationException e) {
				Log.e("Destroy", "cannot remove view: " + view.getId() + ", "
						+ e.getMessage());
			}
		}
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		getAllNotifications();
	}

	/*
	 * @Override public void onPause() { if (notificationReceiver != null){ try{
	 * getActivity().unregisterReceiver(notificationReceiver);
	 * notificationReceiver=null; } catch(IllegalArgumentException e) {
	 * Log.e("NotificationReceiverActivity", "cannot unregister receiver"); } }
	 * super.onPause(); }
	 */

	/*
	 * @Override public void onDestroy() { // TODO Auto-generated method stub if
	 * (notificationReceiver != null){ try{
	 * getActivity().unregisterReceiver(notificationReceiver);
	 * notificationReceiver=null; } catch(IllegalArgumentException e) {
	 * Log.e("NotificationReceiverActivity", "cannot unregister receiver"); } }
	 * super.onDestroy(); }
	 */

	/*
	 * @Override public void onResume() { if (notificationReceiver==null){ try{
	 * notificationReceiver=new NotificationReceiver(); IntentFilter filter =
	 * new IntentFilter();
	 * filter.addAction(RighTipsMonitorService.FOREGROUND_NOTIFICATION);
	 * getActivity().registerReceiver(notificationReceiver, filter); }
	 * catch(IllegalArgumentException e) { Log.e("NotificationReceiverActivity",
	 * "cannot register receiver"); } } getAllNotifications(); super.onResume();
	 * }
	 */

	/*
	 * private class NotificationReceiver extends BroadcastReceiver {
	 * 
	 * @Override public void onReceive(Context context, Intent intent) { if
	 * (intent
	 * .getAction().equals(RighTipsMonitorService.FOREGROUND_NOTIFICATION)) {
	 * NotificationData nd=(NotificationData)
	 * intent.getParcelableExtra(RighTipsMonitorService.NOTIFICATION_DATA);
	 * nd.setStatus("opened"); dbManager.open();
	 * dbManager.insertNotification(nd); getAllNotifications();
	 * onNotificationBroadcastReceived(nd); } } }
	 */
}
