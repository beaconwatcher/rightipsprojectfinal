package com.beaconwatcher.rightips.fragments;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.activities.VideoRecordingActivity;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.events.ApplicationEvent;
import com.bw.libraryproject.utils.BitmapUtilities;

import de.greenrobot.event.EventBus;

public class FragmentSelectImage extends AbstractFragment {
	private static final String ERROR_TAG = "FragmentSelectImage";
	private RighTipsApplication mApp;
	private Context mContext;
	private View mBtnContainer;
	private RelativeLayout mPreviewContainer;
	private ImageView mPreviewImage;
	private TextView mBtnUseImage;
	private TextView mBtnPhotoLibrary;
	private TextView mBtnTakePhoto;
	private TextView mBtnRecordVideo;
	private TextView mBtnCancel;
	private View mBtnTick;

	private static final int FILECHOOSER_REQUESTCODE = 2888;
	private static final int TAKE_PHOTO_REQUESTCODE = 2889;
	private static final int RECORD_VIDEO_REQUESTCODE = 2890;


	private Uri mSelectedImageURI = null;

	//Bitmap rotated after take image is straightened.
	private Bitmap straightBitmap = null;
	private boolean isVideo = false;
	private String videoUrl = "";
	private boolean recordVideo = true;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mApp = (RighTipsApplication) getActivity()
				.getApplicationContext();
		mContext = getActivity();

	}

	public View onCreateView(android.view.LayoutInflater inflater,
			android.view.ViewGroup container, Bundle savedInstanceState) {

		View mRootView = inflater.inflate(R.layout.fragement_select_image,
				container, false);
		mBtnContainer = mRootView.findViewById(R.id.btn_container);
		mPreviewContainer = (RelativeLayout) mRootView
				.findViewById(R.id.preview_container);
		mPreviewImage = (ImageView) mRootView.findViewById(R.id.preview_img);

		mBtnUseImage = (TextView) mRootView.findViewById(R.id.btn_use_image);
		mBtnPhotoLibrary = (TextView) mRootView
				.findViewById(R.id.btn_photo_library);
		mBtnTakePhoto = (TextView) mRootView.findViewById(R.id.btn_take_photo);
		mBtnCancel = (TextView) mRootView.findViewById(R.id.btn_cancel);
		mBtnTick = mRootView.findViewById(R.id.btn_tick);
		mBtnRecordVideo = (TextView) mRootView.findViewById(R.id.btn_record_video);
		mBtnRecordVideo.setOnClickListener(mClickListener);

		if(getArguments()!=null && getArguments().getBoolean(AppConstants.RECORD_VIDEO, false)==false){
			recordVideo= false;
			mBtnRecordVideo.setVisibility(View.GONE);
		}


		mBtnCancel.setOnClickListener(mClickListener);
		mBtnPhotoLibrary.setOnClickListener(mClickListener);
		mBtnTakePhoto.setOnClickListener(mClickListener);
		mBtnUseImage.setOnClickListener(mClickListener);
		mPreviewImage.setOnClickListener(mClickListener);
		mBtnTick.setOnClickListener(mClickListener);
		return mRootView;

	};

	OnClickListener mClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {

			case R.id.btn_photo_library:
				isVideo = false;
				openFileChooser();
				break;

			case R.id.btn_take_photo:
				openTakePhoto();
				break;

			case R.id.btn_record_video:
				isVideo = true;
				openRecordVideo();
				break;

			case R.id.btn_cancel:
				isVideo = false;
				String senderAction = "";
				if (getArguments() != null
						&& getArguments().containsKey(AppConstants.ACTION_TYPE)) {
					senderAction = getArguments().getString(
							AppConstants.ACTION_TYPE);
				}
				EventBus.getDefault().post(
						new ApplicationEvent(
								AppConstants.IMAGE_SELECTION_CANCELED, null,
								senderAction));
				break;

			// Upload image
			case R.id.btn_use_image:
				if (mSelectedImageURI != null) {
					ImageSelected();
				}
				break;

			case R.id.preview_img:
			case R.id.btn_tick:
				if (mBtnTick.getVisibility() == View.VISIBLE) {
					mBtnTick.setVisibility(View.GONE);
					mPreviewImage.setAlpha(Float.parseFloat(".5"));
					mBtnUseImage.setVisibility(View.GONE);
					mBtnContainer.setVisibility(View.VISIBLE);
				} else {
					mBtnTick.setVisibility(View.VISIBLE);
					mPreviewImage.setAlpha(Float.parseFloat("1"));
					mBtnUseImage.setVisibility(View.VISIBLE);
					mBtnContainer.setVisibility(View.GONE);
				}
				break;

			}
		}
	};

	private void ImageSelected() {
		if (mSelectedImageURI == null) {
			return;
		}

		String senderAction = "";
		if (getArguments() != null
				&& getArguments().containsKey(AppConstants.ACTION_TYPE)) {
			senderAction = getArguments().getString(AppConstants.ACTION_TYPE);
		}

		Intent intent=new Intent();
		if(recordVideo==true) {
			intent.putExtra(AppConstants.VIDEO_URL, videoUrl);
		}
		intent.putExtra(AppConstants.IMAGE_URI, mSelectedImageURI);

		EventBus.getDefault().post(
				new ApplicationEvent(AppConstants.IMAGE_SELECTED_FOR_UPLOAD,
						intent, senderAction));
	}

	public void openFileChooser() {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.addCategory(Intent.CATEGORY_OPENABLE);
		intent.setType("image/*");
		startActivityForResult(intent, FILECHOOSER_REQUESTCODE);
	}


	protected void openRecordVideo(){
		//Toast.makeText(mContext, "Clicked", Toast.LENGTH_LONG).show();
		Intent intent=new Intent(getActivity(), VideoRecordingActivity.class);
		startActivityForResult(intent, RECORD_VIDEO_REQUESTCODE);
	}

	public void openTakePhoto() {
		try {
			// Create AndroidExampleFolder at sdcard
			File imageStorageDir = new File(
					Environment
							.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
					"RighTipsFolder");

			if (!imageStorageDir.exists()) {
				// Create AndroidExampleFolder at sdcard
				imageStorageDir.mkdirs();
			}

			// Create camera captured image file path and name
			File file = new File(imageStorageDir + File.separator + "IMG_"
					+ String.valueOf(System.currentTimeMillis()) + ".jpg");

			mSelectedImageURI = Uri.fromFile(file);

			// Camera capture image intent
			Intent captureIntent = new Intent(
					MediaStore.ACTION_IMAGE_CAPTURE);
			captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mSelectedImageURI);
			startActivityForResult(captureIntent, TAKE_PHOTO_REQUESTCODE);

		} catch (Exception e) {
			// Toast.makeText(getBaseContext(), "Exception:" +
			// e,Toast.LENGTH_LONG).show();
			mApp.showErrorToast(mContext, ERROR_TAG + " " + e);
		}
	}

	// openFileChooser for Android 3.0+
	public void openFileChooserOld() {

		// Update message
		// mUploadMessage = "Uploading....";

		try {
			// Create AndroidExampleFolder at sdcard
			File imageStorageDir = new File(
					Environment
							.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
					"RighTipsFolder");

			if (!imageStorageDir.exists()) {
				// Create AndroidExampleFolder at sdcard
				imageStorageDir.mkdirs();
			}

			// Create camera captured image file path and name
			File file = new File(imageStorageDir + File.separator + "IMG_"
					+ String.valueOf(System.currentTimeMillis()) + ".jpg");

			mSelectedImageURI = Uri.fromFile(file);

			// Camera capture image intent
			final Intent captureIntent = new Intent(
					MediaStore.ACTION_IMAGE_CAPTURE);

			captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mSelectedImageURI);

			Intent i = new Intent(Intent.ACTION_GET_CONTENT);
			i.addCategory(Intent.CATEGORY_OPENABLE);
			i.setType("image/*");

			// Create file chooser intent
			Intent chooserIntent = Intent.createChooser(i, "Image Chooser");

			// Set camera intent to file chooser
			chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
					new Parcelable[] { captureIntent });

			// On select image call onActivityResult method of activity
			// startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);

			Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
			intent.addCategory(Intent.CATEGORY_OPENABLE);
			intent.setType("image/*");
			startActivityForResult(intent, 2005);

		} catch (Exception e) {
			// Toast.makeText(getBaseContext(), "Exception:" +
			// e,Toast.LENGTH_LONG).show();
			mApp.showErrorToast(mContext, ERROR_TAG + " " + e);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, intent);

		if (resultCode != Activity.RESULT_OK) {
			return;
		}

		if (requestCode == FILECHOOSER_REQUESTCODE) {
			Uri result = null;
			try {
				result = intent.getData();
				mSelectedImageURI = result;

				straightBitmap=BitmapUtilities.handleSamplingAndRotationBitmap(mContext, mSelectedImageURI);
				/*Bitmap bmp = BitmapUtilities.getThumbnail(mSelectedImageURI,
						400, mContext);*/
				mPreviewImage.setImageBitmap(straightBitmap);
				mPreviewImage.setAlpha(Float.parseFloat("1"));

/*
				BitmapUtilities.handleSamplingAndRotationBitmap(mContext, mSelectedImageURI);
				mPreviewImage.setImageURI(mSelectedImageURI);
				mPreviewImage.setAlpha(Float.parseFloat("1"));
*/
				updateUI();
			} catch (Exception e) {
				// Toast.makeText(getApplicationContext(), "activity :" +
				// e,Toast.LENGTH_LONG).show();
				mApp.showErrorToast(mContext, ERROR_TAG + " " + e);
			}
		}

		else if (requestCode == TAKE_PHOTO_REQUESTCODE) {
			if (mSelectedImageURI == null) {
				return;
			}
			try {

				straightBitmap=BitmapUtilities.handleSamplingAndRotationBitmap(mContext, mSelectedImageURI);
				/*Bitmap bmp = BitmapUtilities.getThumbnail(mSelectedImageURI,
						400, mContext);*/
				mPreviewImage.setImageBitmap(straightBitmap);
				mPreviewImage.setAlpha(Float.parseFloat("1"));
				updateUI();
			} catch (IOException e) {
			}
		}


		else if (requestCode == RECORD_VIDEO_REQUESTCODE) {
			//Check if video uploaded url is sent back
			if(intent.hasExtra("uploadedUrl")){
				videoUrl = intent.getStringExtra("uploadedUrl");
			}

			//Check if video local path is sent back
			if(intent.hasExtra("path")) {
				String path=intent.getStringExtra("path");
				Bitmap thumb = ThumbnailUtils.createVideoThumbnail(path,MediaStore.Images.Thumbnails.MINI_KIND);

				String directory=path.substring(0, path.lastIndexOf("/")+1);
				String fileName=path.substring(path.lastIndexOf("/")+1, path.lastIndexOf("."));

				OutputStream fOut = null;

				try{
					File file = new File(directory, fileName+".jpg"); // the File to save to
					fOut = new FileOutputStream(file);
					thumb.compress(Bitmap.CompressFormat.JPEG, 85, fOut);// saving the Bitmap to a file compressed as a JPEG with 85% compression rate
					fOut.flush();
					fOut.close(); // do not forget to close the stream
					MediaStore.Images.Media.insertImage(mContext.getContentResolver(),file.getAbsolutePath(),file.getName(),file.getName());
					mSelectedImageURI=Uri.fromFile(file);

/*
					BitmapUtilities.handleSamplingAndRotationBitmap(mContext, mSelectedImageURI);
					mPreviewImage.setImageBitmap(thumb);
					mPreviewImage.setAlpha(Float.parseFloat("1"));
*/

					straightBitmap=BitmapUtilities.handleSamplingAndRotationBitmap(mContext, mSelectedImageURI);
					mPreviewImage.setImageBitmap(straightBitmap);
					mPreviewImage.setAlpha(Float.parseFloat("1"));

					updateUI();
				}
				catch (FileNotFoundException e){}
				catch (IOException e){}
			}
		}

	}

	private void updateUI() {
		// mBtnContainer.setVisibility(View.INVISIBLE);
		mBtnUseImage.setVisibility(View.VISIBLE);
		mPreviewContainer.setVisibility(View.VISIBLE);
		mBtnRecordVideo.setVisibility(View.GONE);
		mBtnContainer.setVisibility(View.GONE);
		mBtnTick.setVisibility(View.VISIBLE);
		// Picasso.with(mContext).load(mSelectedImageURI).fit().centerInside().into(mPreviewImage);

		saveStraightBitmapToFile();

	}

	private void saveStraightBitmapToFile(){
		String path=mSelectedImageURI.getPath();

		String directory=path.substring(0, path.lastIndexOf("/")+1);
		String fileName="";

		if(path.lastIndexOf(".")>-1) {
			fileName = path.substring(path.lastIndexOf("/") + 1, path.lastIndexOf("."));
		}
		else{
			fileName=path.substring(path.lastIndexOf("/")+1, path.length());
		}


		OutputStream fOut = null;

		try {
			File file = new File(directory, fileName + ".jpg"); // the File to save to
			fOut = new FileOutputStream(file);
			straightBitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut);// saving the Bitmap to a file compressed as a JPEG with 85% compression rate
			fOut.flush();
			fOut.close(); // do not forget to close the stream
			MediaStore.Images.Media.insertImage(mContext.getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
			mSelectedImageURI = Uri.fromFile(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}



}
