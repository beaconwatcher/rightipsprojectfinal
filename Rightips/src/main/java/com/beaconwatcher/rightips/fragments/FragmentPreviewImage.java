package com.beaconwatcher.rightips.fragments;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.squareup.picasso.Picasso;

public class FragmentPreviewImage extends AbstractFragment {
	// Views
	private View mRootView;
	private ImageView mImage;
	private CustomProgressDialog pDialog;

	private String mUrl;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_preview_image,
				container, false);
		mLoader = mRootView.findViewById(R.id.loader);
		mImage = (ImageView) mRootView.findViewById(R.id.img);

		mRootView.findViewById(R.id.btn_download).setOnClickListener(
				mClickListener);
		mRootView.findViewById(R.id.btn_close).setOnClickListener(
				mClickListener);

		if (getArguments() != null) {
			setImage(getArguments());
		}
		return mRootView;
	}

	OnClickListener mClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.btn_download:
				downloadImage();
				break;

			case R.id.btn_close:
				getActivity().onBackPressed();
				break;
			}

		}
	};

	private void setImage(Bundle b) {
		mUrl = b.getString(AppConstants.IMAGE_URI);
		Picasso.with(getActivity()).load(mUrl).into(mImage);
		// mImage.setImageURI(Uri.parse(mUrl));
	}

	// Progress Dialog
	// Progress dialog type (0 - for Horizontal progress bar)
	public static final int progress_bar_type = 0;

	private void downloadImage() {
		new DownloadFileFromURL().execute(mUrl);
	}

	// Create this class only to hide progress text from right side of progress
	// dialog.
	private class CustomProgressDialog extends ProgressDialog {

		private int progressPercentVisibility = View.VISIBLE;
		private int progressNumberVisibility = View.VISIBLE;

		public CustomProgressDialog(Context context,
				int progressPercentVisibility, int progressNumberVisibility) {
			super(context);

			this.progressPercentVisibility = progressPercentVisibility;
			this.progressNumberVisibility = progressNumberVisibility;
		}

		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);

			setFieldVisibility("mProgressPercent", progressPercentVisibility);
			setFieldVisibility("mProgressNumber", progressNumberVisibility);
		}

		private void setFieldVisibility(String fieldName, int visibility) {
			try {
				Method method = TextView.class.getMethod("setVisibility",
						Integer.TYPE);

				Field[] fields = this.getClass().getSuperclass()
						.getDeclaredFields();

				for (Field field : fields) {
					if (field.getName().equalsIgnoreCase(fieldName)) {
						field.setAccessible(true);
						TextView textView = (TextView) field.get(this);
						method.invoke(textView, visibility);
					}
				}
			} catch (Exception e) {
			}
		}
	}

	private void showDialog(int type) {
		pDialog = new CustomProgressDialog(getActivity(), View.VISIBLE,
				View.INVISIBLE);
		pDialog.setMessage(mContext.getResources().getString(
				R.string.wall_comment_title_download));
		pDialog.setIndeterminate(false);
		pDialog.setMax(100);
		pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		pDialog.setCancelable(true);
		pDialog.show();
	}

	/**
	 * Background Async Task to download file
	 * */
	class DownloadFileFromURL extends AsyncTask<String, String, String> {

		String fileName;

		/**
		 * Before starting background thread Show Progress Bar Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showDialog(progress_bar_type);
		}

		/**
		 * Downloading file in background thread
		 * */
		@Override
		protected String doInBackground(String... f_url) {
			int count;
			try {
				String urlStr = f_url[0];
				int index = urlStr.lastIndexOf('/');
				fileName = urlStr.substring(index + 1);

				URL url = new URL(urlStr);
				URLConnection conection = url.openConnection();
				conection.connect();
				// getting file length
				int lenghtOfFile = conection.getContentLength();

				// input stream to read file - with 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(),
						8192);

				// Output stream to write file
				OutputStream output = new FileOutputStream("/sdcard/"
						+ fileName);

				byte data[] = new byte[1024];

				long total = 0;

				while ((count = input.read(data)) != -1) {
					total += count;
					// publishing the progress....
					// After this onProgressUpdate will be called
					publishProgress("" + (int) ((total * 100) / lenghtOfFile));

					// writing data to file
					output.write(data, 0, count);
				}

				// flushing output
				output.flush();

				// closing streams
				output.close();
				input.close();

			} catch (Exception e) {
				Log.e("Error: ", e.getMessage());
			}

			return null;
		}

		/**
		 * Updating progress bar
		 * */
		protected void onProgressUpdate(String... progress) {
			// setting progress percentage
			pDialog.setProgress(Integer.parseInt(progress[0]));
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		@Override
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after the file was downloaded
			pDialog.dismiss();

			// Displaying downloaded image into image view
			// Reading image path from sdcard
			String imagePath = Environment.getExternalStorageDirectory()
					.toString() + "/" + fileName;
			Toast.makeText(mContext, "File downloaded to: " + imagePath,
					Toast.LENGTH_LONG).show();
		}
	}

}
