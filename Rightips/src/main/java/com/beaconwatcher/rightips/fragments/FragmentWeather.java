package com.beaconwatcher.rightips.fragments;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.bw.libraryproject.entities.LocationData;
import com.survivingwithandroid.weather.lib.WeatherClient;
import com.survivingwithandroid.weather.lib.WeatherClient.ForecastWeatherEventListener;
import com.survivingwithandroid.weather.lib.WeatherClient.WeatherEventListener;
import com.survivingwithandroid.weather.lib.exception.WeatherLibException;
import com.survivingwithandroid.weather.lib.model.CurrentWeather;
import com.survivingwithandroid.weather.lib.model.DayForecast;
import com.survivingwithandroid.weather.lib.model.WeatherForecast;
import com.survivingwithandroid.weather.lib.request.WeatherRequest;

public class FragmentWeather extends AbstractFragment{

	private static final String TAG = "FragmentWeather";
	private RighTipsApplication mApp;
	private Context mContext;

	private View mRootView;
	private TextView mTxtTemperature;
	private LinearLayout mForecastBox;
	private LayoutInflater mInflater;
	private LocationData mLocationData;
	
	private Map iconCodes = new HashMap();

	@Override
	public void onStart() {
		super.onStart();
		initIconCodes();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mApp = (RighTipsApplication) getActivity().getApplicationContext();
		mContext = getActivity();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mInflater = inflater;
		mRootView = inflater.inflate(R.layout.fragement_weather, container, false);
		mTxtTemperature = (TextView) mRootView.findViewById(R.id.txt_temperature);
		mForecastBox = (LinearLayout) mRootView.findViewById(R.id.forecast_box);
		return mRootView;
	}

	public void setLocation(LocationData ld){
		updateCurrentWeather(ld);
		updateForecast(ld);

	}
	
	/***********************************************************
	 * WEATHER MANIPULATION CODE
	 **********************************************************/
	Typeface weatherFont;

	private void updateForecast(LocationData ld) {
		if (ld == null) {
			return;
		}
		WeatherClient client = mApp.getWeatherClient();
		if (client == null) {
			return;
		}

		if (weatherFont == null) {
			weatherFont = Typeface.createFromAsset(getActivity().getAssets(),
					"fonts/owfont-regular.ttf");
		}

		WeatherRequest req = new WeatherRequest(ld.getLongitude(),
				ld.getLatitude());

		client.getForecastWeather(req, new ForecastWeatherEventListener() {
			@Override
			public void onWeatherError(WeatherLibException wle) {
				// TODO Auto-generated method stub
				// mApp.showErrorToast(mContext, "Weather API");
				// Toast.makeText(mContext,
				// "Forecast Error : "+wle.getMessage(),
				// Toast.LENGTH_LONG).show();
			}

			@Override
			public void onConnectionError(Throwable t) {
				// TODO Auto-generated method stub
				// mApp.showErrorToast(mContext, "Weather API Connection");
				// Toast.makeText(mContext, "Forecast Error : "+t.getMessage(),
				// Toast.LENGTH_LONG).show();
			}

			@Override
			public void onWeatherRetrieved(WeatherForecast forecast) {
				// TODO Auto-generated method stub
				DayForecast fc;
				List dayForecastList = forecast.getForecast();
				for (Object dForecast : dayForecastList) {
					// TypeConversion from Object into DayForecast;
					fc = (DayForecast) dForecast;
					addForecastItem(fc);
				}
			}
		});
	}

	private void addForecastItem(DayForecast fc) {
		Long timestamp = fc.timestamp;
		String date = getDateStringFromMilliseconds(timestamp);
		String dayName = date.split(" ")[0];
		String temperature = fc.forecastTemp.min + " - " + fc.forecastTemp.max;
		String detail = fc.weather.currentCondition.getDescr();

		int weatherID = fc.weather.currentCondition.getWeatherId();

		View item = mInflater.inflate(R.layout.weather_forecast_item,
				(ViewGroup)mRootView, false);
		LayoutParams params = new LinearLayout.LayoutParams(0,
				LayoutParams.WRAP_CONTENT, (float) 0.14);
		item.setLayoutParams(params);
		mForecastBox.addView(item);

		TextView tv1 = (TextView) item.findViewById(R.id.weather_day);
		tv1.setText(dayName);

		TextView tv3 = (TextView) item.findViewById(R.id.weather_icon);
		tv3.setTypeface(weatherFont);
		setWeatherIcon(weatherID, tv3);
	}

	private void initIconCodes() {
		// THUNDER STORM
		iconCodes.put("200", "\uEB28");
		iconCodes.put("201", "\uEB29");
		iconCodes.put("202", "\uEB2A");
		iconCodes.put("210", "\uEB32");
		iconCodes.put("211", "\uEB33");
		iconCodes.put("212", "\uEB34");
		iconCodes.put("221", "\uEB3D");
		iconCodes.put("230", "\uEB46");
		iconCodes.put("231", "\uEB47");
		iconCodes.put("232", "\uEB48");

		// DRIZZLE
		iconCodes.put("300", "\uEB8C");
		iconCodes.put("301", "\uEB8D");
		iconCodes.put("302", "\uEB8E");
		iconCodes.put("310", "\uEB96");
		iconCodes.put("311", "\uEB97");
		iconCodes.put("312", "\uEB98");
		iconCodes.put("313", "\uEB99");
		iconCodes.put("314", "\uEB9A");
		iconCodes.put("321", "\uEBA1");

		// RAIN
		iconCodes.put("500", "\uEC54");
		iconCodes.put("501", "\uEC55");
		iconCodes.put("502", "\uEC56");
		iconCodes.put("503", "\uEC57");
		iconCodes.put("504", "\uEC58");
		iconCodes.put("511", "\uEC5F");
		iconCodes.put("520", "\uEC68");
		iconCodes.put("521", "\uEC69");
		iconCodes.put("522", "\uEC6A");
		iconCodes.put("531", "\uEC73");

		// SNOW
		iconCodes.put("600", "\uECB8");
		iconCodes.put("601", "\uECB9");
		iconCodes.put("602", "\uECBA");
		iconCodes.put("611", "\uECC3");
		iconCodes.put("612", "\uECC4");
		iconCodes.put("615", "\uECC7");
		iconCodes.put("616", "\uECC8");
		iconCodes.put("620", "\uECCC");
		iconCodes.put("621", "\uECCD");
		iconCodes.put("622", "\uECCE");

		// ATMOSPHERE
		iconCodes.put("701", "\uED1D");
		iconCodes.put("711", "\uED27");
		iconCodes.put("721", "\uED31");
		iconCodes.put("731", "\uED3B");
		// FOG
		iconCodes.put("741", "\uED45");
		iconCodes.put("751", "\uED4F");
		iconCodes.put("761", "\uED59");
		iconCodes.put("762", "\uED5A");
		iconCodes.put("771", "\uED63");
		iconCodes.put("781", "\uED6D");

		// CLOUDS
		iconCodes.put("800", "\uED80");
		iconCodes.put("801", "\uED81");
		iconCodes.put("802", "\uED82");
		iconCodes.put("803", "\uED83");
		iconCodes.put("804", "\uED84");
		iconCodes.put("951", "\uED80");

		// EXTREME
		iconCodes.put("900", "\uEDE4");
		iconCodes.put("901", "\uEDE5");
		iconCodes.put("902", "\uEDE6");
		iconCodes.put("903", "\uEDE7");
		iconCodes.put("904", "\uEDE8");
		iconCodes.put("905", "\uEDE9");
		iconCodes.put("906", "\uEDEA");

		// ADDITIONAL
		iconCodes.put("950", "\uEE16");
		iconCodes.put("952", "\uEE18");
		iconCodes.put("953", "\uEE19");
		iconCodes.put("954", "\uEE1A");
		iconCodes.put("955", "\uEE1B");
		iconCodes.put("956", "\uEE1C");
		iconCodes.put("957", "\uEE1D");
		iconCodes.put("958", "\uEE1E");
		iconCodes.put("959", "\uEE1F");
		iconCodes.put("960", "\uEE20");
		iconCodes.put("961", "\uEE21");
		iconCodes.put("962", "\uEE22");
	}

	private void setWeatherIcon(int actualId, TextView tv) {
		String str = (String) iconCodes.get(Integer.toString(actualId));
		if (str != null) {
			byte[] b = str.getBytes();
			try {
				String strUTF = new String(b, "UTF-8");
				tv.setText(strUTF);
			} catch (Exception e) {
			}
		}
	}

	private void updateCurrentWeather(LocationData ld) {
		if (ld == null) {
			return;
		}
		WeatherClient client = mApp.getWeatherClient();
		if (client == null) {
			return;
		}

		if (weatherFont == null) {
			weatherFont = Typeface.createFromAsset(getActivity().getAssets(),
					"fonts/owfont-regular.ttf");
		}

		WeatherRequest req = new WeatherRequest(ld.getLongitude(),
				ld.getLatitude());

		client.getCurrentCondition(req, new WeatherEventListener() {
			@Override
			public void onWeatherError(WeatherLibException wle) {
				// TODO Auto-generated method stub
				// mApp.showErrorToast(mContext, "Weather API");
				//Toast.makeText(mContext,"Weather Lib Exception: " + wle.getMessage(),	Toast.LENGTH_LONG).show();
			}

			@Override
			public void onConnectionError(Throwable t) {
				// TODO Auto-generated method stub
				// mApp.showErrorToast(mContext, "Weather API");
				//Toast.makeText(mContext,"Weather Connection Error: " + t.getMessage(),	Toast.LENGTH_LONG).show();
			}

			@Override
			public void onWeatherRetrieved(CurrentWeather weather) {
				// TODO Auto-generated method stub
				float temp = weather.weather.temperature.getTemp();
				int t = (int) Math.round(temp);
				mTxtTemperature.setText(Integer.toString(t) + "\u00B0C");
			}
		});
	}
	
	
	private String getDateStringFromMilliseconds(Long ms) {
		/*
		 * Calendar calendar = Calendar.getInstance(); TimeZone tz =
		 * TimeZone.getDefault(); calendar.add(Calendar.MILLISECOND,
		 * tz.getOffset(calendar.getTimeInMillis()));
		 */

		// Convert and return on day name
		Date date = new Date(ms * 1000);
		CharSequence cs = android.text.format.DateFormat.format("EEEE", date);
		StringBuilder sb = new StringBuilder(cs.length());
		sb.append(cs);
		String dayName = sb.toString();

		String dayNameShort = dayName.substring(0, 3).toUpperCase();
		return dayNameShort;

		// Uncomment following if you want o return format like 15-8-2014
		// 12:00:00
		/*
		 * SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		 * String str=sdf.format(date); return str;
		 */
	}
}
