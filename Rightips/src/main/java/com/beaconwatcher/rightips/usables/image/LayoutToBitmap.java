package com.beaconwatcher.rightips.usables.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.widget.FrameLayout;

public class LayoutToBitmap {

	public static Bitmap createSlideBitmap(Context context, Integer i, int ww,
			int hh) {
		LayoutInflater inflater = LayoutInflater.from(context);
		View slide = inflater.inflate(i, null);
		slide.setLayoutParams(new FrameLayout.LayoutParams(ww, hh));

		slide.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.EXACTLY),
				MeasureSpec.makeMeasureSpec(0, MeasureSpec.EXACTLY));

		slide.layout(0, 0, ww, hh);// ,cluster.getMeasuredHeight());*/

		// TextView clusterSizeText = (TextView)
		// slide.findViewById(R.id.notification_title);
		// clusterSizeText.setText("My Notification");

		final Bitmap slideBitmap = Bitmap.createBitmap(
				slide.getLayoutParams().width, slide.getLayoutParams().height,
				Bitmap.Config.ARGB_8888);

		Canvas canvas = new Canvas(slideBitmap);
		slide.draw(canvas);

		return slideBitmap;
	}
}
