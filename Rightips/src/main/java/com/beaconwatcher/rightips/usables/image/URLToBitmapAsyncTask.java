package com.beaconwatcher.rightips.usables.image;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

public class URLToBitmapAsyncTask extends AsyncTask<String, Void, Bitmap> {

	public interface AsyncBitmapResponse {
		void onBitmapDownloaded(Bitmap bmp);
	}

	private AsyncBitmapResponse responseHandler;

	public URLToBitmapAsyncTask(AsyncBitmapResponse response) {
		responseHandler = response;
	}

	@Override
	protected Bitmap doInBackground(String... params) {
		return DownloadImage(params[0]);
	}

	@Override
	protected void onPostExecute(Bitmap bitmap) {
		if (isCancelled()) {
			bitmap = null;
		}

		if (responseHandler != null) {
			responseHandler.onBitmapDownloaded(bitmap);
		}
	}

	public static Bitmap DownloadImage(String URL) {
		// System.out.println("image inside="+URL);
		Bitmap bitmap = null;
		InputStream in = null;
		try {
			in = OpenHttpConnection(URL);
			bitmap = BitmapFactory.decodeStream(in);
			in.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// System.out.println("image last");
		return bitmap;
	}

	private static InputStream OpenHttpConnection(String urlString)
			throws IOException {
		InputStream in = null;
		int response = -1;

		URL url = new URL(urlString);
		URLConnection conn = url.openConnection();

		if (!(conn instanceof HttpURLConnection))
			throw new IOException("Not an HTTP connection");

		try {
			HttpURLConnection httpConn = (HttpURLConnection) conn;
			httpConn.setAllowUserInteraction(false);
			httpConn.setInstanceFollowRedirects(true);
			httpConn.setRequestMethod("GET");
			httpConn.connect();

			response = httpConn.getResponseCode();
			if (response == HttpURLConnection.HTTP_OK) {
				in = httpConn.getInputStream();
			}
		} catch (Exception ex) {
			throw new IOException("Error connecting");
		}
		return in;
	}
}
