package com.beaconwatcher.rightips.tasks;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;

import com.makeramen.segmented.TabBarRadioButton;

public class GroupIconLoadTask extends AsyncTask<Void, Void, Bitmap> {

	private String url;
	private TabBarRadioButton tab;
	private Resources res;

	public GroupIconLoadTask(String url, TabBarRadioButton t, Resources r) {
		this.url = url;
		this.tab = t;
		this.res = r;
	}

	@Override
	protected Bitmap doInBackground(Void... params) {
		try {
			URL urlConnection = new URL(url);
			HttpURLConnection connection = (HttpURLConnection) urlConnection
					.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			Bitmap myBitmap = BitmapFactory.decodeStream(input);
			return myBitmap;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(Bitmap bmp) {
		super.onPostExecute(bmp);

		Bitmap b = getScaledBitmap(bmp, 200, 50);

		Drawable d = new BitmapDrawable(res, b);
		tab.setDrawable(d);

	}

	public Bitmap getScaledBitmap(Bitmap b, int reqWidth, int reqHeight) {
		int bWidth = b.getWidth();
		int bHeight = b.getHeight();

		int nWidth = reqWidth;
		int nHeight = reqHeight;

		float parentRatio = (float) reqHeight / reqWidth;

		nHeight = bHeight;
		nWidth = (int) (reqWidth * parentRatio);

		return Bitmap.createScaledBitmap(b, nWidth, nHeight, true);
	}

}