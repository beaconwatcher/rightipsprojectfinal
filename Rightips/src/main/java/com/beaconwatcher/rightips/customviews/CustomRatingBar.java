package com.beaconwatcher.rightips.customviews;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.RatingBar;

import com.beaconwatcher.rightips.R;

public class CustomRatingBar extends RatingBar {
	float bitmapWidth;

	public CustomRatingBar(Context c) {
		// TODO Auto-generated constructor stub
		super(c);
	}

	public CustomRatingBar(Context c, AttributeSet atr) {
		super(c, atr);
	}

	public CustomRatingBar(Context c, AttributeSet atr, int defStyle) {
		super(c, atr, defStyle);
	}

	@Override
	protected synchronized void onDraw(Canvas canvas) {
		int stars = getNumStars();
		float rating = getRating();

		try {
			bitmapWidth = getWidth() / stars;
		} catch (Exception e) {
			bitmapWidth = getWidth();
		}
		float x = 0;

		for (int i = 0; i < stars; i++) {
			Bitmap bitmap;
			Resources res = getResources();
			Paint paint = new Paint();

			if ((int) rating > i) {
				bitmap = BitmapFactory.decodeResource(res,
						R.drawable.rating_star_selected);
			} else {
				bitmap = BitmapFactory.decodeResource(res,
						R.drawable.rating_star_normal);
			}
			Bitmap scaled = Bitmap.createScaledBitmap(bitmap, getHeight(),
					getHeight(), true);
			canvas.drawBitmap(scaled, x, 0, paint);
			canvas.save();
			x += bitmapWidth;
		}

		super.onDraw(canvas);
	}

}
