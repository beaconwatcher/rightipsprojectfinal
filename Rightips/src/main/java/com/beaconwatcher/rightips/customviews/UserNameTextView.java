package com.beaconwatcher.rightips.customviews;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.activities.UserProfileActivity;
import com.beaconwatcher.rightips.entities.AppConstants;

public class UserNameTextView extends TextView {

	private String mUserID = "0";
	private Context mContext;

	public void setUserID(String id) {
		mUserID = id;
	}

	public UserNameTextView(final Context context) {
		super(context);
		setup(context);
	}

	public UserNameTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context);
	}

	public UserNameTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setup(context);
	}

	private void setup(final Context context) {
		mContext = context;
		setClickable(true);
		setBackgroundResource(R.drawable.btn_transparent);
		setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				openUserProfile();
			}
		});

	}

	private void openUserProfile() {
		Intent intent = new Intent(mContext, UserProfileActivity.class);
		intent.putExtra(AppConstants.USER_ID, mUserID);
		mContext.startActivity(intent);
	}
}