/*
 * Copyright (C) 2015 RighTips Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.beaconwatcher.rightips.services;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.beaconwatcher.rightips.entities.AppConstants;
import com.bw.libraryproject.entities.SiteData;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

/**
 * This service class get all venues of selected category group used on Google
 * map to render different markers and filter them. As it runs, it loads all
 * venues and broadcasts its result using LocalBroadcastManager; any component
 * that wants to see the results should implement a subclass of
 * BroadcastReceiver and register to receive broadcast Intents with category =
 * AppConstants.CATEGORY_GROUP_VENUES_ACTION
 * 
 */
public class LoadNearbySitesService extends IntentService {
	private static final String TAG = "LoadNearbySitesService";
	// Defines a custom Intent action

	private Context mContext;
	// private int radius=5;
	private String appKey = "MTRfX1JpZ2h0VGlwcw==";

	public LoadNearbySitesService() {
		// TODO Auto-generated constructor stub
		super("LoadNearbySitesService");
	}

	@Override
	protected void onHandleIntent(Intent workIntent) {
		// Gets data from the incoming Intent
		Log.d(TAG, "Sending request to load all venues of group");
		mContext = this;

		String grpID = workIntent.getStringExtra("grpID");
		String lat = workIntent.getStringExtra("latitude");
		String lng = workIntent.getStringExtra("longitude");

		String url = "http://www.beaconwatcher.com/api/index.php?action=getLocSites&key="
				+ appKey
				+ "&catGroup_id="
				+ grpID
				+ "&lat="
				+ lat
				+ "&lng="
				+ lng;
		// http://www.rightips.com/api/index.php?action=likePost&msg_id=80&uid=13&rel=Like
		AsyncHttpClient httpClient = new AsyncHttpClient();
		try {
			httpClient.get(url, responseHandler);
		}

		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	AsyncHttpResponseHandler responseHandler = new AsyncHttpResponseHandler() {
		@Override
		public void onStart() {
			Log.v(TAG, "onStart");
		}

		@Override
		public void onSuccess(String response) {
			Log.v(TAG, "onSuccess");

			// {"status":1,"message":"Beacon details","data":[{"msb_id":"41","msb_name":"Testing Beacons","msb_location":"","msb_uuid":"5144A35F387A7AC0B254DFB1381C9DFF","msb_minor":"1","msb_major":"1","msb_txpower":"0","msb_mac_address":"test","site_id":"20","site_title":"ESOL OFFICE","company":"Right Tips","notifications":[{"nt_id":"21","nt_name":"Pizza Hut Special Deal","nt_title":"Fun 4 All","nt_details":"Big Time Treat Inside Box, 2 hot spicy pizzaz, 8 Garlic breads, 500 ml Cold Drink.","zone":"1","template":"http:\/\/beaconwatcher.com\/api\/template.php?nt_id=21"}]}]}
			// Log.d(TAG, "All Notifications Loaded: "+response);
			JSONObject jo = null;
			try {
				jo = new JSONObject(response);
				if (jo.has("status")) {

					if (jo.getInt("status") == 1) {
						venuesLoaded(jo);
					} else {
						String msg = "Error getting all venues";
						if (jo.has("message")) {
							msg = jo.getString("message");
						}

						broadcastMessage(null, msg);
					}
				}
			} catch (JSONException e) {
				broadcastMessage(null, "Error parsing venues json");
				Log.e("log_tag", "Error parsing data " + e.toString());
			}
		}

		@Override
		public void onFailure(Throwable error, String content) {
			broadcastMessage(null, "Failed to load nearby venues");
			Log.e(TAG, "onFailure error : " + error.toString() + "content : "
					+ content);
		}

		@Override
		public void onFinish() {
			Log.v(TAG, "onFinish");
		}
	};

	private void venuesLoaded(JSONObject jo) {
		ArrayList<SiteData> sites = new ArrayList<SiteData>();

		try {
			JSONArray arr = jo.getJSONArray("data");

			for (int i = 0; i < arr.length(); i++) {
				JSONObject obj = arr.getJSONObject(i);
				String id = "";
				String catID = "0";
				String name = "";
				String address = "";
				String lat = "0";
				String lng = "0";
				String markerUrl = "";
				Integer totalNotes = 0;

				if (obj.has("site_id")) {
					id = obj.getString("site_id");
				}
				if (obj.has("cat_id")) {
					catID = obj.getString("cat_id");
				}
				if (obj.has("site_name")) {
					name = obj.getString("site_name");
				}
				if (obj.has("site_address")) {
					address = obj.getString("site_address");
				}
				if (obj.has("site_lat")) {
					lat = obj.getString("site_lat");
				}
				if (obj.has("site_long")) {
					lng = obj.getString("site_long");
				}
				if (obj.has("catGroup_marker")) {
					markerUrl = obj.getString("catGroup_marker");
				}
				if (obj.has("site_total_notifications")) {
					totalNotes = obj.getInt("site_total_notifications");
				}

				SiteData site = new SiteData();
				site.setSiteID(id);
				site.setCatID(catID);
				site.setSiteName(name);
				site.setSiteAddress(address);
				site.setLatitude(lat);
				site.setLongitude(lng);
				site.setMarkerUrl(markerUrl);
				site.setSiteTotalNotifications(totalNotes);

				sites.add(site);
			}

		} catch (JSONException e) {
			broadcastMessage(null, "Error parcing JSON in Category Groups");
		}

		broadcastMessage(sites, "");
	}

	private void broadcastMessage(ArrayList<SiteData> arr, String msg) {
		/*
		 * Creates a new Intent containing a Uri object BROADCAST_ACTION is a
		 * custom Intent action
		 */
		Intent localIntent = new Intent(
				AppConstants.CATEGORY_GROUP_VENUES_ACTION);
		localIntent.putParcelableArrayListExtra("sites", arr);
		localIntent.putExtra("message", msg);
		// Broadcasts the Intent to receivers in this app.
		LocalBroadcastManager.getInstance(mContext).sendBroadcast(localIntent);
	}
}