package com.beaconwatcher.rightips.services.concrete;

import java.util.ArrayList;

import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.UserLikes;
import com.beaconwatcher.rightips.services.AbstractIntentService;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

public class ReportListItemService extends AbstractIntentService {

	public ReportListItemService() {
		super("ReportListItemService");
		mResultType = new TypeToken<ArrayList<Object>>() {
		}.getType();
	}

	// Abstract method implemented here, called from super class.
	// when onHandleIntent is called.
	public void createUrlParameters() {
		mBaseUrl = "http://www.rightips.com/api/index.php?action=report";
		mParams = new RequestParams();
		mParams.put("uid", mIntent.getStringExtra(AppConstants.USER_ID));
		mParams.put("id", mIntent.getStringExtra(AppConstants.LIST_ITEM_ID));
		mParams.put("status_id",
				mIntent.getStringExtra(AppConstants.LIST_ITEM_REPORT_ID));
		mParams.put("type", mIntent.getStringExtra(AppConstants.LIST_ITEM_TYPE));
	}
}
