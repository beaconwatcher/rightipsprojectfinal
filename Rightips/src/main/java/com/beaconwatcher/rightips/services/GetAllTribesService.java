/*
 * Copyright (C) 2015 RighTips Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.beaconwatcher.rightips.services;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.CategoryItem;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * This service calls API to get details about a specific notification.
 * 
 */
public class GetAllTribesService extends IntentService {
	private static final String TAG = "GetAllTribesService";

	private Context mContext;
	private RighTipsApplication mApp;
	private String action;
	private String[] selItems = { "" };

	public GetAllTribesService() {
		// TODO Auto-generated constructor stub
		super("GetAllTribesService");
	}

	@Override
	protected void onHandleIntent(Intent workIntent) {
		// Gets data from the incoming Intent
		Log.d(TAG, "Sending API call to get all preferences (tribes)");
		mContext = this;
		mApp = (RighTipsApplication) getApplicationContext();

		action = workIntent
				.getStringExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE);
		if (action.equals(AppConstants.ACTION_GET_ALL_TRIBES)) {
			if (workIntent.hasExtra(AppConstants.SELECTED_TRIBES_IDS)) {
				String str = workIntent
						.getStringExtra(AppConstants.SELECTED_TRIBES_IDS);
				if (str != null && !str.equals("")) {
					selItems = str.split("\\,");
				}
			}

			// Check if preferences already cached on device, if yes, dont make
			// server call.
			String prefTribes = mApp.getPreferences(
					AppConstants.PREF_ALL_TRIBES, "");
			if (!prefTribes.equals("")) {
				try {
					JSONObject jo = new JSONObject(prefTribes);
					parseResults(jo);
				} catch (JSONException e) {
					broadcastError(e.getMessage());
				}
			}

			else {
				if (action.equals(AppConstants.ACTION_GET_ALL_TRIBES)) {
					mApp.APICall(
							"http://api.beaconwatcher.com/index.php?action=getCategories",
							new RequestParams(), responseHandler);
				}
			}
		}

		else if (action.equals(AppConstants.ACTION_SAVE_USER_TRIBES)) {
			RequestParams params = new RequestParams();
			params.put("uid", workIntent.getStringExtra("uid"));
			params.put("number", workIntent.getStringExtra("number"));
			params.put("deviceID", workIntent.getStringExtra("deviceID"));
			params.put("categories",
					workIntent.getStringExtra(AppConstants.SELECTED_TRIBES_IDS));

			mApp.APICall(
					"http://www.rightips.com/api/index.php?action=savePreferences",
					params, responseHandler);
		}

	}

	AsyncHttpResponseHandler responseHandler = new AsyncHttpResponseHandler() {
		@Override
		public void onStart() {
			Log.v(TAG, "onStart");
		}

		@Override
		public void onSuccess(String response) {
			Log.v(TAG, "onSuccess response: " + response);
			JSONObject jo = null;
			try {
				jo = new JSONObject(response);
				if (jo.has("status")) {
					int status = jo.getInt("status");
					if (status == 1) {
						// get preferences called
						if (jo.has("data")) {
							parseResults(jo);
							// save loaded preferences
							mApp.savePreferences(AppConstants.PREF_ALL_TRIBES,
									response);
						}

						// Most probably save user preferences called.
						else if (jo.has("message")) {
							broadcastMessage(jo.getString("message"));
						}

					} else if (status == 0) {
						if (jo.has("message")) {
							broadcastMessage(jo.getString("message"));
						} else {
							broadcastError(AppConstants.RESULT_FAILURE);
						}
					}
				}
			} catch (JSONException e) {
				broadcastError("JSON Exception in Comments Webservice");
				Log.e("log_tag", "Error parsing data " + e.toString());
			}
		}

		@Override
		public void onFailure(Throwable error, String content) {
			broadcastError("Failed to load data from Comments Webservice");
			Log.e(TAG, "onFailure error : " + error.toString() + "content : "
					+ content);
		}

		@Override
		public void onFinish() {
			Log.v(TAG, "onFinish");
		}
	};

	private void parseResults(JSONObject jo) {
		if (action.equals(AppConstants.ACTION_GET_ALL_TRIBES)) {
			if (jo.has("data")) {
				try {
					JSONArray arr = jo.getJSONArray("data");
					ArrayList<CategoryItem> catArray = new ArrayList<CategoryItem>();
					for (int i = 0; i < arr.length(); i++) {
						JSONObject jItem = arr.getJSONObject(i);
						CategoryItem cItem = parseCategoryItem(jItem);
						if (cItem != null)
							catArray.add(cItem);
					}

					allTribesLoaded(catArray);
				}

				catch (JSONException e) {
					broadcastError("JSON Exception in Comments Webservice");
					Log.e("log_tag", "Error parsing data " + e.toString());
				}
			}
		}

	}

	private CategoryItem parseCategoryItem(JSONObject jitem) {
		CategoryItem item = new CategoryItem();
		try {
			if (jitem.has("cat_id"))
				item.setId(jitem.getString("cat_id"));
			if (jitem.has("cat_name"))
				item.setName(jitem.getString("cat_name"));
			if (jitem.has("cat_icon"))
				item.setUrl(jitem.getString("cat_icon"));

			if (hasArrayItem(item.getId(), selItems)) {
				item.setSelected(1);
			}
		} catch (JSONException e) {
			return null;
		}
		return item;
	}

	private Boolean hasArrayItem(String item, String[] arr) {
		for (String cat : arr) {
			if (cat.equals(item)) {
				return true;
			}
		}
		return false;
	}

	private void allTribesLoaded(ArrayList<CategoryItem> arr) {
		Intent intent = new Intent(AppConstants.ALL_TRIBES_SERVICE_COMPLETED);
		intent.putParcelableArrayListExtra(AppConstants.INTENT_SERVICE_DATA,
				arr);
		intent.putExtra(AppConstants.INTENT_SERVICE_RESPONSE_TYPE,
				AppConstants.RESPONSE_ALL_TRIBES_LOADED);
		broadcastIntent(intent);
	}

	private void broadcastError(String msg) {
		Intent intent = new Intent(AppConstants.ALL_TRIBES_SERVICE_COMPLETED);
		intent.putExtra(AppConstants.INTENT_SERVICE_ERROR_MESSAGE, msg);
		broadcastIntent(intent);
	}

	private void broadcastMessage(String msg) {
		Intent intent = new Intent(AppConstants.ALL_TRIBES_SERVICE_COMPLETED);
		intent.putExtra(AppConstants.INTENT_SERVICE_MESSAGE, msg);
		broadcastIntent(intent);
	}

	private void broadcastIntent(Intent intent) {
		sendBroadcast(intent);
		// LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

	}

}