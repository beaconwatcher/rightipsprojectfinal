/*
 * Copyright (C) 2015 RighTips Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.beaconwatcher.rightips.services;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.beaconwatcher.rightips.entities.AppConstants;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

/**
 * This service class get all venues of selected category group used on Google
 * map to render different markers and filter them. As it runs, it loads all
 * venues and broadcasts its result using LocalBroadcastManager; any component
 * that wants to see the results should implement a subclass of
 * BroadcastReceiver and register to receive broadcast Intents with category =
 * AppConstants.CATEGORY_GROUP_VENUES_ACTION
 * 
 */
public class GetTimeZoneByLocationService extends IntentService {
	private static final String TAG = "GetTimeZoneByLocationService";
	// Defines a custom Intent action

	private Context mContext;
	// Google API Key
	private String apiKey = "AIzaSyDuq5syL_vE28AgE1WNu9cVnfZLupt4V-E";

	public GetTimeZoneByLocationService() {
		// TODO Auto-generated constructor stub
		super("GetTimeZoneByLocationService");
	}

	@Override
	protected void onHandleIntent(Intent workIntent) {
		// Gets data from the incoming Intent
		Log.d(TAG, "Sending request to get TimeZone by location");
		mContext = this;

		String lat = workIntent.getStringExtra("latitude");
		String lon = workIntent.getStringExtra("longitude");

		// long millis = System.currentTimeMillis() % 1000;
		// String timestamp=Long.toString(millis);

		// String
		// url="https://maps.googleapis.com/maps/api/timezone/json?location="+lat+","+lng+"&timestamp="+timestamp+"&key="+apiKey;
		String url = "http://api.geonames.org/timezoneJSON?formatted=true&lat="
				+ lat + "&lng=" + lon + "&username=beaconwatcher2";

		AsyncHttpClient httpClient = new AsyncHttpClient();
		try {
			httpClient.get(url, responseHandler);
		}

		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	AsyncHttpResponseHandler responseHandler = new AsyncHttpResponseHandler() {
		@Override
		public void onStart() {
			Log.v(TAG, "onStart");
		}

		@Override
		public void onSuccess(String response) {
			Log.v(TAG, "onSuccess");
			/*
			 * {"sunrise": "2015-08-21 06:08", "lng": 34.78176759999999,
			 * "countryCode": "IL", "gmtOffset": 2, "rawOffset": 2, "sunset":
			 * "2015-08-21 19:18", "timezoneId": "Asia/Jerusalem", "dstOffset":
			 * 3, "countryName": "Israel", "time": "2015-08-21 13:46", "lat":
			 * 32.0852999}
			 */

			JSONObject jo = null;
			try {
				jo = new JSONObject(response);
				if (jo.has("timezoneId")) {
					int dif = jo.getInt("gmtOffset");
					String timezoneId = jo.getString("timezoneId");
					broadcastMessage(timezoneId);

					/*
					 * TimeZone tz = TimeZone.getTimeZone(timezoneId); Calendar
					 * cal = Calendar.getInstance(); // If needed in hours
					 * rather than milliseconds int LocalOffSethrs = (int)
					 * ((cal.getTimeZone().getRawOffset()) *(2.77777778
					 * /10000000)); int OtherOffSethrs = (int)
					 * ((tz.getRawOffset()) *(2.77777778 /10000000)); int dts =
					 * cal.getTimeZone().getDSTSavings();
					 * System.out.println("Local Time Zone : " +
					 * cal.getTimeZone().getDisplayName());
					 * System.out.println("Local Day Light Time Saving : " +
					 * dts); System.out.println("Other City Time : " +
					 * tz.getRawOffset());
					 * System.out.println("Local Offset Time from GMT: " +
					 * LocalOffSethrs);
					 * System.out.println("Other Offset Offset Time from GMT: "
					 * + OtherOffSethrs); // Adjust to GMT //
					 * cal.add(Calendar.MILLISECOND
					 * ,-(cal.getTimeZone().getRawOffset())); // Adjust to
					 * Daylight Savings cal.add(Calendar.MILLISECOND, -
					 * cal.getTimeZone().getDSTSavings()); // Adjust to Offset
					 * cal.add(Calendar.MILLISECOND, tz.getRawOffset()); Date dt
					 * = new Date(cal.getTimeInMillis()); System.out.println(
					 * "After adjusting offset Acctual Other CIty Time :" + dt);
					 */
				}
			} catch (JSONException e) {
				// broadcastMessage(null, "Error parsing venues json");
				Log.e("log_tag", "Error parsing data " + e.toString());
			}
		}

		@Override
		public void onFailure(Throwable error, String content) {
			// broadcastMessage(null, "Failed to load nearby venues");
			Log.e(TAG, "onFailure error : " + error.toString() + "content : "
					+ content);
		}

		@Override
		public void onFinish() {
			Log.v(TAG, "onFinish");
		}
	};

	private void broadcastMessage(String TimezoneId) {
		/*
		 * Creates a new Intent containing a Uri object BROADCAST_ACTION is a
		 * custom Intent action
		 */
		Intent localIntent = new Intent(AppConstants.ACTION_TIME_ZONE_FOUND);
		localIntent.putExtra(AppConstants.TIME_ZONE_ID, TimezoneId);
		// Broadcasts the Intent to receivers in this app.
		LocalBroadcastManager.getInstance(mContext).sendBroadcast(localIntent);
	}
}