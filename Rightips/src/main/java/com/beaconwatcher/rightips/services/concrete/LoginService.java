package com.beaconwatcher.rightips.services.concrete;

import java.util.ArrayList;

import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.UserProfileData;
import com.beaconwatcher.rightips.services.AbstractIntentService;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

public class LoginService extends AbstractIntentService {

	public LoginService() {
		super("LoginService");
		mResultType = new TypeToken<ArrayList<UserProfileData>>() {
		}.getType();
	}

	// Abstract method implemented here, called from super class.
	// when onHandleIntent is called.
	public void createUrlParameters() {
		mBaseUrl = "http://www.rightips.com/api/index.php?action=login";
		mParams = new RequestParams();
		mParams.put("sm_id",
				Long.toString(mIntent.getLongExtra(AppConstants.USER_SM_ID, 0)));
		mParams.put("fname",
				mIntent.getStringExtra(AppConstants.USER_FIRST_NAME));
		mParams.put("lname",
				mIntent.getStringExtra(AppConstants.USER_LAST_NAME));
		mParams.put("email", mIntent.getStringExtra(AppConstants.USER_EMAIL));
		mParams.put("pass", mIntent.getStringExtra(AppConstants.USER_PASSWORD));
		mParams.put("provider",
				mIntent.getStringExtra(AppConstants.LOGIN_PROVIDER));
		mParams.put("signup", mIntent.getStringExtra(AppConstants.IS_SIGNUP));
	}
}
