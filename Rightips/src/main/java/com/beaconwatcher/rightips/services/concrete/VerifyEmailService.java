package com.beaconwatcher.rightips.services.concrete;

import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.EmailVerificationData;
import com.beaconwatcher.rightips.services.AbstractIntentService;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

public class VerifyEmailService extends AbstractIntentService {

	public VerifyEmailService() {
		super("VerifyEmailService");
		mResultType = new TypeToken<EmailVerificationData>() {
		}.getType();
	}

	// Abstract method implemented here, called from super class.
	// when onHandleIntent is called.
	public void createUrlParameters() {
		mBaseUrl = "https://bpi.briteverify.com/emails.json?apikey=031b8dbb-2485-4916-923f-e348b9075ca2";
		mParams = new RequestParams();
		mParams.put("address",
				mIntent.getStringExtra(AppConstants.USER_EMAIL));
	}
}
