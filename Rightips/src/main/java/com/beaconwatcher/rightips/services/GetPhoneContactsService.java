/*
 * Copyright (C) 2015 RighTips Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.beaconwatcher.rightips.services;

import java.util.ArrayList;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.beaconwatcher.rightips.entities.ContactListItem;

/**
 * This service calls API to get details about a specific notification.
 * 
 */
public class GetPhoneContactsService extends IntentService {
	private static final String TAG = "GetPhoneContactsService";
	// Defines a custom Intent action
	public static final String GET_PHONE_CONTACTS_ACTION = "com.beaconwatcher.rightips.services.GetPhoneContactsService";
	public static String MODE_SMS = "sms";
	public static String MODE_WHATSAPP = "whatsApp";
	public static String MODE_EMAIL = "email";

	public static String CONTACTS = "Contacts";
	public static String PHONE_CONTACTS = "phoneContacts";
	public static String EMAIL_CONTACTS = "emailContacts";

	private Context mContext;

	private ArrayList<ContactListItem> mContactArray = new ArrayList<ContactListItem>();
	private ArrayList<ContactListItem> mEmailArray = new ArrayList<ContactListItem>();

	public GetPhoneContactsService() {
		// TODO Auto-generated constructor stub
		super("GetSpecificNotificationService");
	}

	@Override
	protected void onHandleIntent(Intent workIntent) {
		// Gets data from the incoming Intent
		Log.d(TAG, "Sending API call to get specific notification details");
		mContext = this;

		String mode = workIntent.getStringExtra("mode");

		if (mode.equals(MODE_SMS)) {
			getContactsWithPhoneNumber();
		} else if (mode.equals(MODE_EMAIL)) {
			getContactsWithEmail();
		}

		/*
		 * 
		 * ContentResolver cr=mContext.getContentResolver(); Cursor cur =
		 * cr.query( ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
		 * null, null, null);
		 * 
		 * String phone = null; String emailContact = null; String image_uri =
		 * "";
		 * 
		 * 
		 * 
		 * if (cur.getCount() > 0) { while (cur.moveToNext()) { String id =
		 * cur.getString(cur .getColumnIndex(ContactsContract.Contacts._ID));
		 * String name = cur .getString(cur
		 * .getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
		 * 
		 * image_uri =
		 * cur.getString(cur.getColumnIndex(ContactsContract.Contacts
		 * .PHOTO_THUMBNAIL_URI));
		 * 
		 * //Phone Numbers if
		 * (Integer.parseInt(cur.getString(cur.getColumnIndex(
		 * ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) { ContactListItem
		 * item = new ContactListItem(); item.setId(id); item.setName(name);
		 * if(image_uri!=null){ item.setPhotoUrl(image_uri); }
		 * 
		 * Cursor pCur = cr
		 * .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
		 * ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new
		 * String[] { id }, null);
		 * 
		 * while (pCur.moveToNext()) { phone = pCur .getString(pCur
		 * .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
		 * item.setContact(phone); } pCur.close(); mContactArray.add(item); }
		 * 
		 * 
		 * //Email Addresses Cursor emailCur = cr.query(
		 * ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
		 * ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new
		 * String[] { id }, null); while (emailCur.moveToNext()) {
		 * ContactListItem item = new ContactListItem(); item.setId(id);
		 * item.setName(name);
		 * 
		 * if(image_uri!=null){ item.setPhotoUrl(image_uri); }
		 * 
		 * emailContact = emailCur .getString(emailCur
		 * .getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
		 * item.setContact(emailContact); mEmailArray.add(item); }
		 * emailCur.close(); /* if (image_uri != null) {
		 * System.out.println(Uri.parse(image_uri)); try { bitmap = MediaStore
		 * .Images.Media.getBitmap(getActivity().getContentResolver(),
		 * Uri.parse(image_uri)); sb.append("\n Image in Bitmap:" + bitmap);
		 * System.out.println(bitmap); } catch (FileNotFoundException e) { //
		 * TODO Auto-generated catch block e.printStackTrace(); } catch
		 * (IOException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 * sb.append("\n........................................"); }
		 */
		/*
		 * } }
		 * 
		 * cur.close(); broadcastMessage();
		 */
	}

	private void getContactsWithPhoneNumber() {
		ContentResolver contentResolver = getContentResolver();
		Cursor cur = contentResolver.query(
				ContactsContract.Contacts.CONTENT_URI, new String[] {
						ContactsContract.PhoneLookup._ID,
						ContactsContract.PhoneLookup.DISPLAY_NAME,
						ContactsContract.Contacts.HAS_PHONE_NUMBER,
						ContactsContract.Contacts.PHOTO_THUMBNAIL_URI }, null,
				null, ContactsContract.Contacts.DISPLAY_NAME + " ASC");

		try {
			if (cur != null && cur.getCount() > 0) {
				while (cur.moveToNext()) {
					if (Integer
							.parseInt(cur.getString(cur
									.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
						String id = cur.getString(cur
								.getColumnIndex(ContactsContract.Contacts._ID));
						String name = cur
								.getString(cur
										.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
						String image_uri = cur
								.getString(cur
										.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI));

						ContactListItem item = new ContactListItem();
						mContactArray.add(item);
						item.setId(id);
						item.setName(name);
						item.setPhotoUrl(image_uri);
					}
				}
			}
		} finally {
			if (cur != null) {
				cur.close();
				broadcastMessage();
			}
		}
	}

	private void getContactsWithEmail() {
		ContentResolver contentResolver = getContentResolver();
		Cursor cur = contentResolver.query(
				ContactsContract.CommonDataKinds.Email.CONTENT_URI,
				new String[] {
						ContactsContract.CommonDataKinds.Email.CONTACT_ID,
						ContactsContract.Contacts.DISPLAY_NAME,
						ContactsContract.CommonDataKinds.Email.DATA }, null,
				null, ContactsContract.Contacts.DISPLAY_NAME + " ASC");

		try {
			if (cur != null && cur.getCount() > 0) {
				while (cur.moveToNext()) {
					String id = cur
							.getString(cur
									.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID));
					String name = cur
							.getString(cur
									.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
					String email = cur
							.getString(cur
									.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));

					ContactListItem item = new ContactListItem();
					mContactArray.add(item);

					item.setId(id);
					item.setName(name);
					item.setContact(email);
				}
			}
		} finally {
			if (cur != null) {
				cur.close();
				broadcastMessage();
			}
		}
	}

	private void broadcastMessage() {
		/*
		 * Creates a new Intent containing a Uri object
		 * GET_PHONE_CONTACTS_ACTION is a custom Intent action
		 */
		Intent broadcastIntent = new Intent(GET_PHONE_CONTACTS_ACTION);
		broadcastIntent.putParcelableArrayListExtra(CONTACTS, mContactArray);
		LocalBroadcastManager.getInstance(mContext).sendBroadcast(
				broadcastIntent);
	}
}