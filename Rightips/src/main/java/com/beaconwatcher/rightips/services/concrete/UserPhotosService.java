package com.beaconwatcher.rightips.services.concrete;

import java.util.ArrayList;

import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.ListItemData;
import com.beaconwatcher.rightips.services.AbstractIntentService;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

public class UserPhotosService extends AbstractIntentService {

	public UserPhotosService() {
		super("UserPhotosService");
		mResultType = new TypeToken<ArrayList<ListItemData>>() {
		}.getType();
	}

	// Abstract method implemented here, called from super class.
	// when onHandleIntent is called.
	public void createUrlParameters() {
		mBaseUrl = "http://www.rightips.com/api/index.php?action=userPhotos";
		mParams = new RequestParams();
		mParams.put("uid", mIntent.getStringExtra(AppConstants.USER_ID));
	}
}
