package com.beaconwatcher.rightips.services.concrete;

import java.util.ArrayList;
import java.util.Locale;

import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.SearchItemData;
import com.beaconwatcher.rightips.services.AbstractIntentService;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

public class SearchService extends AbstractIntentService {

	public SearchService() {
		super("SearchService");
		mResultType = new TypeToken<ArrayList<SearchItemData>>() {
		}.getType();
	}

	// Abstract method implemented here, called from super class.
	// when onHandleIntent is called.
	public void createUrlParameters() {
		mBaseUrl = "http://api.beaconwatcher.com/index.php?action=search";
		mParams = new RequestParams();
		mParams.put("lang_code", Locale.getDefault().getLanguage());
		mParams.put("keyword",
				mIntent.getStringExtra(AppConstants.SEARCH_KEYWORD));
		mParams.put("lat", mIntent.getStringExtra(AppConstants.LAST_LATITUDE));
		mParams.put("lng", mIntent.getStringExtra(AppConstants.LAST_LONGITUDE));
	}
}
