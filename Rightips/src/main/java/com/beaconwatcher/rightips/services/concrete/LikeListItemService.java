package com.beaconwatcher.rightips.services.concrete;

import java.util.ArrayList;

import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.ListItemData;
import com.beaconwatcher.rightips.services.AbstractIntentService;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

public class LikeListItemService extends AbstractIntentService {

	public LikeListItemService() {
		super("LikeListItemService");
		mResultType = new TypeToken<ArrayList<ListItemData>>() {
		}.getType();
	}

	// Abstract method implemented here, called from super class.
	public void createUrlParameters() {
		mBaseUrl = "http://www.rightips.com/api/index.php?action=like";
		mParams = new RequestParams();
		mParams.put("uid", mIntent.getStringExtra(AppConstants.USER_ID));
		mParams.put("id", mIntent.getStringExtra(AppConstants.LIST_ITEM_ID));
		mParams.put("type", mIntent.getStringExtra(AppConstants.LIST_ITEM_TYPE));
		mParams.put("rel", mIntent.getStringExtra(AppConstants.ACTION_LIKE));
	}
}
