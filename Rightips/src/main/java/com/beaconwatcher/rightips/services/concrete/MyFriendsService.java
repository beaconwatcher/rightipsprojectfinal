package com.beaconwatcher.rightips.services.concrete;

import java.util.ArrayList;

import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.MyFriendData;
import com.beaconwatcher.rightips.services.AbstractIntentService;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

public class MyFriendsService extends AbstractIntentService {

	public MyFriendsService() {
		super("MyFriendsService");
		mResultType = new TypeToken<ArrayList<MyFriendData>>() {
		}.getType();
	}

	// Abstract method implemented here, called from super class.
	// when onHandleIntent is called.
	public void createUrlParameters() {
		mBaseUrl = "http://www.rightips.com/api/index.php?action=friendsList";
		mParams = new RequestParams();
		mParams.put("uid", mIntent.getStringExtra(AppConstants.USER_ID));
	}
}
