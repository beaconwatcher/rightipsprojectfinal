package com.beaconwatcher.rightips.services.concrete;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;

import android.graphics.Bitmap;
import android.net.Uri;

import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.UserProfileImage;
import com.beaconwatcher.rightips.services.AbstractIntentService;
import com.bw.libraryproject.utils.BitmapUtilities;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

public class SaveProfilePictureService extends AbstractIntentService {

	public SaveProfilePictureService() {
		super("SaveProfilePictureService");
		mResultType = new TypeToken<ArrayList<UserProfileImage>>() {
		}.getType();
	}

	// Abstract method implemented here, called from super class.
	// when onHandleIntent is called.
	public void createUrlParameters() {
		String requestAction = mIntent
				.getStringExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE);
		String urlAction = requestAction
				.equals(AppConstants.ACTION_SAVE_USER_PROFILE_IMAGE) ? "saveProfilePic"
				: "saveCoverPhoto";
		mBaseUrl = "http://www.rightips.com/api/index.php?action=" + urlAction;

		byte[] byteArray = mIntent
				.getByteArrayExtra(AppConstants.IMAGE_BYTE_ARRAY);
		if (byteArray != null) {
			ByteArrayInputStream stream = new ByteArrayInputStream(byteArray);
			mParams = new RequestParams();
			mParams.put("uid", mIntent.getStringExtra(AppConstants.USER_ID));
			mParams.put("file", stream, "myimage.jpg");
		}
	}
}
