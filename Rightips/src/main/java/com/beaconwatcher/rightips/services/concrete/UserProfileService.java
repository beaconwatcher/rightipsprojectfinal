package com.beaconwatcher.rightips.services.concrete;

import java.util.ArrayList;

import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.UserProfileData;
import com.beaconwatcher.rightips.services.AbstractIntentService;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

public class UserProfileService extends AbstractIntentService {

	public UserProfileService() {
		super("UserProfileService");
		mResultType = new TypeToken<ArrayList<UserProfileData>>() {
		}.getType();
	}

	// Abstract method implemented here, called from super class.
	// when onHandleIntent is called.
	public void createUrlParameters() {
		mBaseUrl = "http://www.rightips.com/api/index.php?action=getProfile";
		mParams = new RequestParams();
		mParams.put("uid", mIntent.getStringExtra(AppConstants.USER_ID));
		mParams.put("request_id",
				mIntent.getStringExtra(AppConstants.CURRENT_LOGGED_USER_ID));
	}
}
