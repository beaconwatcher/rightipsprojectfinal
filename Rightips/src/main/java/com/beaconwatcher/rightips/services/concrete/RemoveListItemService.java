package com.beaconwatcher.rightips.services.concrete;

import java.util.ArrayList;

import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.ListItemData;
import com.beaconwatcher.rightips.services.AbstractIntentService;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

public class RemoveListItemService extends AbstractIntentService {

	public RemoveListItemService() {
		super("RemoveListItemService");
		mResultType = new TypeToken<ArrayList<ListItemData>>() {
		}.getType();
	}

	// Abstract method implemented here, called from super class.
	public void createUrlParameters() {
		mBaseUrl = "http://www.rightips.com/api/index.php?action=delete";
		mParams = new RequestParams();
		mParams.put("uid", mIntent.getStringExtra(AppConstants.USER_ID));
		mParams.put("id", mIntent.getStringExtra(AppConstants.LIST_ITEM_ID));
		mParams.put("type", mIntent.getStringExtra(AppConstants.LIST_ITEM_TYPE));

		// Keep reference of removed item to remove it physical from list.
		mItemID = mIntent.getStringExtra(AppConstants.LIST_ITEM_ID);
	}
}
