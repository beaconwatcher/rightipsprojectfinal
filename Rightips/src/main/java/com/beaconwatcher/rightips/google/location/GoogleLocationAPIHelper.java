package com.beaconwatcher.rightips.google.location;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.ListItemData;
import com.bw.libraryproject.entities.LocationData;

public class GoogleLocationAPIHelper {
	private static String LOG_TAG = "AutoCompleteHelperNew";

	public static final String API_KEY = "AIzaSyDyEBsyBpVjBf4A6_PdsufThBYwX0FdD4k";
	public static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
	public static final String TYPE_AUTOCOMPLETE = "/autocomplete";
	public static final String TYPE_DETAILS = "/details";
	public static final String OUT_JSON = "/json";

	private static final AndroidHttpClient ANDROID_HTTP_CLIENT = AndroidHttpClient
			.newInstance(GoogleLocationAPIHelper.class.getName());
	private boolean running = false;
	private AutoCompleteNotifier notifier;
	private LocationData location;

	public interface AutoCompleteNotifier {
		public void onAutoCompleteNotified(ArrayList<ListItemData> arr);

		public void onLocationFound(LocationData location);

		public void onCityNameFound(LocationData location);

		public void onCityNameError(String str);
	}

	public GoogleLocationAPIHelper(AutoCompleteNotifier notif) {
		this.notifier = notif;
	}

	public void getAutoComplete(final String str) {
		if (running)
			return;

		new AsyncTask<Void, Void, ArrayList<ListItemData>>() {
			protected void onPreExecute() {
				running = true;
			};

			@Override
			protected ArrayList<ListItemData> doInBackground(Void... params) {
				ArrayList<ListItemData> resultList = null;

				HttpURLConnection conn = null;
				StringBuilder jsonResults = new StringBuilder();
				try {
					StringBuilder sb = new StringBuilder(PLACES_API_BASE
							+ TYPE_AUTOCOMPLETE + OUT_JSON);
					sb.append("?key=" + API_KEY);
					// sb.append("&components=country:uk");
					sb.append("&input=" + URLEncoder.encode(str, "utf8"));

					URL url = new URL(sb.toString());
					conn = (HttpURLConnection) url.openConnection();
					InputStreamReader in = new InputStreamReader(
							conn.getInputStream());

					// Load the results into a StringBuilder
					int read;
					char[] buff = new char[1024];
					while ((read = in.read(buff)) != -1) {
						jsonResults.append(buff, 0, read);
					}
				} catch (MalformedURLException e) {
					Log.e(LOG_TAG, "Error processing Places API URL", e);
					return resultList;
				} catch (IOException e) {
					Log.e(LOG_TAG, "Error connecting to Places API", e);
					return resultList;
				} finally {
					if (conn != null) {
						conn.disconnect();
					}
				}

				try {
					// Create a JSON object hierarchy from the results
					JSONObject jsonObj = new JSONObject(jsonResults.toString());
					JSONArray predsJsonArray = jsonObj
							.getJSONArray("predictions");

					// Extract the Place descriptions from the results
					resultList = new ArrayList<ListItemData>(
							predsJsonArray.length());
					for (int i = 0; i < predsJsonArray.length(); i++) {
						ListItemData pd = new ListItemData();
						pd.strId = predsJsonArray.getJSONObject(i).getString(
								"place_id");
						pd.name = predsJsonArray.getJSONObject(i).getString(
								"description");
						resultList.add(pd);
					}
				} catch (JSONException e) {
					Log.e(LOG_TAG, "Cannot process JSON results", e);
				}
				return resultList;
			}

			protected void onPostExecute(ArrayList<ListItemData> arr) {
				running = false;
				if (arr != null) {
					notifier.onAutoCompleteNotified(arr);
					Log.i("AutoCompleteHelperNew", arr.toString());
				}
			};
		}.execute();
	}

	public void getLocationFromId(final String id) {
		if (running)
			return;

		new AsyncTask<Void, Void, LocationData>() {
			protected void onPreExecute() {
				running = true;
			};

			@Override
			protected LocationData doInBackground(Void... params) {

				HttpURLConnection conn = null;
				StringBuilder jsonResults = new StringBuilder();
				try {
					StringBuilder sb = new StringBuilder(PLACES_API_BASE
							+ TYPE_DETAILS + OUT_JSON);
					sb.append("?key=" + API_KEY);
					// sb.append("&components=country:uk");
					sb.append("&placeid=" + id);

					URL url = new URL(sb.toString());
					conn = (HttpURLConnection) url.openConnection();
					InputStreamReader in = new InputStreamReader(
							conn.getInputStream());

					// Load the results into a StringBuilder
					int read;
					char[] buff = new char[1024];
					while ((read = in.read(buff)) != -1) {
						jsonResults.append(buff, 0, read);
					}
				} catch (MalformedURLException e) {
					Log.e(LOG_TAG, "Error processing Places API URL", e);
					return null;
				} catch (IOException e) {
					Log.e(LOG_TAG, "Error connecting to Places API", e);
					return null;
				} finally {
					if (conn != null) {
						conn.disconnect();
					}
				}

				LocationData mLocationData = null;
				try {

					JSONObject jsonObj = new JSONObject(jsonResults.toString());
					// Log.v(LOG_TAG, jsonObj.toString());

					JSONObject result = jsonObj.getJSONObject("result");
					JSONObject loc = result.getJSONObject("geometry")
							.getJSONObject("location");

					mLocationData = new LocationData();
					mLocationData.setLatitude(Double.parseDouble(loc
							.getString("lat")));
					mLocationData.setLongitude(Double.parseDouble(loc
							.getString("lng")));
					mLocationData.setCityName(result.getString("name"));
					mLocationData
							.setSource(AppConstants.LOCATION_SOURCE_SEARCH);

				} catch (JSONException e) {
					Log.e(LOG_TAG, "Cannot process JSON results", e);
				}
				return mLocationData;
			}

			protected void onPostExecute(LocationData location) {
				running = false;
				if (location != null) {
					notifier.onLocationFound(location);
					Log.i("AutoCompleteHelperNew", location.toString());
				}
			};
		}.execute();
	}

	public void fetchCityName(final Context contex, final LocationData l) {
		if (running)
			return;
		location = l;

		new AsyncTask<Void, Void, String>() {
			protected void onPreExecute() {
				running = true;
			};

			@Override
			protected String doInBackground(Void... params) {
				String cityName = null;
				if (Geocoder.isPresent()) {
					try {
						Geocoder geocoder = new Geocoder(contex,
								Locale.getDefault());
						List<Address> addresses = geocoder.getFromLocation(
								location.getLatitude(),
								location.getLongitude(), 1);
						if (addresses.size() > 0) {
							cityName = addresses.get(0).getLocality();
						}
					} catch (IOException exception1) {
						return "Error converting coordinates into address: ["
								+ (location.getLatitude() + ", " + location
										.getLongitude()) + "]";
						// return "error";
					} catch (IllegalArgumentException exception2) {
						return "Error converting coordinates into address: ["
								+ (location.getLatitude() + ", " + location
										.getLongitude()) + "]";
						// return "error";
					} catch (Exception ignored) {
						return "Error converting coordinates into address: ["
								+ (location.getLatitude() + ", " + location
										.getLongitude()) + "]";
					}
				}

				// i.e., Geocoder succeed
				if (cityName != null) {
					return cityName;
				}

				// i.e., Geocoder failed, we fetch address using google map.
				else {
					return fetchCityNameUsingGoogleMap();
				}
			}

			// Geocoder failed :-(
			// Our B Plan : Google Map
			private String fetchCityNameUsingGoogleMap() {
				String googleMapUrl = "https://maps.googleapis.com/maps/api/geocode/json?latlng="
						+ location.getLatitude()
						+ ","
						+ location.getLongitude() + "&key=" + API_KEY;

				try {
					JSONObject jObj = new JSONObject(
							ANDROID_HTTP_CLIENT.execute(new HttpGet(
									googleMapUrl), new BasicResponseHandler()));
					GeoCodedAddress address = parseAddress(jObj);
					if (!address.City.equals(""))
						return address.City;
					if (!address.Address2.equals(""))
						return address.Address2;
					if (!address.County.equals(""))
						return address.County;
					if (!address.State.equals(""))
						return address.State;
					if (!address.Country.equals(""))
						return address.Country;
					return l.getCityName();
				} catch (Exception ignored) {
					ignored.printStackTrace();
				}
				return null;
			}

			protected void onPostExecute(String response) {
				running = false;
				if (response != null
						&& !(response.toLowerCase().contains("error"))) {
					// Do something with cityName
					LocationData ld = new LocationData();
					ld.setLatitude(location.getLatitude());
					ld.setLongitude(location.getLongitude());
					ld.setSource(location.getSource());
					ld.setCityName(response);
					notifier.onCityNameFound(ld);
				} else {
					notifier.onCityNameError("");// location);
				}
			};
		}.execute();
	}

	public GeoCodedAddress parseAddress(JSONObject jsonObj) {
		GeoCodedAddress address = new GeoCodedAddress();

		try {
			JSONArray Results = jsonObj.getJSONArray("results");
			JSONObject zero = Results.getJSONObject(0);
			JSONArray address_components = zero
					.getJSONArray("address_components");
			for (int i = 0; i < address_components.length(); i++) {
				JSONObject zero2 = address_components.getJSONObject(i);
				String long_name = zero2.getString("long_name");
				JSONArray mtypes = zero2.getJSONArray("types");
				String Type = mtypes.getString(0);

				if (TextUtils.isEmpty(long_name) == false
						|| !long_name.equals(null) || long_name.length() > 0
						|| long_name != "") {
					if (Type.equalsIgnoreCase("street_number")) {
						address.Address1 = long_name + " ";
					} else if (Type.equalsIgnoreCase("route")) {
						address.Address1 = address.Address1 + long_name;
					} else if (Type.equalsIgnoreCase("sublocality")) {
						address.Address2 = long_name;
					} else if (Type.equalsIgnoreCase("locality")) {
						// Address2 = Address2 + long_name + ", ";
						address.City = long_name;
					} else if (Type
							.equalsIgnoreCase("administrative_area_level_2")) {
						address.County = long_name;
					} else if (Type
							.equalsIgnoreCase("administrative_area_level_1")) {
						address.State = long_name;
					} else if (Type.equalsIgnoreCase("country")) {
						address.Country = long_name;
					} else if (Type.equalsIgnoreCase("postal_code")) {
						address.PIN = long_name;
					}
				}
			}
		} catch (JSONException e) {
		}

		return address;

	}

	public class GeoCodedAddress {
		public String Address1 = "";
		public String Address2 = "";
		public String City = "";
		public String State = "";
		public String Country = "";
		public String County = "";
		public String PIN = "";
	}

}
