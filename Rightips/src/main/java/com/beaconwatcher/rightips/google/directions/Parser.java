package com.beaconwatcher.rightips.google.directions;

//. by Haseem Saheed
public interface Parser {
	public Route parse();
}