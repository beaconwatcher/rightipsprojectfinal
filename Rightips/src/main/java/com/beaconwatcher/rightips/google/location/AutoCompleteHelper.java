package com.beaconwatcher.rightips.google.location;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.util.Log;

import com.bw.libraryproject.entities.PlaceData;

public class AutoCompleteHelper {
	private static String LOG_TAG = "AutoCompleteHelper";

	public static final String API_KEY = "AIzaSyADNvDGbHp11gm5-Nh3zWmVkoUKm2eWAe4";
	public static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
	public static final String TYPE_AUTOCOMPLETE = "/autocomplete";
	public static final String TYPE_DETAILS = "/details";
	public static final String OUT_JSON = "/json";

	private static final AndroidHttpClient ANDROID_HTTP_CLIENT = AndroidHttpClient
			.newInstance(AutoCompleteHelper.class.getName());
	private boolean running = false;
	private AutoCompleteNotifier notifier;
	private Location location;

	public interface AutoCompleteNotifier {
		public void onAutoCompleteNotified(ArrayList<PlaceData> arr);
	}

	public void getAutoComplete(final Activity activity, final Context contex,
			final String str) {
		if (running)
			return;
		notifier = (AutoCompleteNotifier) activity;

		new AsyncTask<Void, Void, ArrayList<PlaceData>>() {
			protected void onPreExecute() {
				running = true;
			};

			@Override
			protected ArrayList<PlaceData> doInBackground(Void... params) {
				ArrayList<PlaceData> resultList = null;

				HttpURLConnection conn = null;
				StringBuilder jsonResults = new StringBuilder();
				try {
					StringBuilder sb = new StringBuilder(PLACES_API_BASE
							+ TYPE_AUTOCOMPLETE + OUT_JSON);
					sb.append("?key=" + API_KEY);
					// sb.append("&components=country:uk");
					sb.append("&input=" + URLEncoder.encode(str, "utf8"));

					URL url = new URL(sb.toString());
					conn = (HttpURLConnection) url.openConnection();
					InputStreamReader in = new InputStreamReader(
							conn.getInputStream());

					// Load the results into a StringBuilder
					int read;
					char[] buff = new char[1024];
					while ((read = in.read(buff)) != -1) {
						jsonResults.append(buff, 0, read);
					}
				} catch (MalformedURLException e) {
					Log.e(LOG_TAG, "Error processing Places API URL", e);
					return resultList;
				} catch (IOException e) {
					Log.e(LOG_TAG, "Error connecting to Places API", e);
					return resultList;
				} finally {
					if (conn != null) {
						conn.disconnect();
					}
				}

				try {
					// Create a JSON object hierarchy from the results
					JSONObject jsonObj = new JSONObject(jsonResults.toString());
					JSONArray predsJsonArray = jsonObj
							.getJSONArray("predictions");

					// Extract the Place descriptions from the results
					resultList = new ArrayList<PlaceData>(
							predsJsonArray.length());
					for (int i = 0; i < predsJsonArray.length(); i++) {
						PlaceData pd = new PlaceData();
						pd.setId(predsJsonArray.getJSONObject(i).getString(
								"place_id"));
						pd.setDescription(predsJsonArray.getJSONObject(i)
								.getString("description"));
						resultList.add(pd);
					}
				} catch (JSONException e) {
					Log.e(LOG_TAG, "Cannot process JSON results", e);
				}
				return resultList;
			}

			protected void onPostExecute(ArrayList<PlaceData> arr) {
				running = false;
				if (arr != null) {
					notifier.onAutoCompleteNotified(arr);
					Log.i("AutoCompleteHelper", arr.toString());
				}
			};
		}.execute();
	}
}