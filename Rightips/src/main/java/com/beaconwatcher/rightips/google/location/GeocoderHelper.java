package com.beaconwatcher.rightips.google.location;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;

import com.beaconwatcher.rightips.entities.AppConstants;
import com.bw.libraryproject.entities.LocationData;

public class GeocoderHelper {
	private static final AndroidHttpClient ANDROID_HTTP_CLIENT = AndroidHttpClient
			.newInstance(GeocoderHelper.class.getName());
	private boolean running = false;
	private LocationNotifier notifier;
	private Location location;

	public interface LocationNotifier {
		public void onLocationNotified(LocationData ld);

		public void onLocationError(String error);
	}

	public void fetchCityName(final LocationNotifier noti,
			final Context contex, final Location l) {
		if (running)
			return;

		location = l;
		this.notifier = noti;

		new AsyncTask<Void, Void, String>() {
			protected void onPreExecute() {
				running = true;
			};

			@Override
			protected String doInBackground(Void... params) {
				String cityName = null;
				if (Geocoder.isPresent()) {
					try {
						Geocoder geocoder = new Geocoder(contex,
								Locale.getDefault());
						List<Address> addresses = geocoder.getFromLocation(
								location.getLatitude(),
								location.getLongitude(), 1);
						if (addresses.size() > 0) {
							cityName = addresses.get(0).getLocality();
						}
					} catch (IOException exception1) {
						return "Error converting coordinates into address: ["
								+ (location.getLatitude() + ", " + location
										.getLongitude()) + "]";
						// return "error";
					} catch (IllegalArgumentException exception2) {
						return "Error converting coordinates into address: ["
								+ (location.getLatitude() + ", " + location
										.getLongitude()) + "]";
						// return "error";
					} catch (Exception ignored) {
						return "Error converting coordinates into address: ["
								+ (location.getLatitude() + ", " + location
										.getLongitude()) + "]";
					}
				}

				// i.e., Geocoder succeed
				if (cityName != null) {
					return cityName;
				}

				// i.e., Geocoder failed, we fetch address using google map.
				else {
					return fetchCityNameUsingGoogleMap();
				}
			}

			// Geocoder failed :-(
			// Our B Plan : Google Map
			private String fetchCityNameUsingGoogleMap() {
				String googleMapUrl = "http://maps.googleapis.com/maps/api/geocode/json?latlng="
						+ location.getLatitude()
						+ ","
						+ location.getLongitude() + "&sensor=false&language=fr";

				try {
					JSONObject googleMapResponse = new JSONObject(
							ANDROID_HTTP_CLIENT.execute(new HttpGet(
									googleMapUrl), new BasicResponseHandler()));

					// many nested loops.. not great -> use expression instead
					// loop among all results
					JSONArray results = (JSONArray) googleMapResponse
							.get("results");
					for (int i = 0; i < results.length(); i++) {
						// loop among all addresses within this result
						JSONObject result = results.getJSONObject(i);
						if (result.has("address_components")) {
							JSONArray addressComponents = result
									.getJSONArray("address_components");
							// loop among all address component to find a
							// 'locality' or 'sublocality'
							for (int j = 0; j < addressComponents.length(); j++) {
								JSONObject addressComponent = addressComponents
										.getJSONObject(j);
								if (result.has("types")) {
									JSONArray types = addressComponent
											.getJSONArray("types"); // search
																	// for
																	// locality
																	// and
																	// sublocality
									String cityName = null;

									for (int k = 0; k < types.length(); k++) {
										if ("locality".equals(types
												.getString(k))
												&& cityName == null) {
											if (addressComponent
													.has("long_name")) {
												cityName = addressComponent
														.getString("long_name");
											} else if (addressComponent
													.has("short_name")) {
												cityName = addressComponent
														.getString("short_name");
											}
										}
										if ("sublocality".equals(types
												.getString(k))) {
											if (addressComponent
													.has("long_name")) {
												cityName = addressComponent
														.getString("long_name");
											} else if (addressComponent
													.has("short_name")) {
												cityName = addressComponent
														.getString("short_name");
											}
										}
									}
									if (cityName != null) {
										return cityName;
									}
								}
							}
						}
					}
				} catch (Exception ignored) {
					ignored.printStackTrace();
				}
				return null;
			}

			protected void onPostExecute(String response) {
				running = false;
				if (response != null
						&& !(response.toLowerCase().contains("error"))) {
					// Do something with cityName
					LocationData ld = new LocationData();
					ld.setLatitude(location.getLatitude());
					ld.setLongitude(location.getLongitude());
					ld.setCityName(response);
					ld.setSource(AppConstants.LOCATION_SOURCE_GPS);

					notifier.onLocationNotified(ld);// location);
				} else {
					notifier.onLocationError(response);// location);
				}
			};
		}.execute();
	}
}