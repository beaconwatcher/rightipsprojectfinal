package com.beaconwatcher.rightips.customdialogs;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.Window;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.AppConstants;

public class LocationDialog extends Dialog implements View.OnClickListener {

	public Activity mActivity;
	public Dialog d;
	public View btnSettings, btnClose;

	public LocationDialog(Activity a) {
		super(a);
		// TODO Auto-generated constructor stub
		this.mActivity = a;
		this.setCancelable(false);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_location);
		btnSettings =  findViewById(R.id.btn_gps_settings);
		btnClose = findViewById(R.id.btn_close);
		
		btnSettings.setOnClickListener(this);
		btnClose.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_gps_settings:
			dismiss();
			gotoLocationSettings();
			break;
			
		case R.id.btn_close:
			dismiss();
			break;

		default:
			break;
		}
		//dismiss();
	}
	
	private void gotoLocationSettings(){
		Intent intent = new Intent(	Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		mActivity.startActivityForResult(intent,
				AppConstants.REQUEST_CODE_ENABLE_LOCATION_SERVICES);

	}
}