package com.beaconwatcher.rightips.customslides;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.beaconwatcher.rightips.R;
import com.bw.libraryproject.entities.CategoryItem;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;

/**
 * This is a slider with a description TextView.
 */
public class NotificationMiniSliderView extends BaseSliderView {

	private String mTitle;
	private String mDiscount;
	private String mSiteName;
	private String mSiteAddress;
	private Context mContext;

	private ArrayList<CategoryItem> mCategories;

	/**
	 * the title of a slider image.
	 * 
	 * @param title
	 * @return
	 */
	public NotificationMiniSliderView title(String title) {
		mTitle = title;
		return this;
	}

	/**
	 * the site name of a slider image.
	 * 
	 * @param site
	 *            name
	 * @return
	 */
	public NotificationMiniSliderView siteName(String sn) {
		mSiteName = sn;
		return this;
	}

	/**
	 * the discount on slider image.
	 * 
	 * @param dis
	 * @return
	 */
	public NotificationMiniSliderView discount(String dis) {
		mDiscount = dis;
		return this;
	}

	/**
	 * the site address of a slider image.
	 * 
	 * @param site
	 *            name
	 * @return
	 */
	public NotificationMiniSliderView siteAddress(String sa) {
		mSiteAddress = sa;
		return this;
	}

	/**
	 * the site address of a slider image.
	 * 
	 * @param site
	 *            name
	 * @return
	 */
	public NotificationMiniSliderView categories(ArrayList<CategoryItem> cats) {
		mCategories = cats;
		return this;
	}

	public String getTitle() {
		return mTitle;
	}

	public String getSiteName() {
		return mSiteName;
	}

	public String getDiscount() {
		return mDiscount;
	}

	public String getSiteAddress() {
		return mSiteAddress;
	}

	public ArrayList<CategoryItem> getCategories() {
		return mCategories;
	}

	public NotificationMiniSliderView(Context context) {
		super(context);
		mContext = context;

	}

	@Override
	public View getView() {
		View v = LayoutInflater.from(mContext).inflate(
				R.layout.slide_mini_slider, null);
		TextView title = (TextView) v.findViewById(R.id.txt_title);
		TextView desc = (TextView) v.findViewById(R.id.txt_description);

		if (this.getSiteName() != null) {
			String siteName = getSiteName();
			title.setText(siteName);
		}
		desc.setText(getTitle());

		/*
		 * ArrayList<CategoryItem> cats=getCategories(); if(cats!=null &&
		 * cats.size() > 0){ LinearLayout cont= (LinearLayout)
		 * v.findViewById(R.id.cat_container);
		 * 
		 * 
		 * for(int i=0; i<cats.size(); i++){
		 * 
		 * 
		 * CircularImageView img = new CircularImageView(mContext);
		 * img.setBorderColor(Color.WHITE); img.setBorderWidth(2);
		 * 
		 * img.setLayoutParams(new LayoutParams(80, 80));
		 * 
		 * 
		 * String url=cats.get(i).getUrl();
		 * 
		 * if(!url.equals("")){ //loadImageFromURL(url, img);
		 * 
		 * 
		 * // Uri uri=Uri.parse(url); //img.setImageURI(uri);
		 * Picasso.with(mContext
		 * ).load(cats.get(i).getUrl()).placeholder(R.drawable
		 * .cat_no_image).error(R.drawable.cat_no_image).into(img); } else{
		 * img.setImageResource(R.drawable.cat_no_image); } cont.addView(img); }
		 * }
		 */
		ImageView target = (ImageView) v
				.findViewById(R.id.daimajia_slider_image);
		bindEventAndShow(v, target);
		return v;
	}

	/*
	 * public static Drawable LoadImageFromWebOperations(String url) { try {
	 * InputStream is = (InputStream) new URL(url).getContent(); Drawable d =
	 * Drawable.createFromStream(is, "src name"); return d; } catch (Exception
	 * e) { return null; } }
	 */

	public boolean loadImageFromURL(String fileUrl, ImageView iv) {
		try {

			URL myFileUrl = new URL(fileUrl);
			HttpURLConnection conn = (HttpURLConnection) myFileUrl
					.openConnection();
			conn.setDoInput(true);
			conn.connect();

			InputStream is = conn.getInputStream();
			iv.setImageBitmap(BitmapFactory.decodeStream(is));

			return true;

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}
}
