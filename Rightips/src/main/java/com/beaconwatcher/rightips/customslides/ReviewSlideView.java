package com.beaconwatcher.rightips.customslides;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.beaconwatcher.rightips.R;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;

/**
 * This is a slider with a description TextView.
 */
public class ReviewSlideView extends BaseSliderView {

	private String mTitle;
	private String mDiscount;
	private String mSiteName;
	private String mSiteAddress;

	/**
	 * the title of a slider image.
	 * 
	 * @param title
	 * @return
	 */
	public ReviewSlideView title(String title) {
		mTitle = title;
		return this;
	}

	/**
	 * the site name of a slider image.
	 * 
	 * @param site
	 *            name
	 * @return
	 */
	public ReviewSlideView siteName(String sn) {
		mSiteName = sn;
		return this;
	}

	/**
	 * the discount on slider image.
	 * 
	 * @param dis
	 * @return
	 */
	public ReviewSlideView discount(String dis) {
		mDiscount = dis;
		return this;
	}

	/**
	 * the site address of a slider image.
	 * 
	 * @param site
	 *            name
	 * @return
	 */
	public ReviewSlideView siteAddress(String sa) {
		mSiteAddress = sa;
		return this;
	}

	public String getTitle() {
		return mTitle;
	}

	public String getSiteName() {
		return mSiteName;
	}

	public String getDiscount() {
		return mDiscount;
	}

	public String getSiteAddress() {
		return mSiteAddress;
	}

	public ReviewSlideView(Context context) {
		super(context);
	}

	@Override
	public View getView() {
		View v = LayoutInflater.from(getContext()).inflate(
				R.layout.slide_notification, null);

		TextView title = (TextView) v.findViewById(R.id.txt_title);
		title.setText(getTitle());

		TextView discount = (TextView) v.findViewById(R.id.txt_discount);
		discount.setText(getDiscount());

		TextView description = (TextView) v.findViewById(R.id.description);
		description.setText(getDescription());

		TextView siteName = (TextView) v.findViewById(R.id.txt_site_name);
		siteName.setText(getSiteName());

		TextView siteAddress = (TextView) v
				.findViewById(R.id.txt_site_addresss);
		siteAddress.setText(this.getSiteAddress());

		ImageView target = (ImageView) v
				.findViewById(R.id.daimajia_slider_image);
		bindEventAndShow(v, target);
		return v;
	}
}
