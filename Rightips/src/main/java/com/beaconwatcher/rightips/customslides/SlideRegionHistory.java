package com.beaconwatcher.rightips.customslides;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beaconwatcher.rightips.R;
import com.bw.libraryproject.entities.LocationData;
import com.bw.libraryproject.utils.jsonUtilities.JSONUtilities;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

/**
 * This is a slider with a description TextView.
 */
public class SlideRegionHistory extends BaseSliderView {
	private String TAG = "SlideRegionHistory";
	private Context mContext;

	private LocationData mLocation;
	private View mRootView;
	private TextView mTfHistory;
	private RelativeLayout mPLoader;

	private AsyncHttpResponseHandler responseHandler;
	private Boolean mDataLoaded = false;

	private String cityName;
	private String cityHistory = "";

	public SlideRegionHistory(Context context, LocationData ld) {
		super(context);
		mContext = context;
		mLocation = ld;
	}

	@Override
	public View getView() {
		View v = LayoutInflater.from(mContext).inflate(
				R.layout.slide_region_history, null);
		TextView title = (TextView) v.findViewById(R.id.tf_title);
		title.setText(mLocation.getCityName() + " - History");
		mTfHistory = (TextView) v.findViewById(R.id.tf_detail);
		mPLoader = (RelativeLayout) v.findViewById(R.id.pLoader);

		loadHistory();
		return v;
	}

	public void loadHistory() {
		if (mDataLoaded == true) {
			mTfHistory.setText(cityHistory);
			if (mPLoader != null) {
				mPLoader.setVisibility(View.INVISIBLE);
			}
			return;
		}

		responseHandler = new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response) {
				// TODO Auto-generated method stub
				if (mPLoader != null) {
					mPLoader.setVisibility(View.INVISIBLE);
				}

				JSONObject jo;

				try {
					jo = new JSONObject(response);
					Map<String, String> out = new HashMap<String, String>();
					JSONUtilities.getKeyMap(jo, out);

					if (out.containsKey("extract")) {
						String str = out.get("extract");
						if (!str.equals("")) {
							mDataLoaded = true;
							str = str.replaceAll("\n", "\n\n");

							// Remove language information from text
							int start = cityName.length() + 1;
							char bracket = str.charAt(start);
							if (bracket == '(') {
								int end = str.indexOf(")");
								if (end != -1) {
									str = mLocation.getCityName()
											+ " "
											+ str.substring(end + 1,
													str.length());
								}
							}
						}
						mTfHistory.setText(str);
						cityHistory = str;
					} else {
						mTfHistory.setText("No History Found");
					}
				} catch (JSONException e) {
					mPLoader.setVisibility(View.INVISIBLE);
					e.printStackTrace();
				}
				Log.d(TAG, response);
			}

			@Override
			public void onFailure(Throwable error, String msg) {
				// TODO Auto-generated method stub
				mPLoader.setVisibility(View.INVISIBLE);
			}
		};

		AsyncHttpClient client = new AsyncHttpClient();
		cityName = mLocation.getCityName();
		if (cityName.toLowerCase().equals("tel aviv-yafo")) {
			cityName = "Tel Aviv";
		}

		cityName = cityName.replaceAll(" ", "%20");
		String url = "https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exlimit=max&explaintext&exintro&titles="
				+ cityName;

		// normalize city name;
		cityName = cityName.replaceAll("%20", " ");

		Log.d(TAG, url);
		try {
			client.get(url, responseHandler);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
