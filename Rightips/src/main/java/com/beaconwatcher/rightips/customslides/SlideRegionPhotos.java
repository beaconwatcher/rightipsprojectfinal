package com.beaconwatcher.rightips.customslides;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.activities.RegionPhotosSliderActivity;
import com.beaconwatcher.rightips.entities.PanoramioPhoto;
import com.bw.libraryproject.entities.LocationData;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * This is a slider with a description TextView.
 */
public class SlideRegionPhotos extends BaseSliderView {
	private String TAG = "SlideRegionHistory";
	private Context mContext;

	private LocationData mLocation;
	private View mRootView;
	private RelativeLayout mPLoader;
	private GridView mGridView;
	private GridViewAdapter mAdapter;

	private AsyncHttpResponseHandler responseHandler;
	private ArrayList<PanoramioPhoto> mPhotoArray = new ArrayList<PanoramioPhoto>();
	private String jsonString;

	private double panoramioDistance = 0.02;

	private int mGridColumns = 2;
	private int mCellWidth = 100;
	private int mCellHeight = 100;
	private int mMaxSlides = 6;
	private Boolean photosLoaded = false;

	public SlideRegionPhotos(Context context, LocationData ld) {
		super(context);
		mContext = context;
		mLocation = ld;
	}

	public void setNumColumns(int cols) {
		mGridColumns = cols;
		mGridView.setNumColumns(cols);

		if (this.mAdapter != null) {
			if (cols == 2) {
				mAdapter.maxSlides = 6;
			} else if (cols == 4) {
				mAdapter.maxSlides = 8;
			}
		}

	}

	public void setCellWidth(int w) {
		mCellWidth = w;
		if (this.mAdapter != null) {
			mAdapter.cellWidth = w;
		}
	}

	public void setCellHeight(int h) {
		mCellHeight = h;
		if (this.mAdapter != null) {
			mAdapter.cellHeight = h;
		}
	}

	public void setMaxSlides(int m) {
		mMaxSlides = m;
		if (mAdapter != null) {
			mAdapter.maxSlides = m;
		}
	}

	@Override
	public View getView() {
		mRootView = LayoutInflater.from(mContext).inflate(
				R.layout.slide_region_photos, null);
		TextView title = (TextView) mRootView.findViewById(R.id.tf_title);
		title.setText(mLocation.getCityName() + " - Photos");
		mGridView = (GridView) mRootView.findViewById(R.id.gridView);
		mPLoader = (RelativeLayout) mRootView.findViewById(R.id.pLoader);

		mAdapter = new GridViewAdapter(getContext(),
				R.layout.panoramio_photo_item, mPhotoArray, mCellWidth,
				mCellHeight, 0, mMaxSlides);
		mGridView.setNumColumns(mGridColumns);
		mGridView.setAdapter(mAdapter);

		return mRootView;
	}

	public void loadPanoramioPhotos() {
		if (photosLoaded == true) {
			if (mPLoader != null) {
				mPLoader.setVisibility(View.INVISIBLE);
			}
			if (mAdapter != null) {
				mAdapter.maxSlides = mMaxSlides;
				mAdapter.notifyDataSetChanged();
			}

			return;
		}

		responseHandler = new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response) {
				photosLoaded = true;
				// TODO Auto-generated method stub
				JSONObject jo;

				mPLoader.setVisibility(View.INVISIBLE);

				try {
					jo = new JSONObject(response);
					if (jo.has("photos")) {
						jsonString = response;

						JSONArray arr = jo.getJSONArray("photos");
						for (int i = 0; i < arr.length(); i++) {
							JSONObject jPhoto = arr.getJSONObject(i);

							PanoramioPhoto photo = new PanoramioPhoto();
							photo.photo_id = jPhoto.getInt("photo_id");
							photo.photo_title = jPhoto.getString("photo_title");
							photo.photo_url = jPhoto
									.getString("photo_file_url");
							mPhotoArray.add(photo);
						}

						mAdapter.totalSlides = arr.length();
						if (arr.length() < mMaxSlides) {
							mMaxSlides = arr.length();
							mAdapter.maxSlides = mMaxSlides;
						}

						mAdapter.notifyDataSetChanged();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				// Log.d(TAG, response);
			}

			@Override
			public void onFailure(Throwable error, String msg) {
				// TODO Auto-generated method stub
				mPLoader.setVisibility(View.INVISIBLE);

			}
		};

		AsyncHttpClient client = new AsyncHttpClient();
		String url = "http://www.panoramio.com/map/get_panoramas.php?set=public&from=0&to=30&minx="
				+ (mLocation.getLongitude() - panoramioDistance)
				+ "&miny="
				+ (mLocation.getLatitude() - panoramioDistance)
				+ "&maxx="
				+ (mLocation.getLongitude() + panoramioDistance)
				+ "&maxy="
				+ (mLocation.getLatitude() + panoramioDistance)
				+ "&size=medium&mapfilter=true";
		Log.d(TAG, url);
		try {
			client.get(url, responseHandler);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public class GridViewAdapter extends ArrayAdapter {
		private Context context;
		private int layoutResourceId;
		private int cellWidth;
		private int cellHeight;
		private int maxSlides;
		private int totalSlides;

		private ArrayList<PanoramioPhoto> data = new ArrayList<PanoramioPhoto>();

		public GridViewAdapter(Context context, int layoutResourceId,
				ArrayList<PanoramioPhoto> data, int cellWidth, int cellHeight,
				int maxSlides, int totalSlides) {
			super(context, layoutResourceId, data);
			this.layoutResourceId = layoutResourceId;
			this.context = context;
			this.data = data;
			this.cellWidth = cellWidth;
			this.cellHeight = cellHeight;
			this.maxSlides = maxSlides;
			this.totalSlides = data.size();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return maxSlides;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final int pos = position;
			View row = convertView;
			ViewHolder holder = null;

			if (row == null) {
				LayoutInflater inflater = ((Activity) context)
						.getLayoutInflater();
				row = inflater.inflate(layoutResourceId, parent, false);

				LayoutParams params = row.getLayoutParams();
				params.width = cellWidth;
				params.height = cellHeight;

				row.setLayoutParams(params);

				holder = new ViewHolder();
				holder.image = (ImageView) row.findViewById(R.id.image);
				holder.pBar = (ProgressBar) row.findViewById(R.id.preloader);
				row.setTag(holder);
			} else {
				holder = (ViewHolder) row.getTag();
			}

			PanoramioPhoto photo = data.get(pos);
			final ProgressBar pBar = holder.pBar;

			Picasso.with(context).load(photo.photo_url)
					.error(R.drawable.cat_no_image)
					.resize(cellWidth, cellHeight).centerCrop()
					.into(holder.image, new Callback() {
						@Override
						public void onSuccess() {
							// TODO Auto-generated method stub
							pBar.setVisibility(View.INVISIBLE);
						}

						@Override
						public void onError() {
							// TODO Auto-generated method stub

						}
					});

			// View MORE
			if (pos == getCount() - 1) {
				row.findViewById(R.id.more_photos).setVisibility(View.VISIBLE);
				((TextView) row.findViewById(R.id.txt_more)).setText("+"
						+ (data.size() - this.getCount()));
			}

			row.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(mContext,
							RegionPhotosSliderActivity.class);
					intent.putExtra("jsonString", jsonString);
					intent.putExtra("slide", pos);
					mContext.startActivity(intent);
				}
			});

			return row;
		}
	}

	static class ViewHolder {
		ImageView image;
		ProgressBar pBar;
	}

}
