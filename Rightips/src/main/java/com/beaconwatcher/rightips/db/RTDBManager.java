package com.beaconwatcher.rightips.db;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.widget.Toast;

import com.bw.libraryproject.entities.NotificationData;

public class RTDBManager {

	public static final String DATABASE_NAME = "rightips.db";
	public static final String DATABASE_TABLE = "notifications";
	public static final int DATABASE_VERSION = 8;

	public static final String KEY_ID = "id";
	public static final String KEY_NOTE_ID = "note_id";
	public static final int NOTE_ID_COLUMN = 1;
	public static final String KEY_TITLE = "title";
	public static final int TITLE_COLUMN = 2;
	public static final String KEY_DETAIL = "detail";
	public static final int DETAIL_COLUMN = 3;
	public static final String KEY_SITE_NAME = "site_name";
	public static final int SITE_NAME_COLUMN = 4;

	public static final String KEY_SITE_ADDRESS = "site_address";
	public static final int SITE_ADDRESS_COLUMN = 5;

	public static final String KEY_SITE_PHONE = "site_phone";
	public static final int SITE_PHONE_COLUMN = 6;

	public static final String KEY_START_DATE = "start_date";
	public static final int START_DATE_COLUMN = 7;
	public static final String KEY_END_DATE = "end_date";
	public static final int END_DATE_COLUMN = 8;
	public static final String KEY_TEMPLATE = "template";
	public static final int TEMPLATE_COLUMN = 9;
	public static final String KEY_ZONE = "zone";
	public static final int ZONE_COLUMN = 10;

	public static final String KEY_STATUS = "status";
	public static final int STATUS_COLUMN = 11;

	public static final String KEY_MSG_ID = "msg_id";
	public static final int MSG_ID_COLUMN = 12;

	public static final String KEY_LIKES = "likes";
	public static final int LIKES_COLUMN = 13;

	public static final String KEY_IMAGES = "images";
	public static final int IMAGES_COLUMN = 14;
	public static final String KEY_CREATION_DATE = "creation_date";
	public static final int CREATION_DATE_COLUMN = 15;

	public static final String KEY_DISCOUNT = "discount";
	public static final int DISCOUNT_COLUMN = 16;

	public static final String KEY_SOURCE = "source";
	public static final int SOURCE_COLUMN = 17;

	private SQLiteDatabase db;
	private final Context context;

	private RTDBHelper dbHelper;

	public RTDBManager(Context _context) {
		this.context = _context;

		// writeDBToFile();
		dbHelper = new RTDBHelper(context, DATABASE_NAME, null,
				DATABASE_VERSION);
	}

	public void open() throws SQLiteException {
		try {
			db = dbHelper.getWritableDatabase();
		} catch (SQLiteException ex) {
			db = dbHelper.getReadableDatabase();
		}
	}

	public void close() {
		db.close();
	}

	public boolean checkDBOpen() {
		if (db.isOpen()) {
			return true;
		}
		return false;
	}

	public void writeDBToFile() {
		File f = new File(
				"/data/data/com.beaconwatcher.rightips/databases/rightips.db");
		FileInputStream fis = null;
		FileOutputStream fos = null;

		try {
			fis = new FileInputStream(f);
			fos = new FileOutputStream("/mnt/sdcard/rightips.db");
			while (true) {
				int i = fis.read();
				if (i != -1) {
					fos.write(i);
				} else {
					break;
				}
			}
			fos.flush();
			Toast.makeText(context, "DB dump OK", Toast.LENGTH_LONG).show();
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(context, "DB dump ERROR", Toast.LENGTH_LONG).show();
		} finally {
			try {
				fos.close();
				fis.close();
			} catch (IOException ioe) {
			}
		}
	}

	// Insert a new task
	public long insertNotification(NotificationData note) {
		// Create a new row of values to insert.
		ContentValues newTaskValues = new ContentValues();
		// Assign values for each row.
		newTaskValues.put(KEY_NOTE_ID, note.getNoteID());
		newTaskValues.put(KEY_TITLE, note.getTitle());
		newTaskValues.put(KEY_DETAIL, note.getDetail());
		newTaskValues.put(KEY_SITE_NAME, note.getSiteName());
		newTaskValues.put(KEY_SITE_ADDRESS, note.getSiteAddress());
		newTaskValues.put(KEY_SITE_PHONE, note.getSitePhone());
		newTaskValues.put(KEY_START_DATE, note.getStartDate());
		newTaskValues.put(KEY_END_DATE, note.getEndDate());
		newTaskValues.put(KEY_TEMPLATE, note.getTemplate());
		newTaskValues.put(KEY_ZONE, note.getZone());
		newTaskValues.put(KEY_STATUS, note.getStatus());
		newTaskValues.put(KEY_MSG_ID, note.getMsgID());
		newTaskValues.put(KEY_LIKES, note.getLikes());
		newTaskValues.put(KEY_IMAGES, note.getImages());
		Date d = new Date();
		newTaskValues.put(KEY_CREATION_DATE, d.getTime());
		newTaskValues.put(KEY_DISCOUNT, note.getDiscount());
		newTaskValues.put(KEY_SOURCE, note.getSource());

		String noteID = note.getNoteID();
		if (notificationExists(noteID) == false) {
			// Insert the row.
			return db.insert(DATABASE_TABLE, null, newTaskValues);
		} else {
			// Insert the row.
			return db.update(DATABASE_TABLE, newTaskValues, KEY_NOTE_ID + "="
					+ noteID, null);
		}
	}

	// Remove a task based on its index public boolean removeTask(long
	// _rowIndex)

	public ArrayList<NotificationData> getAllNotifications(String limit) {
		ArrayList<NotificationData> arr = new ArrayList<NotificationData>();
		Cursor result = db.query(DATABASE_TABLE, new String[] { KEY_ID,
				KEY_NOTE_ID, KEY_TITLE, KEY_DETAIL, KEY_SITE_NAME,
				KEY_SITE_ADDRESS, KEY_SITE_PHONE, KEY_START_DATE, KEY_END_DATE,
				KEY_TEMPLATE, KEY_ZONE, KEY_STATUS, KEY_MSG_ID, KEY_LIKES,
				KEY_IMAGES, KEY_CREATION_DATE, KEY_DISCOUNT,
				KEY_SOURCE + " DESC" }, null,
		// null, null, null, KEY_CREATION_DATE + " DESC", limit.equals("0") ?
		// null
				null, null, null, KEY_CREATION_DATE, limit.equals("0") ? null

				: limit);
		if ((result.getCount() == 0)) {// || !result.moveToFirst()) {
			return arr;// throw new SQLException("No notification found.");
		}

		// NotificationData nd=
		while (result.moveToNext()) {
			arr.add(0, rowToNotification(result));
		}
		result.close();
		return arr;
	}

	private NotificationData rowToNotification(Cursor c) {
		NotificationData nd = new NotificationData();
		nd.setID(c.getInt(0));
		nd.setNoteID(c.getString(NOTE_ID_COLUMN));
		nd.setTitle(c.getString(TITLE_COLUMN));
		nd.setDetail(c.getString(DETAIL_COLUMN));
		nd.setSiteName(c.getString(SITE_NAME_COLUMN));
		nd.setSiteAddress(c.getString(SITE_ADDRESS_COLUMN));
		nd.setSitePhone(c.getString(SITE_PHONE_COLUMN));
		nd.setStartDate(c.getString(START_DATE_COLUMN));
		nd.setEndDate(c.getString(END_DATE_COLUMN));
		nd.setStatus(c.getString(STATUS_COLUMN));
		nd.setMsgID(c.getString(MSG_ID_COLUMN));
		nd.setLikes(c.getString(LIKES_COLUMN));
		nd.setZone(c.getString(ZONE_COLUMN));
		String imgStr = c.getString(IMAGES_COLUMN);
		nd.setCreationDate(c.getLong(CREATION_DATE_COLUMN));

		if (imgStr.length() > 0) {
			String[] imgsArr = imgStr.split(",");
			ArrayList<String> images = new ArrayList<String>();
			for (int i = 0; i < imgsArr.length; i++) {
				images.add(imgsArr[i]);
			}
			nd.setImages(images);
		}

		nd.setDiscount(c.getString(DISCOUNT_COLUMN));
		nd.setSource(c.getString(SOURCE_COLUMN));

		return nd;
	}

	public Cursor setCursorToNotifications(long _rowIndex) throws SQLException {
		Cursor result = db.query(true, DATABASE_TABLE, new String[] { KEY_ID,
				KEY_NOTE_ID, KEY_TITLE }, KEY_ID + "=" + _rowIndex, null, null,
				null, null, null);
		if ((result.getCount() == 0) || !result.moveToFirst()) {
			throw new SQLException("No notification found for row: "
					+ _rowIndex);
		}
		return result;
	}

	public NotificationData getNotification(long _rowIndex) throws SQLException {
		Cursor cursor = db.query(true, DATABASE_TABLE, new String[] { KEY_ID,
				KEY_TITLE }, KEY_ID + "=" + _rowIndex, null, null, null, null,
				null);
		if ((cursor.getCount() == 0) || !cursor.moveToFirst()) {
			throw new SQLException("No notification found for row: "
					+ _rowIndex);
		}

		String title = cursor.getString(TITLE_COLUMN);
		String detail = cursor.getString(DETAIL_COLUMN);

		NotificationData notif = new NotificationData();
		return notif;
	}

	public boolean notificationExists(String id) {

		String[] columns = { KEY_NOTE_ID };
		String selection = KEY_NOTE_ID + " =?";
		String[] selectionArgs = { id };
		String limit = "1";

		Cursor c = db.query(DATABASE_TABLE, columns, selection, selectionArgs,
				null, null, null, limit);
		boolean exists = (c.getCount() > 0);
		c.close();
		return exists;
	}
}