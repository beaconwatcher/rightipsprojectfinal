package com.beaconwatcher.rightips.db;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class RTDBHelper extends SQLiteOpenHelper {
	private SQLiteDatabase database;

	// SQL Statement to create a new database.
	private static final String DATABASE_CREATE = "create table "
			+ RTDBManager.DATABASE_TABLE + " (" + RTDBManager.KEY_ID
			+ " integer primary key autoincrement, " + RTDBManager.KEY_NOTE_ID
			+ " integer, " + RTDBManager.KEY_TITLE + " text not null, "
			+ RTDBManager.KEY_DETAIL + " text, " + RTDBManager.KEY_SITE_NAME
			+ " text, " + RTDBManager.KEY_SITE_ADDRESS + " text, "
			+ RTDBManager.KEY_SITE_PHONE + " text, "
			+ RTDBManager.KEY_START_DATE + " text, " + RTDBManager.KEY_END_DATE
			+ " text, " + RTDBManager.KEY_TEMPLATE + " text, "
			+ RTDBManager.KEY_ZONE + " text, " + RTDBManager.KEY_STATUS
			+ " text, " + RTDBManager.KEY_MSG_ID + " text, "
			+ RTDBManager.KEY_LIKES + " text, " + RTDBManager.KEY_IMAGES
			+ " text, " + RTDBManager.KEY_CREATION_DATE + " integer, "
			+ RTDBManager.KEY_DISCOUNT + " text, " + RTDBManager.KEY_SOURCE
			+ " text " + ");";

	public RTDBHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase _db) {
		_db.execSQL(DATABASE_CREATE);
		database = _db;
	}

	@Override
	public void onUpgrade(SQLiteDatabase _db, int _oldVersion, int _newVersion) {

		/*
		 * if(_newVersion > _oldVersion){ //First check if column not exist then
		 * add new column. if(!existsColumnInTable(_db,
		 * RTDBManager.KEY_SOURCE)){
		 * _db.execSQL("ALTER TABLE "+RTDBManager.DATABASE_TABLE
		 * +" ADD COLUMN "+RTDBManager.KEY_SOURCE+" TEXT DEFAULT ''"); }
		 * 
		 * }
		 */
		// Drop the old table.
		_db.execSQL("DROP TABLE IF EXISTS " + RTDBManager.DATABASE_TABLE);
		// Create a new one.
		onCreate(_db);
	}

	private boolean existsColumnInTable(SQLiteDatabase db, String columnToCheck) {
		try {
			// query 1 row
			Cursor mCursor = db.rawQuery("SELECT * FROM "
					+ RTDBManager.DATABASE_TABLE + " LIMIT 0", null);

			// getColumnIndex gives us the index (0 to ...) of the column -
			// otherwise we get a -1
			if (mCursor.getColumnIndex(columnToCheck) != -1)
				return true;
			else
				return false;

		} catch (Exception Exp) {
			// something went wrong. Missing the database? The table?
			Log.d("... - existsColumnInTable",
					"When checking whether a column exists in the table, an error occurred: "
							+ Exp.getMessage());
			return false;
		}
	}

	// THIS FUNCTION IS ADDED FOR DBMANAGER activity to track data on device
	public ArrayList<Cursor> getData(String Query) {
		// get writable database
		SQLiteDatabase sqlDB = this.getWritableDatabase();
		String[] columns = new String[] { "mesage" };
		// an array list of cursor to save two cursors one has results from the
		// query
		// other cursor stores error message if any errors are triggered
		ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
		MatrixCursor Cursor2 = new MatrixCursor(columns);
		alc.add(null);
		alc.add(null);

		try {
			String maxQuery = Query;
			// execute the query results will be save in Cursor c
			Cursor c = sqlDB.rawQuery(maxQuery, null);

			// add value to cursor2
			Cursor2.addRow(new Object[] { "Success" });

			alc.set(1, Cursor2);
			if (null != c && c.getCount() > 0) {

				alc.set(0, c);
				c.moveToFirst();

				return alc;
			}
			return alc;
		} catch (SQLException sqlEx) {
			Log.d("printing exception", sqlEx.getMessage());
			// if any exceptions are triggered save the error message to cursor
			// an return the arraylist
			Cursor2.addRow(new Object[] { "" + sqlEx.getMessage() });
			alc.set(1, Cursor2);
			return alc;
		} catch (Exception ex) {

			Log.d("printing exception", ex.getMessage());

			// if any exceptions are triggered save the error message to cursor
			// an return the arraylist
			Cursor2.addRow(new Object[] { "" + ex.getMessage() });
			alc.set(1, Cursor2);
			return alc;
		}

	}
}