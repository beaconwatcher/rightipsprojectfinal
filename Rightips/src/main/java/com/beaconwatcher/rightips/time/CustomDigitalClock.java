package com.beaconwatcher.rightips.time;

import java.util.Calendar;
import java.util.TimeZone;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

public class CustomDigitalClock extends TextView {
	private final static String TAG = "DigitalClock";

	private Calendar mCalendar;

	// private String mFormat = "yyyy.M.d E";
	private String mFormat = "HH:mm:ss:S";
	private TimeZone mTimeZone;

	private Runnable mTicker;
	private Handler mHandler;

	private boolean mTickerStopped = false;

	public CustomDigitalClock(Context context) {
		super(context);
		initClock(context);
	}

	public CustomDigitalClock(Context context, AttributeSet attrs) {
		super(context, attrs);
		initClock(context);
	}

	private void initClock(Context context) {
		if (mCalendar == null) {
			mCalendar = Calendar.getInstance();
		}
	}

	/*
	 * This function is added by Shahbaz to adjust time by timezone.s
	 */
	public void setTimeZone(String id) {
		mTimeZone = TimeZone.getTimeZone(id);
	}

	@Override
	protected void onAttachedToWindow() {
		mTickerStopped = false;
		super.onAttachedToWindow();
		mHandler = new Handler();

		mTicker = new Runnable() {
			public void run() {
				if (mTickerStopped)
					return;
				mCalendar.setTimeInMillis(System.currentTimeMillis());

				// Adjust Time Zone time in device local time
				if (mTimeZone != null) {
					// Adjust to GMT
					mCalendar.add(Calendar.MILLISECOND,
							-(mCalendar.getTimeZone().getRawOffset()));
					// Adjust to Daylight Savings
					mCalendar.add(Calendar.MILLISECOND, -mCalendar
							.getTimeZone().getDSTSavings());
					// Adjust to Offset
					mCalendar.add(Calendar.MILLISECOND,
							mTimeZone.getRawOffset());
				}

				// setText(mSimpleDateFormat.format(mCalendar.getTime()));
				setText(DateFormat.format(mFormat, mCalendar.getTime()));
				invalidate();
				long now = SystemClock.uptimeMillis();
				// long next = now + (1000 - now % 1000);
				long next = now + (1000 - System.currentTimeMillis() % 1000);

				// Debug
				Log.d(TAG, "" + now);
				Log.d(TAG, "" + next);
				Log.d(TAG, "" + mCalendar.getTimeInMillis());

				// TODO
				mHandler.postAtTime(mTicker, next);
			}
		};
		mTicker.run();
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		mTickerStopped = true;
	}

	public void setFormat(String format) {
		mFormat = format;
	}

}