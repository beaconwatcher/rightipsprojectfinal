package com.beaconwatcher.rightips.time;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.widget.TextView;

public class JodaDigitalClock extends TextView {
	private final static String TAG = "JodaDigitalClock";

	private DateTimeZone mTimeZone;
	private DateTime mJodaDateTime;
	private DateTimeFormatter mFormater = DateTimeFormat.forPattern("HH:mm:ss");

	private Runnable mTicker;
	private Handler mHandler;

	private boolean mTickerStopped = false;

	public JodaDigitalClock(Context context) {
		super(context);
		initClock(context);
	}

	public JodaDigitalClock(Context context, AttributeSet attrs) {
		super(context, attrs);
		initClock(context);
	}

	private void initClock(Context context) {
	}

	/*
	 * This function is added by Shahbaz to adjust time by timezone.s
	 */
	public void setTimeZone(String id) {
		mTimeZone = DateTimeZone.forID(id);
	}

	@Override
	protected void onAttachedToWindow() {
		mTickerStopped = false;
		super.onAttachedToWindow();
		mHandler = new Handler();

		mTicker = new Runnable() {
			public void run() {
				if (mTickerStopped)
					return;

				if (mTimeZone != null) {
					mJodaDateTime = new DateTime(mTimeZone);
				} else {
					mJodaDateTime = new DateTime();
				}
				setText(mJodaDateTime.toString("hh:mm:ss a"));

				invalidate();
				long now = SystemClock.uptimeMillis();
				// long next = now + (1000 - now % 1000);
				long next = now + (1000 - System.currentTimeMillis() % 1000);
				// TODO
				mHandler.postAtTime(mTicker, next);
			}
		};
		mTicker.run();
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		mTickerStopped = true;
	}

}