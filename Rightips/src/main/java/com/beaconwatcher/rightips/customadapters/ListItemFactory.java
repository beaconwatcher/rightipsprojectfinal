package com.beaconwatcher.rightips.customadapters;

import android.content.Context;
import android.view.ViewGroup;

import com.beaconwatcher.rightips.customadapters.concrete.ListItemBase;
import com.beaconwatcher.rightips.customadapters.concrete.ListItemLikes;
import com.beaconwatcher.rightips.customadapters.concrete.ListItemLocation;
import com.beaconwatcher.rightips.customadapters.concrete.ListItemPhotosGrid;
import com.beaconwatcher.rightips.customadapters.concrete.ListItemPhotosVertical;
import com.beaconwatcher.rightips.customadapters.concrete.ListItemReviews;
import com.beaconwatcher.rightips.customadapters.concrete.ListItemSearchResult;
import com.beaconwatcher.rightips.customadapters.concrete.ListItemUserWalls;
import com.beaconwatcher.rightips.entities.AppConstants;

public class ListItemFactory {
	private static ListItemFactory instance = null;

	private ListItemFactory() {
	}

	public static synchronized ListItemFactory getInstance() {
		if (instance == null) {
			instance = new ListItemFactory();
		}
		return instance;
	}

	public ListItemBase getListItem(Context context, String type,
			ViewGroup viewGroup) {
		if (type.equals(AppConstants.LIST_ITEM_TYPE_REVIEW)) {
			return new ListItemReviews(context, type);
		} else if (type.equals(AppConstants.LIST_ITEM_USER_WALLS)) {
			return new ListItemUserWalls(context, type);
		} else if (type.equals(AppConstants.LIST_ITEM_TYPE_LIKES)) {
			return new ListItemLikes(context, type);
		} else if (type.equals(AppConstants.LIST_ITEM_TYPE_PHOTOS_VERTICAL)) {
			return new ListItemPhotosVertical(context, type);// ReviewItemView.inflate(viewGroup);
		} else if (type.equals(AppConstants.LIST_ITEM_TYPE_PHOTOS_GRID)) {
			return new ListItemPhotosGrid(context, type);
		}

		else if (type.equals(AppConstants.LIST_ITEM_LOCATIONS)) {
			return new ListItemLocation(context, type);
		}

		else if (type.equals(AppConstants.LIST_ITEM_SEARCH_RESULTS)) {
			return new ListItemSearchResult(context, type);
		}

		return null;
	}

}
