package com.beaconwatcher.rightips.customadapters.concrete;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.TextView;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.ListItemData;

public class ListItemLocation extends ListItemBase {
	private TextView mTitle;

	public ListItemLocation(Context context, String type) {
		super(context, type);
		LayoutInflater.from(context).inflate(R.layout.list_item_location, this,
				true);
		setupChildren();
	}

	protected void setupChildren() {
		mTitle = (TextView) findViewById(R.id.txt_title);
	}

	@Override
	public void setItem(final Object obj) {
		super.setItem(obj);
		ListItemData item = (ListItemData) obj;
		if (item.name != null && !item.name.equals("")
				&& !item.name.equals("null")) {
			mTitle.setText(item.name);
		}
	}
}
