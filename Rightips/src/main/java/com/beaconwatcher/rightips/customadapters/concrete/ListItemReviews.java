package com.beaconwatcher.rightips.customadapters.concrete;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.customviews.UserNameTextView;
import com.beaconwatcher.rightips.entities.ListItemData;
import com.bw.libraryproject.utils.DateUtilities;

public class ListItemReviews extends ListItemBase {
	private Context mContext;
	private UserNameTextView mTitle;
	private TextView mText;
	private TextView mTxtLikes;
	private TextView mDuration;
	private ImageView mProfile;
	private ImageView mImage;

	public ListItemReviews(Context context, String type) {
		super(context, type);
		mContext = context;
		LayoutInflater.from(context).inflate(R.layout.list_item_reviews, this,
				true);
		setupChildren();
	}

	protected void setupChildren() {
		mTitle = (UserNameTextView) findViewById(R.id.txt_username);
		mText = (TextView) findViewById(R.id.txt_comment);
		mDuration = (TextView) findViewById(R.id.txt_duration);
		mProfile = (ImageView) findViewById(R.id.img_profile);
		mImage = (ImageView) findViewById(R.id.img);
		mTxtLikes = (TextView) findViewById(R.id.txt_likes);

		findViewById(R.id.btn_like).setOnClickListener(mClickListener);
		findViewById(R.id.btn_dots).setOnClickListener(mClickListener);
	}

	@Override
	public void setItem(Object obj) {
		super.setItem(obj);
		ListItemData item = (ListItemData) obj;
		if (mTitle != null) {
			mTitle.setText(item.name);
			mTitle.setUserID(Integer.toString(item.uid));
		}

		if (mText != null)
			mText.setText(item.text);

		if (item.profile_pic != null && !item.profile_pic.equals("")
				&& !item.profile_pic.equals("null")) {
			loadImage(mProfile, item.profile_pic, null);
		} else {
			mProfile.setImageBitmap(null);
		}

		if (item.img != null && !item.img.equals("")
				&& !item.img.equals("null")) {
			loadImage(mImage, item.img, findViewById(R.id.loader_list_item));
		} else {
			mImage.setImageBitmap(null);
		}

		if (mDuration != null && item.created != 0)
			mDuration.setText(DateUtilities.getDuration(Integer
					.toString(item.created)));
		if (mTxtLikes != null)
			updatLikesText(mContext, mTxtLikes, item);
	}
}
