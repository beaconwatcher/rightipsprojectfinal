package com.beaconwatcher.rightips.customadapters.concrete;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.ListItemData;
import com.beaconwatcher.rightips.events.ApplicationEvent;
import com.beaconwatcher.rightips.interfaces.ListItemInterface;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import de.greenrobot.event.EventBus;

public class ListItemBase extends RelativeLayout {
	public String type;
	private Object item;
	public ListItemInterface mInterface;

	public ListItemBase(Context c, String type) {
		super(c);
		this.type = type;
	}

	// Abstract Method
	public void setItem(Object item) {
		this.item = item;
	}

	OnClickListener mClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.btn_dots:
				if (item != null)
					EventBus.getDefault()
							.post(new ApplicationEvent(
									AppConstants.LIST_SHOW_ACTIONS, item, type));
				break;

			case R.id.btn_like:
				if (item != null)
					EventBus.getDefault().post(
							new ApplicationEvent(AppConstants.LIKE_LIST_ITEM,
									item, type));
				break;

			case R.id.img:
				if (item != null)
					EventBus.getDefault().post(
							new ApplicationEvent(
									AppConstants.ACTION_SHOW_PREVIEW_IMAGE,
									item, type));
				break;

			default:
				if (item != null)
					EventBus.getDefault()
							.post(new ApplicationEvent(
									AppConstants.LIST_ITEM_CLICKED, item, type));
			}
		}
	};

	public void loadImage(final ImageView img, final String url,
			final View preloader) {
		if (preloader != null)
			preloader.setVisibility(View.VISIBLE);
		if (img.getWidth() > 0) {
			loadPicassoImage(img, url, preloader);
		}

		else
			img.addOnLayoutChangeListener(new OnLayoutChangeListener() {
				@Override
				public void onLayoutChange(View v, int left, int top,
						int right, int bottom, int oldLeft, int oldTop,
						int oldRight, int oldBottom) {
					// TODO Auto-generated method stub
					img.removeOnLayoutChangeListener(this);
					loadPicassoImage(img, url, preloader);
				}
			});
	}

	private void loadPicassoImage(ImageView img, String url,
			final View preloader) {
		Picasso.with(img.getContext()).load(url).resize(img.getWidth(), 0)
				.into(img, new Callback() {
					@Override
					public void onSuccess() {
						if (preloader != null)
							preloader.setVisibility(View.INVISIBLE);
					}

					@Override
					public void onError() {
					}

				});
	}

	public void updatLikesText(Context c, TextView tv, ListItemData data) {
		String str = "";
		if (data.is_liked.equals("1")) {
			str = c.getResources().getString(R.string.wall_comment_txt_likes)
					+ "(" + data.likes + ")";
			tv.setTextColor(Color.parseColor("#FF1C50"));
		} else {
			tv.setTextColor(Color.parseColor("#000000"));
			if (data.likes > 0) {
				str = c.getResources().getString(
						R.string.wall_comment_txt_likes)
						+ "(" + data.likes + ")";
			} else {
				str = c.getResources()
						.getString(R.string.wall_comment_txt_like);
			}
		}
		tv.setText(str);
	}

}
