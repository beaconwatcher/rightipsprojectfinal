package com.beaconwatcher.rightips.customadapters.concrete;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.ListItemData;

public class ListItemPhotosVertical extends ListItemBase {
	private Context mContext;
	private TextView mTitle;
	private TextView mTxtLikes;
	private ImageView mProfile;
	private ImageView mImage;
	private ToggleButton mBtnHeart;

	public ListItemPhotosVertical(Context context, String type) {
		super(context, type);
		mContext = context;
		LayoutInflater.from(context).inflate(
				R.layout.list_item_photos_vertical, this, true);
		setupChildren();
	}

	protected void setupChildren() {
		mTitle = (TextView) findViewById(R.id.txt_username);
		mProfile = (ImageView) findViewById(R.id.img_profile);
		mImage = (ImageView) findViewById(R.id.img);
		mTxtLikes = (TextView) findViewById(R.id.txt_likes);
		mBtnHeart = (ToggleButton) findViewById(R.id.btn_heart);
		findViewById(R.id.btn_like).setOnClickListener(mClickListener);
		findViewById(R.id.btn_dots).setOnClickListener(mClickListener);
		mImage.setOnClickListener(mClickListener);
	}

	@Override
	public void setItem(Object obj) {
		super.setItem(obj);
		ListItemData item = (ListItemData) obj;
		// TODO Auto-generated method stub
		super.setItem(item);
		loadImage(mImage, item.img, findViewById(R.id.loader_list_item));

		if (mTxtLikes != null)
			updatLikesText(mContext, mTxtLikes, item);
	}

	@Override
	public void updatLikesText(Context c, TextView tv, ListItemData data) {
		// TODO Auto-generated method stub
		if (data.is_liked.equals("1")) {
			tv.setTextColor(Color.parseColor("#FF1C50"));
			mBtnHeart.setChecked(true);
		} else {
			tv.setTextColor(Color.parseColor("#000000"));
			mBtnHeart.setChecked(false);
		}
		tv.setText(Integer.toString(data.likes));
	}
}
