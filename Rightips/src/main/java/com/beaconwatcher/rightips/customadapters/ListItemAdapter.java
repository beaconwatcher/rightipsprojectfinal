package com.beaconwatcher.rightips.customadapters;

import java.lang.reflect.Type;
import java.util.ArrayList;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.beaconwatcher.rightips.customadapters.concrete.ListItemBase;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.ListItemData;
import com.beaconwatcher.rightips.events.ApplicationEvent;
import com.beaconwatcher.rightips.events.EventBusEvent;

import de.greenrobot.event.EventBus;

public class ListItemAdapter extends BaseAdapter {
	String mType;
	ArrayList<?> mArray;
	Type mDataType;

	ListItemNotifier mNotifier;

	public interface ListItemNotifier {
		public void onItemClicked(Object item);
	}

	public ListItemAdapter(String type, ArrayList<?> arr) {
		mArray = arr;
		mType = type;

		EventBus.getDefault().register(this);
	}

	public ListItemAdapter(String type, ArrayList<?> arr, ListView mList,
			ListItemNotifier notif) {
		mArray = arr;
		mType = type;
		mNotifier = notif;

		mList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				if (mNotifier != null) {
					mNotifier.onItemClicked(getItem(position));
				}
			}
		});

		EventBus.getDefault().register(this);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mArray.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mArray.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void removeItemById(String id) {
		for (int i = 0; i < mArray.size(); i++) {
			ListItemData item = (ListItemData) mArray.get(i);
			if (id.equals(Integer.toString(item.id))) {
				mArray.remove(i);
				notifyDataSetChanged();
				EventBus.getDefault().post(
						new ApplicationEvent(AppConstants.LIST_ITEM_REMOVED,
								null, ""));
			}
		}
	}

	public void updateLikes(String id, int likes, String isLiked) {
		for (int i = 0; i < mArray.size(); i++) {
			ListItemData item = (ListItemData) mArray.get(i);
			if (id.equals(Integer.toString(item.id))) {
				item.likes = likes;
				item.is_liked = isLiked;
				notifyDataSetChanged();
			}
		}
	}

	public void onEvent(EventBusEvent event) {
		if (event.getType().equals(AppConstants.REMOVE_LIST_ITEM)) {
			ArrayList<ListItemData> arr = (ArrayList<ListItemData>) event
					.getData();
			String itemID = Integer.toString(arr.get(0).id);
			removeItemById(itemID);
		}

		else if (event.getType().equals(AppConstants.LIKE_LIST_ITEM)) {
			ArrayList<ListItemData> arr = (ArrayList<ListItemData>) event
					.getData();
			String itemID = Integer.toString(arr.get(0).id);
			int likes = arr.get(0).likes;
			updateLikes(itemID, likes, arr.get(0).is_liked);
		}
	}

	@Override
	public View getView(int i, View reuseableView, ViewGroup viewGroup) {
		ListItemBase itemView = (ListItemBase) reuseableView;
		if (null == itemView) {
			itemView = ListItemFactory.getInstance().getListItem(
					viewGroup.getContext(), mType, viewGroup);
		}
		itemView.setItem(getItem(i));
		return itemView;
	}
}
