package com.beaconwatcher.rightips.customadapters.concrete;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.CommentItem;
import com.bw.libraryproject.utils.DateUtilities;

public class ListItemUserWalls extends ListItemBase {
	private Context mContext;
	private TextView mTitle;
	private TextView mText;
	private TextView mDuration;
	private ImageView mProfile;

	public ListItemUserWalls(Context context, String type) {
		super(context, type);
		mContext = context;
		LayoutInflater.from(context).inflate(R.layout.list_item_user_wall,
				this, true);
		setupChildren();
	}

	protected void setupChildren() {
		mTitle = (TextView) findViewById(R.id.txt_title);
		mText = (TextView) findViewById(R.id.txt_comment);
		mDuration = (TextView) findViewById(R.id.txt_duration);
		mProfile = (ImageView) findViewById(R.id.img_profile);
		setOnClickListener(mClickListener);
	}

	@Override
	public void setItem(Object obj) {
		super.setItem(obj);
		CommentItem item = (CommentItem) obj;
		if (mTitle != null) {
			mTitle.setText(item.getMessage());
		}

		if (mText != null)
			mText.setText(item.getComment());

		if (item.getProfilePic() != null && !item.getProfilePic().equals("")
				&& !item.getProfilePic().equals("null"))
			loadImage(mProfile, item.getProfilePic(), null);
		else
			mProfile.setImageBitmap(null);

		if (item.getCreated() != null)
			mDuration.setText(DateUtilities.getDuration(item.getCreated()));
	}

}
