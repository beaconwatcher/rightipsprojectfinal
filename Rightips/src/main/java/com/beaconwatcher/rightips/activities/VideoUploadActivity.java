package com.beaconwatcher.rightips.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazon.aws.AWSConstants;
import com.amazon.aws.AWSUtility;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.customadapters.ListItemAdapter;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.CommentItem;
import com.beaconwatcher.rightips.events.EventBusEvent;
import com.beaconwatcher.rightips.services.IntentServiceFactory;
import com.bw.libraryproject.activities.InternetActivity;
import com.bw.libraryproject.events.intentservice.IntentServiceEvent;

import org.w3c.dom.Text;

import java.io.File;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class VideoUploadActivity extends InternetActivity{

    private static String TAG = "VideoUploadActivity";
    private Context mContext;
    private RighTipsApplication mApp;

    private TransferUtility mTransferUtility;
    private ProgressBar mProgressBar;
    private TextView mText;
    private RelativeLayout mLoader;

    private File mSelectedFile;
    private String mNoteID;
    private boolean uploadComplete=false;
    private String uploadedUrl="";



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_upload);

        mContext=this;
        mApp = (RighTipsApplication) this.getApplicationContext();

        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mText = (TextView) findViewById(R.id.txt_uploading);
        mLoader = (RelativeLayout) findViewById(R.id.loader);


        if(this.getIntent().hasExtra("file")){
            File file=(File)this.getIntent().getSerializableExtra("file");
            uploadFile(file);
        }
        else{
            this.setResult(RESULT_CANCELED);
            finish();
        }

        if(getIntent().hasExtra(AppConstants.NOTIFICATION_ID)){
            mNoteID = getIntent().getStringExtra(AppConstants.NOTIFICATION_ID);
        }

    }

    private void uploadFile(File file) {
        mSelectedFile = file;
        mTransferUtility=AWSUtility.getTransferUtility(mContext);
        TransferObserver observer = mTransferUtility.upload(AWSConstants.BUCKET_NAME, "videos/android/"+file.getName(), file);
        observer.setTransferListener(new UploadListener());
    }

    /*
     * A TransferListener class that can listen to a upload task and be notified
     * when the status changes.
     */
    private class UploadListener implements TransferListener {

        // Simply updates the UI list when notified.
        @Override
        public void onError(int id, Exception e) {
            Log.e(TAG, "Error during upload: " + id, e);

        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            Log.d(TAG, String.format("onProgressChanged: %d, total: %d, current: %d",
                    id, bytesTotal, bytesCurrent));
            float percent = (float) ((bytesCurrent*100)/bytesTotal);
            mProgressBar.setProgress((int)percent);


            //uploadComplete is added to make sure webservice is called only once.
            if(percent == 100 && uploadComplete==false){
                uploadComplete = true;
                saveVideoReference();
            }
        }

        @Override
        public void onStateChanged(int id, TransferState newState) {
            Log.d(TAG, "onStateChanged: " + id + ", " + newState);

        }
    }

    private void saveVideoReference(){
        uploadedUrl="https://s3-eu-west-1.amazonaws.com/rtimage/videos/android/"+mSelectedFile.getName();
        showLoader(true);
        Class mClazz = IntentServiceFactory.getInstance()
                .getIntentServiceClass(AppConstants.ACTION_SAVE_VIDEO_IN_DB);
        Intent intent = new Intent(mContext, mClazz);
        intent.putExtra(AppConstants.INTENT_SERVICE_TAG, TAG);
        intent.putExtra(AppConstants.USER_ID, mApp.getUID());
        intent.putExtra(AppConstants.NOTIFICATION_ID, mNoteID);
        intent.putExtra(AppConstants.VIDEO_URL, uploadedUrl);
        intent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
                AppConstants.ACTION_SAVE_VIDEO_IN_DB);
        mContext.startService(intent);
    }

    public void onEvent(IntentServiceEvent event) {
        showLoader(false);
        //Toast.makeText(mContext, event.message, Toast.LENGTH_LONG).show();
        Intent intent=new Intent();
        intent.putExtra("uploadedUrl", uploadedUrl);
        this.setResult(Activity.RESULT_OK, intent);
        finish();
    }
}
