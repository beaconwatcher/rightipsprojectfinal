package com.beaconwatcher.rightips.activities;
import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialcamera.MaterialCamera;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.entities.AppConstants;

import java.io.File;
import java.text.DecimalFormat;

/**
 * @author Shahbaz Ali(RighTips)
 */
public class VideoRecordingActivity extends Activity{

    private final static int CAMERA_RQ = 6969;
    private final static int PERMISSION_RQ = 84;
    private final static int UPLOAD_RQ = 696969;

    private RighTipsApplication mApp;
    private Context mContext;

    private String mNotificationID="";
    private File mVideoFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_recording);

        mApp=(RighTipsApplication) getApplicationContext();
        mContext=this;


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            // Request permission to save videos in external storage
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_RQ);
        }
        else{
            startRecording();
        }

        if(getIntent().hasExtra(AppConstants.NOTIFICATION_ID)){
            mNotificationID=getIntent().getStringExtra(AppConstants.NOTIFICATION_ID);
        }



    }

    private void startRecording(){
        File saveDir = null;
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            // Only use external storage directory if permission is granted, otherwise cache directory is used by default
            saveDir = new File(Environment.getExternalStorageDirectory(), "RighTips_Videos");
            saveDir.mkdirs();
        }

        new MaterialCamera(this)
                .saveDir(saveDir)
                .showPortraitWarning(false)
                .iconRecord(R.drawable.icon_start_recording)
                .iconStop(R.drawable.icon_stop_recording)
                //.countdownSeconds(20)
                .qualityProfile(MaterialCamera.QUALITY_480P)
                .videoEncodingBitRate(1500000)
                .videoFrameRate(20)
                .start(CAMERA_RQ);
    }


    private String readableFileSize(long size) {
        if (size <= 0) return size + " B";
        final String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.##").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    private String fileSize(File file) {
        return readableFileSize(file.length());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Received recording or error from MaterialCamera
        if (requestCode == CAMERA_RQ) {
            if (resultCode == RESULT_OK) {
                mVideoFile = new File(data.getData().getPath());
                String fsize=fileSize(mVideoFile);
                String fpath=mVideoFile.getAbsolutePath();
                uploadVideo(mVideoFile);

            } else if (data != null) {
                Exception e = (Exception) data.getSerializableExtra(MaterialCamera.ERROR_EXTRA);
                if (e != null) {
                    e.printStackTrace();
                    Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
            //Back button pressed on video recording.
            else{
                finish();
            }
        }

        else if(requestCode == UPLOAD_RQ){
            if (resultCode == RESULT_OK) {
                Toast.makeText(mContext, "Your video is uploaded successfuly", Toast.LENGTH_LONG).show();
                Intent resultIntent=new Intent();
                resultIntent.putExtra("path", mVideoFile.getAbsolutePath());
                resultIntent.putExtra("uploadedUrl", data.getStringExtra("uploadedUrl"));
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }
            else {
                Toast.makeText(mContext, "Error uploading video, please try again", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //Permission not granted.
        if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            // Sample was denied WRITE_EXTERNAL_STORAGE permission
            Toast.makeText(this, "Videos will be saved in a cache directory instead of an external storage directory since permission was denied.", Toast.LENGTH_LONG).show();
        }

        startRecording();
    }


    private void uploadVideo(File f){
        Intent uploadIntent = new Intent(mContext, VideoUploadActivity.class);
        uploadIntent.putExtra("file", f);
        if(!mNotificationID.equals("")){
            uploadIntent.putExtra(AppConstants.NOTIFICATION_ID, mNotificationID);
        }
        startActivityForResult(uploadIntent, UPLOAD_RQ);
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_CANCELED);
        super.onBackPressed();
    }


    //This function upload file to UploadCare sdk
    /*
    private void uploadVideo(File file){
        mApp.showDialog(this, "Uploading Video", "Uploading your video, please wait....");

        UploadcareClient client = new UploadcareClient("fb46752c25951401101a", "9582cc2c508c2186f6e9");

        //Uri fileUri = Uri.fromFile(file);
        Uploader uploader = new FileUploader(client, file)
                .store(true);
        uploader.uploadAsync(new UploadcareFileCallback() {
            @Override
            public void onFailure(UploadcareApiException e) {
                Toast.makeText(VideoRecordingActivity.this, "Failed to Upload", Toast.LENGTH_LONG).show();
                finish();
                //handle errors.
            }

            @Override
            public void onSuccess(UploadcareFile file) {
                //successfully uploaded file to Uploadcare.
                Toast.makeText(VideoRecordingActivity.this, "Your file uploaded successfuly", Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }*/
}
