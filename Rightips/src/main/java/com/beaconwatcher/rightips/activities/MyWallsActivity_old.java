package com.beaconwatcher.rightips.activities;

import java.util.ArrayList;

import android.app.ActionBar;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.CommentItem;
import com.beaconwatcher.rightips.services.LoadNotificationCommentsService;
import com.bw.libraryproject.activities.InternetActivity;
import com.bw.libraryproject.customrenderers.CircularImageView;
import com.squareup.picasso.Picasso;

public class MyWallsActivity_old extends InternetActivity {
	private static String ERROR_TAG = "My Walls";
	private static String TAG = "MyWallsActivity";

	private RighTipsApplication mApp;
	private Context mContext;
	private LayoutInflater mInflater;

	private FrameLayout mBtnBack;
	private TextView mTitle;
	private ListView mCommentsList;

	// Data
	private ArrayList<CommentItem> mCommentsArray = new ArrayList<CommentItem>();
	private MyWallsAdapter mAdapter;

	private String userID = "0";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mApp = (RighTipsApplication) getApplicationContext();
		mContext = MyWallsActivity_old.this;

		mInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		setContentView(R.layout.activity_my_walls);

		View topBarView = getLayoutInflater().inflate(
				R.layout.titlebar_with_comment_back, null);
		getActionBar().setDisplayShowCustomEnabled(true);
		ActionBar.LayoutParams params = new ActionBar.LayoutParams(
				ActionBar.LayoutParams.FILL_PARENT,
				ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);
		getActionBar().setCustomView(topBarView, params);

		mTitle = (TextView) findViewById(R.id.tf_note_title);
		mTitle.setText(this.getResources().getString(R.string.lbl_my_walls));

		// Get UI
		mLoader = (RelativeLayout) findViewById(R.id.loader);
		mBtnBack = (FrameLayout) findViewById(R.id.btn_back);
		// mList = (ListView) findViewById(R.id.list_comments);
		mCommentsList = (ListView) findViewById(R.id.walls_list);

		// Set button click listeners
		mBtnBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		// Register Receivers
		addReceiver(wallServiceBroadcastReceiver, wallFilter);
	}

	IntentFilter wallFilter = new IntentFilter(
			AppConstants.WALL_COMMENT_SERVICE_COMPLETED);
	BroadcastReceiver wallServiceBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			showLoader(false);
			String msg = intent
					.getStringExtra(AppConstants.INTENT_SERVICE_MESSAGE);
			if (msg != null) {
				mApp.showToast(mContext, msg, Toast.LENGTH_SHORT);
				return;
			}
			String errorMsg = intent
					.getStringExtra(AppConstants.INTENT_SERVICE_ERROR_MESSAGE);
			String actionType = intent
					.getStringExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE);
			if (errorMsg != null) {
				mApp.showErrorToast(mContext, ERROR_TAG);
				return;
			}

			/*
			 * if(actionType.equals(AppConstants.ACTION_WALL_MY_WALLS_LOADED)){
			 * mCommentsArray
			 * =intent.getParcelableArrayListExtra(AppConstants.INTENT_SERVICE_DATA
			 * ); Log.d(TAG, mCommentsArray.toString());
			 * if(mCommentsArray!=null){ mAdapter = new
			 * MyWallsAdapter(mCommentsArray, mContext);
			 * mCommentsList.setAdapter(mAdapter); //
			 * mAdapter.notifyDataSetChanged(); } }
			 */
		}
	};

	@Override
	protected void onResume() {
		super.onResume();
		userID = getIntent().getStringExtra("uid");
		if (userID != null && !userID.equals("0")) {
			showLoader(true);
			getAllWalls(userID);
		}
	}

	private void getAllWalls(String uid) {
		Log.d(TAG, "starting service to get all walls");

		Intent serviceIntent = new Intent(mContext,
				LoadNotificationCommentsService.class);
		// serviceIntent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
		// AppConstants.ACTION_WALL_GET_MY_WALLS);
		serviceIntent.putExtra("uid", uid);
		startService(serviceIntent);
	}

	public class MyWallsAdapter extends BaseAdapter {
		ViewHolder viewHolder;
		Context context;
		ArrayList<CommentItem> mDataArray;

		public MyWallsAdapter(ArrayList<CommentItem> arr, Context c) {
			mDataArray = arr;
			context = c;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mDataArray.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return mDataArray.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return Long.parseLong(mDataArray.get(position).getMessageID());
		}

		@Override
		public View getView(int i, View reuseableView, ViewGroup viewGroup) {
			final CommentItem cItem = (CommentItem) getItem(i);

			if (reuseableView == null) {
				reuseableView = mInflater.inflate(R.layout.my_walls_item, null);
				/*
				 * reuseableView.setLayoutParams(new AbsListView.LayoutParams(
				 * AbsListView.LayoutParams.MATCH_PARENT, getResources()
				 * .getDimensionPixelSize( R.dimen.contact_item_height)));
				 */
				viewHolder = new ViewHolder();

				// cache the views
				viewHolder.tfTitle = (TextView) reuseableView
						.findViewById(R.id.txt_title);
				viewHolder.tfDuration = (TextView) reuseableView
						.findViewById(R.id.txt_duration);
				viewHolder.tfComment = (TextView) reuseableView
						.findViewById(R.id.txt_comment);
				viewHolder.imgContainer = (RelativeLayout) reuseableView
						.findViewById(R.id.img_container);

				CircularImageView img = (CircularImageView) reuseableView
						.findViewById(R.id.user_img);
				img.setBorderWidth(2);
				;
				viewHolder.img = img;
				/*
				 * viewHolder.contact = (TextView) reuseableView
				 * .findViewById(R.id.txt_contact);
				 */
				// link the cached views to the convertview
				reuseableView.setTag(viewHolder);

				reuseableView
						.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
							@Override
							public void onLayoutChange(View v, int left,
									int top, int right, int bottom,
									int oldLeft, int oldTop, int oldRight,
									int oldBottom) {
								if (v.getWidth() > 0) {
									v.removeOnLayoutChangeListener(this);
									if (!cItem.getProfilePic().equals("")
											&& !cItem.getProfilePic().equals(
													"null")) {
										Picasso.with(mContext)
												.load(cItem.getProfilePic())
												.resize(viewHolder.img
														.getWidth(),
														viewHolder.img
																.getHeight())
												.into(viewHolder.img);
									}
								}
							}
						});

				reuseableView.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(mContext,
								NotificationWallCommentActivity.class);
						intent.putExtra("uid", userID);
						intent.putExtra("msg_id", cItem.getMessageID());
						intent.putExtra("title", cItem.getMessage());
						startActivity(intent);
					}
				});

			} else {
				viewHolder = (ViewHolder) reuseableView.getTag();
			}

			String noteTitle = cItem.getMessage();
			viewHolder.tfTitle.setText(noteTitle);

			if (cItem.getComment().equals("")) {
				viewHolder.tfComment.setVisibility(View.INVISIBLE);
				if (!cItem.getComImage().equals("")) {
					viewHolder.imgContainer.setVisibility(View.VISIBLE);
				}
			} else {
				viewHolder.tfComment.setText(cItem.getComment());
			}

			long miliseconds = Long.parseLong(cItem.getCreated() + "000");
			CharSequence charSequence = DateUtils
					.getRelativeTimeSpanString(miliseconds);
			StringBuilder sb = new StringBuilder(charSequence.length());
			sb.append(charSequence);
			String duration = sb.toString();
			viewHolder.tfDuration.setText(duration);

			return reuseableView;
		}

		// class for caching the views in a row
		private class ViewHolder {
			TextView tfTitle, tfComment, tfDuration;
			RelativeLayout imgContainer;
			ImageView img;
		}
	}

}
