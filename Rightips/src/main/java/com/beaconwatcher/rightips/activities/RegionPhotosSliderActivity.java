package com.beaconwatcher.rightips.activities;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.customslides.NotificationSliderView;
import com.beaconwatcher.rightips.entities.PanoramioPhoto;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;


public class RegionPhotosSliderActivity extends Activity {
	private Context mContext;
	private LayoutInflater inflater;
	private View rootView;
	private SliderLayout mSlider;

	private String slideIDToShow = "";
	private int slideIndexToShow = -1;
	private ArrayList<PanoramioPhoto> mPhotoArray = new ArrayList<PanoramioPhoto>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mContext = this;
		setContentView(R.layout.fragment_slider);
		mSlider = (SliderLayout) this.findViewById(R.id.slider);

		String jsonString = getIntent().getStringExtra("jsonString");
		slideIndexToShow = getIntent().getIntExtra("slide", -1);
		if (jsonString != null) {
			prepareSlides(jsonString);

		}
		/*
		 * getIntent() .getIntExtra(LoginActivity.EXTRAS_LOGIN_TYPE, -1);
		 */

	}

	private void prepareSlides(String response) {
		JSONObject jo;
		try {
			jo = new JSONObject(response);
			if (jo.has("photos")) {
				JSONArray arr = jo.getJSONArray("photos");
				for (int i = 0; i < arr.length(); i++) {
					JSONObject jPhoto = arr.getJSONObject(i);
					PanoramioPhoto photo = new PanoramioPhoto();
					photo.photo_id = jPhoto.getInt("photo_id");
					photo.photo_title = jPhoto.getString("photo_title");
					photo.photo_url = jPhoto.getString("photo_file_url");
					mPhotoArray.add(photo);
				}

				addSlides();

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void addSlides() {
		mSlider.removeAllSliders();

		for (int i = 0; i < mPhotoArray.size(); i++) {
			PanoramioPhoto photo = mPhotoArray.get(i);
			DefaultSliderView slide = new DefaultSliderView(mContext);
			slide.image(photo.photo_url).setScaleType(
					NotificationSliderView.ScaleType.CenterCrop);
			mSlider.addSlider(slide);
		}

		if (slideIndexToShow != -1) {
			mSlider.setCurrentPosition(slideIndexToShow);
			slideIndexToShow = 0;
		}
	}

}
