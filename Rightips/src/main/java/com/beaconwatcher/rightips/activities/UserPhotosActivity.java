package com.beaconwatcher.rightips.activities;

import android.app.ActionBar;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.customviews.CustomToggleGroup;
import com.beaconwatcher.rightips.customviews.CustomToggleGroup.OnCheckedChangeListener;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.ListItemData;
import com.beaconwatcher.rightips.events.ApplicationEvent;
import com.beaconwatcher.rightips.fragments.FragmentPreviewImage;
import com.beaconwatcher.rightips.fragments.FragmentUserPhotos;

public class UserPhotosActivity extends RighTipsListActivity {
	private static final String ERROR_TAG = "User Profile Photos";

	private RighTipsApplication mApp;
	private Context mContext;
	private Intent serviceIntent;

	// Views
	private View mMainContainer;
	private LayoutInflater mInflater;

	// Fragments
	FragmentUserPhotos mPhotosFragment;

	// Data
	private String mUserID;
	private String mUserName;
	private String mProfilePic;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		TAG = "UserPhotosActivity";
		mApp = (RighTipsApplication) getApplicationContext();
		mContext = UserPhotosActivity.this;
		mInflater = getLayoutInflater();

		mFragmentManager = getFragmentManager();

		setContentView(R.layout.activity_user_photos);

		View titleBar = getLayoutInflater().inflate(
				R.layout.titlebar_user_photos, null);
		getActionBar().setDisplayShowCustomEnabled(true);
		ActionBar.LayoutParams params = new ActionBar.LayoutParams(
				ActionBar.LayoutParams.FILL_PARENT,
				ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);
		getActionBar().setCustomView(titleBar, params);

		// Get UI
		mMainContainer = findViewById(R.id.main_container);
		mPhotosFragment = (FragmentUserPhotos) getFragmentManager()
				.findFragmentById(R.id.fragment_photos);

		CustomToggleGroup grp = (CustomToggleGroup) findViewById(R.id.tab_bar);
		grp.setOnCheckedChangeListener(tabListener);
		findViewById(R.id.btn_back).setOnClickListener(mClickListener);

		Intent intent = getIntent();
		if (intent != null) {
			mUserID = intent.getExtras().getString(AppConstants.USER_ID);
			mUserName = intent.getExtras().getString(AppConstants.USER_NAME);
			mProfilePic = intent.getExtras().getString(
					AppConstants.USER_PROFILE_PIC);
			switchMode(AppConstants.PROFILE_PHOTOS_MODE_GRID);
		}
	}

	OnCheckedChangeListener tabListener = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CustomToggleGroup group, int checkedId) {
			// TODO Auto-generated method stub
			// TODO Auto-generated method stub
			switch (checkedId) {
			case R.id.btn_grid:
				switchMode(AppConstants.PROFILE_PHOTOS_MODE_GRID);
				break;

			case R.id.btn_vertical:
				switchMode(AppConstants.PROFILE_PHOTOS_MODE_VERTICAL);
				break;
			}

		}
	};

	private void switchMode(String mode) {
		if (mUserID != null) {
			mPhotosFragment.changeMode(mUserID, mUserName, mProfilePic, mode);
		}
	}

	OnClickListener mClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {

			case R.id.btn_back:
				finish();
				break;
			}
		}
	};
}
