package com.beaconwatcher.rightips.activities;

import java.util.ArrayList;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.customadapters.ListItemAdapter;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.CommentItem;
import com.beaconwatcher.rightips.events.ApplicationEvent;
import com.beaconwatcher.rightips.events.EventBusEvent;
import com.beaconwatcher.rightips.services.IntentServiceFactory;

public class MyWallsActivity extends RighTipsListActivity {
	private static String ERROR_TAG = "My Walls";

	private LayoutInflater mInflater;

	private FrameLayout mBtnBack;
	private TextView mTitle;
	private ListView mCommentsList;

	// Data
	private String mUserID = "0";
	private ListItemAdapter mAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		TAG = "MyWallsActivity";
		mApp = (RighTipsApplication) getApplicationContext();
		mContext = MyWallsActivity.this;

		mInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		setContentView(R.layout.activity_my_walls);

		View topBarView = getLayoutInflater().inflate(
				R.layout.titlebar_with_comment_back, null);
		getActionBar().setDisplayShowCustomEnabled(true);
		ActionBar.LayoutParams params = new ActionBar.LayoutParams(
				ActionBar.LayoutParams.FILL_PARENT,
				ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);
		getActionBar().setCustomView(topBarView, params);

		mTitle = (TextView) findViewById(R.id.tf_note_title);
		mTitle.setText(this.getResources().getString(R.string.lbl_my_walls));

		// Get UI
		mLoader = (RelativeLayout) findViewById(R.id.loader);
		mBtnBack = (FrameLayout) findViewById(R.id.btn_back);
		// mList = (ListView) findViewById(R.id.list_comments);
		mCommentsList = (ListView) findViewById(R.id.walls_list);

		// Set button click listeners
		mBtnBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	@Override
	public void onResume() {
		super.onResume();
		mUserID = getIntent().getStringExtra("uid");
		if (mUserID != null && !mUserID.equals("0")) {
			getAllWalls();
		}
	}

	private void getAllWalls() {
		showLoader(true);
		Class mClazz = IntentServiceFactory.getInstance()
				.getIntentServiceClass(AppConstants.ACTION_GET_MY_WALLS);
		Intent intent = new Intent(mContext, mClazz);
		intent.putExtra(AppConstants.INTENT_SERVICE_TAG, TAG);
		intent.putExtra(AppConstants.USER_ID, mUserID);
		intent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
				AppConstants.ACTION_GET_MY_WALLS);
		mContext.startService(intent);
	}

	public void onEvent(EventBusEvent event) {
		showLoader(false);
		if (event.getType().equals(AppConstants.ACTION_GET_MY_WALLS)) {
			ArrayList<CommentItem> arr = (ArrayList<CommentItem>) event
					.getData();
			if (arr != null) {
				mAdapter = new ListItemAdapter(
						AppConstants.LIST_ITEM_USER_WALLS, arr);
				mCommentsList.setAdapter(mAdapter);
			}
		}
	}

	public void onEvent(ApplicationEvent event) {
		if (event.getType().equals(AppConstants.LIST_ITEM_CLICKED)
				&& event.getSenderAction().equals(
						AppConstants.LIST_ITEM_USER_WALLS)) {
			CommentItem item = (CommentItem) event.getData();
			Intent intent = new Intent(mContext,
					NotificationWallCommentActivity.class);
			intent.putExtra("uid", mUserID);
			intent.putExtra("msg_id", item.getMessageID());
			intent.putExtra("title", item.getMessage());
			startActivity(intent);
		}
	}

}
