package com.beaconwatcher.rightips.activities;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RadioGroup.LayoutParams;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.google.directions.AbstractRouting.TravelMode;
import com.beaconwatcher.rightips.google.directions.Route;
import com.beaconwatcher.rightips.google.directions.Routing;
import com.beaconwatcher.rightips.google.directions.RoutingListener;
import com.beaconwatcher.rightips.services.GetSpecificNotificationService;
import com.beaconwatcher.rightips.services.LoadNearbySitesService;
import com.bw.libraryproject.activities.InternetActivity;
import com.bw.libraryproject.entities.CategoryGroupData;
import com.bw.libraryproject.entities.NotificationData;
import com.bw.libraryproject.entities.SiteData;
import com.bw.libraryproject.utils.BitmapUtilities;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.makeramen.segmented.GoogleDirectionsRadioGroup;
import com.makeramen.segmented.TabBarRadioButton;
import com.makeramen.segmented.TabBarRadioGroup;

public class GoogleMapActivity extends InternetActivity implements
		RoutingListener {
	private static final String TAG = "RighTips Map Activity";
	private static final String ERROR_TAG = "Google Map";

	RighTipsApplication mApp;
	Context mContext;

	// Google Map
	private GoogleMap mGoogleMap;
	private int selGroupID = 1;
	private Marker selMarker;
	private SiteData selSite;

	// Google Map Directions
	private Boolean routeRequested = false;
	private LatLng start;
	private LatLng end;
	private TravelMode mTravelMode;
	private Polyline mLastRoute;

	// Extra Data
	private NotificationData mNotification;
	private LatLng mMapInitialPosition;
	private Boolean mLoadDataOnStart = false;

	// UI
	private View mSiteContainer;
	private TextView mSiteName;
	private TextView mSiteAddress;
	private TextView mTxtTime;
	private TextView mTxtDistance;
	private TextView mTotalNotifications;
	private FrameLayout mBtnAllNotes;

	private GoogleDirectionsRadioGroup mDirectionsGroup;
	private LinearLayout mDirectionBar;
	private TabBarRadioGroup mTabBar;
	private LinearLayout mBtnDirections;
	private RelativeLayout mLoader;

	// Listeners
	private OnCheckedChangeListener groupChangeListener;

	private int totalGroups = 0;
	private ArrayList<LatLng> allMarkersPosition = new ArrayList<LatLng>();

	// Data sets
	private HashMap<Marker, SiteData> allMarkersData = new HashMap<Marker, SiteData>();
	private HashMap<String, Bitmap> allMarkersBitmaps = new HashMap<String, Bitmap>();
	private ArrayList<CategoryGroupData> mCategoryGroups = new ArrayList<CategoryGroupData>();
	private ArrayList<int[]> mMarkerIcons = new ArrayList<int[]>();

	// Broadcast Receivers
	// BroadcastReceiver categoryGroupsBroadCastReceiver;
	/*
	 * When nearby sites area loeaded from IntentService we register this
	 * receiver to invoke UI to render markers on Google Map and Zoom to those
	 * markers.
	 */
	IntentFilter venuesFilter = new IntentFilter(
			AppConstants.CATEGORY_GROUP_VENUES_ACTION);
	BroadcastReceiver allVenuesBroadCastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			if (mLoader != null)
				mLoader.setVisibility(View.INVISIBLE);
			String msg = intent.getStringExtra("message");

			if (!msg.equals("")) {
				// Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
				mApp.showErrorToast(mContext, ERROR_TAG);

			} else {
				ArrayList<SiteData> sites = intent
						.getParcelableArrayListExtra("sites");
				addMarkers(sites);
			}
		}
	};

	/*
	 * When user click all notifications button, it load all notification for
	 * selected site through an intent service, we have to register a
	 * broadcastreceiver to listen and get all notifications.
	 */
	IntentFilter noteIntentFilter = new IntentFilter(
			GetSpecificNotificationService.GET_SPECIFIC_NOTIFICATION_SERVICE);
	BroadcastReceiver allNotificationsBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			if (mLoader != null)
				mLoader.setVisibility(View.INVISIBLE);
			ArrayList<NotificationData> arr = intent
					.getParcelableArrayListExtra("notifications");
			if (arr != null) {
				Intent mainIntent = new Intent(GoogleMapActivity.this,
						MainActivity.class);
				mainIntent.putExtra("notifications", arr);
				startActivity(mainIntent);
				finish();
			}
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_google_map);

		// Add Receivers
		addReceiver(allVenuesBroadCastReceiver, venuesFilter);
		addReceiver(allNotificationsBroadcastReceiver, noteIntentFilter);

		mApp = (RighTipsApplication) getApplicationContext();
		mContext = this;

		mSiteContainer = findViewById(R.id.shop_detail);
		mSiteName = (TextView) findViewById(R.id.site_title);
		mSiteAddress = (TextView) findViewById(R.id.site_address);
		mTotalNotifications = (TextView) findViewById(R.id.total_notifications);
		mBtnAllNotes = (FrameLayout) findViewById(R.id.btn_all_notes);

		mTxtTime = (TextView) findViewById(R.id.txt_time);
		mTxtDistance = (TextView) findViewById(R.id.txt_distance);

		mDirectionBar = (LinearLayout) findViewById(R.id.google_directions_bar);

		mDirectionsGroup = (GoogleDirectionsRadioGroup) findViewById(R.id.google_direction_modes_bar);
		mTabBar = (TabBarRadioGroup) findViewById(R.id.group_tabbar);
		mBtnDirections = (LinearLayout) findViewById(R.id.btn_directions);
		mLoader = (RelativeLayout) findViewById(R.id.loader);

		// Initialize Data
		mCategoryGroups.add(new CategoryGroupData(1, "Buildings",
				R.drawable.cat_group_icon_hotels, 0, 0));
		mCategoryGroups.add(new CategoryGroupData(2, "Restaurents",
				R.drawable.cat_group_icon_food, 0, 0));
		mCategoryGroups.add(new CategoryGroupData(3, "Events",
				R.drawable.cat_group_icon_tickets, 0, 0));

		mMarkerIcons.add(new int[] { R.drawable.map_bar_marker,
				R.drawable.map_bar_marker_over });
		mMarkerIcons.add(new int[] { R.drawable.map_shop_marker,
				R.drawable.map_shop_marker_over });
		mMarkerIcons.add(new int[] { R.drawable.map_services_marker,
				R.drawable.map_services_marker_over });
		mMarkerIcons.add(new int[] { R.drawable.map_entertainment_marker,
				R.drawable.map_entertainment_marker_over });
		mMarkerIcons.add(new int[] { R.drawable.map_tourism_marker,
				R.drawable.map_tourism_marker_over });
		mMarkerIcons.add(new int[] { R.drawable.map_events_marker,
				R.drawable.map_events_marker_over });
		mMarkerIcons.add(new int[] { R.drawable.map_students_marker,
				R.drawable.map_students_marker_over });

		mBtnDirections.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				requestCurrentLocation();
			}
		});

		// Load All Notification for selected site.
		mBtnAllNotes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mLoader != null) {
					mLoader.setVisibility(View.VISIBLE);
				}
				Intent intent = new Intent(mContext,
						GetSpecificNotificationService.class);
				if (selSite != null) {
					intent.putExtra("site_id", selSite.getSiteID());
				} else if (mNotification != null) {
					intent.putExtra("site_id", mNotification.getSiteID());
				}
				intent.putExtra("singleNote", false);
				mContext.startService(intent);
			}
		});

		groupChangeListener = new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				if (mLoader != null) {
					mLoader.setVisibility(View.VISIBLE);
				}
				mDirectionBar.setVisibility(View.GONE);
				selGroupID = checkedId;

				selMarker = null;
				selSite = null;
				mGoogleMap.clear();
				

				//Hide site details
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
						LayoutParams.FILL_PARENT, 0);
				mSiteContainer.setLayoutParams(params);

				loadAllSites(selGroupID);

			}
		};

		mTravelMode = TravelMode.DRIVING;

		OnCheckedChangeListener routeTypeChangeListener = new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub

				switch (checkedId) {
				case R.id.btn_drive:
					mTravelMode = TravelMode.DRIVING;
					break;

				case R.id.btn_transit:
					mTravelMode = TravelMode.TRANSIT;
					break;

				case R.id.btn_walk:
					mTravelMode = TravelMode.WALKING;
					break;

				default:
					mTravelMode = TravelMode.DRIVING;
					break;
				}

				// Request current location, after successfully get current
				// location
				// It calls Google Directions service.
				requestCurrentLocation();
			}
		};

		mDirectionsGroup.setOnCheckedChangeListener(routeTypeChangeListener);
		/*
		 * Get notification data here.
		 */
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mMapInitialPosition = extras.getParcelable("position");
			if (mMapInitialPosition != null) {
				mTabBar.check(1);
			}

			mNotification = extras.getParcelable("notification");
			if (mNotification != null) {
				mMapInitialPosition = new LatLng(
						Double.parseDouble(mNotification.getSiteLat()),
						Double.parseDouble(mNotification.getSiteLong()));
				selGroupID = Integer.parseInt(mNotification.getCatGroupID());
			}
		}

		addCatGroups(mCategoryGroups);
	}

	private void loadAllSites(int grpId) {
		Intent intent = new Intent(mContext, LoadNearbySitesService.class);
		intent.putExtra("grpID", String.valueOf(grpId));
		intent.putExtra("latitude",
				String.valueOf(mMapInitialPosition.latitude));
		intent.putExtra("longitude",
				String.valueOf(mMapInitialPosition.longitude));
		startService(intent);
	}

	/**
	 * function to load map. If map is not created it will create it for you
	 * */
	private void initilizeMap() {
		if (mGoogleMap == null) {
			mGoogleMap = ((MapFragment) getFragmentManager().findFragmentById(
					R.id.map)).getMap();
			if (mGoogleMap == null) {
				// Toast.makeText(mContext,
				// "Sorry! unable to create maps",Toast.LENGTH_SHORT).show();
				mApp.showErrorToast(mContext, ERROR_TAG);

			} else {
				mGoogleMap.setMyLocationEnabled(true); //
				mGoogleMap
						.setOnMyLocationChangeListener(myLocationChangeListener);

				mGoogleMap.getUiSettings().setZoomControlsEnabled(false); // true
																			// to
																			// enable
				mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);

				mGoogleMap
						.setOnMarkerClickListener(new OnMarkerClickListener() {
							@Override
							public boolean onMarkerClick(Marker marker) {
								// TODO Auto-generated method stub
								if (allMarkersData.size() > 0) {
									SiteData mData = allMarkersData.get(marker);
									if (mData != null) {

										if (selMarker != null) {
											Drawable res = mContext
													.getResources()
													.getDrawable(
															mMarkerIcons
																	.get(Integer
																			.parseInt(mData
																					.getCatID()) - 1)[0]);
											Bitmap bitmap = ((BitmapDrawable) res)
													.getBitmap();
											Bitmap icon = BitmapUtilities
													.resizeBitmap(bitmap, 75);
											selMarker
													.setIcon(BitmapDescriptorFactory
															.fromBitmap(icon));

										}

										mDirectionBar.setVisibility(View.GONE);
										updateSiteInfo(
												mData.getSiteName(),
												mData.getSiteAddress(),
												String.valueOf(mData
														.getSiteTotalNotifications()));

										Drawable res = mContext
												.getResources()
												.getDrawable(
														mMarkerIcons.get(Integer.parseInt(mData
																.getCatID()) - 1)[1]);
										Bitmap bitmap = ((BitmapDrawable) res)
												.getBitmap();
										Bitmap icon = BitmapUtilities
												.resizeBitmap(bitmap, 150);
										marker.setIcon(BitmapDescriptorFactory
												.fromBitmap(icon));
										selSite = mData;
										selMarker = marker;

										// Move Camera to location & zoom.
										CameraPosition cameraPosition = new CameraPosition.Builder()
												.target(marker.getPosition())
												.zoom(mGoogleMap
														.getCameraPosition().zoom)
												.build();
										mGoogleMap
												.animateCamera(CameraUpdateFactory
														.newCameraPosition(cameraPosition));

									}
								}
								return true;
							}
						});

				if (mNotification != null) {
					// UI
					updateSiteInfo(mNotification.getSiteName(),
							mNotification.getSiteAddress(),
							String.valueOf(mNotification
									.getSiteTotalNotifications()));
					SiteData markerData = new SiteData();
					markerData.setSiteID(mNotification.getSiteID());
					markerData.setCatID(mNotification.getCatID());
					markerData.setSiteName(mNotification.getSiteName());
					markerData.setSiteAddress(mNotification.getSiteAddress());
					markerData.setLatitude(mNotification.getSiteLat());
					markerData.setLongitude(mNotification.getSiteLong());
					addMarkerOnMap(markerData);
				}
				
				//No notification selected, get all sites on map by default (1) category.
				else{
					loadAllSites(selGroupID);
				}
				
				
				

				if (mMapInitialPosition != null) {
					zoomMapAtStart(mMapInitialPosition);
				}
			}
		}
	}

	private void zoomMapAtStart(LatLng pos) {
		// Move Camera to location & zoom.
		CameraPosition cameraPosition = new CameraPosition.Builder()
				.target(pos).zoom(16).build();
		mGoogleMap.animateCamera(CameraUpdateFactory
				.newCameraPosition(cameraPosition));

	}

	private void updateSiteInfo(String sname, String address, String totalNotes) {
		// ViewGroup.LayoutParams params=mSiteContainer.getLayoutParams();
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		mSiteContainer.setLayoutParams(params);

		mSiteName.setText(sname);
		mSiteAddress.setText(address);
		mTotalNotifications.setText(totalNotes);
	}

	/********************************************************
	 * Add Group Tab Bar at Bottom of screen
	 ********************************************************/
	private void addCatGroups(ArrayList<CategoryGroupData> arr) {
		// mTabBar.removeAllViews();
		totalGroups = arr.size();

		TabBarRadioButton btn;
		LayoutParams lp;
		Drawable res;
		Bitmap bitmap;
		Bitmap icon;
		Drawable d;

		for (int i = 0; i < totalGroups; i++) {
			btn = new TabBarRadioButton(mContext);
			btn.setId(arr.get(i).getCatGroupID());
			lp = new LayoutParams(mContext, null);
			lp.weight = (100 / totalGroups);

			
			if(i==selGroupID-1){
				btn.setChecked(true);
			}
			
			else if (i == totalGroups - 1) {
				lp.weight = (100 / totalGroups) + 1;
			}

			lp.height = 120;
			lp.width = 0;
			mTabBar.addView(btn, lp);

			btn.setGroupID(arr.get(i).getCatGroupID());
			btn.setMarkerID(arr.get(i).getCatGroupMarker());

			res = mContext.getResources().getDrawable(
					arr.get(i).getCatGroupIcon());
			bitmap = ((BitmapDrawable) res).getBitmap();
			icon = BitmapUtilities.resizeBitmap(bitmap, 50);
			d = new BitmapDrawable(getResources(), icon);
			btn.setDrawable(d);

			/*
			 * String url=arr.get(i).getCatGroupIcon(); if(!url.equals("") &&
			 * !(url==null)){
			 * 
			 * GroupIconLoadTask task=new GroupIconLoadTask(url, btn,
			 * getResources()); task.execute();
			 * 
			 * Target target = new Target() {
			 * 
			 * @Override public void onBitmapLoaded(Bitmap bitmap,
			 * Picasso.LoadedFrom from) { // iv.setImageBitmap(bitmap); //Bitmap
			 * nBmp=BitmapUtilities.getScaledBitmap(bitmap, 90); Drawable d =
			 * new BitmapDrawable(getResources(), bitmap); btn.setDrawable(d); }
			 * public void onBitmapFailed(Drawable arg0) {} public void
			 * onPrepareLoad(Drawable arg0) {}; };
			 * 
			 * Picasso.with(mContext).load(url).resize(90, 90).into(target); }
			 */
		}

		// After adding groups tab bar, we need to initialize google map.
		// if (mNotification != null) {
		try {
			// Loading map
			initilizeMap();
		} catch (Exception e) {
			e.printStackTrace();
		}
		// }

		// Add Listener to tab bar here so that i does not revoek
		// on startup when we set selected tab checked according
		// to notification.
		// We only want to invoke it when user click on tab bar.
		mTabBar.setOnCheckedChangeListener(groupChangeListener);
	}

	private void addMarkers(ArrayList<SiteData> sites) {
		allMarkersPosition.clear();
		allMarkersData.clear();
		allMarkersBitmaps.clear();

		mGoogleMap.clear();

		SiteData site;
		Marker marker;
		double latitude;
		double longitude;
		LatLng l;

		for (int i = 0; i < sites.size(); i++) {
			site = sites.get(i);
			marker = addMarkerOnMap(site);
			allMarkersData.put(marker, site);

			latitude = Double.parseDouble(site.getLatitude());
			longitude = Double.parseDouble(site.getLongitude());
			l = new LatLng(latitude, longitude);

			allMarkersPosition.add(l);
		}

		zoomToCoverAllMarkers(allMarkersPosition);
	}

	private Marker addMarkerOnMap(final SiteData site) {
		// Add Marker
		double latitude = Double.parseDouble(site.getLatitude());
		double longitude = Double.parseDouble(site.getLongitude());
		// create marker
		MarkerOptions marker = new MarkerOptions().position(
				new LatLng(latitude, longitude)).title(site.getSiteName());

		Drawable res = mContext.getResources().getDrawable(
				mMarkerIcons.get(Integer.parseInt(site.getCatID()) - 1)[0]);
		Bitmap bitmap = ((BitmapDrawable) res).getBitmap();
		Bitmap icon = BitmapUtilities.resizeBitmap(bitmap, 75);
		marker.icon(BitmapDescriptorFactory.fromBitmap(icon));

		return mGoogleMap.addMarker(marker);

		/*
		 * Target target = new Target() {
		 * 
		 * @Override public void onBitmapLoaded(Bitmap bmp, Picasso.LoadedFrom
		 * from) { //Bitmap bmp=BitmapUtilities.addShadow(bitmap,
		 * bitmap.getWidth(), bitmap.getHeight(), Color.BLACK, 10, 0, 0);
		 * allMarkersBitmaps.put(site.getSiteID(), bmp);
		 * marker.icon(BitmapDescriptorFactory.fromBitmap(bmp)); // adding
		 * marker } public void onBitmapFailed(Drawable arg0) {} public void
		 * onPrepareLoad(Drawable arg0) {}; };
		 * 
		 * 
		 * if(site.getMarkerUrl()!=""){
		 * Picasso.with(mContext).load(site.getMarkerUrl()).resize(60,
		 * 96).into(target); }
		 */
	}

	private void zoomToCoverAllMarkers(ArrayList<LatLng> latLngList) {
		LatLngBounds.Builder builder = new LatLngBounds.Builder();
		for (LatLng marker : latLngList) {
			builder.include(marker);
		}

		LatLngBounds bounds = builder.build();
		int padding = 90; // offset from edges of the map in pixels
		CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
		mGoogleMap.moveCamera(cu);
		mGoogleMap.animateCamera(cu);
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (mGoogleMap != null) {
			if (getFragmentManager().findFragmentById(R.id.map) != null) {
				getFragmentManager()
						.beginTransaction()
						.remove(getFragmentManager().findFragmentById(R.id.map))
						.commit();
			}
		}
	}

	/********************************************************************************
	 * GOOGLE MAP DIRECTIONS downloaded from
	 * https://github.com/jd-alexander/Google-Directions-Android
	 *******************************************************************************/

	private void requestCurrentLocation() {
		if (mApp.isLocationEnabled(mContext) == false) {
			// Toast.makeText(mContext, "Location Services are not enabled",
			// Toast.LENGTH_LONG).show();
			mApp.showErrorToast(mContext, ERROR_TAG);
			return;
		}

		if (mLoader != null) {
			mLoader.setVisibility(View.VISIBLE);
		}
		routeRequested = true;
		mGoogleMap.getMyLocation();
	}

	/**
	 * Get the Google Direction between mDevice location and the touched
	 * location using the Walk
	 * 
	 * @param point
	 */

	GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
		@Override
		public void onMyLocationChange(Location location) {
			if (routeRequested == true) {
				routeRequested = false;
				start = new LatLng(location.getLatitude(),
						location.getLongitude());
				if (selMarker != null) {
					end = selMarker.getPosition();
				} else {
					end = new LatLng(mMapInitialPosition.latitude,
							mMapInitialPosition.longitude);
				}

				Routing routing = new Routing(mTravelMode);
				routing.registerListener(GoogleMapActivity.this);
				routing.execute(start, end);
			}
		}
	};

	@Override
	public void onRoutingStart() {

	}

	@Override
	public void onRoutingFailure() {
		if (mLoader != null) {
			mLoader.setVisibility(View.INVISIBLE);
		}
	}

	@Override
	public void onRoutingSuccess(PolylineOptions mPolyOptions, Route route) {
		if (mLoader != null) {
			mLoader.setVisibility(View.INVISIBLE);
		}

		if (mLastRoute != null) {
			mLastRoute.remove();
		}

		mDirectionBar.setVisibility(View.VISIBLE);
		mTxtTime.setText(route.getDurationText());
		mTxtDistance.setText(route.getDistanceText());

		PolylineOptions polyOptions = new PolylineOptions();
		polyOptions.color(Color.argb(255, 0, 179, 253));
		polyOptions.width(10);
		polyOptions.addAll(mPolyOptions.getPoints());
		mLastRoute = mGoogleMap.addPolyline(polyOptions);

		// Zoom to whole route.
		LatLngBounds bounds = route.getLatLgnBounds();
		int padding = 40; // offset from edges of the map in pixels
		CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
		mGoogleMap.moveCamera(cu);
		mGoogleMap.animateCamera(cu);
	}

}
