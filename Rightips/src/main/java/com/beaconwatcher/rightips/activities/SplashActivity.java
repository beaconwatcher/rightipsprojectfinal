package com.beaconwatcher.rightips.activities;

import io.branch.referral.Branch.BranchReferralInitListener;
import io.branch.referral.BranchError;

import java.util.ArrayList;
import java.util.List;

import org.brickred.socialauth.Contact;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ActivityManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.FriendRequest;
import com.beaconwatcher.rightips.services.NLService;
import com.google.gson.reflect.TypeToken;

public class SplashActivity extends Activity {

	// Splash screen timer
	private static int SPLASH_TIME_OUT = 0;// 1000;
	private static final int RESULT_SETTINGS = 1;
	private static final int RESULT_LOGIN = 2;

	private Boolean noteAccessGranted;

	private RighTipsApplication mApp;
	private Context mContext;
	private Boolean mBlueToothChecked = false;

	private boolean appInstalledFirstTime;
	private Boolean mBLEAvailable = false;

	FriendRequest mFriendRequest;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mApp = (RighTipsApplication) getApplicationContext();
		mApp.appKey = "MTRfX1JpZ2h0VGlwcw==";

		mContext = this;

		setContentView(R.layout.activity_splash);

		/*
		 * To create application shortcut on home screen.
		 */
		if (mApp.getPreferences("shortcutCreated", "").equals("")) {
			// removeShortcut();
			// addShortcut();
		}

		// App is installed and run first time on this device.
		if (mApp.getPreferences("RighTipsInstalled", "").equals("")) {
			mApp.savePreferences("RighTipsInstalled", "true");

			// Add appdownload event to Piwik
			mApp.getTracker().trackAppDownload();
			appInstalledFirstTime = true;
		}

		// App is already available on device.
		else {
			appInstalledFirstTime = false;
		}

		checkUpdate();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// First check if BLE is available
		if (isBLEAvailable()) {
			mApp.isBLEAvailable = true;
			if (isBluetoothEnabled()) {
				mBlueToothChecked = true;
				checkBranchLink();
			} else {
				if (mBlueToothChecked == false) {
					Intent enableBtIntent = new Intent(
							BluetoothAdapter.ACTION_REQUEST_ENABLE);
					startActivity(enableBtIntent);
					mBlueToothChecked = true;
				} else {
					checkBranchLink();
				}
			}
		} else {
			mApp.isBLEAvailable = false;
			mBlueToothChecked = true;
			checkBranchLink();
		}
	}

	public boolean isBLEAvailable() {
		if (!getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_BLUETOOTH_LE)) {
			return false;
		}
		return true;
	}

	public boolean isBluetoothEnabled() {
		if (((BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE))
				.getAdapter().isEnabled()) {
			return true;
		}
		return false;
	}

	private void checkPermission() {
		noteAccessGranted = checkNotificationSetting();
		if (noteAccessGranted == false
				|| (noteAccessGranted == true && isNLServiceCrashed() == true)) {
			findViewById(R.id.permission_cont).setVisibility(View.VISIBLE);
			findViewById(R.id.btnAllowNoteAccess).setOnClickListener(
					clickListener);
		} else {
			checkBranchLink();
		}
	}

	/*private void runSplashTimer() {
		new Handler().postDelayed(new Runnable() {
			*//*
			 * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company This method will be
			 * executed once the timer is over
			 *//*

			@Override
			public void run() {

				// App is installed and run first time on this device.
				// We need to check if it was installed by clicking a link
				// Sent by some user as Friend Invitation feature?
				if (mApp.getPreferences("RighTipsInstalled", "").equals("")) {
					mApp.savePreferences("RighTipsInstalled", "true");

					// Add appdownload event to Piwik
					mApp.getTracker().trackAppDownload();
					appInstalledFirstTime = true;
				}

				// App is already available on device.
				else {
					appInstalledFirstTime = false;
				}

				checkBranchLink();

			}
		}, SPLASH_TIME_OUT);
	}
*/
	OnClickListener clickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (v.getId() == R.id.btnAllowNoteAccess) {
				findViewById(R.id.permission_cont).setVisibility(View.GONE);
				showAndroidSettingsScreen();
			}
		}
	};

	private void checkBranchLink() {
	/*	mFriendRequest=new FriendRequest();
		mFriendRequest.id="10000";
		mFriendRequest.name="Shahbaz Ali";
		mFriendRequest.photo_url="http://studioepic.com/blog/blog_images/2_Male_Social_Media_Image.jpg";
		saveInvitationToLocalStorage(mFriendRequest);
*/

		mApp.getBranch().initSession(new BranchReferralInitListener() {
			@Override
			public void onInitFinished(JSONObject json, BranchError error) {
				if (error == null) {
					try {
						Boolean linkClicked = json.getBoolean("+clicked_branch_link");
						// App installed by clicking invitation link
						if (linkClicked == true) {
							if (json.has("id") && json.has("name")) {
								String id = json.getString("id");
								String pName = json.getString("name");
								String image_url = json.getString("image_url");

								// Write code to show friend request screen
								// here using above information.
								mFriendRequest=new FriendRequest();
								mFriendRequest.id=id;
								mFriendRequest.name=pName;
								mFriendRequest.photo_url=image_url;
								saveInvitationToLocalStorage(mFriendRequest);
								showNextScreen();
							}
						}

						// App installed without clicking invitation link
						else {
							showNextScreen();
						}
					} catch (JSONException e) {
						showNextScreen();
					}
				}

				// Error geting branch link from branch.io server to show
				// appropriate screen
				// We must handle it
				else {
					showNextScreen();
				}
			}
		}, getIntent().getData(), SplashActivity.this);
	}

	private void showNextScreen() {
		if (mFriendRequest != null) {
			showInvitationScreen();
		}

		else{
			if (appInstalledFirstTime==true) {
				showLoginScreen();
			}
			else{
				showHomeScreen();
			}
		}
	}



	private void showInvitationScreen() {
		Intent i = new Intent(SplashActivity.this, InvitationActivity.class);
		String requestStr=mApp.getGson().toJson(mFriendRequest, new TypeToken<FriendRequest>() {}.getType());
		i.putExtra(AppConstants.FRIEND_REQUEST, requestStr);
		i.putExtra(AppConstants.APP_INSTALLED_FIRST_TIME, appInstalledFirstTime);
		startActivity(i);
		finish();
	}

	private void showHomeScreen() {
		Intent i = new Intent(SplashActivity.this, MainActivity.class);
		startActivity(i);
		finish();
	}

	private void showLoginScreen() {
		Intent i = new Intent(SplashActivity.this, LoginActivity.class);
		startActivity(i);
		finish();
	}

	private void showAndroidSettingsScreen() {
		Intent i = new Intent(
				"android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
		startActivityForResult(i, RESULT_SETTINGS);
	}

	// Store invitation information to local storage
	private void saveInvitationToLocalStorage(FriendRequest request) {
		ArrayList<FriendRequest> allRequests;

		String requstsStr = mApp.getPreferences(AppConstants.ALL_FRIEND_REQUESTS, "");
		//No previous request.
		if (requstsStr.equals("")) {
			allRequests = new ArrayList<FriendRequest>();
		}
		else {
			allRequests = mApp.getGson().fromJson(requstsStr, new TypeToken<ArrayList<FriendRequest>>() {}.getType());
		}

		if(hasInvitation(allRequests, request)==false){
			allRequests.add(request);
			String str = mApp.getGson().toJson(allRequests, new TypeToken<ArrayList<FriendRequest>>() {}.getType());
			mApp.savePreferences(AppConstants.ALL_FRIEND_REQUESTS, str);
		}
	}

	private boolean hasInvitation(ArrayList<FriendRequest> all, FriendRequest request){
		for(int i=0; i<all.size(); i++){
			if(all.get(i).id.equals(request.id)){
				return true;
			}
		}
		return false;
	}



	private boolean checkNotificationSetting() {
		ContentResolver contentResolver = getContentResolver();
		String enabledNotificationListeners = Settings.Secure.getString(
				contentResolver, "enabled_notification_listeners");
		String packageName = getPackageName();

		return !(enabledNotificationListeners == null || !enabledNotificationListeners
				.contains(packageName));
	}

	private boolean isNLServiceCrashed() {
		ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningServiceInfo> runningServiceInfos = manager
				.getRunningServices(Integer.MAX_VALUE);

		if (runningServiceInfos != null) {
			for (ActivityManager.RunningServiceInfo service : runningServiceInfos) {

				// NotificationListener.class is the name of my class (the one
				// that has to extend from NotificationListenerService)
				if (NLService.class.getName().equals(
						service.service.getClassName())) {
					if (service.crashCount > 0) {
						// in this situation we know that the notification
						// listener service is not working for the app
						return true;
					}
					return false;
				}
			}
		}
		return false;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case RESULT_SETTINGS:
			// checkPermission();
			break;
		}
	}

	/*
	 * Add App Shortcut on first time installation
	 */

	private void addShortcut() {
		Intent shortcutIntent = new Intent(this, SplashActivity.class);
		shortcutIntent.setComponent(new ComponentName(getApplicationContext()
				.getPackageName(), SplashActivity.class.getName()));

		Intent addIntent = new Intent();
		addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
		addIntent.putExtra("duplicate", false);
		addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME,
				getString(R.string.app_name));
		addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
				Intent.ShortcutIconResource.fromContext(
						getApplicationContext(), R.drawable.ic_launcher));
		addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");

		sendBroadcast(addIntent);
		mApp.savePreferences("shortcutCreated", "true");
	}

	private void checkUpdate() {
		try {
			PackageInfo packageInfo = SplashActivity.this.getPackageManager()
					.getPackageInfo(SplashActivity.this.getPackageName(), 0);
			int versionCode = packageInfo.versionCode;

			String savedVersionCode = mApp.getPreferences("versionCode", "0");

			if (savedVersionCode.equals("0")) {
				mApp.clearApplicationData();
				mApp.savePreferences("versionCode", String.valueOf(versionCode));
			} else if (!savedVersionCode.equals(String.valueOf(versionCode))) {
				// App Updated
				mApp.clearApplicationData();
				mApp.savePreferences("versionCode", String.valueOf(versionCode));
			}
		} catch (NameNotFoundException e) {
			Log.d("SplashActivity", e.getMessage());
		}
	}

}