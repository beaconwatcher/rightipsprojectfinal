package com.beaconwatcher.rightips.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.beaconwatcher.rightips.R;

public class CircleLayoutActivity extends Activity {

	private char mState = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.circle_layout_test);

		/*
		 * All code below is NOT required. I've added it just for demonstration
		 * of different layout modes
		 */

		final View pie = findViewById(R.id.pie);
		final View normal = findViewById(R.id.normal);
		final View normalWithRange = findViewById(R.id.normalWithRange);

		final Button switchBtn = (Button) findViewById(R.id.switchBtn);
		switchBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				switch (mState) {
				case 0:
					normalWithRange.setVisibility(View.GONE);
					pie.setVisibility(View.GONE);
					normal.setVisibility(View.VISIBLE);

					switchBtn.setText("Normal Range");
					mState++;
					break;
				case 1:
					normalWithRange.setVisibility(View.VISIBLE);
					pie.setVisibility(View.GONE);
					normal.setVisibility(View.GONE);

					switchBtn.setText("PIE");
					mState++;
					break;
				case 2:
					normalWithRange.setVisibility(View.GONE);
					pie.setVisibility(View.VISIBLE);
					normal.setVisibility(View.GONE);

					switchBtn.setText("Normal");
					mState = 0;
					break;

				default:
					break;
				}
			}
		});
	}

}
