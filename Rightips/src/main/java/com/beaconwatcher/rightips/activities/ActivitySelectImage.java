package com.beaconwatcher.rightips.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.events.ApplicationEvent;
import com.beaconwatcher.rightips.fragments.FragmentSelectImage;
import com.bw.libraryproject.activities.InternetActivity;

public class ActivitySelectImage extends InternetActivity {
	private static final String ERROR_TAG = "ActivitySelectImage";
	public static final int IMAGE_SELECTION_RESULT_CODE = 5100;
	public static final int IMAGE_SELECTION_CANCEL_CODE = 5200;


	private RighTipsApplication mApp;
	private Context mContext;
	private FragmentSelectImage mFragmentSelectImage;
	private Uri mSelectedImageUri;
	private Intent mIntent;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mApp = (RighTipsApplication) getApplicationContext();
		mContext = this;
		mIntent = getIntent();
		setContentView(R.layout.activity_select_image);

		addImageSelection();
	}

	private void addImageSelection() {
		mFragmentSelectImage = new FragmentSelectImage();
		Bundle b=new Bundle();
		b.putBoolean(AppConstants.RECORD_VIDEO, false);
		mFragmentSelectImage.setArguments(b);
		getFragmentManager()
				.beginTransaction()
				.add(R.id.fragment_container, mFragmentSelectImage,
						FragmentSelectImage.class.getName())
				.addToBackStack(FragmentSelectImage.class.getName()).commit();
	}


	public void onEvent(ApplicationEvent event) {
		if (event.getType().equals(AppConstants.IMAGE_SELECTED_FOR_UPLOAD)) {
			Intent intent = (Intent) event.getData();
			mSelectedImageUri = intent.getParcelableExtra(AppConstants.IMAGE_URI);

			mIntent.putExtra(AppConstants.IMAGE_URI, mSelectedImageUri);
			setResult(IMAGE_SELECTION_RESULT_CODE, mIntent);
			finish();
		}

		else if(event.getType().equals(AppConstants.IMAGE_SELECTION_CANCELED)){
			setResult(IMAGE_SELECTION_CANCEL_CODE, mIntent);
			finish();
		}
	}
}
