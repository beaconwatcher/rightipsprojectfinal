package com.beaconwatcher.rightips.activities;

import java.util.ArrayList;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.ListItemData;
import com.beaconwatcher.rightips.events.ApplicationEvent;
import com.beaconwatcher.rightips.events.EventBusEvent;
import com.beaconwatcher.rightips.fragments.FragmentListItemActions;
import com.beaconwatcher.rightips.fragments.FragmentPreviewImage;
import com.beaconwatcher.rightips.services.IntentServiceFactory;
import com.bw.libraryproject.events.intentservice.IntentServiceEvent;

public class RighTipsListActivity extends NotificationReceiverActivity {
	public RighTipsApplication mApp;
	public Context mContext;

	private FrameLayout mFragmentContainer;
	private ViewGroup mRootView;
	private FragmentListItemActions mFragmentActions;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		TAG = "RighTipsListActivity";
		mContext = this;
		mApp = (RighTipsApplication) getApplicationContext();
	}

	public void onEvent(ApplicationEvent event) {
		if (event.getType().equals(AppConstants.LIST_SHOW_ACTIONS)) {
			showActions(event.getData());
		} else if (event.getType().equals(AppConstants.LIST_ACTIONS_CANCELED)) {
			hideActions();
		}

		else if (event.getType().equals(AppConstants.LIKE_LIST_ITEM)) {
			likeListItem((ListItemData) event.getData());
		}

		else if (event.getType().equals(AppConstants.ACTION_SHOW_PREVIEW_IMAGE)) {
			ListItemData item = (ListItemData) event.getData();
			if (item != null
					&& (item.img != null && item.img != "" && item.img != "null")) {
				showBigPreview(item.img);
			}
		}
	}

	private void showBigPreview(String url) {
		Bundle b = new Bundle();
		b.putString(AppConstants.IMAGE_URI, url);
		ViewGroup mRootView = (ViewGroup) getWindow().getDecorView()
				.findViewById(android.R.id.content);
		FrameLayout mFragmentContainer = new FrameLayout(this);
		mFragmentContainer.setLayoutParams(new LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		mFragmentContainer.setId(9999);
		mRootView.addView(mFragmentContainer);

		Fragment fragment = new FragmentPreviewImage();
		fragment.setArguments(b);
		changeFragment(fragment, mFragmentContainer.getId(), null);
	}

	public void showActions(Object obj) {
		ListItemData item = (ListItemData) obj;
		String str = mApp.getJSONStringFromClassObject(item);
		Bundle b = new Bundle();
		b.putString(AppConstants.LIST_ITEM_DATA, str);
		/*
		 * b.putInt(AppConstants.LIST_ITEM_USER_ID, item.uid);
		 * b.putInt(AppConstants.LIST_ITEM_ID, item.id);
		 * b.putString(AppConstants.LIST_ITEM_TYPE, item.type);
		 */

		ViewGroup mRootView = (ViewGroup) getWindow().getDecorView()
				.findViewById(android.R.id.content);
		FrameLayout mFragmentContainer = new FrameLayout(this);
		mFragmentContainer.setLayoutParams(new LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		mFragmentContainer.setId(9999);
		mRootView.addView(mFragmentContainer);

		mFragmentActions = new FragmentListItemActions();
		mFragmentActions.setArguments(b);
		changeFragment(mFragmentActions, mFragmentContainer.getId(), null);
	}

	private void likeListItem(ListItemData item) {
		if (item == null)
			return;
		showLoader(true);
		Class mClazz = IntentServiceFactory.getInstance()
				.getIntentServiceClass(AppConstants.LIKE_LIST_ITEM);
		Intent intent = new Intent(mContext, mClazz);
		intent.putExtra(AppConstants.INTENT_SERVICE_TAG, TAG);
		intent.putExtra(AppConstants.USER_ID, mApp.getUID());
		intent.putExtra(AppConstants.LIST_ITEM_ID, Integer.toString(item.id));
		intent.putExtra(AppConstants.LIST_ITEM_TYPE, item.type);
		intent.putExtra(AppConstants.ACTION_LIKE,
				item.is_liked.equals("0") ? "Like" : "Unlike");
		intent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
				AppConstants.LIKE_LIST_ITEM);
		mContext.startService(intent);
	}

	public void onEvent(EventBusEvent event) {
		if (event.getType().equals(AppConstants.REMOVE_LIST_ITEM)) {
			hideActions();
		} else if (event.getType().equals(AppConstants.REPORT_LIST_ITEM)) {
			hideActions();
			hideActions();
		}
	}

	public void hideActions() {
		onBackPressed();
	}
}
