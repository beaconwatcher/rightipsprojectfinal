package com.beaconwatcher.rightips.activities;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.customviews.CustomToggleGroup;
import com.beaconwatcher.rightips.customviews.CustomToggleGroup.OnCheckedChangeListener;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.fragments.FragmentReviews;
import com.beaconwatcher.rightips.fragments.FragmentUserLikes;
import com.bw.libraryproject.activities.InternetActivity;

public class UserReviewsActivity extends RighTipsListActivity {
	private static final String ERROR_TAG = "User Reviews";
	private static final String TAG = "UserReviewsActivity";

	private RighTipsApplication mApp;
	private Context mContext;
	private Intent serviceIntent;

	// Views
	private View mMainContainer;
	private LayoutInflater mInflater;

	// Fragments
	FragmentReviews mReviewsFragment;

	// Data
	private String mUserID;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mApp = (RighTipsApplication) getApplicationContext();
		mContext = this;
		mInflater = getLayoutInflater();

		mFragmentManager = getFragmentManager();

		setContentView(R.layout.activity_user_reviews);

		View titleBar = getLayoutInflater().inflate(
				R.layout.titlebar_user_reviews, null);
		getActionBar().setDisplayShowCustomEnabled(true);
		ActionBar.LayoutParams params = new ActionBar.LayoutParams(
				ActionBar.LayoutParams.FILL_PARENT,
				ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);
		getActionBar().setCustomView(titleBar, params);

		// Get UI
		mMainContainer = findViewById(R.id.main_container);
		mLoader = (RelativeLayout) findViewById(R.id.loader1);
		mReviewsFragment = (FragmentReviews) getFragmentManager()
				.findFragmentById(R.id.fragment_reviews);

		findViewById(R.id.btn_back).setOnClickListener(mClickListener);

		Intent intent = getIntent();
		if (intent != null && intent.hasExtra(AppConstants.USER_ID)) {
			mUserID = intent.getExtras().getString(AppConstants.USER_ID);
			mReviewsFragment.loadUserReviews(mUserID);
		}
	}

	OnClickListener mClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {

			case R.id.btn_back:
				finish();
				break;
			}
		}
	};

}
