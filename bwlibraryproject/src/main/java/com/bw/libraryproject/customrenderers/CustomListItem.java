package com.bw.libraryproject.customrenderers;
import android.content.Context;
import android.graphics.Color;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bw.libraryproject.R;
import com.bw.libraryproject.entities.CustomData;

public class CustomListItem extends LinearLayout {
	private TextView mName;
	
	public CustomListItem(Context c, CustomData d){
		super(c);
		this.setOrientation(VERTICAL);
		this.setPadding(10, 10, 10, 10);
	//	this.setClickable(true);
		this.setBackgroundResource(R.drawable.custom_button);
	    mName = new TextView(c);
	    mName.setText(d.getName());
	    mName.setTextSize(19);
	    mName.setTextColor(Color.LTGRAY);
	    addView(mName, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
	}
	
	public void setTitle(String s){
		mName.setText(s);
	}
}
