package com.bw.libraryproject.customrenderers;
import android.content.Context;
import android.graphics.Color;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bw.libraryproject.R;
import com.bw.libraryproject.entities.UserAppData;

public class CustomAppItem extends LinearLayout {
	private TextView mName;
	
	public CustomAppItem(Context c, UserAppData d){
		super(c);
		this.setOrientation(VERTICAL);
		this.setPadding(10, 10, 10, 10);
	//	this.setClickable(true);
		this.setBackgroundResource(R.drawable.custom_button);
	    mName = new TextView(c);
	    mName.setText(d.getName());
	    mName.setTextSize(19);
	    mName.setTextColor(Color.LTGRAY);
	    addView(mName, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
	}
	
	public void setTitle(String s){
		mName.setText(s);
	}
}
