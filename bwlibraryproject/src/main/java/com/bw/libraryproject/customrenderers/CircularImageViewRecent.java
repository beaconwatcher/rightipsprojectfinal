package com.bw.libraryproject.customrenderers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.bw.libraryproject.R;

public class CircularImageViewRecent extends ImageView {

	private int borderWidth = 4;
	private int viewWidth;
	private int viewHeight;
	private Bitmap image;
	private Paint paint;
	private Paint paintBorder;
	private BitmapShader shader;
	private Context mContext;
	private AttributeSet mAttrs;
	private int defStyle=0;
	
	public CircularImageViewRecent(Context context) {
		super(context);
		setup();
	}

	public CircularImageViewRecent(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext=context;
		mAttrs=attrs;
		setup();
	}

	public CircularImageViewRecent(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mContext=context;
		mAttrs=attrs;
		this.defStyle=defStyle;
		setWillNotDraw(false);
		setup();
	}
	
	private void setup()
	{
		// init paint
		paint = new Paint();
		paint.setAntiAlias(true);
		
		paintBorder = new Paint();
		paintBorder.setAntiAlias(true);
		
		if(mAttrs!=null && defStyle!=0){
			// load the styled attributes and set their properties
			TypedArray attributes = mContext.obtainStyledAttributes(mAttrs, R.styleable.CircularImageView, defStyle, 0);
			
			int defaultBorderSize = (int) (2 * mContext.getResources().getDisplayMetrics().density + 0.5f);
/*
			setBorderWidth(attributes.getDimensionPixelOffset(R.styleable.CircularImageView_myborder_width, defaultBorderSize));
			setBorderColor(attributes.getColor(R.styleable.CircularImageView_border_color, Color.WHITE));
*/

			// We no longer need our attributes TypedArray, give it back to cache
			attributes.recycle();
		}
	}
	
	
	public void setBorderWidth(int borderWidth){
		this.borderWidth = borderWidth;
		this.invalidate();
	}
	
	public void setBorderColor(int borderColor)	{		
		if(paintBorder != null)
			paintBorder.setColor(borderColor);
		
		this.invalidate();
	}
	
	private void loadBitmap()
	{
		BitmapDrawable bitmapDrawable = (BitmapDrawable) this.getDrawable();
		
		if(bitmapDrawable != null)
			image = bitmapDrawable.getBitmap();
	}
	
	@SuppressLint("DrawAllocation")
	@Override
    public void onDraw(Canvas canvas)
	{
		//load the bitmap
		loadBitmap();
					
		// init shader
		if(image !=null)
		{			
			shader = new BitmapShader(Bitmap.createScaledBitmap(image, canvas.getWidth(), canvas.getHeight(), false), Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
			paint.setShader(shader);
			int circleCenter = viewWidth / 2;
	
			// circleCenter is the x or y of the view's center
			// radius is the radius in pixels of the cirle to be drawn
			// paint contains the shader that will texture the shape
			canvas.drawCircle(circleCenter + borderWidth, circleCenter + borderWidth, circleCenter + borderWidth, paintBorder);
			canvas.drawCircle(circleCenter + borderWidth, circleCenter + borderWidth, circleCenter, paint);
		}		
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
    	int width = measureWidth(widthMeasureSpec);
    	int height = measureHeight(heightMeasureSpec, widthMeasureSpec);    	
    	
    	viewWidth = width - (borderWidth *2);
    	viewHeight = height - (borderWidth*2);
    	
    	setMeasuredDimension(width, height);
	}
	
	private int measureWidth(int measureSpec)
	{
	        int result = 0;
	        int specMode = MeasureSpec.getMode(measureSpec);
	        int specSize = MeasureSpec.getSize(measureSpec);

	        if (specMode == MeasureSpec.EXACTLY) {
	            // We were told how big to be
	            result = specSize;
	        } else {
	            // Measure the text
	            result = viewWidth;
	            
	        }

		return result;
	}
	
	private int measureHeight(int measureSpecHeight, int measureSpecWidth) {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpecHeight);
        int specSize = MeasureSpec.getSize(measureSpecHeight);

        if (specMode == MeasureSpec.EXACTLY) {
            // We were told how big to be
            result = specSize;
        } else {
            // Measure the text (beware: ascent is a negative number)
            result = viewHeight;           
        }
        return result;
    }
}