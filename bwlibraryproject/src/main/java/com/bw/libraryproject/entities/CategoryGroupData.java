package com.bw.libraryproject.entities;

import android.os.Parcel;
import android.os.Parcelable;
 
/**
 * @author Shahbaz Ali
 *
 * A basic object that can be parcelled to
 * transfer between objects
 *
 */
public class CategoryGroupData implements Parcelable {
	private int catGroupID=0;
	private String catGroupName="";
	private int catGroupIcon=0;
	private int catGroupMarker=0;
	private int catGroupMarker_sel=0;
	
	/**
	 * Standard basic constructor for non-parcel
	 * object creation
	 */
	public CategoryGroupData() { ; };
	
	
	
	
	public CategoryGroupData(int id, String name, int icon, int marker, int marker_sel){
		catGroupID=id;
		catGroupName=name;
		catGroupIcon=icon;
		catGroupMarker=marker;
		catGroupMarker_sel=marker_sel;
	}
	
	
	/**
	 *
	 * Constructor to use when re-constructing object
	 * from a parcel
	 *
	 * @param in a parcel from which to read this object
	 */
	public CategoryGroupData(Parcel in) {
		readFromParcel(in);
	}
 
	/**
	 * standard getter functions
	 */
	
	public int getCatGroupID(){return catGroupID;}
	public String getCatGroupName(){return catGroupName;}
	public int getCatGroupIcon(){return catGroupIcon;}
	public int getCatGroupMarker(){return catGroupMarker;}
	public int getCatGroupMarkerSelected(){return catGroupMarker_sel;}


	
	/**
	 * Standard setter functions
	 */
	public void setCatGroupID(int cgid){catGroupID=cgid;}
	public void setCatGroupName(String cgn){catGroupName=cgn;}
	public void setCatGroupIcon(int cgi){catGroupIcon=cgi;}
	public void setCatGroupMarker(int cgm){catGroupMarker=cgm;}
	public void setCatGroupMarkerSelected(int cgm_sel){catGroupMarker=cgm_sel;}

	
	
	
	@Override
	public int describeContents() {
		return 0;
	}
 
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// We just need to write each field into the
		// parcel. When we read from parcel, they
		// will come back in the same order
		dest.writeInt(catGroupID);
		dest.writeString(catGroupName);
		dest.writeInt(catGroupIcon);
		dest.writeInt(catGroupMarker);
		dest.writeInt(catGroupMarker_sel);
	}
 
	/**
	 *
	 * Called from the constructor to create this
	 * object from a parcel.
	 *
	 * @param in parcel from which to re-create object
	 */
	private void readFromParcel(Parcel in) {
 
		// We just need to read back each
		// field in the order that it was
		// written to the parcel
		catGroupID=in.readInt();
		catGroupName=in.readString();
		catGroupIcon=in.readInt();
		catGroupMarker=in.readInt();
		catGroupMarker_sel=in.readInt();
	}
 
    /**
     *
     * This field is needed for Android to be able to
     * create new objects, individually or as arrays.
     *
     * This also means that you can use use the default
     * constructor to create the object and use another
     * method to hyrdate it as necessary.
     *
     * I just find it easier to use the constructor.
     * It makes sense for the way my brain thinks ;-)
     *
     */
    public static final Creator CREATOR =
    	new Creator() {
            public CategoryGroupData createFromParcel(Parcel in) {
                return new CategoryGroupData(in);
            }
 
            public CategoryGroupData[] newArray(int size) {
                return new CategoryGroupData[size];
            }
    };
    
}

