package com.bw.libraryproject.entities;

public class PlaceData {
	private String mId;
	private String mDescription;
	
	
	
	public void PlaceData(String id, String desc){
		mId=id;
		mDescription=desc;
	}
	
	public void setId(String id){
		mId =id;
	}
	
	public void setDescription(String d){
		mDescription=d;
	}
	
	public String getId(){
		return mId;
	}
	
	public String getDescription(){
		return mDescription;
	}
	
}


