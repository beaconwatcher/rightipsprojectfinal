package com.bw.libraryproject.entities;

import android.os.Parcel;
import android.os.Parcelable;
 
/**
 * @author Shahbaz Ali
 *
 * A basic object that can be parcelled to
 * transfer between objects
 *
 */
public class SiteData implements Parcelable {
	private String siteID="0";
	private String catID="0";
	private String siteName="";
	private String siteAddress="";
	private String siteLatitude="0";
	private String siteLongitude="0";
	private String markerUrl="";
	private Integer siteTotalNotes=0;
	
	/**
	 * Standard basic constructor for non-parcel
	 * object creation
	 */
	public SiteData() { ; };
	
	public void SiteData(String id, String cid, String name, String address){
		siteID=id;
		catID=cid;
		siteName=name;
		siteAddress=address;
	}
	
 
	/**
	 *
	 * Constructor to use when re-constructing object
	 * from a parcel
	 *
	 * @param in a parcel from which to read this object
	 */
	public SiteData(Parcel in) {
		readFromParcel(in);
	}
 
	/**
	 * standard getter functions
	 */
	
	public String getSiteID(){return siteID;}
	public String getCatID(){return catID;}
	public String getSiteName(){return siteName;}
	public String getSiteAddress(){return siteAddress;}
	public String getLatitude(){return siteLatitude;}
	public String getLongitude(){return siteLongitude;}
	public String getMarkerUrl(){return markerUrl;}
	public Integer getSiteTotalNotifications(){return siteTotalNotes;}
	
	/**
	 * Standard setter functions
	 */
	public void setSiteID(String cgid){siteID=cgid;}
	public void setCatID(String cid){catID=cid;}
	public void setSiteName(String cgn){siteName=cgn;}
	public void setSiteAddress(String address){siteAddress=address;}
	public void setLatitude(String l){siteLatitude=l;}
	public void setLongitude(String lng){siteLongitude=lng;}
	public void setMarkerUrl(String url){markerUrl=url;}
	public void setSiteTotalNotifications(Integer t){siteTotalNotes=t;}
	
	
	@Override
	public int describeContents() {
		return 0;
	}
 
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// We just need to write each field into the
		// parcel. When we read from parcel, they
		// will come back in the same order
		dest.writeString(siteID);
		dest.writeString(catID);
		dest.writeString(siteName);
		dest.writeString(siteAddress);
		dest.writeString(siteLatitude);
		dest.writeString(siteLongitude);
		dest.writeString(markerUrl);
		dest.writeInt(siteTotalNotes);
	}
 
	/**
	 *
	 * Called from the constructor to create this
	 * object from a parcel.
	 *
	 * @param in parcel from which to re-create object
	 */
	private void readFromParcel(Parcel in) {
 
		// We just need to read back each
		// field in the order that it was
		// written to the parcel
		siteID=in.readString();
		catID=in.readString();
		siteName=in.readString();
		siteAddress=in.readString();
		siteLatitude=in.readString();
		siteLongitude=in.readString();
		markerUrl=in.readString();
		siteTotalNotes=in.readInt();
	}
 
    /**
     *
     * This field is needed for Android to be able to
     * create new objects, individually or as arrays.
     *
     * This also means that you can use use the default
     * constructor to create the object and use another
     * method to hyrdate it as necessary.
     *
     * I just find it easier to use the constructor.
     * It makes sense for the way my brain thinks ;-)
     *
     */
    public static final Creator CREATOR =
    	new Creator() {
            public SiteData createFromParcel(Parcel in) {
                return new SiteData(in);
            }
 
            public SiteData[] newArray(int size) {
                return new SiteData[size];
            }
    };
    
}

