package com.bw.libraryproject.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * @author Shahbaz Ali
 *
 * A base class for RighTips Notifications that can be parcelled to
 * transfer between objects
 *
 */
public class LoginUserDataDeprecated implements Parcelable {
	private String userID="0";
	private String uid="0";
	private String email="";
	private String password="";
	private String firstName="";
	private String lastName="";
	private String fullName="";
	private String gender="";
	private String provider="";
	private String profileImageURL="";
	private String profileImageLocalPath="";
	private ArrayList<UserAppData> userApps=new ArrayList<UserAppData>();
	
	
	
	/**
	 * Standard basic constructor for non-parcel
	 * object creation
	 */
	public LoginUserDataDeprecated() { ; };
	

	public LoginUserDataDeprecated(String id, String ud, String m, String pwd, String p){
		userID=id;
		uid=ud;
		email=m;
		password=pwd;
		provider=p;
	}
	
	
	public LoginUserDataDeprecated(String response){
		JSONObject jo=null;
		try{
			jo=new JSONObject(response);
			JSONObject loginInfo=jo.getJSONObject("data");
			
			userID = loginInfo.getString("userID");
			uid = loginInfo.getString("uid");
			firstName = loginInfo.getString("first_name");
			lastName = loginInfo.getString("last_name");
			
			if(loginInfo.has("profile_pic")){
				profileImageURL=loginInfo.getString("profile_pic");
			}
			
			
			if(loginInfo.has("apps")){
				JSONArray apps = loginInfo.getJSONArray("apps");
				if(apps.length() > 0){
					for(int i=0; i<apps.length(); i++){
						JSONObject app = apps.getJSONObject(i);
						UserAppData appData = new UserAppData(app.getString("apk_id"), app.getString("apk_title"), app.getString("apk_uuid"), app.getString("apk_key"));
						userApps.add(appData);
					}
				}
			}
		}
		catch(JSONException e){
            Log.d("Login", e.getMessage());
		}
	}
	

	
	
	
	/**
	 *
	 * Constructor to use when re-constructing object
	 * from a parcel
	 *
	 * @param in a parcel from which to read this object
	 */
	public LoginUserDataDeprecated(Parcel in) {
		readFromParcel(in);
	}
	
	/**
	 * standard getter functions
	 */
	public String getUserID(){return this.userID;}
	public String getUID(){return this.uid;}
	public String getEmail(){return this.email;}
	public String getPassword(){return this.password;}
	public String getFirstName(){return this.firstName;}
	public String getLastName(){return this.lastName;}
	public String getFullName(){
		if(fullName!=""){
			return fullName;
		}
		else{
			return firstName+" "+lastName;
		}
	}
	public String getGender(){return this.gender;}
	public String getProvider(){return this.provider;}
	public String getProfileImageURL(){return this.profileImageURL;}
	public String getProfileImageLocalPath(){return this.profileImageLocalPath;}
	public ArrayList<UserAppData> getApps(){return this.userApps;}
	public String getAppUUID(){
		if(userApps.size() > 0){
			return userApps.get(0).getUUID();	
		}
		return null;
	}
	
	public String getAppKey(){
		if(userApps.size() > 0){
			return userApps.get(0).getKey();	
		}
		return null;
	}
	


	
	/**
	 * Standard setter functions
	 */
	public void setUserID(String id){this.userID=id;}
	public void setUID(String ud){this.uid=ud;}
	public void setEmail(String m){this.email=m;}
	public void setPassword(String p){this.password=p;}
	public void setFirstName(String fName){this.firstName=fName;}
	public void setLastName(String lName){this.lastName=lName;}
	public void setFullName(String fName){this.fullName=fName;}
	public void setGender(String g){this.gender=g;}
	public void setProvider(String p){this.provider=p;}
	public void setProfileImageURL(String url){this.profileImageURL=url;}
	public void setProfileImageLocalPath(String url){this.profileImageLocalPath=url;}
	public void setApps(ArrayList<UserAppData> apps){this.userApps=apps;}

	
	
	@Override
	public int describeContents() {
		return 0;
	}
 
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// We just need to write each field into the
		// parcel. When we read from parcel, they
		// will come back in the same order
		dest.writeString(userID);
		dest.writeString(uid);
		dest.writeString(email);
		dest.writeString(password);
		dest.writeString(firstName);
		dest.writeString(lastName);
		dest.writeString(fullName);
		dest.writeString(gender);
		dest.writeString(provider);
		dest.writeString(profileImageURL);
		dest.writeString(profileImageLocalPath);
		dest.writeTypedList(userApps);
	}
 
	/**
	 *
	 * Called from the constructor to create this
	 * object from a parcel.
	 *
	 * @param in parcel from which to re-create object
	 */
	private void readFromParcel(Parcel in) {
 
		// We just need to read back each
		// field in the order that it was
		// written to the parcel
		userID=in.readString();
		uid=in.readString();
		email=in.readString();
		password=in.readString();
		firstName=in.readString();
		lastName=in.readString();
		fullName=in.readString();
		gender=in.readString();
		provider=in.readString();
		profileImageURL=in.readString();
		profileImageLocalPath=in.readString();
		userApps=in.readArrayList(UserAppData.class.getClassLoader());
	}
 
    /**
     *
     * This field is needed for Android to be able to
     * create new objects, individually or as arrays.
     *
     * This also means that you can use use the default
     * constructor to create the object and use another
     * method to hyrdate it as necessary.
     *
     * I just find it easier to use the constructor.
     * It makes sense for the way my brain thinks ;-)
     *
     */
    public static final Creator CREATOR =
    	new Creator() {
            public LoginUserDataDeprecated createFromParcel(Parcel in) {
                return new LoginUserDataDeprecated(in);
            }
 
            public LoginUserDataDeprecated[] newArray(int size) {
                return new LoginUserDataDeprecated[size];
            }
    };
    
	
}


