package com.bw.libraryproject.entities;

import android.util.Log;

import com.bw.libraryproject.utils.NotificationUtilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class BeaconWithNotification{
	public String UUID;
	public String Major;
	public String Minor;
	public String MacAddress;
	
	public ArrayList<NotificationData> notifications=new ArrayList<NotificationData>();
	
	public BeaconWithNotification(String uuid, String major, String minor, String address){
		this.UUID=uuid;
		this.Major=major;
		this.Minor=minor;
		this.MacAddress=address;
	}
	
	
	
	public static HashMap<String, BeaconWithNotification> convertNotifications(String notes){
		HashMap<String, BeaconWithNotification> beacons= new HashMap<String, BeaconWithNotification>();
		try{
			JSONObject jo=new JSONObject(notes);
			
			JSONArray jBeacons=jo.getJSONArray("data");
			for(int i=0; i<jBeacons.length(); i++){
				JSONObject jBeacon = jBeacons.getJSONObject(i);
				BeaconWithNotification bn=
						new BeaconWithNotification(
								jBeacon.getString("msb_uuid"),
								jBeacon.getString("msb_major"),
								jBeacon.getString("msb_minor"),
								jBeacon.getString("msb_mac_address"));
				
				if (!bn.UUID.equals("")){
					beacons.put(bn.UUID+"_"+bn.Major+"_"+bn.Minor, bn);
					
					JSONArray jNotes = jBeacon.getJSONArray("notifications");
					bn.notifications=NotificationUtilities.JsonToNotifications(jNotes);
					/*
					
					for(int n=0; n<jNotes.length(); n++){
						JSONObject jNote=jNotes.getJSONObject(n);
						NotificationData nd=NotificationData.getNotificationFromJson(jNote);
						bn.notifications.add(nd);
					}*/
				}
			}
		}
		
		catch(JSONException e){
            Log.e("log_tag", "Error parsing data "+e.toString());
		}
		
		
		return beacons;
	}
}


