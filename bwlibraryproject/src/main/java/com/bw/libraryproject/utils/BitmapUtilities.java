package com.bw.libraryproject.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.BlurMaskFilter.Blur;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Matrix.ScaleToFit;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class BitmapUtilities {
	 public static Bitmap getScaledBitmap(Bitmap b, int reqHeight)  {
	        int bWidth = b.getWidth();
	        int bHeight = b.getHeight();

	        int nHeight = reqHeight;
	        float percent=(((float)nHeight)/(float)bHeight)*(float)100;
	        int nWidth=(bWidth/100)*((int)percent);
	        
	        return Bitmap.createScaledBitmap(b, nWidth, nHeight, false);
	    }
	 
	 public static Bitmap resizeBitmap(Bitmap b, int reqWidth)  {
		 	float aspectRatio = b.getWidth() / (float) b.getHeight();
		    int width = reqWidth;
		    int height = Math.round(width / aspectRatio);

		    return Bitmap.createScaledBitmap(b, width, height, false);
	 }
	 
	 
	 
	 public static Drawable scaleDrawable(Drawable image, float scaleFactor, Resources r) {
		    if ((image == null) || !(image instanceof BitmapDrawable)) {
		        return image;
		    }
		    Bitmap b = ((BitmapDrawable)image).getBitmap();

		    int sizeX = Math.round(image.getIntrinsicWidth() * scaleFactor);
		    int sizeY = Math.round(image.getIntrinsicHeight() * scaleFactor);
		    Bitmap bitmapResized = Bitmap.createScaledBitmap(b, sizeX, sizeY, false);
		    image = new BitmapDrawable(r, bitmapResized);
		    return image;
		}
	 
	 
	 
	 public static Bitmap addShadow(final Bitmap bm, final int dstWidth, final int dstHeight, int color, int size, int dx, int dy) {
		    final Bitmap mask = Bitmap.createBitmap(dstWidth+dx, dstHeight+dy, Config.ALPHA_8);

		    final Matrix scaleToFit = new Matrix();
		    final RectF src = new RectF(0, 0, bm.getWidth(), bm.getHeight());
		    final RectF dst = new RectF(0, 0, dstWidth - dx, dstHeight - dy);
		    scaleToFit.setRectToRect(src, dst, ScaleToFit.CENTER);

		    final Matrix dropShadow = new Matrix(scaleToFit);
		    dropShadow.postTranslate(dx, dy);

		    final Canvas maskCanvas = new Canvas(mask);
		    final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		    maskCanvas.drawBitmap(bm, scaleToFit, paint);
		    paint.setXfermode(new PorterDuffXfermode(Mode.SRC_OUT));
		    maskCanvas.drawBitmap(bm, dropShadow, paint);

		    final BlurMaskFilter filter = new BlurMaskFilter(size, Blur.NORMAL);
		    paint.reset();
		    paint.setAntiAlias(true);
		    paint.setColor(color);
		    paint.setMaskFilter(filter);
		    paint.setFilterBitmap(true);

		    final Bitmap ret = Bitmap.createBitmap(dstWidth+size, dstHeight+size, Config.ARGB_8888);
		    final Canvas retCanvas = new Canvas(ret);
		    retCanvas.drawBitmap(mask, 0,  0, paint);
		    retCanvas.drawBitmap(bm, scaleToFit, null);
		    mask.recycle();
		    return ret;
		}
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	    public static ByteArrayInputStream getByteArrayStream(Uri uri, int thumbnailSize, Context context, Bitmap bitmap){
	    	try{
		    	if(bitmap==null){
		    		bitmap=getThumbnail(uri, thumbnailSize, context);
		    	}
	    		ByteArrayOutputStream out = new ByteArrayOutputStream();
	    		bitmap.compress(Bitmap.CompressFormat.PNG, 85, out);
	    		byte[] myByteArray = out.toByteArray();
	    		out.close();
	    		return new ByteArrayInputStream(myByteArray);
	    	}
	    	catch(IOException e){return null;}
	    }
	    
	    
	    
	    public static byte[] getByteArray(Uri uri, int thumbnailSize, Context context){
	    	try{
	    		Bitmap bitmap=getThumbnail(uri, thumbnailSize, context);
	    		ByteArrayOutputStream out = new ByteArrayOutputStream();
	    		bitmap.compress(Bitmap.CompressFormat.PNG, 85, out);
	    		byte[] myByteArray = out.toByteArray();
	    		out.close();
	    		return myByteArray;
	    	}
	    	catch(IOException e){return null;}
	    }
	    
	    
		public static  Bitmap getThumbnail(Uri uri, int thumbnailSize, Context context) throws FileNotFoundException, IOException{
		        InputStream input = context.getContentResolver().openInputStream(uri);

		        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
		        onlyBoundsOptions.inJustDecodeBounds = true;
		        onlyBoundsOptions.inDither=true;//optional
		        onlyBoundsOptions.inPreferredConfig= Config.ARGB_8888;//optional
		        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
		        input.close();
		        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1))
		            return null;

		        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

		        double ratio = (originalSize > thumbnailSize) ? (originalSize / thumbnailSize) : 1.0;

		        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
		        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
		        bitmapOptions.inDither=true;//optional
		        bitmapOptions.inPreferredConfig= Config.ARGB_8888;//optional
		        input = context.getContentResolver().openInputStream(uri);
		        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
		        input.close();
		        return bitmap;
		    }

		    private static int getPowerOfTwoForSampleRatio(double ratio){
		        int k = Integer.highestOneBit((int)Math.floor(ratio));
		        if(k==0) return 1;
		        else return k;
		    }








	/**
	 * This method is responsible for solving the rotation issue if exist. Also scale the images to
	 * 1024x1024 resolution
	 *
	 * @param context       The current context
	 * @param selectedImage The Image URI
	 * @return Bitmap image results
	 * @throws IOException
	 */
	public static Bitmap handleSamplingAndRotationBitmap(Context context, Uri selectedImage)
			throws IOException {
		int MAX_HEIGHT = 1024;
		int MAX_WIDTH = 1024;

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		InputStream imageStream = context.getContentResolver().openInputStream(selectedImage);
		BitmapFactory.decodeStream(imageStream, null, options);
		imageStream.close();

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		imageStream = context.getContentResolver().openInputStream(selectedImage);
		Bitmap img = BitmapFactory.decodeStream(imageStream, null, options);

		img = rotateImageIfRequired(img, selectedImage);
		return img;
	}



	/**
	 * Calculate an inSampleSize for use in a {@link BitmapFactory.Options} object when decoding
	 * bitmaps using the decode* methods from {@link BitmapFactory}. This implementation calculates
	 * the closest inSampleSize that will result in the final decoded bitmap having a width and
	 * height equal to or larger than the requested width and height. This implementation does not
	 * ensure a power of 2 is returned for inSampleSize which can be faster when decoding but
	 * results in a larger bitmap which isn't as useful for caching purposes.
	 *
	 * @param options   An options object with out* params already populated (run through a decode*
	 *                  method with inJustDecodeBounds==true
	 * @param reqWidth  The requested width of the resulting bitmap
	 * @param reqHeight The requested height of the resulting bitmap
	 * @return The value to be used for inSampleSize
	 */
	private static int calculateInSampleSize(BitmapFactory.Options options,
											 int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and width
			final int heightRatio = Math.round((float) height / (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will guarantee a final image
			// with both dimensions larger than or equal to the requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

			// This offers some additional logic in case the image has a strange
			// aspect ratio. For example, a panorama may have a much larger
			// width than height. In these cases the total pixels might still
			// end up being too large to fit comfortably in memory, so we should
			// be more aggressive with sample down the image (=larger inSampleSize).

			final float totalPixels = width * height;

			// Anything more than 2x the requested pixels we'll sample down further
			final float totalReqPixelsCap = reqWidth * reqHeight * 2;

			while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
				inSampleSize++;
			}
		}
		return inSampleSize;
	}



	/**
	 * Rotate an image if required.
	 *
	 * @param img           The image bitmap
	 * @param selectedImage Image URI
	 * @return The resulted Bitmap after manipulation
	 */
	private static Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage) throws IOException {

		ExifInterface ei = new ExifInterface(selectedImage.getPath());
		int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

		switch (orientation) {
			case ExifInterface.ORIENTATION_ROTATE_90:
				return rotateImage(img, 90);
			case ExifInterface.ORIENTATION_ROTATE_180:
				return rotateImage(img, 180);
			case ExifInterface.ORIENTATION_ROTATE_270:
				return rotateImage(img, 270);
			default:
				return img;
		}
	}


	private static Bitmap rotateImage(Bitmap img, int degree) {
		Matrix matrix = new Matrix();
		matrix.postRotate(degree);
		Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
		img.recycle();
		return rotatedImg;
	}


	    

}
