package com.bw.libraryproject.utils;

import android.text.format.DateUtils;

public class DateUtilities {
	
	public static String getDuration(String val){
		if(val.equals(""))return val;
		if(val.length() < 13){
			for(int a=val.length(); a< 13; a++){
				val=val+"0";
			}
		}
		long miliseconds=Long.parseLong(val);
		CharSequence charSequence=DateUtils.getRelativeTimeSpanString(miliseconds);
		StringBuilder sb = new StringBuilder(charSequence.length());
		sb.append(charSequence);
		String duration=sb.toString();
		return duration;
	}
}
