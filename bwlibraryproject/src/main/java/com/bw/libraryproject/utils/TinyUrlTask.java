package com.bw.libraryproject.utils;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;


public class TinyUrlTask extends AsyncTask<String, Void, String> {
	private static String T_URL = "http://tinyurl.com/api-create.php?url=";
    private final static String TAG = "TinyUrlTask";

	
	
	public interface TinyUrlResponseHandler {
	    void onUrlShortened(String url);
	} 
	
    private TinyUrlResponseHandler responseHandler;


    public TinyUrlTask(TinyUrlResponseHandler response) {
    	responseHandler = response;
    }

    @Override
    protected String doInBackground(String... params) {
        return getTinyUrl(params[0]);
    }

    @Override
    protected void onPostExecute(String url) {
        if (isCancelled()) {
            url = "";
        }

        if (responseHandler != null) {
        	responseHandler.onUrlShortened(url);
        }
    }
    
    

    public static String getTinyUrl(String longUrl) {
        String tinyUrl = "";
        String urlString = T_URL + longUrl;
        
        try {
            URL url = new URL(urlString);
            
	        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
	        String str;
	        
	        while ((str = in.readLine()) != null) {
	          tinyUrl += str;
	        }
	        in.close();
        }
        catch (Exception e) {
           Log.e(TAG, "Can not create an tinyurl link",e);
        }
        return tinyUrl;
    }

}