package com.bw.libraryproject.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.bw.libraryproject.R;
import com.radiusnetworks.ibeacon.IBeaconConsumer;
import com.radiusnetworks.ibeacon.IBeaconManager;
import com.radiusnetworks.ibeacon.MonitorNotifier;
import com.radiusnetworks.ibeacon.Region;

public class BeaconMonitorService extends Service implements IBeaconConsumer {
	private static final String TAG="BeaconMonitorService";
	
	private IBeaconManager iBeaconManager = IBeaconManager.getInstanceForApplication(this);
	private Intent notificationIntent;
	
	private Boolean notificationSent=false;
	
	
	//For stopping this service for a predefined time.
	//If we don't stop, it will continue monitoring
	//in the background and drain the battery.
	private static int SELF_STOP_TIME = (1000*60)*(2);
	

	@Override
	public IBinder onBind(Intent arg0) {
		Log.d(TAG, "Service is bound to: "+arg0.getPackage());
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		iBeaconManager.bind(this);
		Log.d(TAG,  "Service onCreate called");
	}

	@Override
	public void onDestroy() {
		Log.d(TAG,  "Service onDestroy called");
		if(iBeaconManager.isBound(this)){
			iBeaconManager.unBind(this);
		}
		super.onDestroy();
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if(notificationIntent==null){
			notificationIntent=intent;
		}
		Log.d(TAG,  "Service On StartCommand called");

		//We must stop this background service
		//because if it is not stopped, it will foreever run
		//and will drain the battery.
		final Handler handler = new Handler();
		final Runnable runnable = new Runnable() {
			@Override
			public void run() {
				Log.d(TAG,  "Service stopSelf called after: "+SELF_STOP_TIME);
				iBeaconManager.unBind(BeaconMonitorService.this);
				stopSelf();
			}
		};
		handler.postDelayed(runnable, SELF_STOP_TIME);
		return START_STICKY;
	}

	

	@Override
	public void onIBeaconServiceConnect() {
		Log.d(TAG,  "Service onIBeaconServiceConnect called");
		iBeaconManager.setMonitorNotifier(new MonitorNotifier() {
			@Override
			public void didEnterRegion(Region region) {
				Log.d(TAG, "didEnterRegion called");
				if(!notificationSent){
					generateNotification(BeaconMonitorService.this);
				}
			}

			@Override
			public void didExitRegion(Region region) {
				//Log.e("BeaconDetactorService", "didExitRegion");
				//generateNotification(BeaconDetactorService.this);
			}

			@Override
			public void didDetermineStateForRegion(int state, Region region) {
				//Log.e("BeaconDetactorService", "didDetermineStateForRegion:" + state);
			}

		});

		try {
			iBeaconManager.startMonitoringBeaconsInRegion(new Region("myMonitoringUniqueId", null, null, null));
		} catch (RemoteException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Issues a notification to inform the user that you entered into a region.
	 */
	private void generateNotification(Context context) {
        //Intent notifyIntent = new Intent();//, MonitoringActivity.class);
        String targetName = notificationIntent.getStringExtra("targetActivity");
        
        
        if(targetName!=null){
	        try {
	           Class<?> clazz = Class.forName(targetName);
	           notificationIntent.setClass(context, clazz);
	        }
	        catch (ClassNotFoundException e) {
	        	//Log.e("BeaconDetactorService", "No target activity defined for notification");
	        }
        }
        
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);// .FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivities(
	            context,
	            0,
	            new Intent[]{notificationIntent},
	            PendingIntent.FLAG_UPDATE_CURRENT);
	        Notification notification = new Notification.Builder(context)
	            .setSmallIcon(R.drawable.beacon_gray)
	            .setContentTitle("BW Simulation Notification")
	            .setContentText("There is something interesting")
	            .setAutoCancel(true)
	            .setContentIntent(pendingIntent)
	            .build();
	        notification.defaults |= Notification.DEFAULT_SOUND;
	        notification.defaults |= Notification.DEFAULT_LIGHTS;
	        ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).notify(1, notification);
	        
	        
	        notificationSent = true;
	        
	}
}