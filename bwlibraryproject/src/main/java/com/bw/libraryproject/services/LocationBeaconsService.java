/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bw.libraryproject.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.bw.libraryproject.entities.NotificationData;
import com.bw.libraryproject.utils.NotificationUtilities;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * This service pulls notifications from the api contained in the incoming Intent (see
 * onHandleIntent()). As it runs, it broadcasts its status using LocalBroadcastManager; any
 * component that wants to see the status should implement a subclass of BroadcastReceiver and
 * register to receive broadcast Intents with category = CATEGORY_DEFAULT and action
 * Constants.BROADCAST_ACTION.
 *
 */
public class LocationBeaconsService extends IntentService {
	private static final String TAG="LocationBeaconsService";
	// Defines a custom Intent action
    public static final String ACTION_LOCATION_NOTIFICATIONS_FOUND ="com.beaconwatcher.rightips.services.ACTION_LOCATION_NOTIFICATIONS_FOUND";
    // Defines the key for the status "extra" in an Intent
    public static final String NOTIFICATIONS_ARRAY ="com.beaconwatcher.rightips.services.NOTIFICATION_ARRAY";
	
	
	private Context mContext;
	private int radius=5;
	private String appKey="MTRfX1JpZ2h0VGlwcw==";
	
	
	public LocationBeaconsService() {
		super("LocationBeaconsService");
		// TODO Auto-generated constructor stub
	}
	
    @Override
    protected void onHandleIntent(Intent workIntent) {
        // Gets data from the incoming Intent
    	Log.d(TAG, "Loading all notifications by location");
    	
    	mContext=this;
    	String lat=workIntent.getStringExtra("latitude");
    	String lng=workIntent.getStringExtra("longitude");
    	
		String url="https://api.beaconwatcher.com/index.php?action=getLocNotification&key="+appKey+"&lat="+lat+"&lng="+lng+"&km="+radius;
		
		AsyncHttpClient httpClient=new AsyncHttpClient();
		try{
			httpClient.get(url, responseHandler);
		}
		
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
    }
    
    
    AsyncHttpResponseHandler responseHandler=new AsyncHttpResponseHandler() {
		@Override
		public void onStart() {
			Log.v(TAG, "onStart");
		}

		@Override
		public void onSuccess(String response) {
			Log.v(TAG, "onSuccess");
			
			//{"status":1,"message":"Beacon details","data":[{"msb_id":"41","msb_name":"Testing Beacons","msb_location":"","msb_uuid":"5144A35F387A7AC0B254DFB1381C9DFF","msb_minor":"1","msb_major":"1","msb_txpower":"0","msb_mac_address":"test","site_id":"20","site_title":"ESOL OFFICE","company":"Right Tips","notifications":[{"nt_id":"21","nt_name":"Pizza Hut Special Deal","nt_title":"Fun 4 All","nt_details":"Big Time Treat Inside Box, 2 hot spicy pizzaz, 8 Garlic breads, 500 ml Cold Drink.","zone":"1","template":"http:\/\/beaconwatcher.com\/api\/template.php?nt_id=21"}]}]}
			//Log.d(TAG, "All Notifications Loaded: "+response);
			JSONObject jo=null;
			ArrayList<NotificationData> notes;
			
			try{
				jo=new JSONObject(response);
				
				if(jo.has("data")){
					JSONArray jNotes=jo.getJSONArray("data");
					if(jNotes.length()>0){
						notes=NotificationUtilities.JsonToNotifications(jNotes);
						broadcastMessage(notes);
					}
				}
				
				else if(jo.has("status")){
					String status=jo.getString("status");
					if(status.equals("1")){
						JSONArray jNotes=jo.getJSONArray("data");
						notes=NotificationUtilities.JsonToNotifications(jNotes);
						broadcastMessage(notes);
					}
					else{
						broadcastMessage(null);
					}
				}
				
				
				else{
					broadcastMessage(null);
				}
			}
			catch(JSONException e){
                Log.e("log_tag", "Error parsing data "+e.toString());
                broadcastMessage(null);
			}
		}

		@Override
		public void onFailure(Throwable error, String content) {
			Log.e(TAG, "onFailure error : " + error.toString()
					+ "content : " + content);
			broadcastMessage(null);
		}

		@Override
		public void onFinish() {
			Log.v(TAG, "onFinish");
		}
	};
	
	private void broadcastMessage(ArrayList<NotificationData> notes){
		/*
	     * Creates a new Intent containing a Uri object
	     * BROADCAST_ACTION is a custom Intent action
	     */
		
		if(notes!=null){
			int total=notes.size();
			for(int i=0; i<total; i++){
				notes.get(i).setSource("location");
			}
		}
		
		
	    Intent localIntent = new Intent(ACTION_LOCATION_NOTIFICATIONS_FOUND)
	            // Puts the status into the Intent
	            .putExtra(NOTIFICATIONS_ARRAY, notes);
	    // Broadcasts the Intent to receivers in this app.
	    LocalBroadcastManager.getInstance(mContext).sendBroadcast(localIntent);					
	}
}