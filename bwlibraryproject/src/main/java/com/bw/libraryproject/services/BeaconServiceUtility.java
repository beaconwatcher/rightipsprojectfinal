
package com.bw.libraryproject.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.radiusnetworks.ibeacon.IBeaconConsumer;
import com.radiusnetworks.ibeacon.IBeaconManager;

public class BeaconServiceUtility {

	private Context context;
	private PendingIntent pintent;
	private AlarmManager alarm;
	private Intent serviceIntent;
	
	public Boolean isInBackground = false;

	public BeaconServiceUtility(Context context) {
		super();
		this.context = context;
		serviceIntent = new Intent(context, BeaconMonitorService.class);
		//alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		pintent = PendingIntent.getService(context, 0, serviceIntent, 0);
		
		stopBackgroundScan();
	}

	public void setNotificationTarget(Intent intent){
		serviceIntent = intent;
		serviceIntent.setClass(context, BeaconMonitorService.class);
		pintent = PendingIntent.getService(context, 0, serviceIntent, 0);
	}
	
	public void onStart(IBeaconManager iBeaconManager, IBeaconConsumer consumer) {
		stopBackgroundScan();
		iBeaconManager.bind(consumer);

	}

	public void onStop(IBeaconManager iBeaconManager, IBeaconConsumer consumer) {
		iBeaconManager.unBind(consumer);
		startBackgroundScan();
	}

	public void stopBackgroundScan() {
		if(alarm!=null){
			alarm.cancel(pintent);
		}
		context.stopService(serviceIntent);
	}

	private void startBackgroundScan() {
		//If you need to start service after specified number of time
		//then uncomment following 3 lines.
		//In case service is destroyed or self started, following code will
		//start the service again.
		
		/*Calendar cal = Calendar.getInstance();
		cal.add(Calendar.SECOND, 2);
		alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), (1000*60), pintent); // 6*60 * 1000*/
		
		context.startService(serviceIntent);
	}
}
