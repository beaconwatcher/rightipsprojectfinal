package com.bw.libraryproject.usables.image;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.bw.libraryproject.R;

public class ScaledImageView extends ImageView {
	//Attributes
	Boolean fitExactly=false;
	
	
    public ScaledImageView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        applyXmlAttributes(attrs);
    }
    
    private void applyXmlAttributes(AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.ScaledImageView);
        this.fitExactly = a.getBoolean(R.styleable.ScaledImageView_fitExactly, false);
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        Drawable d = getDrawable();
        
        if(getBackground()!=null){
        	d = getBackground();
        }
        
        

        if (d != null) {
            int width;
            int height;
            
            if(this.fitExactly==true){
            	width = MeasureSpec.getSize(widthMeasureSpec);
                height = (int) Math.ceil(width * (float) d.getIntrinsicHeight() / d.getIntrinsicWidth());
            }
            
            
            
            width = MeasureSpec.getSize(widthMeasureSpec);
            height = (int) Math.ceil(width * (float) d.getIntrinsicHeight() / d.getIntrinsicWidth());          
            setMeasuredDimension(width, height);
        }
        
        else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
}