package com.bw.libraryproject.usables.image;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class FitImageView extends ImageView {
    public FitImageView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        Drawable d = getDrawable();
        
        if(getBackground()!=null){
        	d = getBackground();
        }
        
        

        if (d != null) {
            int width;
            int height;
            
            width = MeasureSpec.getSize(widthMeasureSpec);
            height = (int) Math.ceil(width * (float) d.getIntrinsicHeight() / d.getIntrinsicWidth());

            
            if (MeasureSpec.getMode(heightMeasureSpec) != MeasureSpec.EXACTLY) {
                height = MeasureSpec.getSize(heightMeasureSpec);
                width = (int) Math.ceil(height * (float) d.getIntrinsicWidth() / d.getIntrinsicHeight());
            } else {
            }
            setMeasuredDimension(width, height);
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
}