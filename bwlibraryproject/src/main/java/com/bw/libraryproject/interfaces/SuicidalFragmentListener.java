package com.bw.libraryproject.interfaces;

public interface SuicidalFragmentListener {
    void onFragmentSuicide(String tag);
}
