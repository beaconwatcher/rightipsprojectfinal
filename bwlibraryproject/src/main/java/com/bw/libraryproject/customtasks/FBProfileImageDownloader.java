package com.bw.libraryproject.customtasks;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class FBProfileImageDownloader extends AsyncTask<String, Void, String> {
	private static final String TAG="FBProfileImageDownloader";
	
	public interface FBProfilePicResponseHandler{
		public void onProfilePicSaved(String path);
		public void onError();
	}
	
	
	private Context context;
    private String url;
    private String uid;
    private ImageView imageView;
    FBProfilePicResponseHandler handler;

    public FBProfileImageDownloader(Context c, String url, String uid, FBProfilePicResponseHandler handler) {
    	this.context=c;
    	this.handler = handler;
        this.url = url+"?type=large&width=200&height=200";
        this.uid = uid;
    }

    @Override
    protected String doInBackground(String... params) {
    	AndroidHttpClient client = AndroidHttpClient.newInstance("Android");
        final HttpGet request = new HttpGet(url);
        
    	try {
            HttpResponse response = client.execute(request);
            final int statusCode = 
                response.getStatusLine().getStatusCode();

            if (statusCode != HttpStatus.SC_OK) {
                Header[] headers = response.getHeaders("Location");
                if (headers != null && headers.length != 0) {
                    String newUrl =headers[headers.length - 1].getValue();
                    Log.d(TAG, "Facebook Profile Picture Url: "+newUrl);
                    
                    
                    URL urlConnection = new URL(newUrl);
                    HttpURLConnection connection = (HttpURLConnection) urlConnection
                            .openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    Bitmap myBitmap = BitmapFactory.decodeStream(input);
                    
                    File pictureFile = getOutputMediaFile();
                    Log.d(TAG, "Local Storage Path Created: "+pictureFile);
                    
                    
                    if (pictureFile != null) {
                        try {
                            FileOutputStream fos = new FileOutputStream(pictureFile);
                            myBitmap.compress(Bitmap.CompressFormat.PNG, 90, fos);
                            fos.close();
                            return pictureFile.getAbsolutePath();
                        }
                    	catch (FileNotFoundException e) {
                    	}
                    	catch (IOException e) {
                    	}  
                    } 
                }
            }
        }
        catch (Exception e) {
            request.abort();
        }
    	return "";
    }
    
    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if(handler!=null){
        	  handler.onProfilePicSaved(result);
        }
    }
    
    
    /** Create a File for saving an image or video */
    private  File getOutputMediaFile(){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this. 
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"+context.getPackageName()+"/files/profile");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        } 
        // Create a media file name
//        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
            String mImageName=uid +".jpg";
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);  
        return mediaFile;
    } 

}
