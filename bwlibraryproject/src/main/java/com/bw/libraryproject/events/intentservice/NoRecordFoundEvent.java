package com.bw.libraryproject.events.intentservice;

public class NoRecordFoundEvent {
	public String tag="";
    public String message="";
    
    public NoRecordFoundEvent(String tag, String message) {
    	this.tag = tag;
        this.message = message;
    }
}