package com.bw.libraryproject.events.intentservice;

public class IntentServiceMessageEvent {
    public final String message;

    public IntentServiceMessageEvent(String message) {
        this.message = message;
    }
}