package com.bw.libraryproject.events.intentservice;

public class IntentServiceErrorEvent {
    public final String message;

    public IntentServiceErrorEvent(String message) {
        this.message = message;
    }
}