package com.bw.libraryproject.events.intentservice;

public class IntentServiceDataEvent {
    private String type;
    private Object data;
    

    public IntentServiceDataEvent(String type, Object data) {
        this.type = type;
        this.data = data;
    }
    
    public String getType(){
    	return type;
    }
    
    public Object getData(){
    	return data;
    }
}