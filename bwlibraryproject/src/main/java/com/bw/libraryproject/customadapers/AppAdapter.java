package com.bw.libraryproject.customadapers;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bw.libraryproject.customrenderers.CustomAppItem;
import com.bw.libraryproject.entities.UserAppData;

import java.util.ArrayList;
import java.util.List;

public class AppAdapter extends BaseAdapter {
	/** Remember our context so we can use it when constructing views. */
	private Context mContext;
	 
	/**
	* Hold onto a copy of the entire Country List.
	*/
	private List<UserAppData> mItems = new ArrayList<UserAppData>();
	 
	public AppAdapter(Context context, ArrayList<UserAppData> items) {
	     mContext = context;
	     mItems = items;
	}
	 
	public int getCount() {
	     return mItems .size();
	}
	 
	public Object getItem(int position) {
	     return mItems.get(position);
	}
	 
	 /** Use the array index as a unique id. */
	public long getItemId(int position) {
	     return position;
	}
	 
	/**
	* @param convertView
	*            The old view to overwrite, if one is passed
	* @returns a ContactEntryView that holds wraps around an ContactEntry
	*/
	public View getView(int position, View convertView, ViewGroup parent) {
		CustomAppItem item;
	    if (convertView == null) {
	    	item = new CustomAppItem(mContext, mItems.get(position));
	    } else {
	         item = (CustomAppItem) convertView;
	    }
	    
        String name = mItems.get(position).getName();
        item.setTitle(name);
        return item;
	}
}
