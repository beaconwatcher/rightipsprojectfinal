package com.bw.libraryproject.customadapers;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bw.libraryproject.customrenderers.CustomListItem;
import com.bw.libraryproject.entities.CustomData;

import java.util.ArrayList;
import java.util.List;

public class SitesAdapter extends BaseAdapter {
	/** Remember our context so we can use it when constructing views. */
	private Context mContext;
	 
	/**
	* Hold onto a copy of the entire Country List.
	*/
	private List<CustomData> mItems = new ArrayList<CustomData>();
	 
	public SitesAdapter(Context context, ArrayList<CustomData> items) {
	     mContext = context;
	     mItems = items;
	}
	 
	public int getCount() {
	     return mItems .size();
	}
	 
	public Object getItem(int position) {
	     return mItems.get(position);
	}
	 
	 /** Use the array index as a unique id. */
	public long getItemId(int position) {
	     return position;
	}
	 
	/**
	* @param convertView
	*            The old view to overwrite, if one is passed
	* @returns a ContactEntryView that holds wraps around an ContactEntry
	*/
	public View getView(int position, View convertView, ViewGroup parent) {
		CustomListItem item;
	    if (convertView == null) {
	    	item = new CustomListItem(mContext, mItems.get(position));
	    } else {
	         item = (CustomListItem) convertView;
	    }
	    
        String name = mItems.get(position).getName();
        item.setTitle(name);
        return item;
	}
}
