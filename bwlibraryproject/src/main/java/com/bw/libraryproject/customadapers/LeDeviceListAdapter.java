package com.bw.libraryproject.customadapers;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bw.libraryproject.R;
import com.bw.libraryproject.activities.LoginActivity;
import com.bw.libraryproject.app.ApplicationController;
import com.radiusnetworks.ibeacon.IBeacon;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;



/**
 * Displays basic information about beacon.
 *
 * @author wiktor@estimote.com (Wiktor Gworek)
 */
public class LeDeviceListAdapter extends BaseAdapter {

  private ArrayList<IBeacon> beacons;
  private LayoutInflater inflater;

  
  private Context mContext;
  private ApplicationController mApp;
  
  public LeDeviceListAdapter(Context context) {
    this.inflater = LayoutInflater.from(context);
    this.beacons = new ArrayList<IBeacon>();
    this.mContext = context;
    mApp = (ApplicationController) context.getApplicationContext();
  }

  public void updateItems(Collection<IBeacon> newBeacons) {
	  this.beacons.clear();
	  this.beacons.addAll(newBeacons);
	  
	  //Apply some sorting on macAddress
	  Collections.sort(beacons, new Comparator<IBeacon>(){
	      public int compare(IBeacon beacon1, IBeacon beacon2) {
	            // TODO Auto-generated method stub
	            return beacon1.getMacAddress().compareToIgnoreCase(beacon2.getMacAddress());
	      }
	});
    notifyDataSetChanged();
  }

  @Override
  public int getCount() {
    return beacons.size();
  }

  @Override
  public IBeacon getItem(int position) {
    return beacons.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View view, ViewGroup parent) {
    view = inflateIfRequired(view, position, parent);
    bind(getItem(position), view);
    return view;
  }

  private void bind(final IBeacon beacon, View view) {
    final ViewHolder holder = (ViewHolder) view.getTag();
    if(beacon.getDeviceName()!=null){
    	holder.nameTextView.setText(beacon.getDeviceName());
    }
    else{
    	holder.nameTextView.setText("UNKNOWN DEVICE");
    }
    
    holder.macTextView.setText(beacon.getMacAddress());
    holder.uuidTextView.setText(String.format("UUID: %s (%.2fm)", beacon.getProximityUuid(), beacon.getAccuracy()));
    holder.majorTextView.setText("Major: " + beacon.getMajor());
    holder.minorTextView.setText("Minor: " + beacon.getMinor());
    holder.measuredPowerTextView.setText("MPower: " + beacon.getTxPower());
    holder.rssiTextView.setText("RSSI: " + beacon.getRssi());
    
    
    holder.btnAdd.setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
//			mApp.setSelectedBeacon(beacon);
			
			Intent intent;
			/*if(mApp.isLoggedIn()){
				intent = new Intent(mContext, AddBeaconActivity.class);
			}
			else{
				intent = new Intent(mContext, LoginActivity.class);
			}*/
			
			intent = new Intent(mContext, LoginActivity.class);
			mContext.startActivity(intent);
		}
	});
  }

  private View inflateIfRequired(View view, int position, ViewGroup parent) {
    if (view == null) {
      view = inflater.inflate(R.layout.device_item, null);
      view.setTag(new ViewHolder(view));
      
      if ( position % 2 == 0)
          view.setBackgroundResource(R.drawable.beacon_list_item_even);
        else
          view.setBackgroundResource(R.drawable.beacon_list_item_odd);
    }
    return view;
  }

  static class ViewHolder {
	final TextView nameTextView;
    final TextView macTextView;
    final TextView uuidTextView;
    final TextView majorTextView;
    final TextView minorTextView;
    final TextView measuredPowerTextView;
    final TextView rssiTextView;
    final ImageView btnAdd;

    ViewHolder(View view) {
      nameTextView = (TextView) view.findViewWithTag("dname");
      macTextView = (TextView) view.findViewWithTag("mac");
      uuidTextView = (TextView) view.findViewWithTag("uuid");
      majorTextView = (TextView) view.findViewWithTag("major");
      minorTextView = (TextView) view.findViewWithTag("minor");
      measuredPowerTextView = (TextView) view.findViewWithTag("mpower");
      rssiTextView = (TextView) view.findViewWithTag("rssi");
      btnAdd = (ImageView) view.findViewWithTag("btnAdd");
    }
  }
}
