package com.bw.libraryproject.activities;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Toast;

import com.radiusnetworks.ibeacon.BleNotAvailableException;

public class BluetoothLEActivity extends SuperActivity{
	public String TAG = "BluetoothLEActivity";
	private static final int REQUEST_ENABLE_BT = 100;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (!this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
			Toast.makeText(this, "Bluetooth LE not supported by this device", Toast.LENGTH_SHORT).show();
			throw new BleNotAvailableException("Bluetooth LE not supported by this device");
		}		
		else {
			if (((BluetoothManager) this.getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter().isEnabled()==false){
				Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
		        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
			}
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if(requestCode==REQUEST_ENABLE_BT && resultCode==Activity.RESULT_CANCELED){
			finish();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
}