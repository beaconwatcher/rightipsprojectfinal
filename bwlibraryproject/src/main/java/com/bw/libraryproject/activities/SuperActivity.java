package com.bw.libraryproject.activities;

import android.app.Activity;

import com.bw.libraryproject.app.ApplicationController;
public class SuperActivity extends Activity {
	@Override
	public void onResume(){
	    super.onResume();

	    ApplicationController myApp = (ApplicationController)this.getApplication();
	    if (myApp.wasInBackground)  {
	        //Do specific came-here-from-background code
	    }
	    myApp.stopActivityTransitionTimer();
	}

	@Override
	public void onPause(){
	    super.onPause();
	    ((ApplicationController) this.getApplication()).startActivityTransitionTimer();
	}
}
