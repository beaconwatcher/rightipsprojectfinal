package com.bw.libraryproject.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.bw.libraryproject.R;
import com.bw.libraryproject.app.ApplicationController;
import com.bw.libraryproject.customadapers.SitesAdapter;
import com.bw.libraryproject.entities.CustomData;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SelectSiteActivity extends Activity{
	String TAG = "SelectSiteActivity";

	private ApplicationController mApp;
	public Context mContext;

	private String mSelSiteID;
	private String mUserID;
	
	private ArrayList<CustomData> userSites;
	private SitesAdapter mSitesAdapter;
	
	private ListView mSitesList;
	private LinearLayout mNoSiteView;
	private Button mRefreshButton;
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Set View to register.xml
		setContentView(R.layout.activity_select_site);
		mContext = SelectSiteActivity.this;
		mApp=(ApplicationController) getApplicationContext();
		
		
		mNoSiteView = (LinearLayout) findViewById(R.id.no_site_box);
		mRefreshButton = (Button) findViewById(R.id.btnReloadSites);
		mSitesList = (ListView) findViewById(R.id.sites_list);
		
		userSites = new ArrayList<CustomData>();
		mSitesAdapter = new SitesAdapter(mContext, userSites);
		mSitesList.setAdapter(mSitesAdapter);
		
		mUserID=mApp.getPreferences(LoginActivity.PREF_LOGIN_USER_ID, "");
		
		if(!mUserID.equals("")){
			getUserSites(mUserID);
		}
		
		mRefreshButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!mUserID.equals("")){
					getUserSites(mUserID);
				}
			}
		});
		
		
		mSitesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				CustomData d = (CustomData) mSitesList.getAdapter().getItem(arg2);
				mSelSiteID=d.getID();
				
				
				Intent intent = new Intent();
				intent.putExtra("result", mSelSiteID);
				setResult(RESULT_OK, intent);
				finish();
				
	/*			Intent intent=new Intent(mContext, AddBeaconActivity.class);
				intent.putExtra(SelectDemoActivity.EXTRAS_BEACON, getIntent().getParcelableExtra(SelectDemoActivity.EXTRAS_BEACON));
				intent.putExtra("siteID", mSelSiteID);
				startActivity(intent);*/
				
			}
		});
		
		
	}

	
	public boolean getUserSites(String userID) {
		mApp.showDialog(mContext, "API Call", "Getting Sites...");
		RequestParams params = new RequestParams();
		params.put("userID", userID);

		mApp.APICall("http://api.beaconwatcher.com/index.php?action=getSites", params, 
			new AsyncHttpResponseHandler() {
				@Override
				public void onStart() {
					Log.v(TAG, "onStart");
				}

				@Override
				public void onSuccess(String response) {
					mApp.hideDialog();
					
					try{
						JSONObject jo=new JSONObject(response);
						int status=jo.getInt("status");
						
						if(status==0){
							mApp.showToast(mContext, jo.getString("message"), Toast.LENGTH_LONG);
							mNoSiteView.setVisibility(View.VISIBLE);
						}
						else{
							mNoSiteView.setVisibility(View.GONE);
							
/*							if(userSites==null){
								userSites=new ArrayList<CustomData>();
							}*/
							userSites.clear();
							
							JSONArray arr = jo.getJSONArray("data");
							
							for(int i=0; i<arr.length(); i++){
								try{
									JSONObject site=arr.getJSONObject(i);
									CustomData item=new CustomData();
									item.setID(site.getString("site_id"));
									item.setName(site.getString("site_name"));
									userSites.add(item);
								}
								catch(JSONException e){
								}
							}
							mSitesAdapter.notifyDataSetChanged();
							
							/*
							JSONObject site=jo.getJSONObject("data");
							CustomData item=new CustomData();
							item.setID(site.getString("site_id"));
							item.setName(site.getString("site_name"));
							userSites.add(item);
							mSitesAdapter.notifyDataSetChanged();*/
						}
					}
					catch(JSONException e){
						Log.e("log_tag", "Error parsing data "+e.toString());
					}
				}

				@Override
				public void onFailure(Throwable error, String content) {
					mApp.hideDialog();
					mApp.showToast(mContext, "Network error", Toast.LENGTH_LONG);
					Log.e(TAG, "onFailure error : " + error.toString()	+ "content : " + content);
					SelectSiteActivity.this.finish();
				}

				@Override
				public void onFinish() {
					Log.v(TAG, "onFinish");
				}

			});
		return true;
	}
	
	private void beaconAdded(){
/*		Intent i = new Intent(context, BeaconDetailActivity.class);
		i.putExtra("uuid", ap.getSelectedBeacon());
		startActivity(i);*/
	}
	
	
	private void addNewSite(){
/*		Intent i = new Intent(context, AddSiteActivity.class);
		startActivity(i);*/
	}
}