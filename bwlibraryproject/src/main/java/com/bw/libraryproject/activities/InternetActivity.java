package com.bw.libraryproject.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.bw.libraryproject.R;

public class InternetActivity extends LocalBroadcastReceiverActivity{
	public FragmentManager mFragmentManager;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_no_internet);
		mFragmentManager=getFragmentManager();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	
	
	
	
	
	
	public void changeFragment(Fragment fragment, int container_id, Bundle bundle) {
		// Set Fragement based on menu item selected
		if (fragment != null) {
			if (bundle != null) {
				// set Fragmentclass Arguments
				fragment.setArguments(bundle);
			}

			String backStateName = fragment.getClass().getName();
			boolean fragmentPopped = mFragmentManager.popBackStackImmediate(
					backStateName, 0);

			// if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) ==
			// null)
			if (!fragmentPopped && findFragmentByTag(backStateName) == null) {
				// fragment not in back stack, create it.
				mFragmentManager
						.beginTransaction()
						.replace(container_id, fragment,
								backStateName).addToBackStack(backStateName)
						.commit();
			}
		}
	}
	
	public Fragment findFragmentByTag(String tag) {
		Fragment f = mFragmentManager.findFragmentByTag(tag);
		if (f != null) {
			if (f.isVisible()) {
				return f;
			}
		}
		return null;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public Boolean checkInternetAvailability(){
		//First of all check if device is connected to any wireless connection.
		//i.e. home/office wifi.
		if (checkNetworkConnection()){
			/*
	        try{
	        	//Connection is on but we need to make sure if internet is available.
	            HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
	            urlc.setRequestProperty("User-Agent", "Test");
	            urlc.setRequestProperty("Connection", "close");
	            urlc.setConnectTimeout(3000); //choose your own timeframe
	            urlc.setReadTimeout(4000); //choose your own timeframe
	            urlc.connect();
	            //networkcode2 = urlc.getResponseCode();
	            return (urlc.getResponseCode() == 200);
	        }
	        catch (IOException e){
	            return (false);  //connectivity exists, but no internet.
	        }*/
			
			
			return true;
	    }
		
		//Device is not connected to any network.
		else{
	        return false;  //no connectivity
	    }
	}
	
	
	public Boolean checkNetworkConnection(){
		ConnectivityManager connectivity = (ConnectivityManager) InternetActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) 
                for (int i = 0; i < info.length; i++) 
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
	}
}