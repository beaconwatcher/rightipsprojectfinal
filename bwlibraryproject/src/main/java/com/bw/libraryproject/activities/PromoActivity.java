package com.bw.libraryproject.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.bw.libraryproject.R;


public class PromoActivity extends Activity {
	protected static final String TAG = "PromoActivity";

	private TextView mMsg;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "oncreate");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_promo);
		
		mMsg = (TextView) findViewById(R.id.txt_msg);
		
		String bVal = getIntent().getStringExtra("msg");
		if (bVal!=""){
			mMsg.setText(bVal);
		}
	}
}
