package com.bw.libraryproject.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bw.libraryproject.R;
import com.bw.libraryproject.app.ApplicationController;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends Activity implements OnClickListener {
	String TAG = "LoginActivity";
	
	public static final String EXTRAS_TARGET_ACTIVITY = "extraTargetActivity";
	public static final String EXTRAS_LOGIN_RESULT = "extraLoginResult";
	public static final String EXTRAS_ACTIVITY_RESULT = "extraActivityResult";
	
	public static String PREF_LOGGED_IN = "LoggedIn";
	public static String PREF_LOGIN_USER_EMAIL = "LoginUserEmail";
	public static String PREF_LOGIN_USER_PASSWORD = "LoginUserPassword";
	
	public static String PREF_LOGIN_USER_ID = "LoginUserId";
	public static String PREF_LOGIN_USER_NAME = "LoginUserName";

	
	
	AsyncHttpResponseHandler handler;
	
	private Button mBtnLogin;
	private ApplicationController mApp;
	private EditText mTxtUserName;
	private EditText mTxtPassword;
	private Context mContext;
	
	String userEmail = "";
	String password = "";
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		mContext = LoginActivity.this;
		mApp= (ApplicationController) getApplicationContext();
		
		mTxtUserName = (EditText) findViewById(R.id.login_username);
		mTxtPassword = (EditText) findViewById(R.id.login_password);

		mBtnLogin = (Button) findViewById(R.id.btnLogin);
		mBtnLogin.setOnClickListener(this);
		
		userEmail = mApp.getPreferences(PREF_LOGIN_USER_EMAIL, "");
		password = mApp.getPreferences(PREF_LOGIN_USER_PASSWORD, "");
		
		
		if(!userEmail.equals("") && !password.equals("")){
			mTxtUserName.setText(userEmail);
			mTxtPassword.setText(password);
		}
	}

	public boolean authenticateUser() {
		//txtPassword.getText().toString();
		userEmail = mApp.verifyEditTextData(mContext, mTxtUserName,	true, true);
		password = mApp.verifyEditTextData(mContext, mTxtPassword,	true, true);
		if (userEmail == null || password == null) {
			return false;
		}
		
		
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == mBtnLogin.getId()) {
			if (authenticateUser()) {
				Login(userEmail, password);
			}
		}
	}

	public void Login(String userEmail, String pass) {
		mApp.savePreferences(PREF_LOGIN_USER_EMAIL, userEmail);
		mApp.savePreferences(PREF_LOGIN_USER_PASSWORD, password);
		
		mApp.showDialog(mContext, "API Call", "Logging In...");
		
		RequestParams params = new RequestParams();
		params.put("email", userEmail);
		params.put("pass", pass);
		
		mApp.APICall("http://api.beaconwatcher.com/index.php?action=login", params, responseHandler()); 
	}
	
	protected AsyncHttpResponseHandler responseHandler(){
		if(handler!=null){
			return handler;
		}
		
		AsyncHttpResponseHandler handler=new AsyncHttpResponseHandler() {
			@Override
			public void onStart() {
				Log.v(TAG, "onStart");
			}

			@Override
			public void onSuccess(String response) {
				mApp.hideDialog();
				Log.v(TAG, "onSuccess");
				JSONObject jo=null;
				
				try{
					jo=new JSONObject(response);
					int status=jo.getInt("status");
					String msg=jo.getString("message");
					
					if(status==0){
						mApp.showToast(mContext, msg, Toast.LENGTH_SHORT);
					}
					else{
						//save login response in app preferences
						//so that other components can use it laters.
						mApp.savePreferences(LoginActivity.EXTRAS_LOGIN_RESULT, response);
						
						
						//showAlert("Login Successful");
						mApp.showToast(mContext, "Logged In Successfuly", Toast.LENGTH_SHORT);
						JSONObject loginInfo=jo.getJSONObject("data");
						String fn = loginInfo.getString("first_name");
						String ln = loginInfo.getString("last_name");

						
						mApp.savePreferences(LoginActivity.PREF_LOGGED_IN, "1");
						mApp.savePreferences(PREF_LOGIN_USER_ID, loginInfo.getString("userID"));
//						mApp.setSites(loginInfo.getJSONArray("sites"));
						loggedIn(response);
					}
				}
				catch(JSONException e){
	                Log.e("log_tag", "Error parsing data "+e.toString());
				}
			}

			@Override
			public void onFailure(Throwable error, String content) {
				mApp.hideDialog();
				Log.e(TAG, "onFailure error : " + error.toString()
						+ "content : " + content);
				loginFailed();
			}

			@Override
			public void onFinish() {
				mApp.hideDialog();
				Log.v(TAG, "onFinish");
			}
		};
		return handler;
	}
	
	
	
	private void loggedIn(String response){
		//If parent activity sent a reference to open new activity after login
		//then start that activity.
		if (getIntent().getStringExtra(EXTRAS_TARGET_ACTIVITY) != null) {
			try {
				Class<?> clazz = Class.forName(getIntent().getStringExtra(EXTRAS_TARGET_ACTIVITY));
				Intent intent = new Intent(LoginActivity.this, clazz);
				intent.putExtra(EXTRAS_ACTIVITY_RESULT, response);
				startActivity(intent);
			} catch (ClassNotFoundException e) {
				Log.e(TAG, "Finding class by name failed", e);
			}
		}
  	  
  	  //Finish the activity return result to parent activity.
  	  	else{
			Intent intent = new Intent();
			intent.putExtra(EXTRAS_ACTIVITY_RESULT, response);
			setResult(RESULT_OK, intent);
			finish();
  	  }
	}	
	
	private void loginFailed(){
		mApp.showToast(mContext, "Login Failed, please try again...", Toast.LENGTH_SHORT);
		
		
//		setResult(Activity.RESULT_CANCELED);
		//finish();
	}
	
	
	
}