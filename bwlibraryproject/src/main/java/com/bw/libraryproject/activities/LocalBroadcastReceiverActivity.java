package com.bw.libraryproject.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.Toast;

import com.bw.libraryproject.app.ApplicationController;
import com.bw.libraryproject.entities.BroadcastReceiverEntity;
import com.bw.libraryproject.events.intentservice.IntentServiceEvent;
import com.bw.libraryproject.events.intentservice.NoRecordFoundEvent;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class LocalBroadcastReceiverActivity extends Activity{
	protected Context mContext;
	protected ArrayList<BroadcastReceiverEntity> receivers=null;
	protected static String INTENT_SERVICE_MESSAGE="IntentServiceMessage";
	protected static String INTENT_SERVICE_ERROR_MESSAGE="IntentServiceErrorMessage";
	
	public String TAG;//="LocalBroadcastReceiverActivity";
	
	public View mLoader;
	
	public void addReceiver(BroadcastReceiver br, IntentFilter filter){
		if(receivers==null){
			receivers=new ArrayList<BroadcastReceiverEntity>();
		}
		
		
		if(containsReceiver(filter)==false){
			BroadcastReceiverEntity receiver=new BroadcastReceiverEntity();
			receiver.receiver=br;
			receiver.filter=filter;
			receivers.add(receiver);
		}
	}
	
	private Boolean containsReceiver(IntentFilter filter){
		if(receivers!=null){
			for(int i=0; i<receivers.size(); i++){
				BroadcastReceiverEntity entity=receivers.get(i);
				if(entity.filter.getAction(0).equals(filter.getAction(0))){
					return true;
				}
			}
		}
		
		
		return false;
	}
	
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext=this;
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		unregisterReceivers();
		registerReceivers();
		EventBus.getDefault().register(this);
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		unregisterReceivers();
		EventBus.getDefault().unregister(this);
		super.onPause();
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	
	public void onEvent(IntentServiceEvent event){
		if(event.tag.equals(TAG)){
			showLoader(false);
			((ApplicationController)this.getApplicationContext()).hideDialog();
			if(event.type.equals(INTENT_SERVICE_MESSAGE)){
				((ApplicationController)this.getApplicationContext()).showToast(this, TAG+ ": "+event.message, Toast.LENGTH_SHORT);
			}
			else if(event.type.equals(INTENT_SERVICE_ERROR_MESSAGE)){
				((ApplicationController)this.getApplicationContext()).showErrorToast(this, TAG);
			}
		}
	}
	
	public void onEvent(NoRecordFoundEvent event){
		if(event.tag.equals(TAG)){
			showLoader(false);
			((ApplicationController)this.getApplicationContext()).hideDialog();
			((ApplicationController)this.getApplicationContext()).showToast(this, event.message, Toast.LENGTH_SHORT);
		}
	}
	

	
	
	
	
	protected void registerReceivers(){
		if(receivers!=null){
			for(int i=0; i<receivers.size(); i++){
				BroadcastReceiverEntity r=receivers.get(i);
				LocalBroadcastManager.getInstance(mContext).registerReceiver(r.receiver, r.filter);
			}
		}
	}
	
	
	protected void unregisterReceivers(){
		if(receivers!=null){
			for(int i=0; i<receivers.size(); i++){
				BroadcastReceiverEntity r=receivers.get(i);
				LocalBroadcastManager.getInstance(mContext).unregisterReceiver(r.receiver);
			}
		}
	}
	
	public void showLoader(Boolean b){
		if(mLoader!=null) mLoader.setVisibility((b==true) ? View.VISIBLE : View.INVISIBLE);
	}

}