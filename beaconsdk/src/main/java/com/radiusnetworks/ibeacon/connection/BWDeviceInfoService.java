/**
 * BeaconWatcher, Inc.
 * http://www.beaconwatcher.com
 *
 * @author Shahbaz Ali
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.radiusnetworks.ibeacon.connection;
 
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
/**
 * This class is used to keep DeviceInfo BluetoothGattCharacteristic reference.
 */
public class BWDeviceInfoService implements BluetoothService{
	private final HashMap<UUID, BluetoothGattCharacteristic> characteristics = new HashMap();
	private final HashMap<UUID, ConnectionManager.WriteCallback> writeCallbacks = new HashMap();
 
	
	public void processGattServices(List<BluetoothGattService> services){
		for (BluetoothGattService service : services)
			if (BWUuid.BW_BEACON_DEVICE_INFO_SERVICE.equals(service.getUuid())) {
				this.characteristics.put(BWUuid.BW_BEACON_UUID_CHAR, service.getCharacteristic(BWUuid.BW_BEACON_UUID_CHAR));
				this.characteristics.put(BWUuid.BW_MAJOR_CHAR, service.getCharacteristic(BWUuid.BW_MAJOR_CHAR));
				this.characteristics.put(BWUuid.BW_MINOR_CHAR, service.getCharacteristic(BWUuid.BW_MINOR_CHAR));
				this.characteristics.put(BWUuid.BW_POWER_CHAR, service.getCharacteristic(BWUuid.BW_POWER_CHAR));
//				this.characteristics.put(BWUuid.BW_CHANGE_PASSWORD_CHAR, service.getCharacteristic(BWUuid.BW_CHANGE_PASSWORD_CHAR));
				this.characteristics.put(BWUuid.BW_ADVERTISING_INTERVAL_CHAR, service.getCharacteristic(BWUuid.BW_ADVERTISING_INTERVAL_CHAR));
				this.characteristics.put(BWUuid.BW_BEACON_KEEP_CONNECT_CHAR, service.getCharacteristic(BWUuid.BW_BEACON_KEEP_CONNECT_CHAR));
				this.characteristics.put(BWUuid.BW_BEACON_SOFT_REBOOT_CHAR, service.getCharacteristic(BWUuid.BW_BEACON_SOFT_REBOOT_CHAR));
			}
	}
 
	public boolean hasCharacteristic(UUID uuid){
		return this.characteristics.containsKey(uuid);
	}
	
	public String getBeaconUUID(){
		return this.characteristics.containsKey(BWUuid.BW_BEACON_UUID_CHAR) ?
				getStringValue(((BluetoothGattCharacteristic)this.characteristics.get(BWUuid.BW_BEACON_UUID_CHAR)).getValue()) : null;
	}
	
	public String getBeaconMajor(){
		return this.characteristics.containsKey(BWUuid.BW_MAJOR_CHAR) ?
				getStringValue(((BluetoothGattCharacteristic)this.characteristics.get(BWUuid.BW_MAJOR_CHAR)).getValue()) : null;		
	}
	
	public String getBeaconMinor(){
		return this.characteristics.containsKey(BWUuid.BW_MINOR_CHAR) ?
				getStringValue(((BluetoothGattCharacteristic)this.characteristics.get(BWUuid.BW_MINOR_CHAR)).getValue()) : null;		
	}
	
	public String getBeaconPower(){
		return this.characteristics.containsKey(BWUuid.BW_POWER_CHAR) ?
				getStringValue(((BluetoothGattCharacteristic)this.characteristics.get(BWUuid.BW_POWER_CHAR)).getValue()) : null;		
	}
	
	public String getBeaconBroadcastRate(){
		return this.characteristics.containsKey(BWUuid.BW_ADVERTISING_INTERVAL_CHAR) ?
				getStringValue(((BluetoothGattCharacteristic)this.characteristics.get(BWUuid.BW_ADVERTISING_INTERVAL_CHAR)).getValue()) : null;		
	}
	
	public String getSoftReboot(){
		return this.characteristics.containsKey(BWUuid.BW_BEACON_SOFT_REBOOT_CHAR) ?
				getStringValue(((BluetoothGattCharacteristic)this.characteristics.get(BWUuid.BW_BEACON_SOFT_REBOOT_CHAR)).getValue()) : null;		
	}
	
 
	
	public void update(BluetoothGattCharacteristic characteristic){
		this.characteristics.put(characteristic.getUuid(), characteristic);
	}
 
	public Collection<BluetoothGattCharacteristic> getAvailableCharacteristics() {
		List chars = new ArrayList(this.characteristics.values());
		chars.removeAll(Collections.singleton(null));
		return chars;
	}
 
	public BluetoothGattCharacteristic beforeCharacteristicWrite(UUID uuid, ConnectionManager.WriteCallback callback) {
		this.writeCallbacks.put(uuid, callback);
		return (BluetoothGattCharacteristic)this.characteristics.get(uuid);
	}
 
	public void onCharacteristicWrite(BluetoothGattCharacteristic characteristic, int status) {
		ConnectionManager.WriteCallback writeCallback = (ConnectionManager.WriteCallback)this.writeCallbacks.remove(characteristic.getUuid());
		if (status == 0)
			writeCallback.onSuccess();
		else
			writeCallback.onError();
	}
 
	private static String getStringValue(byte[] bytes) {
		String stmp="";  
        StringBuilder sb = new StringBuilder("");  
        for (int n = 0; n < bytes.length; n++)  
        {  
            stmp = Integer.toHexString(bytes[n] & 0xFF);  
            sb.append((stmp.length()==1)? "0"+stmp : stmp);  
            sb.append(" ");  
        }  
        return sb.toString().toUpperCase().trim();  
	}
	
	private static int unsignedByteToInt(byte value){
		return value & 0xFF;
	}
	
	private static int getUnsignedInt16(byte[] bytes) {
		return unsignedByteToInt(bytes[0]) + (unsignedByteToInt(bytes[1]) << 8);
	}
}
