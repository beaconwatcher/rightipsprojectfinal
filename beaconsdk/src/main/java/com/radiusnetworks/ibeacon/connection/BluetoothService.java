package com.radiusnetworks.ibeacon.connection;

import android.bluetooth.BluetoothGattCharacteristic;
/**
 * http://www.beaconwatcher.com/
 * This project is for developers, not for commercial purposes.
 * 
 * @author Shahbaz Ali
 * 
 */
public abstract interface BluetoothService
{
  public abstract void update(BluetoothGattCharacteristic paramBluetoothGattCharacteristic);
}
