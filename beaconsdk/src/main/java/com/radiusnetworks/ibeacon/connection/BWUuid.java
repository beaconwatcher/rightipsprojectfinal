package com.radiusnetworks.ibeacon.connection;

import java.util.UUID;

public class BWUuid{
	public static final UUID BW_BEACON_DEVICE_INFO_SERVICE = UUID.fromString("0000fff0-0000-1000-8000-00805f9b34fb");
	public static final UUID BW_BEACON_UUID_CHAR = UUID.fromString("0000fff1-0000-1000-8000-00805f9b34fb");
	public static final UUID BW_MAJOR_CHAR = UUID.fromString("0000fff2-0000-1000-8000-00805f9b34fb");
	public static final UUID BW_MINOR_CHAR = UUID.fromString("0000fff3-0000-1000-8000-00805f9b34fb");
	public static final UUID BW_POWER_CHAR = UUID.fromString("0000fff4-0000-1000-8000-00805f9b34fb");
	public static final UUID BW_MEASURED_POWER_CHAR = UUID.fromString("0000fff5-0000-1000-8000-00805f9b34fb");
	public static final UUID BW_CHANGE_PASSWORD_CHAR = UUID.fromString("0000fff6-0000-1000-8000-00805f9b34fb");	
	public static final UUID BW_ADVERTISING_INTERVAL_CHAR = UUID.fromString("0000fff7-0000-1000-8000-00805f9b34fb");
	public static final UUID BW_BEACON_KEEP_CONNECT_CHAR = UUID.fromString("0000fff8-0000-1000-8000-00805f9b34fb");
	public static final UUID BW_BEACON_SOFT_REBOOT_CHAR = UUID.fromString("0000ffff-0000-1000-8000-00805f9b34fb");
	

	
	public static final UUID BW_BEACON_ALERT_SERVICE = UUID.fromString("00001802-0000-1000-8000-00805f9b34fb");
	public static final UUID BW_BEACON_ALERT_CHAR = UUID.fromString("00002a06-0000-1000-8000-00805f9b34fb");
	
	public static final UUID BW_BEACON_NAME_SERVICE = UUID.fromString("00001800-0000-1000-8000-00805f9b34fb");
	public static final UUID BW_BEACON_NAME_CHAR = UUID.fromString("00002a00-0000-1000-8000-00805f9b34fb");

	public static final UUID BW_BEACON_BATTERY_LIFE_SERVICE = UUID.fromString("0000180f-0000-1000-8000-00805f9b34fb");
	public static final UUID BW_BEACON_BATTERY_LIFE_CHAR = UUID.fromString("00002a19-0000-1000-8000-00805f9b34fb");
}
