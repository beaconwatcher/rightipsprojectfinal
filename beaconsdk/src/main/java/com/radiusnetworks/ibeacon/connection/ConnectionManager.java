/**
 * BeaconWatcher, Inc.
 * http://www.beaconwatcher.com
 *
 * @author Shahbaz Ali
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.radiusnetworks.ibeacon.connection;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.radiusnetworks.ibeacon.internal.HashCode;
import com.radiusnetworks.ibeacon.internal.Objects;
import com.radiusnetworks.ibeacon.service.IBeaconData;

/**
 * This class is used to establish connection between Beacon and BLE 4.0 device as a GattServer.
 */
public class ConnectionManager{
	private static final String TAG = "ConnectionManager";
	
	
	public static Set<Integer> ALLOWED_POWER_LEVELS = Collections.unmodifiableSet(new HashSet(Arrays.asList(new Integer[] { Integer.valueOf(-30), Integer.valueOf(-20), Integer.valueOf(-16), Integer.valueOf(-12), Integer.valueOf(-8), Integer.valueOf(-4), Integer.valueOf(0), Integer.valueOf(4) })));
	private final Context context;
	private final BluetoothDevice device;
	private final ConnectionCallback connectionCallback;
	private final Handler handler;
	private final BluetoothGattCallback bluetoothGattCallback;
//	private final Runnable timeoutHandler;
	private final BeaconNameService mNameService;
	private final BWBatteryLifeService mBatteryService;	
	private final BWDeviceInfoService mBeaconInfoService;
	private final AlertService mAlertService;
	private final Map<UUID, BluetoothService> uuidToService;
	private boolean didReadCharacteristics;
	private LinkedList<BluetoothGattCharacteristic> toFetch;
	private long aAuth;
	private long bAuth;
	private BluetoothGatt bluetoothGatt;

	public ConnectionManager(Context context, IBeaconData beacon, ConnectionCallback connectionCallback){
		this.context = context;
		this.device = deviceFromBeacon(beacon);
		this.toFetch = new LinkedList<BluetoothGattCharacteristic>();
		this.handler = new Handler();
		this.connectionCallback = connectionCallback;
		this.bluetoothGattCallback = createBluetoothGattCallback();
//		this.timeoutHandler = createTimeoutHandler();
		this.mNameService = new BeaconNameService();
		
		this.mBatteryService = new BWBatteryLifeService();
		this.mBeaconInfoService = new BWDeviceInfoService();
		this.mAlertService = new AlertService();
		this.uuidToService = new HashMap<UUID, BluetoothService>();

		this.uuidToService.put(BWUuid.BW_BEACON_NAME_SERVICE, mNameService);		
		this.uuidToService.put(BWUuid.BW_BEACON_DEVICE_INFO_SERVICE, mBeaconInfoService);
		this.uuidToService.put(BWUuid.BW_BEACON_BATTERY_LIFE_SERVICE, mBatteryService);
		this.uuidToService.put(BWUuid.BW_BEACON_ALERT_SERVICE, mAlertService);
	}
	
	/**
	 * Takes scanned device (IBeaconData) and return BluetoothDevice
	 * to be used for making connection.
	 * @param beacon
	 * @return BluetoothDevice
	 */

	private BluetoothDevice deviceFromBeacon(IBeaconData beacon){
	     BluetoothManager bluetoothManager = (BluetoothManager)this.context.getSystemService("bluetooth");
	     BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
	     return bluetoothAdapter.getRemoteDevice(beacon.getMacAddress());
	}

	public void authenticate(){
		Log.d(TAG, "Trying to connect to GATT");
		this.didReadCharacteristics = true;
		this.bluetoothGatt = this.device.connectGatt(this.context, true, this.bluetoothGattCallback);
//		this.handler.postDelayed(this.timeoutHandler, TimeUnit.SECONDS.toMillis(10L));
	}

	public void close(){
		if (this.bluetoothGatt != null) {
			this.bluetoothGatt.disconnect();
			this.bluetoothGatt.close();
		}
//		this.handler.removeCallbacks(this.timeoutHandler);
	}

	public boolean isConnected() {
		BluetoothManager bluetoothManager = (BluetoothManager)this.context.getSystemService("bluetooth");
		int connectionState = bluetoothManager.getConnectionState(this.device, 7);
//		return (connectionState == 2) && (this.didReadCharacteristics);
		return (connectionState == 2);
	}
	
	public void CallBeacon(WriteCallback writeCallback)	{
		if ((!isConnected()) || (!this.mAlertService.hasCharacteristic(BWUuid.BW_BEACON_ALERT_CHAR))){
			Log.d(TAG, "Not connected to beacon. Discarding changing proximity UUID.");
			writeCallback.onError();
			return;
		}
		BluetoothGattCharacteristic AlertChar = this.mAlertService.beforeCharacteristicWrite(BWUuid.BW_BEACON_ALERT_CHAR, writeCallback);
	    byte[] arrayOfByte = new byte[1];
	    arrayOfByte[0] = 1;
	    AlertChar.setValue(arrayOfByte);
		this.bluetoothGatt.writeCharacteristic(AlertChar);		
	}
	
	public void BeaconKeepConnect(WriteCallback writeCallback){
		if ((!isConnected()) || (!this.mBeaconInfoService.hasCharacteristic(BWUuid.BW_BEACON_KEEP_CONNECT_CHAR))){
			Log.d(TAG, "Not connected to beacon. Discarding changing proximity UUID.");
			writeCallback.onError();
			return;
		}
		BluetoothGattCharacteristic uuidChar = this.mBeaconInfoService.beforeCharacteristicWrite(BWUuid.BW_BEACON_KEEP_CONNECT_CHAR, writeCallback);

	    byte[] arrayOfByte = new byte[1];
	    arrayOfByte[0] = 1;
	    
		uuidChar.setValue(arrayOfByte);
		
		this.bluetoothGatt.writeCharacteristic(uuidChar);
	}

	
	public void writeBeaconName(String name, WriteCallback writeCallback)
	{
		if ((!isConnected()) || (!this.mNameService.hasCharacteristic(BWUuid.BW_BEACON_NAME_CHAR))) {
			Log.d(TAG, "Not connected to beacon. Discarding changing proximity UUID.");
			writeCallback.onError();
			return;
		}
		//byte[] nameAsBytes = HashCode.fromString(name.replaceAll("-", "").toLowerCase()).asBytes();
		BluetoothGattCharacteristic nameChar = this.mNameService.beforeCharacteristicWrite(BWUuid.BW_BEACON_NAME_CHAR, writeCallback);
		
		if(ConnectionManager.isCharacteristicWriteable(nameChar)){
			nameChar.setValue(name);//nameAsBytes);
			this.bluetoothGatt.writeCharacteristic(nameChar);
		}
		else{
			ConnectionManager.this.notifyAuthenticationError();
		}
	}
	

	public void writeProximityUuid(String proximityUuid, WriteCallback writeCallback)
	{
		if ((!isConnected()) || (!this.mBeaconInfoService.hasCharacteristic(BWUuid.BW_BEACON_UUID_CHAR))) {
			Log.d(TAG, "Not connected to beacon. Discarding changing proximity UUID.");
			writeCallback.onError();
			return;
		}
		byte[] uuidAsBytes = HashCode.fromString(proximityUuid.replaceAll("-", "").toLowerCase()).asBytes();
		BluetoothGattCharacteristic uuidChar = this.mBeaconInfoService.beforeCharacteristicWrite(BWUuid.BW_BEACON_UUID_CHAR, writeCallback);

		uuidChar.setValue(uuidAsBytes);
		this.bluetoothGatt.writeCharacteristic(uuidChar);
	}

	public void writeAdvertisingInterval(String BroadcastRate, WriteCallback writeCallback)
	{
		if ((!isConnected()) || (!this.mBeaconInfoService.hasCharacteristic(BWUuid.BW_ADVERTISING_INTERVAL_CHAR))) {
			Log.d(TAG, "Not connected to beacon. Discarding changing advertising interval.");
			writeCallback.onError();
			return;
		}
		
		byte[] BroadcastRateAsBytes = HashCode.fromString(BroadcastRate.replaceAll("-", "").toLowerCase()).asBytes();
		
		BluetoothGattCharacteristic intervalChar = this.mBeaconInfoService.beforeCharacteristicWrite(BWUuid.BW_ADVERTISING_INTERVAL_CHAR, writeCallback);

		intervalChar.setValue(BroadcastRateAsBytes);
		this.bluetoothGatt.writeCharacteristic(intervalChar);
	}

	public void writeBroadcastingPowerValue(String powerDBM, WriteCallback writeCallback)
	{
		if ((!isConnected()) || (!this.mBeaconInfoService.hasCharacteristic(BWUuid.BW_POWER_CHAR))) {
			Log.d(TAG, "Not connected to beacon. Discarding changing broadcasting power.");
			writeCallback.onError();
			return;
		}
		
//		if (!ALLOWED_POWER_LEVELS.contains(Integer.valueOf(powerDBM))) {
//			Log.d(TAG, "Not allowed power level. Discarding changing broadcasting power.");
//			writeCallback.onError();
//			return;
//		}
		
		byte[] powerDBMAsBytes = HashCode.fromString(powerDBM.replaceAll("-", "").toLowerCase()).asBytes();
		BluetoothGattCharacteristic powerChar = this.mBeaconInfoService.beforeCharacteristicWrite(BWUuid.BW_POWER_CHAR, writeCallback);

		powerChar.setValue(powerDBMAsBytes);
		this.bluetoothGatt.writeCharacteristic(powerChar);
	}

	public void writeMajor(String major, WriteCallback writeCallback)
	{
		if (!isConnected()) {
			Log.d(TAG, "Not connected to beacon. Discarding changing major.");
			writeCallback.onError();
			return;
		}
		
		byte[] MajorAsBytes = HashCode.fromString(major.replaceAll("-", "").toLowerCase()).asBytes();
		BluetoothGattCharacteristic majorChar = this.mBeaconInfoService.beforeCharacteristicWrite(BWUuid.BW_MAJOR_CHAR, writeCallback);

		majorChar.setValue(MajorAsBytes);
		this.bluetoothGatt.writeCharacteristic(majorChar);
	}

	public void writeMinor(String minor, WriteCallback writeCallback)
	{
		if (!isConnected()) {
			Log.d(TAG, "Not connected to beacon. Discarding changing minor.");
			writeCallback.onError();
			return;
		}
//		minor = Utils.normalize16BitUnsignedInt(minor);
		byte[] MinorAsBytes = HashCode.fromString(minor.replaceAll("-", "").toLowerCase()).asBytes();
		BluetoothGattCharacteristic minorChar = this.mBeaconInfoService.beforeCharacteristicWrite(BWUuid.BW_MINOR_CHAR, writeCallback);

		minorChar.setValue(MinorAsBytes);
		this.bluetoothGatt.writeCharacteristic(minorChar);
	}
	
	
	
	public void writeSoftReboot(String val, WriteCallback writeCallback)
	{
		if (!isConnected()) {
			Log.d(TAG, "Not connected to beacon. Discarding changing SoftReboot.");
			writeCallback.onError();
			return;
		}
//		minor = Utils.normalize16BitUnsignedInt(minor);
		byte[] SoftRebootAsBytes = HashCode.fromString(val.replaceAll("-", "").toLowerCase()).asBytes();
		BluetoothGattCharacteristic rebootChar = this.mBeaconInfoService.beforeCharacteristicWrite(BWUuid.BW_BEACON_SOFT_REBOOT_CHAR, writeCallback);

		rebootChar.setValue(SoftRebootAsBytes);
		this.bluetoothGatt.writeCharacteristic(rebootChar);
	}

	private Runnable createTimeoutHandler() 
	{
		return new Runnable()
		{
			public void run() {
				Log.d(TAG, "Timeout while authenticating");
				if (!ConnectionManager.this.didReadCharacteristics) {
					if (ConnectionManager.this.bluetoothGatt != null) {
						ConnectionManager.this.bluetoothGatt.disconnect();
						ConnectionManager.this.bluetoothGatt.close();
						ConnectionManager.this.bluetoothGatt = null;
					}
					ConnectionManager.this.notifyAuthenticationError();
				}
			}
		};
	}

	private BluetoothGattCallback createBluetoothGattCallback() {
		return new BluetoothGattCallback(){
			public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
				if (newState == 2) {
					Log.d(TAG, "Connected to GATT server, discovering services: " + gatt.discoverServices());
				} else if ((newState == 0) && (!ConnectionManager.this.didReadCharacteristics)) {
					Log.d(TAG, "Disconnected from GATT server");
					ConnectionManager.this.notifyAuthenticationError();
				} else if (newState == 0) {
					Log.d(TAG, "Disconnected from GATT server");
					ConnectionManager.this.notifyDisconnected();
				}
			}

			public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status){
				if (status == 0) {
					BluetoothService temp = ((BluetoothService)ConnectionManager.this.uuidToService.get(characteristic.getService().getUuid()));
					if (temp != null){
						temp.update(characteristic);
					}
					ConnectionManager.this.readCharacteristics(gatt);
				} else {
					Log.d(TAG, "Failed to read characteristic");
					ConnectionManager.this.toFetch.clear();
					ConnectionManager.this.notifyAuthenticationError();
				}
			}

			public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status){
				/*if (ConnectionManager.this.mNameService.isAuthSeedCharacteristic(characteristic)) {
					if (status == 0) {
						ConnectionManager.this.onSeedWriteCompleted(gatt, characteristic);
					} else {
						Log.d(TAG, "Authentication failed: seed not negotiated");
						ConnectionManager.this.notifyAuthenticationError();
					}
				} 
				else if (ConnectionManager.this.mNameService.isAuthVectorCharacteristic(characteristic)) {
					if (status == 0) {
						ConnectionManager.this.onAuthenticationCompleted(gatt);
					} else {
						Log.d(TAG, "Authentication failed: auth source write failed");
						ConnectionManager.this.notifyAuthenticationError();
					}
				} */
				
//				((BluetoothService)BeaconConnection.this.uuidToService.get(characteristic.getService().getUuid())).onCharacteristicWrite(characteristic, status);
				
				if (BWUuid.BW_BEACON_DEVICE_INFO_SERVICE.equals(characteristic.getService().getUuid())){
					ConnectionManager.this.mBeaconInfoService.onCharacteristicWrite(characteristic, status);
					ConnectionManager.this.onAuthenticationCompleted(gatt);
				}
				else if (BWUuid.BW_BEACON_NAME_SERVICE.equals(characteristic.getService().getUuid())){
					//do something
					ConnectionManager.this.mNameService.onCharacteristicWrite(characteristic, status);
					ConnectionManager.this.onAuthenticationCompleted(gatt);
				}
			}

			public void onServicesDiscovered(BluetoothGatt gatt, int status){
				if (status == 0) {
					Log.d(TAG, "Services discovered");
					ConnectionManager.this.processDiscoveredServices(gatt.getServices());
//					BeaconConnection.this.startAuthentication(gatt);
					//keep connect
					ConnectionManager.this.handler.postDelayed(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							ConnectionManager.this.BeaconKeepConnect(new WriteCallback() {
								
								@Override
								public void onSuccess() {
									// TODO Auto-generated method stub
									Log.i("BeaconConnection", "Keep Connect Successful");
								}
								
								@Override
								public void onError() {
									// TODO Auto-generated method stub
									Log.i("BeaconConnection", "Keep Connect Failed");
								}
							});	
							
						}
					}, TimeUnit.SECONDS.toMillis(1L));

				} else {
					Log.d(TAG, "Could not discover services, status: " + status);
					ConnectionManager.this.notifyAuthenticationError();
				}
			}
		};
	}

	private void notifyAuthenticationError() {
//		this.handler.removeCallbacks(this.timeoutHandler);
		this.connectionCallback.onAuthenticationError();
	}

	private void notifyDisconnected() {
		this.connectionCallback.onDisconnected();
	}

	private void processDiscoveredServices(List<BluetoothGattService> services) {
		this.mBatteryService.processGattServices(services);
		this.mNameService.processGattServices(services);
		this.mBeaconInfoService.processGattServices(services);
		this.mAlertService.processGattServices(services);

		this.toFetch.clear();
		this.toFetch.addAll(this.mNameService.getAvailableCharacteristics());
		this.toFetch.addAll(this.mBeaconInfoService.getAvailableCharacteristics());
		this.toFetch.addAll(this.mBatteryService.getAvailableCharacteristics());
		this.toFetch.addAll(this.mAlertService.getAvailableCharacteristics());
	}

//	private void startAuthentication(BluetoothGatt gatt)
//	{
//		if (!this.mNameService.isAvailable()) {
//			Log.d(TAG, "Authentication service is not available on the beacon");
//			notifyAuthenticationError();
//			return;
//		}
//		this.aAuth = AuthMath.randomUnsignedInt();
//		BluetoothGattCharacteristic seedChar = this.mNameService.getAuthSeedCharacteristic();
//		seedChar.setValue(AuthMath.firstStepSecret(this.aAuth), 20, 0);
//		gatt.writeCharacteristic(seedChar);
//	}

	private void onSeedWriteCompleted(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic)
	{
		this.handler.postDelayed(new Runnable()
		{
			public void run() {
				gatt.readCharacteristic(characteristic);
			}
		}
		, 500L);
	}

	private void onBeaconSeedResponse(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic)
	{
//		Integer intValue = characteristic.getIntValue(20, 0);
//		this.bAuth = UnsignedInteger.fromIntBits(intValue.intValue()).longValue();
//		String macAddress = this.device.getAddress().replace(":", "");
//		BluetoothGattCharacteristic vectorChar = this.mNameService.getAuthVectorCharacteristic();
//		vectorChar.setValue(AuthMath.secondStepSecret(this.aAuth, this.bAuth, macAddress));
//		gatt.writeCharacteristic(vectorChar);
	}

	private void onAuthenticationCompleted(final BluetoothGatt gatt)
	{
		this.handler.postDelayed(new Runnable()
		{
			public void run() {
				ConnectionManager.this.readCharacteristics(gatt);
			}
		}
		, 500L);
	}

	private void readCharacteristics(BluetoothGatt gatt)
	{
		if (this.toFetch.size() != 0){
			BluetoothGattCharacteristic chara = (BluetoothGattCharacteristic) this.toFetch.poll();
			if (chara!=null && isCharacterisitcReadable(chara)){
				gatt.readCharacteristic(chara);
			}
			else{
				readCharacteristics(gatt);
			}
		}
		else if (this.bluetoothGatt != null){
			onAuthenticated();
		}
	}

	private void onAuthenticated()
	{
		Log.d(TAG, "Authenticated to beacon");
//		this.handler.removeCallbacks(this.timeoutHandler);
		this.didReadCharacteristics = true;
		this.connectionCallback.onAuthenticated(new BeaconCharacteristics(this.mBeaconInfoService, this.mNameService, this.mBatteryService));
	}
	
	
	
	/**
     * @return Returns <b>true</b> if property is writable
     */
    public static boolean isCharacteristicWriteable(BluetoothGattCharacteristic pChar) {
        return (pChar.getProperties() & (BluetoothGattCharacteristic.PROPERTY_WRITE | BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE)) != 0;
    }

    /**
     * @return Returns <b>true</b> if property is Readable
     */
    public static boolean isCharacterisitcReadable(BluetoothGattCharacteristic pChar) {
        return ((pChar.getProperties() & BluetoothGattCharacteristic.PROPERTY_READ) != 0);
    }

    /**
     * @return Returns <b>true</b> if property is supports notification
     */
    public boolean isCharacterisiticNotifiable(BluetoothGattCharacteristic pChar) {
        return (pChar.getProperties() & BluetoothGattCharacteristic.PROPERTY_NOTIFY) != 0;
    }
	
	
	
	
	
	
	
	
	
	
	

	public static abstract interface WriteCallback
	{
		public abstract void onSuccess();

		public abstract void onError();
	}

	public static abstract interface ConnectionCallback
	{
		public abstract void onAuthenticated(ConnectionManager.BeaconCharacteristics paramBeaconCharacteristics);

		public abstract void onAuthenticationError();

		public abstract void onDisconnected();
	}

	public static class BeaconCharacteristics
	{
		private final String BeaconUUID;
		private final String BeaconMajor;
		private final String BeaconMinor;
		private final String BeaconPowerValue;
    	private final String BeaconBroadcast;
    	private final String BeaconName;
    	private final String BeaconSoftRebootValue;
    	private final String BatteryLevel;

    	public BeaconCharacteristics(BWDeviceInfoService InfoService, BeaconNameService NameService, BWBatteryLifeService BatteryService){
    		String bName = NameService.getBeaconName();
    		if(bName==null){bName=" ";}
    		this.BeaconName = bName.replaceAll(" ", "");
    		
    		
    		String bUUID=InfoService.getBeaconUUID();
    		if(bUUID==null){bUUID=" ";}
    		this.BeaconUUID = bUUID.replaceAll(" ", "");
    		
    		String bMajor=InfoService.getBeaconMajor();
    		if(bMajor==null){bMajor=" ";}
    		this.BeaconMajor = bMajor.replaceAll(" ", "");
    		
    		String bMinor=InfoService.getBeaconMinor();
    		if(bMinor==null){bMinor=" ";}
    		this.BeaconMinor = bMinor.replaceAll(" ", "");
    		
    		String bPower=InfoService.getBeaconPower();
    		if(bPower==null){bPower=" ";}
    		this.BeaconPowerValue = bPower.replaceAll(" ", "");
    		
    		String bBroadcast=InfoService.getBeaconBroadcastRate();
    		if(bBroadcast==null){bBroadcast=" ";}
    		this.BeaconBroadcast = bBroadcast.replaceAll(" ", "");

    		String bBatteryLevel=BatteryService.getBatteryPercent().toString()+"%";
    		if(bBatteryLevel==null){bBatteryLevel=" ";}
    		this.BatteryLevel = bBatteryLevel.replaceAll(" ", "");
    		this.BeaconSoftRebootValue = "";
    	}

    	public String getBeaconUUID() {
    		return this.BeaconUUID;
    	}
    	public String getBroadcastingPower() {
    		return this.BeaconPowerValue;
    	}
    	public String getBroadcastRate() {
    		return this.BeaconBroadcast;
    	}
    	public String getMajor() {
    		return this.BeaconMajor;
    	}
    	public String getMinor() {
    		return this.BeaconMinor;
    	}
    	public String getBeaconName()
    	{
    		return this.BeaconName;
    	}
    	
    	public String getBeaconSoftReboot()
    	{
    		return this.BeaconSoftRebootValue;
    	}
    	
    	public String getBatteryLevel(){
    		return this.BatteryLevel;
    	}
    	
    	

    	public String toString() {
        	
    		return Objects.toStringHelper(this)
    				.add("BeaconUUID", this.BeaconUUID)
    				.add("BeaconMajor", this.BeaconMajor)
    				.add("BeaconMinor", this.BeaconMinor)
    				.add("BeaconPowerValue", this.BeaconPowerValue)
    				.add("BeaconBroadcast", this.BeaconBroadcast)
    				.add("BeaconName", this.BeaconName).toString();
    		
    	}
	}
}
